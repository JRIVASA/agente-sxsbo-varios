DECLARE @TmpQuery NVARCHAR(MAX)

USE VAD10

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modpos')
BEGIN
	
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'SAP_SyncVentasPOS')
	BEGIN
		
		INSERT INTO ESTRUC_MENU (Relacion, TipoRel, Clave, Texto, Imagen, Tag, Pos)
		VALUES ('modpos', 'tvwchild', 'SAP_SyncVentasPOS', 'Sincronizaci�n de Transacciones SAP', 'rep', 'SAP_SyncVentasPOS', 0)

		PRINT 'Opci�n de Men� agregada con exito.'
		
	END
	ELSE
	BEGIN
		
		UPDATE ESTRUC_MENU SET
		Relacion = 'modpos',
		TipoRel = 'tvwchild',
		Texto = 'Sincronizaci�n de Transacciones SAP',
		Imagen = 'rep',
		Tag = 'SAP_SyncVentasPOS',
		Pos = 0
		WHERE Clave = 'SAP_SyncVentasPOS'

		PRINT 'Opci�n de Men� actualizada con exito.'
		
	END

	IF EXISTS(SELECT Clave_Menu FROM CONF_MENU_USER WHERE Clave_Menu = 'SAP_SyncVentasPOS')
	BEGIN
		UPDATE CONF_MENU_USER SET 
		Relacion = 'modpos',
		Texto = 'Sincronizaci�n de Transacciones SAP',
		Icono = 'rep',
		Forma = 'SAP_SyncVentasPOS'
		WHERE Clave_Menu = 'SAP_SyncVentasPOS'
	END

	-- A partir de ahora, luego de agregar un Estruc_Menu se debe agregar este componente al script
	-- Colocar el Id de Recurso Multilingue para cada opci�n de men� de la siguiente forma:

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		
		-- Cambiar el 0 por el String acorde al identificador de recurso.
		
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''0'' WHERE Clave = ''SAP_SyncVentasPOS''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''0'' WHERE Clave_Menu = ''SAP_SyncVentasPOS''
		'
		
		EXEC (@TmpQuery)
		
	END

END
ELSE
BEGIN
	PRINT 'No se agreg� la opci�n de men� debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opci�n de men� Padre para' + CHAR(13) +
	'mantener la jerarqu�a Y evitar da�os en el sistema. No se realiz� ning�n cambio.'
END

-------------------------------------------