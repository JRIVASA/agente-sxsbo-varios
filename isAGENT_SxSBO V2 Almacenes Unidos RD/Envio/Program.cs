﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using isAGENT_SxSBO.Clases;
using SAPbobsCOM;
//using System.Diagnostics;

using System.Runtime.InteropServices;   //GuidAttribute
using System.Reflection;                //Assembly
using System.Threading;                 //Mutex
using System.Security.AccessControl;    //MutexAccessRule
using System.Security.Principal;        //SecurityIdentifier

namespace isAGENT_SxSBO
{
    static class Program
    {

        private static Mutex MultiInstanceCheck = null;
        private static String AssemblyGuid = (Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), true)[0] as GuidAttribute).Value;

        public static Int64 gCodProducto = 859;
        public static String gNombreProducto = "Stellar BUSINESS";
        public static String gPK;

        public const Int64 LogIDAuditoria = 10001;
        public const String ErrorLog = "Error";
        public const String InfoLog = "Information";
        public const String WarningLog = "Warning";

        /* Advertencia: NO Cambiar ninguno de los parámetros anteriores. Para efectos de resguardo de las
        credenciales de acceso a SQL se esta utilizando la misma combinación de parámetros de Stellar BUSINESS.
        Para que las claves encriptadas sean las mismas en todos los Agentes de Sincronización Web.
        Despues que las claves hayan sido generadas si esto se llegara a cambiar no funcionaría la conexión SQL
        Al desplegar una nueva versión. Adicionalmente hay constantes para uso interno y efectivo de la aplicación.
        */

        public static String mStellarDBUser = null;
        public static String mStellarDBPass = null;

        public static String mSAPDBUser = null;
        public static String mSAPDBPass = null;

        public static String mSAPUserLogin = null;
        public static String mSAPUserPwd = null;

        public static System.Collections.Generic.Dictionary<String, String> ListaAsociacionMonedas = null;
        public static System.Collections.Generic.Dictionary<String, String> ListaAsociacionFormaPago = null;
        public static System.Collections.Generic.Dictionary<String, String> ListaAsociacionBancos = null;

        public static System.Collections.Generic.Dictionary<String, String> TaxList1 = null;
        public static System.Collections.Generic.Dictionary<String, String> TaxList2 = null;
        public static System.Collections.Generic.Dictionary<String, String> TaxList3 = null;

        public static System.Collections.Generic.Dictionary<String, String> ListaAsociacionTipoPrecio = null;
        public static System.Collections.Generic.Dictionary<String, String> ListaProductosCupon = null;

        public static ADODB.Connection mCnLocal = null;
        public static SAPbobsCOM.Company mCnSAP = null;
        //public static dynamic mCnSAP = null;
        //public static Sap.Data.Hana.HanaConnection mCnHanaDB = null;
        //public static dynamic mCnHanaDB = null;

        public static String DI_API_Version = String.Empty;

        public static String gCorrelativo = null;

        public static Boolean gDebugMode = false;

        public static String gPath = Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe", String.Empty);

        //public static FSLogger gLogger = Logger;
        //public static FSLogger Logger = new FSLogger(System.Environment.CurrentDirectory + "\\Log.txt");
        public static FSLogger Logger = new FSLogger(gPath + "Log.txt");

        /// <summary> 
        /// Gets the name of the application. 
        /// </summary> 
        /// <value></value> 
        /// <remarks></remarks> 
        public static String ApplicationName
        {

            get
            {

                try
                {
                    var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
                    var applicationTitle = ((System.Reflection.AssemblyTitleAttribute)entryAssembly.
                    GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false)[0]).Title;
                    if (String.IsNullOrWhiteSpace(applicationTitle))
                    {
                        applicationTitle = entryAssembly.GetName().Name;
                    }
                    return applicationTitle;
                }
                catch (Exception)
                {
                    return String.Empty;
                }

            }

        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            // unique id for global mutex - Global prefix means it is global to the machine
            String MutexID = string.Format("Global\\{{{0}}}", gPath.Replace("\\", "|") + AssemblyGuid);

            // Need a place to store a return value in Mutex() constructor call
            Boolean CreatedNew;

            // edited by Jeremy Wiebe to add example of setting up security for multi-user usage
            // edited by 'Marc' to work also on localized systems (don't use just "Everyone") 
            MutexAccessRule allowEveryoneRule =
            new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null)
            , MutexRights.FullControl
            , AccessControlType.Allow
            );

            MutexSecurity SecuritySettings = new MutexSecurity();
            SecuritySettings.AddAccessRule(allowEveryoneRule);

            // edited by MasonGZhwiti to prevent race condition on security settings via VanNguyen
            using (Mutex TmpMutex = new Mutex(false, MutexID, out CreatedNew, SecuritySettings))
            {
                // edited by acidzombie24
                Boolean HasHandle = false;
                try
                {
                    try
                    {
                        // note, you may want to time out here instead of waiting forever
                        // edited by acidzombie24
                        // mutex.WaitOne(Timeout.Infinite, false);
                        Console.WriteLine("Checking for another live instance.");
                        HasHandle = TmpMutex.WaitOne(5000, false);
                        if (!HasHandle)
                            throw new TimeoutException("Timeout waiting for exclusive access");
                    }
                    catch (AbandonedMutexException)
                    {
                        // Log the fact that the mutex was abandoned in another process,
                        // it will still get acquired
                        HasHandle = true;
                    }
                    catch (Exception AnyOther)
                    {
                        Console.WriteLine("Found another instance running. Leaving...");
                        System.Environment.Exit(0);
                    }

                    // Perform your work here.

                    //MessageBox.Show(System.Environment.CurrentDirectory + "\\Log.txt");
                    //MessageBox.Show(gPath + "Log.txt");

                    Console.WriteLine("Instance Ready.");

                    gPK = String.Empty + Chr(55) + Chr(81) + Chr(76) + "_"
                    + Chr(73) + Chr(55) + Chr(51) + Chr(51)
                    + Chr(83) + Chr(78) + Chr(75) + Chr(52)
                    + Chr(78) + Chr(50) + Chr(69) + Chr(74)
                    + Chr(68) + Chr(49) + Chr(83) + "-"
                    + Chr(66) + Chr(44) + Chr(66) + Chr(71)
                    + Chr(52) + Chr(44) + Chr(90) + "_" + Chr(71);

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1());

                }
                finally
                {
                    // edited by acidzombie24, added if statement
                    if (HasHandle)
                        TmpMutex.ReleaseMutex();
                }

            }

        }

        private static String Chr(int CharCode)
        {
            return Microsoft.VisualBasic.Strings.Chr(CharCode).ToString();
        }

    }


}
