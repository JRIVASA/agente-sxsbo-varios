﻿using isAGENT_SxSBO.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SAPbobsCOM;
using Sap.Data.Hana;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.Compatibility.VB6;
using System.IO;
using System.Diagnostics;

namespace isAGENT_SxSBO.Database_Sync_Classes
{
    class DBSync
    {

        // SELECT * FROM VAD20..MA_PAGOS WHERE cs_Sync_SxS IN ('', '!!!!!!!!!!', '..........', '-_-_-_-_-')

        public const String RegistrosPendientesPorCorrida = "-_-_-_-_-";
        public const String RegistrosPendientesPorLote = "..........";
        public const String RegistrosPendientesFallidos = "!!!!!!!!!!";
        public const String RegistrosNuevos = "";

        public static long nLotesMaxEjecucion = 0;
        public static long nLotesProcesados = 0;
        public static long nRegistrosLote = 0;

        private static void RecuperacionInicialDeCredenciales()
        {
            
            dynamic mClsTmp = null;

            Program.mStellarDBUser = Properties.Settings.Default.Stellar_SQLUser;
            Program.mStellarDBPass = Properties.Settings.Default.Stellar_SQLPass;

            if (!(String.Equals(Program.mStellarDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mStellarDBPass.Length == 0))
            {

                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mStellarDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBUser);
                    Program.mStellarDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPDBUser = Properties.Settings.Default.SAP_DBUser;
            Program.mSAPDBPass = Properties.Settings.Default.SAP_DBPass;

            if (!(String.Equals(Program.mSAPDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mSAPDBPass.Length == 0))
            {
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBUser);
                    Program.mSAPDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPUserLogin = Properties.Settings.Default.SAP_CompanyUser;
            Program.mSAPUserPwd = Properties.Settings.Default.SAP_CompanyPass;

            //if (!(String.Equals(Program.mUserFTP, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassFTP.Length == 0))
            //{
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPUserLogin = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserLogin);
                    Program.mSAPUserPwd = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserPwd);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            //}

        }

        private static SqlConnection ConexionStellar()
        {

            SqlConnection mCnLocal = null;

        Retry:

            try
            {
                mCnLocal = (Properties.Settings.Default.Stellar_TrustedConnection ?
                Functions.getAlternateTrustedConnection(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, 30) :
                Functions.getAlternateConnection(Properties.Settings.Default.Stellar_SQLServerName,
                Properties.Settings.Default.Stellar_SQLDBName, 
                Program.mStellarDBUser, Program.mStellarDBPass, 30));
                mCnLocal.Open();
            }
            catch (SqlException SQLAny)
            {

                if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                {
                    
                    dynamic mClsTmp = null;
                    
                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");
                    
                    if (mClsTmp == null) goto Otros;
                    
                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");
                    
                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);
                    
                    if  (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog( "Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }
                    
                }

                Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            if (mCnLocal.State != ConnectionState.Open)
            {
                Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor Stellar.");
                System.Environment.Exit(0);
            }

            return mCnLocal;

        }

        private static String CadenaConexionADO(String pServidor, String pBD, String pUser = "SA", String pPassword = "", 
        Boolean pTrustedConnection = false){
            
            String mCadenaConexion = String.Empty;

            if (pTrustedConnection)
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                "Integrated Security=SSPI;";
            else
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                (pUser.isUndefined() || pPassword.isUndefined() ? "Persist Security Info=False;User ID=" + pUser + ";"
                : "Persist Security Info=True;User ID=" + pUser + ";Password=" + pPassword + ";");

            return mCadenaConexion;

        }

        private static ADODB.Connection ConexionStellar(Boolean pADO = true)
        {

            ADODB.Connection mCnLocal = null;

        Retry:

            try
            {

                mCnLocal = new ADODB.Connection();

                mCnLocal.ConnectionTimeout = Properties.Settings.Default.Stellar_ConnectionTimeout;
                //mCnLocal.Provider = "SQLOLEDB.1";
                mCnLocal.ConnectionString = CadenaConexionADO(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, Program.mStellarDBUser, 
                Program.mStellarDBPass, Properties.Settings.Default.Stellar_TrustedConnection);

                mCnLocal.Open();

            }
            catch (System.Runtime.InteropServices.COMException SQLAny)
            {

                //if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                if (SQLAny.HResult == (-2147217843))
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            return mCnLocal;

        }

        private static SAPbobsCOM.Company ConexionSAP(Boolean ManagedObject)
        {
            SAPbobsCOM.Company Obj = ConexionSAP();
            return Obj;
        }

        private static dynamic ConexionSAP()
        {

            dynamic mCnSAP = null;

            Int32 ErrorNumber; String ErrorDesc;

        Retry:

            try
            {

                //mCnSAP = new Company();
                mCnSAP = Functions.SafeCreateObject("SAPbobsCOM.Company" + Program.DI_API_Version);

	            mCnSAP.Server = Properties.Settings.Default.SAP_Server;
	            mCnSAP.LicenseServer = Properties.Settings.Default.SAP_LicenseServer;

                mCnSAP.DbServerType = (BoDataServerTypes)Properties.Settings.Default.SAP_ServerTypeID;  //BoDataServerTypes.dst_MSSQL2014;
	            mCnSAP.DbUserName = Program.mSAPDBUser;
	            mCnSAP.DbPassword = Program.mSAPDBPass;
	            mCnSAP.language = (BoSuppLangs) Properties.Settings.Default.SAP_LangID; //BoSuppLangs.ln_Spanish_La;
                mCnSAP.UseTrusted = false;

                mCnSAP.CompanyDB = Properties.Settings.Default.SAP_DBName;
                mCnSAP.UserName = Program.mSAPUserLogin;
                mCnSAP.Password = Program.mSAPUserPwd;
	        
	            if(mCnSAP.Connect() != 0) 
                {
                
	                mCnSAP.GetLastError(out ErrorNumber, out ErrorDesc);
                    mCnSAP.Disconnect();

                    throw new Exception(ErrorDesc) { HelpLink = ErrorNumber.ToString() };

	            }

            }
            catch (Exception SQLAny)
            {

                if (SQLAny.HelpLink == "-4008" || SQLAny.HelpLink == "-132")
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPDBUser = TmpVar[0];
                        Program.mSAPDBPass = TmpVar[1];
                        Properties.Settings.Default.SAP_DBUser = TmpVar[2];
                        Properties.Settings.Default.SAP_DBPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        //goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server, 
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP",  Program.mCnLocal);
                    }

                /*}
                else if (SQLAny.HelpLink == "-8023")
                {*/

                    //dynamic mClsTmp = null;
                    mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión a la Compañia SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    //dynamic 
                    TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPUserLogin = TmpVar[0];
                        Program.mSAPUserPwd = TmpVar[1];
                        Properties.Settings.Default.SAP_CompanyUser = TmpVar[2];
                        Properties.Settings.Default.SAP_CompanyPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server,
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor SAP.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, 
                "Estableciendo conexión al Servidor SAP.", "DBSync.ConexionSAP()", "Servidor SAP",
                Properties.Settings.Default.SAP_Server,
                (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                Console.WriteLine(SQLAny);

            }

            if (mCnSAP != null)
            {
                if (!mCnSAP.Connected)
                {
                    Marshal.ReleaseComObject(mCnSAP);
                    Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor SAP.");
                    System.Environment.Exit(0);
                }
            }
            else
            {
                Program.Logger.EscribirLog("Verifique que SAP BUSINESS ONE y la DI API esten correctamente instalados.");
                System.Environment.Exit(0);
            }

            return mCnSAP;

        }

        public static void IniciarAgente()
        {

            ADODB.Connection mCnLocal = null;
            Company SAPCon = null;
            //dynamic SAPCon = null;
            //HanaConnection HanaDB = null;
            //dynamic HanaDB = null;

            try
            {

                //Functions.LoadSettings();

                RecuperacionInicialDeCredenciales();

                DateTime tsInicioProc, tsFinProc;

                tsInicioProc = DateTime.Now;

                mCnLocal = ConexionStellar(true);
                Program.mCnLocal = mCnLocal;

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo de Conexión Stellar: "));

                if (mCnLocal == null)
                {
                    Program.Logger.EscribirLog("Conexión al Servidor Stellar no disponible / Timeout.");
                    System.Environment.Exit(500);
                }

                if(mCnLocal.State != Convert.ToInt32(ADODB.ObjectStateEnum.adStateOpen))
                {
                    Program.Logger.EscribirLog("Conexión al Servidor Stellar en estado no abierto.");
                    System.Environment.Exit(500);
                }

                Program.DI_API_Version = Properties.Settings.Default.APP_DEFINED_SAP_DI_API_VERSION;

                tsInicioProc = DateTime.Now;

                SAPCon = ConexionSAP();
                Program.mCnSAP = SAPCon;

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo de Conexión SAP: "));

                if (SAPCon == null)
                {
                    Program.Logger.EscribirLog("Conexión al Servidor SAP nula.");
                    System.Environment.Exit(999);
                }

                if (!SAPCon.Connected)
                {
                    Program.Logger.EscribirLog("Warning: Conexión al Servidor SAP no establecida.");
                    System.Environment.Exit(998);
                }

                // Comun para ambos

                Program.ListaProductosCupon = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.Stellar_ListaProductosCupon);

                if (Program.ListaProductosCupon == null) Program.ListaProductosCupon = new Dictionary<String, String>();

                // Iniciar Modalidad.

                if (Properties.Settings.Default.ModalidadAgente.Left(1).Equals("1"))
                {
                    ActualizarDatosMaestrosPendientes();
                }
                else if (Properties.Settings.Default.ModalidadAgente.Equals("2"))
                {

                    if (!Properties.Settings.Default.ObtenerListasAsociacionBD)
                    {

                        Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionMonedas);
                        Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionBancos);
                        Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionFormaPago);

                        if (Program.ListaAsociacionMonedas == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos.");
                            return;
                        }

                        if (Program.ListaAsociacionFormaPago == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos.");
                            return;
                        }

                        if (Program.ListaAsociacionBancos == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos.");
                            return;
                        }

                    }

                    if (Properties.Settings.Default.SAP_CodLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Codigo de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    if (Properties.Settings.Default.SAP_AliasLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Alias de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    /*if (Properties.Settings.Default.SAP_ClaveOpcionesLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Clave de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }*/

                    if (Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Almacen de SAP no definido. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    Program.gCorrelativo = Functions.Correlativo(mCnLocal, "Corrida_Agente_SAP_Trans");

                    if (!Program.gCorrelativo.isUndefined())
                    {

                        Program.ListaAsociacionTipoPrecio = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.SAP_Linea_ListaTipoPrecio);

                        Program.TaxList1 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList1);
                        Program.TaxList2 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList2);
                        Program.TaxList3 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList3);

                        if (Program.TaxList1 == null && Program.TaxList2 == null && Program.TaxList3 == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos");
                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos",
                            "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                            return;
                        }

                        if (Properties.Settings.Default.ObtenerListasAsociacionBD)
                        {

                            String mLista;
                            ADODB.Recordset x; Object Records = null;

                            try
                            {

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_Codigo + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_BANCOS WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionBancos == null && !Properties.Settings.Default.SAP_FormasPagoFallbackBanco)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Bancos inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ';' + c_CodDenomina + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_DENOMINACIONES WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionFormaPago == null && !Properties.Settings.Default.SAP_FormasPagoFallbackEfectivo)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Formas de Pago inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_MONEDAS WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionMonedas == null && !Properties.Settings.Default.SAP_FormasPagoFallbackMoneda)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Monedas inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                            }
                            catch (Exception ex)
                            {
                                Program.Logger.EscribirLog(ex, "Error al obtener datos de asociaciones por BD.");
                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Error al obtener datos de asociaciones por BD.",
                                "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, (ex.HResult + "|" + ex.Message), "Validacion Stellar", Program.mCnLocal);
                                return;
                            }

                        }

                        nRegistrosLote = Properties.Settings.Default.nRegistrosLote;
                        nLotesMaxEjecucion = Properties.Settings.Default.nLotesCorrida;

                        Boolean result = PrepararBD(ref mCnLocal);

                        SincronizarVentas();

                    }
                    else
                    {
                        throw new Exception("Falla al obtener correlativo de ejecución de Agente.");
                    }

                }

            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Ejecutando Agente.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Ejecutando Agente.", "DBSync.IniciarAgente()", "Corrida Agente",
                Program.gCorrelativo, Program.mCnLocal);
            }
            finally
            {

                if (SAPCon != null)
                if (SAPCon.Connected)
                    SAPCon.Disconnect();

                if (mCnLocal != null)
                if (mCnLocal.State != Convert.ToInt32(ADODB.ObjectStateEnum.adStateClosed))
                    mCnLocal.Close();

            }

        }

        public static Boolean PrepararBD(ref ADODB.Connection pCn)
        {

            Boolean result = true; Object Records;
            long mCmdTOut_Original = 0; Boolean Reintentando = false;

            mCmdTOut_Original = Properties.Settings.Default.Stellar_CommandTimeout;

        Retry:
            
            for (int i = 0; i <= 0; i++) {
                
                try {

                    result = true;

                    Properties.Settings.Default.Stellar_CommandTimeout = 0;

                    if (!Functions.ExisteCampoTabla("cs_Sync_SxS", "MA_PAGOS", ref pCn, "VAD20")) {
                        pCn.Execute("ALTER TABLE [VAD20].[DBO].[MA_PAGOS]" + "\n" + 
                        "ADD cs_Sync_SxS NVARCHAR(50) NOT NULL CONSTRAINT" + "\n" +
                        "[MA_PAGOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')", out Records);
            
                        result = result && true;
                    }

                    pCn.Execute("IF NOT EXISTS(SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] WHERE ID = " + Program.LogIDAuditoria + ")" + "\n" +
                    "INSERT INTO [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] (ID, Descripcion, DescResourceID)" + "\n" +
                    "SELECT " + Program.LogIDAuditoria  + " AS ID, 'Log Error / Evento isAGENT_SxSBO', 0 AS DescResourceID" + "\n" +
                    "", out Records);

                    if (!Functions.ExisteCampoTabla("CodigoRetorno", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD CodigoRetorno NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_CodigoRetorno] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    if (!Functions.ExisteCampoTabla("AccionRealizada", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD AccionRealizada NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_AccionRealizada] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    /*if (gTransferirCierres) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_CIERRES", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_CIERRES]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_CIERRES_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    /*if (gTransferirDepositosCP) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_DEPOSITOS", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_DEPOSITOS]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_DEPOSITOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    return result;

                }
                catch (Exception SQLAny)
                {
                    Program.Logger.EscribirLog(SQLAny, "PrepararBD:");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                        "Ejecutando validaciones y requerimientos de Base de Datos.", "DBSync.PrepararBD()", "Servidor Stellar",
                        Properties.Settings.Default.Stellar_SQLServerName,
                        (SQLAny.HResult + "|" + SQLAny.Message), "PrepararBD", Program.mCnLocal);
                        if (!Reintentando) {
                            Reintentando = true;
                            goto Retry;
                        } else {
                        // Sin Exito.
                    }
                    
                }

            }

            return result;
            
        }

        private static void SincronizarVentas()
        {

            if (Properties.Settings.Default.Stellar_ControlSync_FechaIni.isUndefined() 
            && Properties.Settings.Default.Stellar_ControlSync_FechaFin.isUndefined()
            && Properties.Settings.Default.Stellar_ControlSync_Transacciones.isUndefined())
            {
                if (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
                HayDatosPendientes(RegistrosPendientesPorLote) ||
                HayDatosPendientes(RegistrosPendientesFallidos))
                {
                    ProcesarCorrida(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
                }

                if (nLotesProcesados < nLotesMaxEjecucion 
                && !Properties.Settings.Default.Stellar_ControlSync_Estatus.Equals("1")) // Siempre y cuando no sea "1" que quiere decir que solo procesar los fallidos.
                    if (MarcarRegistros(RegistrosPendientesPorCorrida))
                    { // Procesar registros nuevos.
                        ProcesarCorrida();
                    }
            }
            else
            {
                ConstruirRegistrosDeVentas();
            }

        }

        private static Boolean HayDatosPendientes(String pEstatus)
        {

            try
            {

                ADODB.Recordset RsDatosPendientes; Object Records;

                String ExcluirVNF = String.Empty;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal, "VAD20"))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                RsDatosPendientes = Program.mCnLocal.Execute("SELECT COUNT(*) AS Registros " +
                "FROM [VAD20].[DBO].[MA_PAGOS] WHERE cs_Sync_SxS = " +
                "'" + pEstatus + "'" + ExcluirVNF, out Records);

                return (Convert.ToDouble(RsDatosPendientes.Fields["Registros"].Value) > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ProcesarCorrida()
        {

            if (!Properties.Settings.Default.Stellar_ControlSync_Estatus.Equals("0")) // Si esta variable esta puesta en 0, se ignoran los fallidos.
                if (HayDatosPendientes(RegistrosPendientesFallidos))
                    ConstruirRegistrosDeVentas(true); // Intentar procesar los Fallidos una sola vez.

            if (!Properties.Settings.Default.Stellar_ControlSync_Estatus.Equals("1")) // Si esta variable esta puesta en 1, se tratan solo los fallidos.
                while (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
                HayDatosPendientes(RegistrosPendientesPorLote))
                {
                    if (nLotesProcesados < nLotesMaxEjecucion)
                        ProcesarLote();
                    else
                        break;
                }

        }

        private static void ProcesarLote()
        {

            if (HayDatosPendientes(RegistrosPendientesPorLote))
            {
                // Procesar primero los pendientes
                // Antes de marcar el próximo Lote.
                ConstruirRegistrosDeVentas();
                nLotesProcesados++;
            }

            if (nLotesProcesados < nLotesMaxEjecucion)
                if (MarcarRegistros(RegistrosPendientesPorLote))
                {
                    ConstruirRegistrosDeVentas();
                    nLotesProcesados++;
                }

        }

        private static Boolean MarcarRegistros(String pEstatus, Boolean pFallidos = false)
        {

            try
            {

                long RegistrosAfectados = 0; String pEstatusPrevio = String.Empty; String mRegistrosXLote = String.Empty;

                String ExcluirVNF = String.Empty; String SQLMarcaje = String.Empty; Object Records;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal, "VAD20"))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                switch (pEstatus)
                {
                    case RegistrosPendientesPorCorrida:
                        pEstatusPrevio = "'" + RegistrosNuevos + "'";
                        break;
                    case RegistrosPendientesPorLote:
                        if (pFallidos)
                            pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                        else
                        {
                            pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                            mRegistrosXLote = "AND (c_Sucursal + c_Concepto + c_Numero) IN (" + "\n" +
                            "SELECT TOP (" + nRegistrosLote + ") (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                            "WHERE cs_Sync_SxS = " + pEstatusPrevio + ExcluirVNF + " ORDER BY f_Fecha, f_Hora, ID)";
                        }
                        break;
                    default: // Guardados con Exito.
                        pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                        break;
                }

                SQLMarcaje = "UPDATE [VAD20].[DBO].[MA_PAGOS] SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" +
                "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

                if (Program.gDebugMode) Program.Logger.EscribirLog(SQLMarcaje);

                Program.mCnLocal.Execute(SQLMarcaje, out Records);

                RegistrosAfectados = Convert.ToInt64(Records);

                return (RegistrosAfectados > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ConstruirRegistrosDeVentas(Boolean pReprocesarFallidos = false)
        {
            
            ADODB.Recordset RsVentas = null; ADODB.Recordset RsItems = null; ADODB.Recordset RsDetPagos = null;
            ADODB.Recordset RsImpuestos = null; ADODB.Recordset RsDatosFiscales;
            ADODB.Recordset RsPendXEntregaMA = null; ADODB.Recordset RsPendXEntregaTR = null;
            ADODB.Recordset RsSeriales = null;
            ADODB.Recordset RsDocRel = null; 
            ADODB.Recordset RsDocRelDatosFiscales = null;

            System.Collections.ArrayList RegistrosFallidos = new System.Collections.ArrayList();

            Double Cont = 0; Int32 ItmCount = 0;
            String mUltDoc = String.Empty; Double mUltTot = 0; DateTime mUltFec = DateTime.Now; DateTime mUltDocDateOnly = DateTime.Today;
            String mDocRel = String.Empty; String Related_StellarDocID = String.Empty;

            Dictionary<String, String[]> LineRelation = new Dictionary<String, String[]>();
            String StellarItemID = String.Empty; String StellarItemLn = String.Empty; String StellarItemCode = String.Empty;

            Dictionary<String, Object[]> ListaPendXEntrega = null;
            Dictionary<String, Object[]> ListaControlSeriales = null;
            Dictionary<String, Dictionary<String, Object[]>> SerialComboParentItems = null;

            String mEtapa = String.Empty;
            SAPbobsCOM.Recordset SAPmRs; ADODB.Recordset TmpRs; 
            String mDatoTemp; String mDatoTemp2;

            Object RecordsAffected;

            DateTime tsInicioTrans = DateTime.Now, tsFinTrans;
            DateTime tsInicioProc, tsFinProc;

            SAPbobsCOM.Company SAP = Program.mCnSAP;
            //dynamic SAP = Program.mCnSAP;
            ADODB.Connection mCn = Program.mCnLocal;

            try 
	        {

                if (pReprocesarFallidos)
                    MarcarRegistros(RegistrosPendientesPorLote, true);

                if (ObtenerDatosVentas(out RsVentas, out RsItems, out RsDetPagos, out RsImpuestos, 
                out RsDatosFiscales, out RsPendXEntregaMA, out RsPendXEntregaTR, out RsSeriales, 
                pReprocesarFallidos))
                {

                    Cont = 0;
                    
                    //RsVentas.Sort = "ID";
                    RsVentas.MoveFirst();

                Continue:

                    while (!RsVentas.EOF)
                    {

                        Cont = Cont + 1;

                        //

                        tsInicioTrans = DateTime.Now;

                        mEtapa = "Iniciando Procesamiento de Documentos Ventas / Devoluciones POS. Iniciando Transaccion SAP";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        String Loc = RsVentas.Fields["c_Sucursal"].Value.ToString();
                        String Concepto = RsVentas.Fields["c_Concepto"].Value.ToString();
                        String NumDoc = RsVentas.Fields["c_Numero"].Value.ToString();

                        String TransID = Loc + Concepto + NumDoc;

                        if (Properties.Settings.Default.Stellar_ControlSync_NoPermitirTransaccionPOSDuplicada)
                        {

                            mEtapa = "Verificando que documento no este ya registrado previamente: " + TransID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            String mTblTmp1 = (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase) ? "OINV" : "ORIN");

                            mDatoTemp =
                            "SELECT IfNULL(\"DocEntry\", -1) AS \"DocEntry\", " + "\n" +
                            "IfNULL(\"DocNum\", -1) AS \"DocNum\" " + "\n" +
                            "FROM \"" + mTblTmp1 + "\" " + "\n" +
                            "WHERE \"Comments\" LIKE '%Stellar POS. " + TransID + "%' " + "\n" +
                            "";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!(SAPmRs.EoF && SAPmRs.BoF))
                            {

                                String mTmpEntry = Convert.ToString(SAPmRs.Fields.Item("DocEntry").Value);
                                String mTmpDoc = Convert.ToString(SAPmRs.Fields.Item("DocNum").Value);

                                Int64 mTmpNumEntry = 0; Int64 mTmpNumDoc = 0;

                                Int64.TryParse(mTmpEntry, out mTmpNumEntry);
                                Int64.TryParse(mTmpDoc, out mTmpNumDoc);

                                if ((mTmpNumEntry > -1 && mTmpNumDoc > -1))
                                {

                                    Program.mCnLocal.Execute(
                                    "UPDATE [VAD20].[DBO].[MA_PAGOS] SET cs_Sync_SxS = '" + Program.gCorrelativo + "|" + mTmpEntry + "|" + mTmpDoc + "' " +
                                    "WHERE (c_Sucursal + c_Concepto + c_Numero) = '" + TransID + "'", out RecordsAffected);

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.InfoLog,
                                    "Transaccion POS [" + TransID + "] enviada con exito. " +
                                    "SAP Num. Doc. [" + mTmpDoc + "] ID Interno [" + mTmpEntry + "] ", "DBSync.ConstruirRegistrosDeVentas()",
                                    "POSTransaccionEnviada", TransID, "0", "EnviarTransaccionPOS", Program.mCnLocal);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento ya existía previamente. Marcado como enviado. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();

                                    goto Continue;

                                }

                            }

                            SAPmRs = null;

                        }

                        String StellarDocID = "POS" + "_" + TransID;

                        mUltDoc = StellarDocID;
                        mUltDocDateOnly = Convert.ToDateTime(RsVentas.Fields["f_Fecha"].Value);
                        mUltFec = mUltDocDateOnly;

                        DateTime mUltHora = Convert.ToDateTime(RsVentas.Fields["f_Hora"].Value);

                        mUltFec = new DateTime(mUltFec.Year, mUltFec.Month, mUltFec.Day,
                        mUltHora.Hour, mUltHora.Minute, mUltHora.Second, mUltHora.Millisecond);

                        //

                        Boolean PendXEntrega = false;
                        Boolean GenerarDocEntrega = false;

                        RsPendXEntregaTR.Filter = "TransID = '" + TransID + "'";

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase) && !RsPendXEntregaTR.EOF)
                        {
                            PendXEntrega = true;
                        }

                        Boolean TmpDraftDoc = false; Boolean TmpDraftDeliveryDoc = false;
                        SerialComboParentItems = new Dictionary<String, Dictionary<String, Object[]>>();

                        mEtapa = "Verificando si Documento [" + StellarDocID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] contiene combos para guardar como Draft";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        RsItems.Filter = "TransID = '" + TransID + "'";

                        String DocItmCodes = String.Empty;
                        
                        while (!RsItems.EOF)
                        {

                            StellarItemCode = RsItems.Fields["Cod_Principal"].Value.ToString();

                            if (DocItmCodes.Length > 0)
                                DocItmCodes += "UNION ALL" + "\n";
                            else
                                DocItmCodes = "SELECT \"CodTmp\" FROM (" + "\n";

                            DocItmCodes += "SELECT TOP 1 '" + StellarItemCode + "' AS \"CodTmp\" FROM \"OITM\"" + "\n";

                            //Boolean EsComboVenta = false;

                            //ProductTrees ComboItm;

                            //ComboItm = (ProductTrees)SAP.GetBusinessObject(BoObjectTypes.oProductTrees);
                            
                            //EsComboVenta = ComboItm.GetByKey(StellarItemCode);

                            //if (EsComboVenta)
                            //{

                            //    EsComboVenta = (ComboItm.Items.Count > 0);
                            //    EsComboVenta = (EsComboVenta && ComboItm.TreeType == BoItemTreeTypes.iSalesTree);

                            //    if (EsComboVenta)
                            //    {
                            //        for (int i = 1; i <= ComboItm.Items.Count; i++)
                            //        {

                            //        }
                            //    }

                            //}

                            RsItems.MoveNext();

                        }

                        if (DocItmCodes.Length > 0)
                        {

                            DocItmCodes += ") TBTemp GROUP BY \"CodTmp\"" + "\n";

                            //"SELECT IfNull(COUNT(*), 0) AS \"Seriales\"" + "\n" +

                            mDatoTemp = String.Empty +
                            "SELECT T1.\"Father\", T1.\"VisOrder\", T2.\"ItemCode\", T1.\"Quantity\"" + "\n" +
                            "FROM \"OITT\" T0, \"ITT1\" T1, \"OITM\" T2" + "\n" +
                            "WHERE T0.\"Code\" = T1.\"Father\"" + "\n" +
                            "AND T1.\"Code\" = T2.\"ItemCode\"" + "\n" +
                            "--AND T0.\"TreeType\" IN ('S')" + "\n" +
                            "AND T2.\"ManSerNum\" = 'Y'" + "\n" +
                            "AND T1.\"Father\" IN(" + "\n" +
                            DocItmCodes +
                            ") ORDER BY T1.\"Father\", T1.\"VisOrder\"";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {

                                //if (Convert.ToInt64(SAPmRs.Fields.Item("Seriales").Value) > 0)
                                TmpDraftDoc = (!PendXEntrega); //true;

                                while (!SAPmRs.EoF)
                                {

                                    Dictionary<String, Object[]> SerialComboItems = null;

                                    StellarItemCode = SAPmRs.Fields.Item("Father").Value;
                                    
                                    if (!SerialComboParentItems.ContainsKey(StellarItemCode))
                                    {
                                        SerialComboItems = new Dictionary<String, Object[]>();
                                        SerialComboParentItems.Add(StellarItemCode, SerialComboItems);
                                    }
                                    else
                                        SerialComboItems = SerialComboParentItems[StellarItemCode];

                                    StellarItemCode = SAPmRs.Fields.Item("ItemCode").Value;
                                    StellarItemLn = Convert.ToString(SAPmRs.Fields.Item("VisOrder").Value);
                                    Double ItmQty = Convert.ToDouble(SAPmRs.Fields.Item("Quantity").Value);

                                    SerialComboItems.Add(StellarItemCode, new Object[] { StellarItemCode, StellarItemLn, ItmQty });

                                    SAPmRs.MoveNext();

                                }

                            }

                            RsItems.MoveFirst();

                        }

                        //

                        mEtapa = "Iniciando Llenado de Datos Generales. Documento [" + StellarDocID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "]";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        SAPbobsCOM.Documents Trans; SAPbobsCOM.Documents AffectedTrans;
                        //dynamic Trans;

                        // Determinar si es Factura (VEN) o Devolucion (DEV).

                        Double mTotalDocStellar = 0, mTotalDocSAP = 0;

                        Double Signo = 1;

                        mDocRel = RsVentas.Fields["cs_Documento_Rel"].Value.ToString();

                        RsDocRel = null;

                        Int32 TransSeries; String TransComments, TransRef1, TransRef2;

                        String GLAccountDev = String.Empty; String GLAccountVen = String.Empty;

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase) 
                        && (Properties.Settings.Default.SAP_CuentaContableFactura.Equals("*")))
                        {

                            mDatoTemp =
                            "SELECT \"DebPayAcct\" FROM \"OCRD\" T0 " +
                            "WHERE \"CardCode\" = '" + Properties.Settings.Default.SAP_ClienteContado + "' " + "\n";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {
                                GLAccountVen = SAPmRs.Fields.Item("DebPayAcct").Value;
                            }

                        }
                        else if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase)
                        && (Properties.Settings.Default.SAP_CuentaContableNotaCredito.Equals("*")))
                        {

                            mDatoTemp =
                            "SELECT \"AcctCode\" FROM \"CRD3\" T0 " +
                            "WHERE \"AcctType\" = 'D' " + 
                            "AND \"CardCode\" = '" + Properties.Settings.Default.SAP_ClienteContado + "' " + "\n";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {
                                GLAccountDev = SAPmRs.Fields.Item("AcctCode").Value;
                            } 
                            else
                            {

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Error de integridad. No esta definida la cuenta contable asociada a devoluciones.");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Error de integridad. No esta definida la cuenta contable asociada a devoluciones.",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, "Null-DEVAcct", "Validacion Stellar", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();

                                goto Continue;

                            }

                        }

                        SAP.StartTransaction();

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {

                            Related_StellarDocID = String.Empty;

                            if (!TmpDraftDoc )
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);
                            else
                            {
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oDrafts);
                                Trans.DocObjectCode = BoObjectTypes.oInvoices;
                            }

                            Int32 Serie = 0; Int32 SerieNextNum = 0;

                            if (!Properties.Settings.Default.SAP_SerieDocumentoFactura.isUndefined())
                                if (Properties.Settings.Default.SAP_SerieDocumentoFactura.Equals("*"))
                                {

                                    // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                    mEtapa = "Buscando la Serie correspondiente a Documento de Venta";
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    mDatoTemp =
                                    "SELECT * FROM \"NNM1\" " + "\n" +
                                    "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                    "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                    "AND \"ObjectCode\" = " + ((Int32) BoObjectTypes.oInvoices).ToString() + " " + "\n";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (SAPmRs.EoF)
                                    {

                                        // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "Error de integridad. No esta definida en SAP la Serie de Documentos Venta para esta localidad.");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                                        "No esta definida en SAP la Serie de Documentos Venta para esta localidad.",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                        Trans = null; //Trans.Cancel();
                                        if (SAP.InTransaction)
                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                        tsFinTrans = DateTime.Now;

                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                        RsVentas.MoveNext();

                                        goto Continue;

                                    }
                                    else
                                    {
                                        Serie = SAPmRs.Fields.Item("Series").Value;
                                        SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                        Trans.Series = Serie;
                                    }

                                }
                                else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoFactura, out Serie))
                                    Trans.Series = Serie;

                        }
                        else
                        {

                            Signo = -1;

                            if (!TmpDraftDoc)
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);
                            else
                            {
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oDrafts);
                                Trans.DocObjectCode = BoObjectTypes.oCreditNotes;
                            }

                            Related_StellarDocID = "POS" + "_" + Loc + "VEN" + mDocRel;

                            RsDocRel = mCn.Execute("SELECT * FROM [VAD20].[DBO].[MA_PAGOS] " + 
                            "WHERE c_Concepto = 'VEN' " +
                            "AND c_Numero = '" + mDocRel + "' " + 
                            "AND c_Sucursal = '" + Loc + "' ", out RecordsAffected);

                            Int32 Serie = 0; Int32 SerieNextNum = 0;

                            if (!Properties.Settings.Default.SAP_SerieDocumentoNC.isUndefined())
                                if (Properties.Settings.Default.SAP_SerieDocumentoNC.Equals("*"))
                                {

                                    // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                    mEtapa = "Buscando la Serie correspondiente a Documento Nota de Crédito";
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    mDatoTemp =
                                    "SELECT * FROM \"NNM1\" " + "\n" +
                                    "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                    "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                    "AND \"ObjectCode\" = " + ((Int32) BoObjectTypes.oCreditNotes).ToString() + " " + "\n";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (SAPmRs.EoF)
                                    {

                                        // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "Error de integridad. No esta definida en SAP la Serie de Documentos Nota de Crédito para esta localidad.");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "No esta definida en SAP la Serie de Documentos Nota de Crédito para esta localidad.",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                        Trans = null; //Trans.Cancel();
                                        if (SAP.InTransaction)
                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                        tsFinTrans = DateTime.Now;

                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                        RsVentas.MoveNext();

                                        goto Continue;

                                    }
                                    else
                                    {
                                        Serie = SAPmRs.Fields.Item("Series").Value;
                                        SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                        Trans.Series = Serie;
                                    }

                                }
                            if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoNC, out Serie))
                                    Trans.Series = Serie;

                        }

                        TransSeries = Trans.Series;

                        //Trans.Comments = "Stellar POS";
                        Trans.Comments = "Stellar POS" + ". " + TransID;

                        Int32 RelDocEntry = 0;
                        Int32 RelDocObjType = 0;
                        Int32 RelDocNum = 0;

                        Boolean FoundRelTrans = false;

                        if (RsDocRel != null)
                        {

                            Boolean DocRelDisponible = false;
                            
                            Trans.Reference1 = mDocRel;

                            Trans.Comments = Trans.Comments +
                            (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) ?
                            " -> " + "VEN" + mDocRel : String.Empty);

                            String[] mDatosSync = (RsDocRel.Fields["cs_Sync_SxS"].Value).ToString().Split(new Char[] { '|' });
                            if (mDatosSync.Length >= 3)
                            {

                                Int32 RelatedDocType = Convert.ToInt32(BoObjectTypes.oInvoices);
                                Int32 RelatedDocEntry;

                                if (Int32.TryParse(mDatosSync[1], out RelatedDocEntry))
                                {

                                    /*Trans.RelatedType = RelatedDocType;
                                    Trans.RelatedEntry = RelatedDocEntry;*/
                                    Trans.Reference2 = mDatosSync[2];
                                    Trans.Comments = Trans.Comments +
                                    " " + "(SBO No. " + mDatosSync[2] + ")";

                                    RelDocEntry = RelatedDocEntry;
                                    RelDocObjType = (Int32) BoObjectTypes.oInvoices;

                                    DocRelDisponible = true;

                                }

                            }else if (RsDocRel.Fields["cs_Sync_SxS"].Value.ToString().Equals("CARGA_INICIAL", StringComparison.OrdinalIgnoreCase))
                            {
                                RelDocEntry = -1;
                                RelDocObjType = (Int32)BoObjectTypes.oInvoices;
                                DocRelDisponible = true;
                                Trans.Comments = Trans.Comments + "\n\nNota: Esta nota de crédito no esta relacionada debido a que no hay referencia a un documento origen en SAP.";
                            }

                            if (!DocRelDisponible)
                            {

                                // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Error de integridad. Documento Afectado [" + mDocRel + "] no registrado.");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Documento Afectado [" + mDocRel + "] no registrado.",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, "Null-Fac", "Validacion Stellar", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();

                                goto Continue;

                            }

                        }

                        TransRef1 = Trans.Reference1;
                        TransRef2 = Trans.Reference2;
                        TransComments = Trans.Comments;

                        // false INTENCIONALMENTE para que deje de entrar aca sin comentar / volar el codigo. 
                        // Ya no vamos a enlazar devolucion con factura origen.
                        // Para poder colocar la N_C en la cuenta contable que corresponda. Sino no lo permite.

                        if (false && Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) && RelDocEntry != -1)
                        {

                            //

                            mEtapa = "Transaccion " + StellarDocID + " Reabriendo transacción afectada " + Related_StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            AffectedTrans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);

                            FoundRelTrans = AffectedTrans.GetByKey(RelDocEntry);

                            if(FoundRelTrans)
                            {

                                if(AffectedTrans.ReserveInvoice == BoYesNoEnum.tYES)
                                {

                                    mDatoTemp =
                                    "SELECT \"INV1\".\"Quantity\", \"INV1\".\"OpenQty\" FROM \"INV1\" " + "\n" +
                                    "INNER JOIN \"OINV\" " + "\n" +
                                    "ON \"INV1\".\"DocEntry\" = \"OINV\".\"DocEntry\" " + "\n" +
                                    "WHERE \"OINV\".\"DocEntry\" = " + AffectedTrans.DocEntry + " " + "\n" +
                                    "AND \"INV1\".\"OpenQty\" < \"INV1\".\"Quantity\" " + "\n" +
                                    "--ORDER BY \"OINV\".\"DocEntry\" DESC " + "\n" +
                                    "";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (!SAPmRs.EoF)
                                    {
                                        TransComments = TransComments + "\n\nNota: Esta nota de crédito no esta relacionada con su documento origen debido a que fue realizada contra una factura de reserva que tuvo entregas parciales.";
                                        FoundRelTrans = false;
                                    }
                                    
                                }

                            }

                            Int32 ReOpenTrans;

                            if (FoundRelTrans)
                            if (AffectedTrans.DocumentStatus != BoStatus.bost_Open) // Que esto no me tranque la sincronización de todo lo demas.
                            {
                                try
                                {
                                    ReOpenTrans = AffectedTrans.Reopen();

                                    if (ReOpenTrans != 0)
                                    {

                                        Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                        SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                        Console.WriteLine(mErrorNumber.ToString() + mErrorDesc);

                                        FoundRelTrans = false;

                                        //// No se pudo realizar la apertura del documento afectado

                                        Program.Logger.EscribirLog("Error al reabrir factura afectada por devolución [" + StellarDocID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "]. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error al reabrir factura afectada por devolución [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "]. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, "ReOpen-Fail", "Validacion SAP", Program.mCnLocal);

                                            //ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            //Trans = null; //Trans.Cancel();
                                            //if (SAP.InTransaction)
                                            //    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            //tsFinTrans = DateTime.Now;

                                            //Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            //RsVentas.MoveNext();

                                            //goto Continue;




                                            /**
                                             * SAPbobsCOM.InternalReconciliationParams ix1;
                                            SAPbobsCOM.InternalReconciliationsService ixs;
                                            SAPbobsCOM.InternalReconciliationOpenTrans ix2;

                                            ix2.CardOrAccount = CardOrAccountEnum.coaCard
                                                ix2.InternalReconciliationOpenTransRows.Item(0).
                                            **/



                                        TransComments = TransComments + "\n\nNota: Esta nota de crédito no esta relacionada debido a que SAP no permitió la re-apertura del documento afectado.";
                                        
                                    }

                                    //AffectedTrans.Close();
                                    AffectedTrans = null;

                                }
                                catch (Exception ReOpenEx) // Que esto no me tranque la sincronización de todo lo demas.
                                {
                                    Program.Logger.EscribirLog(ReOpenEx, mEtapa);
                                    //AffectedTrans.Close();
                                    AffectedTrans = null;

                                    TransComments = TransComments + "\n\nNota: Esta nota de crédito no esta relacionada debido a que SAP no permitió la re-apertura del documento afectado.";
                                }

                            }

                            if (!FoundRelTrans)
                            {
                                if (SAP.InTransaction) SAP.EndTransaction(BoWfTransOpt.wf_RollBack);
                                SAP.StartTransaction();
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);
                                Trans.Series = TransSeries;
                                Trans.Reference1 = TransRef1;
                                Trans.Reference2 = TransRef2;
                                Trans.Comments = TransComments;
                            }

                            //

                        }

                        // Asignar datos generales de cabecero de transacción.

                        mEtapa = "Asignando datos generales de cabecero de transacción";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        if (Properties.Settings.Default.SAP_OmitirFichaClientes)
                            Trans.CardCode = Properties.Settings.Default.Stellar_ClienteContado;
                        else
                            Trans.CardCode = RsVentas.Fields["c_Cliente"].Value.ToString();

                        if (Trans.CardCode.Equals(Properties.Settings.Default.Stellar_ClienteContado))
                            Trans.CardCode = Properties.Settings.Default.SAP_ClienteContado;

                        mTotalDocStellar = (Convert.ToDouble(RsVentas.Fields["n_Total"].Value) * Signo);

                        if (Properties.Settings.Default.SAP_AplicarDescuentoORecargoPorDiferenciaEntreTotales)
                        {
                            // Si se especifica esta propiedad SAP generará automaticamente un Descuento positivo o Negativo a nivel de totales para el documento por la diferencia.
                            Trans.DocTotal = Math.Round(mTotalDocStellar, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                            // Lo bueno es que los ajustes los hace a lo interno. En la pantalla del documento en SAP no se refleja que cambie el precio ni el 
                            // porcentaje de descuento asignado en las líneas. Solo se refleja el descuento a nivel de cabecero. Eso si, afecta impuesto, pero ni modo.
                            // Con esto podemos cerrar el capitulo de las diferencias tanto en factura como en nota de crédito, y evitamos incongruencias en los pagos.
                        }

                        SAPbobsCOM.Fields CamposOrg;
                        
                        CamposOrg = Trans.UserFields.Fields;

                        CamposOrg.Item("U_FACNIT").Value = RsVentas.Fields["c_Rif"].Value;
                        CamposOrg.Item("U_FACNOM").Value = RsVentas.Fields["c_Desc_Cliente"].Value;

                        Trans.Address = RsVentas.Fields["cu_Direccion_Cliente"].Value.ToString();

                        Trans.DocDate = Convert.ToDateTime(RsVentas.Fields["f_Fecha"].Value);
                        Trans.DocDueDate = Trans.DocDate.AddHours(23).AddMinutes(59).AddSeconds(59); //Trans.DocDate.AddMonths(1);
                        //Trans.TaxDate = Trans.DocDate; // Autocolocado en SAP

                        CamposOrg.Item("U_FACFECHA").Value = RsVentas.Fields["f_Fecha"].Value;

                        switch (RsVentas.Fields["c_Rif"].Value.ToString().Length)
                        {
                            case 9:
                                CamposOrg.Item("U_TNEGOCIO").Value = "1";
                                break;
                            case 11:
                                CamposOrg.Item("U_TNEGOCIO").Value = "2";
                                break;
                            default:
                                CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;
                                break;
                        };

                        CamposOrg.Item("U_CGASTOS").Value = "09";

                        CamposOrg.Item("U_CAJA").Value = RsVentas.Fields["c_Caja"].Value;

                        CamposOrg.Item("U_CUADRE").Value = Convert.ToInt32(RsVentas.Fields["Turno"].Value);

                        if (Properties.Settings.Default.SAP_ValidarDocumentoFiscal > 0)
                        {

                            //

                            mEtapa = "Buscando y validando los datos fiscales de la transacción.";
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                            RsDatosFiscales.Filter = "TransID = '" + TransID + "'";

                            Boolean mDatosFiscalesExisten = false;

                            mDatosFiscalesExisten = !RsDatosFiscales.EOF;

                            if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                            {

                                RsDocRelDatosFiscales = mCn.Execute(
                                "SELECT * FROM [VAD20].[DBO].[MA_DOCUMENTOS_FISCAL] " +
                                "WHERE cu_DocumentoTipo = 'VEN' " +
                                "AND cu_DocumentoStellar = '" + mDocRel + "' " +
                                "AND cu_Localidad = '" + Loc + "' ", out RecordsAffected);

                                mDatosFiscalesExisten = (mDatosFiscalesExisten && !RsDocRelDatosFiscales.EOF);

                            }

                            Boolean PasarValidacionFiscal = mDatosFiscalesExisten;
                            Boolean PasarConFallbackFiscal = false;

                            if (!PasarValidacionFiscal && Properties.Settings.Default.SAP_ValidarDocumentoFiscal == 2)
                            {

                                if (Convert.ToBoolean(RsVentas.Fields["bs_Impresa"].Value))
                                {

                                    RsDocRelDatosFiscales = mCn.Execute(
                                    "SELECT * FROM [VAD20].[DBO].[MA_CIERRES] " +
                                    "WHERE c_Concepto = 'CIT' " +
                                    "AND c_CodCaja = '" + RsVentas.Fields["c_Caja"].Value + "' " +
                                    "AND Turno = '" + RsVentas.Fields["Turno"].Value + "' " +
                                    "AND c_CodLocalidad = '" + Loc + "' ", out RecordsAffected);

                                    PasarConFallbackFiscal = (!RsDocRelDatosFiscales.EOF);

                                    if (!PasarConFallbackFiscal)
                                    {

                                        RsDocRelDatosFiscales = mCn.Execute(
                                        "SELECT * FROM [VAD20].[DBO].[TR_CAJA] " +
                                        "WHERE c_Estado = 'C' " +
                                        "AND c_Codigo = '" + RsVentas.Fields["c_Caja"].Value + "' " +
                                        "AND Turno = '" + RsVentas.Fields["Turno"].Value + "' ", out RecordsAffected);

                                        PasarConFallbackFiscal = (!RsDocRelDatosFiscales.EOF);

                                    }

                                    if (!PasarConFallbackFiscal)
                                    {
                                        if (Properties.Settings.Default.SAP_FallbackVentaSinDatosFiscales_MinutosMax > 0)
                                        {
                                            if ((DateTime.Now - mUltFec).TotalMinutes > Properties.Settings.Default.SAP_FallbackVentaSinDatosFiscales_MinutosMax)
                                            {
                                                PasarConFallbackFiscal = true;
                                            }
                                        }
                                    }

                                }

                                if (PasarConFallbackFiscal) PasarValidacionFiscal = true;

                            }

                            if (!PasarValidacionFiscal)
                            {

                                // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Error de integridad. El documento fiscal no esta impreso.");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "El documento fiscal no está impreso.",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, "Null-NCF", "Validacion Stellar", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();

                                goto Continue;

                            }
                            else
                            {

                                if (PasarConFallbackFiscal)
                                {
                                    CamposOrg.Item("U_FACSERIE").Value = RsVentas.Fields["c_TipoComprobanteFiscal"].Value;
                                    //CamposOrg.Item("U_FACNUM").Value = DBNull.Value;
                                    //CamposOrg.Item("U_SERIAL_IMP").Value = DBNull.Value;
                                    //CamposOrg.Item("U_NIF").Value = DBNull.Value;
                                    //CamposOrg.Item("U_ZFISCAL").Value = DBNull.Value;
                                    //CamposOrg.Item("U_NCO").Value = DBNull.Value;
                                }
                                else
                                {
                                    CamposOrg.Item("U_FACSERIE").Value = RsVentas.Fields["c_TipoComprobanteFiscal"].Value;
                                    CamposOrg.Item("U_FACNUM").Value = RsDatosFiscales.Fields["cu_NCF"].Value; // NCF
                                    CamposOrg.Item("U_SERIAL_IMP").Value = RsDatosFiscales.Fields["cu_SerialImpresora"].Value;
                                    CamposOrg.Item("U_NIF").Value = RsDatosFiscales.Fields["cu_DocumentoFiscal"].Value; // NIF
                                    CamposOrg.Item("U_ZFISCAL").Value = RsDatosFiscales.Fields["cu_ZFiscal"].Value;

                                    //Trans.Printed = PrintStatusEnum.psYes;
                                    //Trans.POSManufacturerSerialNumber = RsDatosFiscales.Fields["cu_SerialImpresora"].Value;
                                    //Trans.FiscalDocNum = RsDatosFiscales.Fields["cu_DocumentoFiscal"].Value; // NIF

                                    if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                                    {
                                        CamposOrg.Item("U_NCO").Value = RsDocRelDatosFiscales.Fields["cu_NCF"].Value; // NCF
                                    }
                                }
                                
                            }

                        }
                        else
                        {

                            CamposOrg.Item("U_FACSERIE").Value = RsVentas.Fields["c_TipoComprobanteFiscal"].Value;
                            //CamposOrg.Item("U_FACNUM").Value = DBNull.Value;
                            //CamposOrg.Item("U_SERIAL_IMP").Value = DBNull.Value;
                            //CamposOrg.Item("U_NIF").Value = DBNull.Value;
                            //CamposOrg.Item("U_ZFISCAL").Value = DBNull.Value;
                            //CamposOrg.Item("U_NCO").Value = DBNull.Value;

                        }

                        //

                        mEtapa = "Buscando la fecha de documento válido de la serie.";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT \"@COMPROBANTES\".* FROM \"@COMPROBANTES\" " + "\n" +
                        "INNER JOIN \"NNM1\" " + "\n" +
                        "ON \"@COMPROBANTES\".\"U_SERIE_SAP\" = \"NNM1\".\"Series\" " + "\n" +
                        "WHERE \"NNM1\".\"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND \"@COMPROBANTES\".\"U_TIPO_COMPROBANTE\" = '" + CamposOrg.Item("U_FACSERIE").Value + "' " + "\n" +
                        "";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            // No hay enlace entre series NCF y Series SAP

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Fiscales para esta localidad.");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "No esta definida en SAP la Serie de Documentos Fiscales para esta localidad.",
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, "Null-FV6", "Validacion Stellar", Program.mCnLocal);

                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            RsVentas.MoveNext();

                            goto Continue;

                        }
                        
                        CamposOrg.Item("U_FV6").Value = SAPmRs.Fields.Item("U_FECHAVALIDO").Value;

                        String mSerialCuponNC = String.Empty;

                        if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) 
                        && (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada))
                        {

                            mEtapa = "Consultando número de vale de nota de crédito para registro interno de control en SAP.";
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            String TmpSQL1 = "SELECT * FROM VAD20.DBO.MA_TRANSACCION_CUPONES CUP " +
                            "WHERE Concepto = 'DEV' " +
                            "AND CodLocalidad = '" + Loc + "' " +
                            "AND Numero = '" + NumDoc + "' " +
                            "AND CodProducto = '*N_C_POS*' ";

                            ADODB.Recordset pRsCuponNC = new ADODB.Recordset();

                            pRsCuponNC.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                            pRsCuponNC.Open(TmpSQL1, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                            pRsCuponNC.ActiveConnection = null;

                            if (!pRsCuponNC.EOF)
                            {

                                mSerialCuponNC = Convert.ToString(pRsCuponNC.Fields["SerialCupon"].Value);
                                
                                if (Properties.Settings.Default.SAP_NotaCredito_ConFechaVencimientoDelCupon)
                                {
                                    Trans.DocDueDate = Convert.ToDateTime(pRsCuponNC.Fields["FechaValidezFin"].Value);
                                    if (Trans.DocDueDate < DateTime.Now) Trans.DocDueDate = DateTime.Today.AddHours(23).AddMinutes(59).AddMinutes(59);
                                }

                            } 
                            else
                            {
                                mSerialCuponNC = Loc + NumDoc;
                            }
                            
                        }

                        Trans.NumAtCard = TransID;

                        //

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {
                            Trans.TrackingNumber = NumDoc;
                        }
                        else if(Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                        {

                            Trans.TrackingNumber = NumDoc + " -> " + mDocRel;

                            if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada) && 
                            Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC.Equals("TrackNo", StringComparison.OrdinalIgnoreCase))
                            {
                                Trans.TrackingNumber = mSerialCuponNC; //MAX 30
                            }

                            if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada) &&
                            Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC.Equals("PaymentRef", StringComparison.OrdinalIgnoreCase))
                            {
                                Trans.PaymentReference = mSerialCuponNC; // MAX 27
                            }

                            /*if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada) &&
                            Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC.Equals("NumAtCard", StringComparison.OrdinalIgnoreCase))
                            {
                                Trans.NumAtCard = mSerialCuponNC; // MAX 100
                            }*/

                            if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada) &&
                            Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC.StartsWith("U_", StringComparison.OrdinalIgnoreCase))
                            {
                                CamposOrg.Item(Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC).Value = mSerialCuponNC; // MAX 30 - 50
                            }

                        }

                        //Trans.JournalMemo = String.Empty; // Autocolocado en SAP
                        Trans.Rounding = BoYesNoEnum.tNO;

                        Int32 POSCashierNumber = 0;

                        if (Int32.TryParse(RsVentas.Fields["c_Usuario"].Value.ToString(), out POSCashierNumber))
                            Trans.POSCashierNumber = POSCashierNumber;

                        /*
                        Int32 POSReceiptNumber = 0;

                        if (Int32.TryParse(RsVentas.Fields["c_Numero"].Value, out POSReceiptNumber))
                            Trans.POSReceiptNo = POSReceiptNumber;

                        Int32 POS_CashRegister = 0;

                        if (Int32.TryParse(RsVentas.Fields["c_Caja"].Value, out POS_CashRegister))
                            Trans.POS_CashRegister = POS_CashRegister;
                        
                        Int32 POSDailySummaryNo = 0;

                        if (Int32.TryParse(Convert.ToString(RsVentas.Fields["Turno"].Value), out POSDailySummaryNo))
                            Trans.POSDailySummaryNo = POSDailySummaryNo;

                        Trans.POSEquipmentNumber = RsVentas.Fields["c_Caja"].Value;
                        */

                        if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                        {

                            mDatoTemp = RsVentas.Fields["c_Motivo"].Value.ToString();
                            mDatoTemp = mDatoTemp.Replace("'", "''");

                            if (!mDatoTemp.Trim().isUndefined())
                            { 

                                TmpRs = mCn.Execute(
                                "SELECT * \n" +
                                "FROM [VAD10].[DBO].[MA_AUX_GRUPO] \n" +
                                "WHERE cs_Tipo = 'MOTIVOS_DEV' \n" +
                                "AND cs_Grupo = '" + mDatoTemp + "' ", out RecordsAffected);

                                if (!TmpRs.EOF)
                                {
                                    CamposOrg.Item("U_TIPO_DEV_VTA").Value = TmpRs.Fields["cs_Codigo_Opcion"].Value;
                                }

                            }

                            CamposOrg.Item("U_TIPONC").Value = "07";

                        }

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase) && !Properties.Settings.Default.SAP_CuentaContableFactura.Trim().isUndefined())
                        {
                            // Si no se define explicitamente se toma el valor configurado como ControlAccount en la cuenta del Socio de Negocio / Business Partner.
                            if (Properties.Settings.Default.SAP_CuentaContableFactura.Equals("*"))
                                Trans.ControlAccount = GLAccountVen;
                            else
                                Trans.ControlAccount = Properties.Settings.Default.SAP_CuentaContableFactura;
                        }
                        else if(Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) && !Properties.Settings.Default.SAP_CuentaContableNotaCredito.Trim().isUndefined())
                        {
                            // Si no se define explicitamente se toma el valor configurado como ControlAccount en la cuenta del Socio de Negocio / Business Partner.
                            if (Properties.Settings.Default.SAP_CuentaContableNotaCredito.Equals("*"))
                                Trans.ControlAccount = GLAccountDev;
                            else
                                Trans.ControlAccount = Properties.Settings.Default.SAP_CuentaContableNotaCredito;
                        }

                        if (PendXEntrega)
                        {
                            Trans.ReserveInvoice = BoYesNoEnum.tYES;
                            ListaPendXEntrega = new Dictionary<String, Object[]>();
                        }

                        // Buscar Items del Documento.

                        LineRelation = new Dictionary<String, String[]>();
                        ListaControlSeriales = new Dictionary<String, Object[]>();

                        mEtapa = "Ingresando Items del Documento";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        RsItems.Filter = "TransID = '" + TransID + "'";

                        if (RsItems.EOF)
                        {

                            // No hay items. Error de Integridad.

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Error de integridad. El documento no posee items registrados.");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "El documento no posee items registrados.", 
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, "Null-Items", "Validacion Stellar", Program.mCnLocal);

                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            RsVentas.MoveNext();

                            goto Continue;

                        }

                        // Registrar los productos de la transacción.

                        ItmCount = 0;

                        while (!RsItems.EOF)
                        {

                            StellarItemID = RsItems.Fields["ID"].Value.ToString();
                            StellarItemLn = RsItems.Fields["n_Linea"].Value.ToString();
                            StellarItemCode = RsItems.Fields["Cod_Principal"].Value.ToString();

                            IDocument_Lines Line = Trans.Lines;
                            //dynamic Line = Trans.Lines;

                            if (ItmCount > 0){
                                Line.Add();
                                //Line.SetCurrentLine(ItmCount+1);
                                Line.SetCurrentLine(ItmCount);
                            }

                            ItmCount++;

                            //

                            if (RelDocEntry > 0 && RelDocObjType == (Int32) BoObjectTypes.oInvoices)
                            {

                                mEtapa = "Buscando Linea de Item relacionada para Nota de Credito.";
                                Console.WriteLine(mEtapa);
                                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                mDatoTemp =
                                "SELECT IfNULL(\"INV1\".\"LineNum\", 0) AS \"BaseLine\" " + "\n" +
                                "FROM \"INV1\" " + "\n" +
                                "WHERE \"INV1\".\"ItemCode\" = '" + StellarItemCode + "' " + "\n" +
                                "AND \"INV1\".\"DocEntry\" = '" + RelDocEntry + "' " + "\n" +
                                "";

                                SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                SAPmRs.DoQuery(mDatoTemp);

                                if (!SAPmRs.EoF)
                                {

                                    //Trans.ReopenOriginalDocument = BoYesNoEnum.tYES;
                                    //Trans.ReopenManuallyClosedOrCanceledDocument = BoYesNoEnum.tYES;

                                    if(FoundRelTrans)
                                    {
                                        Line.BaseLine = SAPmRs.Fields.Item("BaseLine").Value;
                                        Line.BaseEntry = RelDocEntry;
                                        Line.BaseType = RelDocObjType;
                                    }

                                    // Estos campos son para relacionar la nota de crédito a la factura, pero SAP maneja un concepto de 
                                    // "Documentos Cerrados" contablemente y si el documento está en ese estado da error al relacionarle la N_C
                                    // No debería pero... ni modo. Asi funciona ese sistema. Por lo tanto no relacionamos directamente para que no nos de ningun error.

                                    //Console.WriteLine(SAPmRs.Fields.Item("BaseLine").Value);

                                }

                            }

                            Line.ItemCode = StellarItemCode;

                            mEtapa = "Añadiendo item DocLn " + ItmCount + ", ID " + StellarItemID + ", TransLn " + StellarItemLn + ", ItemCode " + Line.ItemCode;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            //Program.Logger.EscribirLog("TransID:" + TransID + "|LineNum:" + Line.LineNum.ToString());

                            LineRelation.Add((TransID + "|" + ItmCount.ToString()),
                            new String[] { StellarItemID, StellarItemLn });

                            Line.Quantity = (Convert.ToDouble(RsItems.Fields["Cantidad"].Value) * Signo);
                            Double CantAsignarRestante = 0;

                            if (Properties.Settings.Default.SAP_Linea_DeterminarPorcentajeDescuentoItem)
                            {

                                Double mPrecioOrig = Math.Abs(Convert.ToDouble(RsItems.Fields["n_PrecioReal"].Value));
                                Double mPrecioFinal = Math.Abs(Convert.ToDouble(RsItems.Fields["Precio"].Value));
                                Double mDctoUnit = Math.Round((mPrecioOrig - mPrecioFinal), 8, MidpointRounding.AwayFromZero);

                                if ((mDctoUnit > 0) && (mPrecioOrig != 0))
                                {
                                    Double mPorcDescLn = ((mDctoUnit / mPrecioOrig) * 100); //(Math.Round(mDctoUnit / mPrecioOrig, 6) * 100);
                                    Line.UnitPrice = mPrecioOrig;
                                    Line.DiscountPercent = mPorcDescLn;
                                }
                                else
                                {
                                    Line.UnitPrice = mPrecioFinal;
                                    Line.DiscountPercent = 0;
                                }
                                
                            }
                            else
                            {
                                Line.UnitPrice = Math.Abs(Convert.ToDouble(RsItems.Fields["Precio"].Value));
                                Line.DiscountPercent = 0;
                            }

                            if (Program.ListaProductosCupon.ContainsValue(Line.ItemCode))
                            {
                                // QUE NO GENERE UTILIDAD
                                Line.GrossBase = -10;
                                Line.GrossBuyPrice = Math.Abs(Convert.ToDouble(RsItems.Fields["Precio"].Value));
                            }

                            // Categorizar Impuesto del producto.

                            String LnTax1 = String.Empty;
                            String LnTax2 = String.Empty;
                            String LnTax3 = String.Empty;
                            String LnTax = String.Empty;

                            Double ItemTax = 0;

                            ItemTax = Math.Abs(
                                Convert.ToDouble(RsItems.Fields["Impuesto1"].Value) +
                                Convert.ToDouble(RsItems.Fields["Impuesto2"].Value) +
                                Convert.ToDouble(RsItems.Fields["Impuesto3"].Value)
                            );

                            if (Program.TaxList1 != null)
                            if (Program.TaxList1.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList1.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax1);
                            }

                            if (Program.TaxList2 != null)
                            if (Program.TaxList2.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList2.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax2);
                            }

                            if (Program.TaxList3 != null)
                            if (Program.TaxList3.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList3.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax3);
                            }

                            if (LnTax.isUndefined() && !LnTax1.isUndefined()) LnTax = LnTax1;
                            if (LnTax.isUndefined() && !LnTax2.isUndefined()) LnTax = LnTax2;
                            if (LnTax.isUndefined() && !LnTax3.isUndefined()) LnTax = LnTax3;

                            Line.TaxCode = LnTax;

                            CamposOrg = Line.UserFields.Fields;

                            // Valor opcional de Tipo Lista de Precio.

                            if (Program.ListaAsociacionTipoPrecio != null)
                            {

                                String nTipoPrecio = RsItems.Fields["n_TipoPrecio"].Value.ToString();
                                String TipoPrecio = String.Empty;

                                if (Program.ListaAsociacionTipoPrecio.ContainsKey(nTipoPrecio))
                                {
                                    Program.ListaAsociacionTipoPrecio.TryGetValue(nTipoPrecio, out TipoPrecio);
                                }

                                CamposOrg.Item("U_Listaprecio").Value = TipoPrecio;

                            }

                            Boolean ManejaSerial; Int32 CantDec = 2;

                            mDatoTemp =
                            "SELECT IfNULL(\"OITM\".\"U_TIPOMONTO\", 'B') AS \"TipoMonto\", " + "\n" +
                            "IfNULL(\"OITM\".\"U_DIVISION\", NULL) AS \"Division\", " + "\n" +
                            "IfNULL(\"OITM\".\"U_NUMERODECIMALES\", 0) AS \"CantDec\", " + "\n" +
                            "\"OITM\".\"ManSerNum\" AS \"ManejaSerial\" " + "\n" +
                            "FROM \"OITM\" " + "\n" +
                            "WHERE \"OITM\".\"ItemCode\" = '" + Line.ItemCode + "' " + "\n" +
                            "";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {
                                mDatoTemp = SAPmRs.Fields.Item("TipoMonto").Value;
                                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;
                                mDatoTemp = SAPmRs.Fields.Item("Division").Value;
                                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                Line.CostingCode2 = mDatoTemp;
                                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                mDatoTemp = SAPmRs.Fields.Item("ManejaSerial").Value;
                                ManejaSerial = (mDatoTemp.ToUpper() == "Y");
                                CantDec = SAPmRs.Fields.Item("CantDec").Value;
                            }
                            else
                            {
                                CamposOrg.Item("U_TIPOMONTO").Value = "B";
                                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                Line.CostingCode2 = String.Empty;
                                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                ManejaSerial = false;
                            }

                            if (ManejaSerial)
                            {
                                if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                    ListaControlSeriales.Add(Line.ItemCode,
                                    new Object[] { Line.ItemCode, (Int64) 0 });
                            }

                            if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                            {

                                if (!TmpDraftDoc && Trans.ReserveInvoice == BoYesNoEnum.tNO && ManejaSerial)
                                {
                                    
                                    mEtapa = "Verificando / Asignando Seriales. Producto " + Line.ItemCode;
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    Int64 MTS_ID = 0;

                                    if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                        ListaControlSeriales.Add(Line.ItemCode,
                                        new Object[] { Line.ItemCode, MTS_ID });
                                    else
                                        MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                    RsSeriales.Filter = "TransID = '" + TransID + "' " + 
                                    "AND Cod_Principal = '" + Line.ItemCode + "' " + 
                                    "AND ID > " + MTS_ID.ToString();

                                    if (!RsSeriales.EOF)
                                    {

                                        SerialNumbers LnSr = Line.SerialNumbers;

                                        Int32 SerIndex = -1;
                                        Int32 mUltNum = 0;
                                        String UsedSN = String.Empty;
                                        
                                        CantAsignarRestante = Line.Quantity;

                                        while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                        {

                                            SerIndex++;

                                            LnSr.SetCurrentLine(SerIndex);

                                            MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                            ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                            UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                            //LnSr.ManufacturerSerialNumber = UsedSN;
                                            LnSr.InternalSerialNumber = UsedSN;

                                            mDatoTemp = String.Empty; //"SELECT * FROM (" + "\n";

                                            mDatoTemp = mDatoTemp + 
                                            "SELECT * FROM (SELECT TOP 5 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 1 AS \"TmpPrioridad\" " + "\n" +
                                            "FROM \"OSRN\" " + "\n" +
                                            "LEFT JOIN \"OSRI\" " + "\n" +
                                            "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                            "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                            "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                            "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                            "--AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                            "ORDER BY CASE WHEN IfNULL(\"OSRI\".\"WhsCode\", '') = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') THEN 1 ELSE 0 END DESC, \"OSRN\".\"SysNumber\" DESC" + "\n" +
                                            ")" + "\n";

                                            mDatoTemp = mDatoTemp + "UNION ALL" + "\n";

                                            mDatoTemp = mDatoTemp +
                                            "SELECT * FROM (SELECT TOP 3 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 2 AS \"TmpPrioridad\" " + "\n" +
                                            "FROM \"OSRN\" " + "\n" +
                                            "LEFT JOIN \"OSRI\" " + "\n" +
                                            "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                            "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                            "AND (IfNULL(\"OSRN\".\"MnfSerial\", '') = '' AND IfNULL(\"OSRN\".\"DistNumber\", '') = '') " + "\n" +
                                            "AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                            "AND UPPER(IfNULL(\"OSRI\".\"WhsCode\", '')) = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') " + "\n" +
                                            "ORDER BY \"OSRN\".\"SysNumber\" ASC" + "\n" +
                                            ")" + "\n";

                                            //mDatoTemp = mDatoTemp + ") TB ORDER BY \"TmpPrioridad\", CASE WHEN \"TmpPrioridad\" = 1 THEN \"SysNumber\" ELSE 0 END DESC, CASE WHEN \"TmpPrioridad\" = 2 THEN \"SysNumber\" ELSE 0 END ASC" + "\n";

                                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                            SAPmRs.DoQuery(mDatoTemp);

                                            if (!SAPmRs.EoF)
                                            {

                                                /*
                                                mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                                if (!mDatoTemp.isUndefined())
                                                {
                                                    LnSr.ManufacturerSerialNumber = mDatoTemp;
                                                }*/
                                                mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                                mDatoTemp2 = SAPmRs.Fields.Item("MnfSerial").Value;

                                                //LnSr.SystemSerialNumber = SAPmRs.Fields.Item("SysSerial").Value;
                                                //LnSr.InternalSerialNumber = SAPmRs.Fields.Item("IntrSerial").Value;

                                                //LnSr.ExpiryDate = SAPmRs.Fields.Item("ExpDate").Value;
                                                //LnSr.ReceptionDate = SAPmRs.Fields.Item("InDate").Value;
                                                //LnSr.ManufactureDate = SAPmRs.Fields.Item("MnfDate").Value;
                                                //LnSr.WarrantyStart = SAPmRs.Fields.Item("GrntStart").Value;
                                                //LnSr.WarrantyEnd = SAPmRs.Fields.Item("GrntExp").Value;

                                                //mUltNum = LnSr.SystemSerialNumber;
                                                
                                                if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(0))
                                                && Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase)
                                                && mDatoTemp.isUndefined() && mDatoTemp2.isUndefined()) // Hay un slot disponible pero el serial no estaba pre creado.
                                                {

                                                    SAPbobsCOM.CompanyService SNCompanyService;
                                                    SNCompanyService = SAP.GetCompanyService();

                                                    SAPbobsCOM.SerialNumberDetailsService SNDetailService;
                                                    SNDetailService = SNCompanyService.GetBusinessService(ServiceTypes.SerialNumberDetailsService);

                                                    SAPbobsCOM.SerialNumberDetailParams SNDP;
                                                    SNDP = SNDetailService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                                                    SNDP.DocEntry = SAPmRs.Fields.Item("AbsEntry").Value; //'Put here the actual key of the serialnumber record

                                                    SAPbobsCOM.SerialNumberDetail SNDetails;
                                                    SNDetails = SNDetailService.Get(SNDP);

                                                    //SNDetails.MfrSerialNo = UsedSN; //"STELLAR11";
                                                    SNDetails.SerialNumber = UsedSN;

                                                    try
                                                    {
                                                        SNDetailService.Update(SNDetails);
                                                    }
                                                    catch (Exception SNDS1)
                                                    {

                                                        // No se pudo actualizar el Serial

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                        UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                        UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }

                                                    //LnSr.ManufacturerSerialNumber = UsedSN;
                                                    LnSr.InternalSerialNumber = UsedSN;
                                                    LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                    //Line.WarehouseCode = "1001"; // SOLO TEST.... Ni modo. Tendran que trasladar los seriales antes
                                                    // de poderlos vender.

                                                } else { 
                                                    
                                                    if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(1)) ||
                                                    !Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase))
                                                    {

                                                        // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " + 
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "El serial [" + UsedSN + "] " +
                                                        "no existe ni hay un slot disponible en el Almacen " + 
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }

                                                    LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                } // El serial ya existía asi que simplemente lo utilizamos.

                                            } else
                                            {

                                                // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " + 
                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "El serial [" + UsedSN + "] " +
                                                "no existe ni hay un slot disponible en el Almacen " + 
                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                tsFinTrans = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                RsVentas.MoveNext();

                                                goto Continue;

                                            }

                                            //LnSr.BaseLineNumber = Line.LineNum;
                                            //LnSr.InternalSerialNumber = Convert.ToString(SerIndex + 1);

                                            //LnSr.ExpiryDate = DateTime.Today.AddMonths(3);
                                            //LnSr.ReceptionDate = DateTime.Today;
                                            //LnSr.ManufactureDate = DateTime.Today;
                                            //LnSr.WarrantyStart = DateTime.Today;
                                            //LnSr.WarrantyEnd = DateTime.Today.AddMonths(3);

                                            LnSr.Quantity = 1;

                                            CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                            RsSeriales.MoveNext();

                                            if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                                LnSr.Add();

                                        }
                                        
                                    }

                                }
                                else if (Trans.ReserveInvoice == BoYesNoEnum.tYES)
                                {

                                    if (ListaPendXEntrega.ContainsKey(Line.ItemCode))
                                    {

                                        Object[] TmpListItem = ListaPendXEntrega[Line.ItemCode];
                                        
                                        TmpListItem[1] = Math.Round(Convert.ToDouble(TmpListItem[1]) + Line.Quantity, Convert.ToInt32(TmpListItem[4]), MidpointRounding.AwayFromZero);
                                        TmpListItem[3] = Math.Round(Convert.ToDouble(TmpListItem[3]) + Line.Quantity, Convert.ToInt32(TmpListItem[4]), MidpointRounding.AwayFromZero);
                                        
                                        if (Convert.ToDouble(TmpListItem[3]) > 0)
                                        {

                                            GenerarDocEntrega = true;

                                            if (SerialComboParentItems.ContainsKey(Line.ItemCode))
                                                TmpDraftDeliveryDoc = true;

                                        }

                                        ListaPendXEntrega[Line.ItemCode] = TmpListItem;

                                    } else {
                                        
                                        mEtapa = "Verificando si el producto esta pendiente de entrega. Producto " + Line.ItemCode;
                                        Console.WriteLine(mEtapa);
                                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                        RsPendXEntregaTR.Filter = "TransID = '" + TransID + "' AND CodProducto = '" + Line.ItemCode + "'";

                                        if (!RsPendXEntregaTR.EOF)
                                        {

                                            Double mCantidadPendiente = 0;

                                            while (!RsPendXEntregaTR.EOF)
                                            {
                                                mCantidadPendiente += Math.Round(Convert.ToDouble(RsPendXEntregaTR.Fields["CantPendiente"].Value), CantDec, MidpointRounding.AwayFromZero);
                                                RsPendXEntregaTR.MoveNext();
                                            }

                                            Double mCantidadEntregaImmediata = Math.Round(Line.Quantity - mCantidadPendiente, CantDec, MidpointRounding.AwayFromZero);

                                            if (mCantidadEntregaImmediata > 0)
                                            {

                                                GenerarDocEntrega = true;

                                                if (SerialComboParentItems.ContainsKey(Line.ItemCode))
                                                    TmpDraftDeliveryDoc = true;

                                            }
                                            
                                            ListaPendXEntrega.Add(Line.ItemCode,
                                            new Object[] { Line.ItemCode, Line.Quantity, mCantidadPendiente, mCantidadEntregaImmediata, 
                                            CantDec, ManejaSerial });

                                        }
                                        else
                                        {
                                            GenerarDocEntrega = true;
                                            ListaPendXEntrega.Add(Line.ItemCode,
                                            new Object[] { Line.ItemCode, Line.Quantity, 0, Line.Quantity,
                                            CantDec, ManejaSerial });
                                        }

                                    }

                                }

                            }
                            else if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                            {

                                if (!TmpDraftDoc && ManejaSerial)
                                {

                                    SerialNumbers LnSr = Line.SerialNumbers;

                                    Int32 SerIndex = -1;
                                    String UsedSN = String.Empty;

                                    Int64 MTS_ID = 0;

                                    if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                        ListaControlSeriales.Add(Line.ItemCode,
                                        new Object[] { Line.ItemCode, MTS_ID });
                                    else
                                        MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                    RsSeriales.Filter = "TransID = '" + TransID + "' " +
                                    "AND Cod_Principal = '" + Line.ItemCode + "' " +
                                    "AND ID > " + MTS_ID.ToString();

                                    CantAsignarRestante = Line.Quantity;

                                    while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                    {

                                        SerIndex++;

                                        LnSr.SetCurrentLine(SerIndex);

                                        MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                        ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                        UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                        //LnSr.ManufacturerSerialNumber = UsedSN;
                                        //LnSr.InternalSerialNumber = UsedSN;

                                        mDatoTemp =
                                        "SELECT \"OSRN\".* FROM \"OSRN\" " + "\n" +
                                        "LEFT JOIN \"OSRI\" " + "\n" +
                                        "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                        "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER(IfNULL(\"OSRI\".\"IntrSerial\", '')) OR \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\") " + "\n" +
                                        "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                        "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                        "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                        "AND IfNULL(\"OSRI\".\"Status\", 0) = 1 " + "\n" +
                                        "ORDER BY IfNULL(\"OSRN\".\"SysNumber\", 0) DESC";

                                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                        SAPmRs.DoQuery(mDatoTemp);

                                        if (!SAPmRs.EoF)
                                        {

                                            // Si existe (Debería existir a juro y estar en estatus no disponible) simplemente lo usamos.
                                            /*mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                            if (!mDatoTemp.isUndefined())
                                            {
                                                LnSr.ManufacturerSerialNumber = mDatoTemp;
                                            }*/
                                            mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                            if (!mDatoTemp.isUndefined())
                                                LnSr.InternalSerialNumber = mDatoTemp;
                                            else
                                            {
                                                LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);
                                            }

                                        }
                                        else
                                        {

                                            // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Error de integridad. El serial a devolver [" + UsedSN + "] no se encontró en el sistema, Producto [" + Line.ItemCode + "].");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "El serial a devolver [" + UsedSN + "] " +
                                            "no se encontró en el sistema, Producto [" + Line.ItemCode + "]." +
                                            "Error ingresando Transaccion POS [" + TransID + "] en SAP.",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        LnSr.Quantity = 1;

                                        CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                        RsSeriales.MoveNext();

                                        if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                            LnSr.Add();

                                    }

                                }

                            }

                            RsItems.MoveNext();

                        }

                        // Fin Productos.

                        // Grabar la transacción.

                        mEtapa = "Procediendo a grabar (registrar) la transacción " + StellarDocID;
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        tsInicioProc = DateTime.Now;

                        String TXML = String.Empty; String TXML2 = String.Empty;
                        TXML = Trans.GetAsXML();

                        Int32 TmpResult = Trans.Add();

                        tsFinProc = DateTime.Now;

                        Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.Add(): "));

                        if (TmpResult != 0)
                        {

                            Int32 mErrorNumber; String mErrorDesc = String.Empty;

                            SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                            if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                            {
                                String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                if (Step1.Length > 1)
                                {
                                    String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                    if (Step2.Length > 1)
                                    {
                                        try 
	                                    {
                                            //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                            Int32 mLinea = Convert.ToInt32(Step2[0]);
                                            //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                            //IDocument_Lines x;
                                            //Trans.Lines.SetCurrentLine(mLinea);
                                            String mKey = (TransID + "|" + mLinea.ToString());

                                            //Program.Logger.EscribirLog("mKey:" + mKey);

                                            String[] mLineData = LineRelation[mKey];

                                            //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                            RsItems.Filter = "ID = " + "(" + mLineData[0] + ")";

                                            if (!RsItems.EOF)
                                            {
                                                String mDescripcion = mCn.Execute("SELECT c_Descri FROM VAD20.DBO.MA_PRODUCTOS " +
                                                "WHERE c_Codigo = '" + RsItems.Fields["Cod_Principal"].Value.ToString() + "'", 
                                                out RecordsAffected).Fields["c_Descri"].Value.ToString();
                                                //Program.Logger.EscribirLog("Datos de Linea Cargada");
                                                mErrorDesc += ". Datos de la línea (Cod|Desc|Cant|Prc): " + "(" +
                                                RsItems.Fields["Cod_Principal"].Value.ToString() + "|" + mDescripcion + "|" +
                                                RsItems.Fields["Cantidad"].Value.ToString() + "|" + RsItems.Fields["Precio"].Value.ToString() + ")";
                                            }

	                                    }
	                                    catch (Exception ItemEx)
	                                    {
                                            Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
	                                    }
                                    }
                                }

                            }

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, mErrorNumber.ToString(), "Documents.Add()", Program.mCnLocal);

                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            RsVentas.MoveNext();
                            goto Continue;

                        }

                        // Documento Registrado.

                        String LastDocEntry; Int32 DocEntry; Double TotalDoc; String LastDocNum; Int32 DocNum;

                        SAP.GetNewObjectCode(out LastDocEntry);
                        DocEntry = Convert.ToInt32(LastDocEntry);

                        tsInicioProc = DateTime.Now;

                        Boolean FoundTrans = Trans.GetByKey(DocEntry);

                        tsFinProc = DateTime.Now;

                        Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.GetByKey(): "));

                        Double mDiferenciaTotales = 0;

                        if (FoundTrans)
                        {

                            if (TmpDraftDoc) { TXML2 = Trans.GetAsXML(); }
                            
                            DocNum = Trans.DocNum;
                            LastDocNum = Convert.ToString(DocNum);

                            TotalDoc = Trans.DocTotal;
                            mTotalDocSAP = TotalDoc;

                            if (Math.Round(mTotalDocStellar, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) !=
                            Math.Round(mTotalDocSAP, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero))
                                // Para calcular la diferencia exacta sin redondeo
                                mDiferenciaTotales = Math.Round((mTotalDocSAP - mTotalDocStellar), 8, MidpointRounding.AwayFromZero);

                            if (Math.Abs(Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero)) >= 0.01)
                            {
                                Console.WriteLine("Diferencia entre totales: " + "\n\n" +
                                Convert.ToString(mTotalDocStellar) + "\n" +
                                Convert.ToString(mTotalDocSAP) + "\n" +
                                "Diferencia: " + mDiferenciaTotales.ToString());

                                if (Properties.Settings.Default.DebugMode)
                                {
                                    MessageBox.Show("Diferencia entre totales: " + "\n\n" +
                                    Convert.ToString(mTotalDocStellar) + "\n" +
                                    Convert.ToString(mTotalDocSAP) + "\n" +
                                    "Diferencia: " + mDiferenciaTotales.ToString());
                                }
                            } else
                            {
                                // Diferencia no significativa. No afectá el pago.
                                mDiferenciaTotales = 0; 
                            }

                        }
                        else
                        {

                            Int32 mErrorNumber; String mErrorDesc = String.Empty;

                            SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                            Program.Logger.EscribirLog("Error buscando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error buscando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, mErrorNumber.ToString(), "Documents.GetByKey(" + DocEntry + ")", Program.mCnLocal);

                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            RsVentas.MoveNext();
                            goto Continue;

                        }

                        if (TmpDraftDoc)
                        {

                            IDocument_Lines Line = Trans.Lines;

                            CamposOrg = Line.UserFields.Fields;

                            Int32 LineComboStart = -1;
                            String CurrentComboCode = String.Empty;
                            Double CurrentComboQty = 0;

                            for (Int32 i = 0; i < Line.Count; i++)
                            {

                                Boolean AplicaSeriales = false;
                                Object[] CurrentComboItemData = null;

                                Line.SetCurrentLine(i);
                                StellarItemCode = Line.ItemCode;

                                if (Line.TreeType == BoItemTreeTypes.iSalesTree && SerialComboParentItems.ContainsKey(Line.ItemCode))
                                {

                                    LineComboStart = i;
                                    CurrentComboCode = Line.ItemCode;
                                    CurrentComboQty = Line.Quantity;

                                } else if (Line.TreeType == BoItemTreeTypes.iIngredient)
                                {
                                    if (SerialComboParentItems.ContainsKey(CurrentComboCode))
                                        if  (SerialComboParentItems[CurrentComboCode].ContainsKey(Line.ItemCode))
                                        {
                                            // Es un ingrediente del combo que maneja seriales. Aplicar lógica de seriales
                                            AplicaSeriales = true;
                                            CurrentComboItemData = SerialComboParentItems[CurrentComboCode][Line.ItemCode];
                                        }
                                } else
                                {

                                    LineComboStart = -1;
                                    CurrentComboCode = String.Empty;
                                    CurrentComboQty = 0;

                                    AplicaSeriales = (ListaControlSeriales.ContainsKey(Line.ItemCode));

                                    /*if (Line.SerialNumbers.Count > Line.Quantity)
                                    {
                                        for (Int32 k = 0; k <= Line.Quantity - 1; k++)
                                        {
                                            Line.SerialNumbers.SetCurrentLine(Convert.ToInt32(Line.Quantity) + k);
                                            Line.SerialNumbers.Quantity = 0;
                                            Line.SerialNumbers.SystemSerialNumber = 0;
                                            Line.SerialNumbers.InternalSerialNumber = String.Empty;
                                        }
                                        
                                    }*/

                                }

                                if  (Line.TreeType == BoItemTreeTypes.iIngredient)
                                {

                                    mDatoTemp =
                                    "SELECT IfNULL(\"OITM\".\"U_TIPOMONTO\", 'B') AS \"TipoMonto\", " + "\n" +
                                    "IfNULL(\"OITM\".\"U_DIVISION\", NULL) AS \"Division\", " + "\n" +
                                    "IfNULL(\"OITM\".\"U_NUMERODECIMALES\", 0) AS \"CantDec\", " + "\n" +
                                    "\"OITM\".\"ManSerNum\" AS \"ManejaSerial\" " + "\n" +
                                    "FROM \"OITM\" " + "\n" +
                                    "WHERE \"OITM\".\"ItemCode\" = '" + Line.ItemCode + "' " + "\n" +
                                    "";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (!SAPmRs.EoF)
                                    {
                                        mDatoTemp = SAPmRs.Fields.Item("TipoMonto").Value;
                                        CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;
                                        mDatoTemp = SAPmRs.Fields.Item("Division").Value;
                                        Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                        Line.CostingCode2 = mDatoTemp;
                                        Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                    }
                                    else
                                    {
                                        CamposOrg.Item("U_TIPOMONTO").Value = "B";
                                        Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                        Line.CostingCode2 = String.Empty;
                                        Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                    }
                                }

                                if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase) && AplicaSeriales)
                                {

                                    Double CantAsignarRestante = 0;

                                    mEtapa = "Verificando / Asignando Seriales. Producto " + Line.ItemCode;
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    Int64 MTS_ID = 0;

                                    if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                        ListaControlSeriales.Add(Line.ItemCode,
                                        new Object[] { Line.ItemCode, MTS_ID });
                                    else
                                        MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                    RsSeriales.Filter = "TransID = '" + TransID + "' " +
                                    "AND Cod_Principal = '" + Line.ItemCode + "' " +
                                    "AND ID > " + MTS_ID.ToString();

                                    if (!RsSeriales.EOF)
                                    {

                                        SerialNumbers LnSr = Line.SerialNumbers;

                                        Int32 SerIndex = -1;
                                        Int32 mUltNum = 0;
                                        String UsedSN = String.Empty;

                                        CantAsignarRestante = Line.Quantity; //(Line.Quantity * Convert.ToDouble(CurrentComboItemData[2]));

                                        while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                        {

                                            SerIndex++;

                                            LnSr.SetCurrentLine(SerIndex);

                                            MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                            ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                            UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                            //LnSr.ManufacturerSerialNumber = UsedSN;
                                            LnSr.InternalSerialNumber = UsedSN;

                                            mDatoTemp = String.Empty; //"SELECT * FROM (" + "\n";

                                            mDatoTemp = mDatoTemp +
                                            "SELECT * FROM (SELECT TOP 5 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 1 AS \"TmpPrioridad\" " + "\n" +
                                            "FROM \"OSRN\" " + "\n" +
                                            "LEFT JOIN \"OSRI\" " + "\n" +
                                            "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                            "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                            "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                            "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                            "--AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                            "ORDER BY CASE WHEN IfNULL(\"OSRI\".\"WhsCode\", '') = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') THEN 1 ELSE 0 END DESC, \"OSRN\".\"SysNumber\" DESC" + "\n" +
                                            ")" + "\n";

                                            mDatoTemp = mDatoTemp + "UNION ALL" + "\n";

                                            mDatoTemp = mDatoTemp +
                                            "SELECT * FROM (SELECT TOP 3 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 2 AS \"TmpPrioridad\" " + "\n" +
                                            "FROM \"OSRN\" " + "\n" +
                                            "LEFT JOIN \"OSRI\" " + "\n" +
                                            "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                            "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                            "AND (IfNULL(\"OSRN\".\"MnfSerial\", '') = '' AND IfNULL(\"OSRN\".\"DistNumber\", '') = '') " + "\n" +
                                            "AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                            "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                            "AND UPPER(IfNULL(\"OSRI\".\"WhsCode\", '')) = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') " + "\n" +
                                            "ORDER BY \"OSRN\".\"SysNumber\" ASC" + "\n" +
                                            ")" + "\n";

                                            //mDatoTemp = mDatoTemp + ") TB ORDER BY \"TmpPrioridad\", CASE WHEN \"TmpPrioridad\" = 1 THEN \"SysNumber\" ELSE 0 END DESC, CASE WHEN \"TmpPrioridad\" = 2 THEN \"SysNumber\" ELSE 0 END ASC" + "\n";

                                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                            SAPmRs.DoQuery(mDatoTemp);

                                            if (!SAPmRs.EoF)
                                            {

                                                /*
                                                mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                                if (!mDatoTemp.isUndefined())
                                                {
                                                    LnSr.ManufacturerSerialNumber = mDatoTemp;
                                                }*/
                                                mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                                mDatoTemp2 = SAPmRs.Fields.Item("MnfSerial").Value;

                                                //LnSr.SystemSerialNumber = SAPmRs.Fields.Item("SysSerial").Value;
                                                //LnSr.InternalSerialNumber = SAPmRs.Fields.Item("IntrSerial").Value;

                                                //LnSr.ExpiryDate = SAPmRs.Fields.Item("ExpDate").Value;
                                                //LnSr.ReceptionDate = SAPmRs.Fields.Item("InDate").Value;
                                                //LnSr.ManufactureDate = SAPmRs.Fields.Item("MnfDate").Value;
                                                //LnSr.WarrantyStart = SAPmRs.Fields.Item("GrntStart").Value;
                                                //LnSr.WarrantyEnd = SAPmRs.Fields.Item("GrntExp").Value;

                                                //mUltNum = LnSr.SystemSerialNumber;

                                                if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(0))
                                                && Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase)
                                                && mDatoTemp.isUndefined() && mDatoTemp2.isUndefined()) // Hay un slot disponible pero el serial no estaba pre creado.
                                                {

                                                    SAPbobsCOM.CompanyService SNCompanyService;
                                                    SNCompanyService = SAP.GetCompanyService();

                                                    SAPbobsCOM.SerialNumberDetailsService SNDetailService;
                                                    SNDetailService = SNCompanyService.GetBusinessService(ServiceTypes.SerialNumberDetailsService);

                                                    SAPbobsCOM.SerialNumberDetailParams SNDP;
                                                    SNDP = SNDetailService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                                                    SNDP.DocEntry = SAPmRs.Fields.Item("AbsEntry").Value; //'Put here the actual key of the serialnumber record

                                                    SAPbobsCOM.SerialNumberDetail SNDetails;
                                                    SNDetails = SNDetailService.Get(SNDP);

                                                    //SNDetails.MfrSerialNo = UsedSN; //"STELLAR11";
                                                    SNDetails.SerialNumber = UsedSN;

                                                    try
                                                    {
                                                        SNDetailService.Update(SNDetails);
                                                    }
                                                    catch (Exception SNDS1)
                                                    {

                                                        // No se pudo actualizar el Serial

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                        UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                        UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }

                                                    //LnSr.ManufacturerSerialNumber = UsedSN;
                                                    LnSr.InternalSerialNumber = UsedSN;
                                                    LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                    //Line.WarehouseCode = "1001"; // SOLO TEST.... Ni modo. Tendran que trasladar los seriales antes
                                                    // de poderlos vender.

                                                }
                                                else
                                                {

                                                    if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(1)) ||
                                                    !Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase))
                                                    {

                                                        // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " +
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "El serial [" + UsedSN + "] " +
                                                        "no existe ni hay un slot disponible en el Almacen " +
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }

                                                    LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                } // El serial ya existía asi que simplemente lo utilizamos.

                                            }
                                            else
                                            {

                                                // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " +
                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "El serial [" + UsedSN + "] " +
                                                "no existe ni hay un slot disponible en el Almacen " +
                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                tsFinTrans = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                RsVentas.MoveNext();

                                                goto Continue;

                                            }

                                            //LnSr.BaseLineNumber = Line.LineNum;
                                            //LnSr.InternalSerialNumber = Convert.ToString(SerIndex + 1);

                                            //LnSr.ExpiryDate = DateTime.Today.AddMonths(3);
                                            //LnSr.ReceptionDate = DateTime.Today;
                                            //LnSr.ManufactureDate = DateTime.Today;
                                            //LnSr.WarrantyStart = DateTime.Today;
                                            //LnSr.WarrantyEnd = DateTime.Today.AddMonths(3);

                                            LnSr.Quantity = 1;

                                            CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                            RsSeriales.MoveNext();

                                            if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                                LnSr.Add();

                                        }

                                    }

                                } else if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) && AplicaSeriales)
                                {

                                    Double CantAsignarRestante = 0;

                                    SerialNumbers LnSr = Line.SerialNumbers;

                                    Int32 SerIndex = -1;
                                    String UsedSN = String.Empty;

                                    Int64 MTS_ID = 0;

                                    if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                        ListaControlSeriales.Add(Line.ItemCode,
                                        new Object[] { Line.ItemCode, MTS_ID });
                                    else
                                        MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                    RsSeriales.Filter = "TransID = '" + TransID + "' " +
                                    "AND Cod_Principal = '" + Line.ItemCode + "' " +
                                    "AND ID > " + MTS_ID.ToString();

                                    CantAsignarRestante = Line.Quantity;

                                    while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                    {

                                        SerIndex++;

                                        LnSr.SetCurrentLine(SerIndex);

                                        MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                        ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                        UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                        //LnSr.ManufacturerSerialNumber = UsedSN;
                                        //LnSr.InternalSerialNumber = UsedSN;

                                        mDatoTemp =
                                        "SELECT \"OSRN\".* FROM \"OSRN\" " + "\n" +
                                        "LEFT JOIN \"OSRI\" " + "\n" +
                                        "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                        "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER(IfNULL(\"OSRI\".\"IntrSerial\", '')) OR \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\") " + "\n" +
                                        "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                        "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                        "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                        "AND IfNULL(\"OSRI\".\"Status\", 0) = 1 " + "\n" +
                                        "ORDER BY IfNULL(\"OSRN\".\"SysNumber\", 0) DESC";

                                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                        SAPmRs.DoQuery(mDatoTemp);

                                        if (!SAPmRs.EoF)
                                        {

                                            // Si existe (Debería existir a juro y estar en estatus no disponible) simplemente lo usamos.
                                            /*mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                            if (!mDatoTemp.isUndefined())
                                            {
                                                LnSr.ManufacturerSerialNumber = mDatoTemp;
                                            }*/
                                            mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                            if (!mDatoTemp.isUndefined())
                                                LnSr.InternalSerialNumber = mDatoTemp;
                                            else
                                            {
                                                LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);
                                            }

                                        }
                                        else
                                        {

                                            // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Error de integridad. El serial a devolver [" + UsedSN + "] no se encontró en el sistema, Producto [" + Line.ItemCode + "].");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "El serial a devolver [" + UsedSN + "] " +
                                            "no se encontró en el sistema, Producto [" + Line.ItemCode + "]." +
                                            "Error ingresando Transaccion POS [" + TransID + "] en SAP.",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        LnSr.Quantity = 1;

                                        CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                        RsSeriales.MoveNext();

                                        if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                            LnSr.Add();

                                    }

                                }

                            }

                            // Ahora que hemos colocado los seriales faltantes de los combos, procedemos a convertir el 
                            // Draft en Doc.

                            TmpResult = Trans.Update();

                            if (TmpResult != 0)
                            {

                                Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                                {
                                    String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                    if (Step1.Length > 1)
                                    {
                                        String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                        if (Step2.Length > 1)
                                        {
                                            try
                                            {
                                                //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                                Int32 mLinea = Convert.ToInt32(Step2[0]);
                                                //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                                //IDocument_Lines x;
                                                //Trans.Lines.SetCurrentLine(mLinea);
                                                String mKey = (TransID + "|" + mLinea.ToString());

                                                //Program.Logger.EscribirLog("mKey:" + mKey);

                                                String[] mLineData = LineRelation[mKey];

                                                //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                                RsItems.Filter = "ID = " + "(" + mLineData[0] + ")";

                                                if (!RsItems.EOF)
                                                {
                                                    String mDescripcion = mCn.Execute("SELECT c_Descri FROM VAD20.DBO.MA_PRODUCTOS " +
                                                    "WHERE c_Codigo = '" + RsItems.Fields["Cod_Principal"].Value.ToString() + "'",
                                                    out RecordsAffected).Fields["c_Descri"].Value.ToString();
                                                    //Program.Logger.EscribirLog("Datos de Linea Cargada");
                                                    mErrorDesc += ". Datos de la línea (Cod|Desc|Cant|Prc): " + "(" +
                                                    RsItems.Fields["Cod_Principal"].Value.ToString() + "|" + mDescripcion + "|" +
                                                    RsItems.Fields["Cantidad"].Value.ToString() + "|" + RsItems.Fields["Precio"].Value.ToString() + ")";
                                                }

                                            }
                                            catch (Exception ItemEx)
                                            {
                                                Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
                                            }
                                        }
                                    }

                                }

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error Actualizando preliminar Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, mErrorNumber.ToString(), "Documents.Update()", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();

                                goto Continue;

                            }

                            TmpResult = Trans.SaveDraftToDocument();

                            if (TmpResult != 0)
                            {

                                Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                                {
                                    String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                    if (Step1.Length > 1)
                                    {
                                        String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                        if (Step2.Length > 1)
                                        {
                                            try
                                            {
                                                //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                                Int32 mLinea = Convert.ToInt32(Step2[0]);
                                                //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                                //IDocument_Lines x;
                                                //Trans.Lines.SetCurrentLine(mLinea);
                                                String mKey = (TransID + "|" + mLinea.ToString());

                                                //Program.Logger.EscribirLog("mKey:" + mKey);

                                                String[] mLineData = LineRelation[mKey];

                                                //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                                RsItems.Filter = "ID = " + "(" + mLineData[0] + ")";

                                                if (!RsItems.EOF)
                                                {
                                                    String mDescripcion = mCn.Execute("SELECT c_Descri FROM VAD20.DBO.MA_PRODUCTOS " +
                                                    "WHERE c_Codigo = '" + RsItems.Fields["Cod_Principal"].Value.ToString() + "'",
                                                    out RecordsAffected).Fields["c_Descri"].Value.ToString();
                                                    //Program.Logger.EscribirLog("Datos de Linea Cargada");
                                                    mErrorDesc += ". Datos de la línea (Cod|Desc|Cant|Prc): " + "(" +
                                                    RsItems.Fields["Cod_Principal"].Value.ToString() + "|" + mDescripcion + "|" +
                                                    RsItems.Fields["Cantidad"].Value.ToString() + "|" + RsItems.Fields["Precio"].Value.ToString() + ")";
                                                }

                                            }
                                            catch (Exception ItemEx)
                                            {
                                                Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
                                            }
                                        }
                                    }

                                }

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error Completando preliminar Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, mErrorNumber.ToString(), "Documents.SaveDraftToDocument()", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();
                                goto Continue;

                            }

                            SAP.GetNewObjectCode(out LastDocEntry);
                            DocEntry = Convert.ToInt32(LastDocEntry);

                            tsInicioProc = DateTime.Now;

                            if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);
                            else
                                Trans = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);

                            FoundTrans = Trans.GetByKey(DocEntry);

                        }

                        mEtapa = "Documento grabado exitosamente. " + StellarDocID;
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        Int32 TransJournalRowId = 0;

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase)) 
                        { 

                            mDatoTemp =
                            "SELECT TOP 1 \"JDT1\".\"Line_ID\" FROM \"OINV\" " + "\n" +
                            "INNER JOIN \"JDT1\" " + "\n" +
                            "ON \"OINV\".\"TransId\" = \"JDT1\".\"TransId\" " + "\n" +
                            "AND UPPER(\"OINV\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\") " + "\n" +
                            "WHERE \"OINV\".\"DocEntry\" = " + Trans.DocEntry + " " + "\n" +
                            "AND UPPER(\"OINV\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\")";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {
                                TransJournalRowId = SAPmRs.Fields.Item("Line_ID").Value;
                            }

                        } 
                        else if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                        {

                            mDatoTemp =
                            "SELECT TOP 1 \"JDT1\".\"Line_ID\" FROM \"ORIN\" " + "\n" +
                            "INNER JOIN \"JDT1\" " + "\n" +
                            "ON \"ORIN\".\"TransId\" = \"JDT1\".\"TransId\" " + "\n" +
                            "AND UPPER(\"ORIN\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\") " + "\n" +
                            "WHERE \"ORIN\".\"DocEntry\" = " + Trans.DocEntry + " " + "\n" +
                            "AND UPPER(\"ORIN\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\")";

                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                            SAPmRs.DoQuery(mDatoTemp);

                            if (!SAPmRs.EoF)
                            {
                                TransJournalRowId = SAPmRs.Fields.Item("Line_ID").Value;
                            }

                        }

                        SAPmRs = null;

                        if (GenerarDocEntrega && Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {

                            // Crear Documento de Entrega.

                            mEtapa = "Validando si es necesario generar documento de entrega. Documento " + StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            SAPbobsCOM.Documents Delivery = null;

                            /*if (TmpDraftDeliveryDoc)
                            {
                                Delivery = SAP.GetBusinessObject(BoObjectTypes.oDrafts);
                                Delivery.DocObjectCode = BoObjectTypes.oDeliveryNotes;
                            }
                            else*/
                                Delivery = SAP.GetBusinessObject(BoObjectTypes.oDeliveryNotes);

                            Int32 Serie = 0; Int32 SerieNextNum = 0;

                            if (!Properties.Settings.Default.SAP_SerieDocumentoEntrega.isUndefined())
                                if (Properties.Settings.Default.SAP_SerieDocumentoEntrega.Equals("*"))
                                {

                                    // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                    mEtapa = "Buscando la Serie correspondiente a Documento de Entrega";
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    mDatoTemp =
                                    "SELECT * FROM \"NNM1\" " + "\n" +
                                    "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                    "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                    "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oDeliveryNotes).ToString() + " " + "\n";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (SAPmRs.EoF)
                                    {

                                        // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "Error de integridad. No esta definida en SAP la Serie de Documentos Entrega para esta localidad.");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error ingresando Nota de Entrega para documento [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "No esta definida en SAP la Serie de Documentos Entrega para esta localidad.",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                        Trans = null; //Trans.Cancel();
                                        Delivery = null;

                                        if (SAP.InTransaction)
                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                        tsFinTrans = DateTime.Now;

                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                        RsVentas.MoveNext();

                                        goto Continue;

                                    }
                                    else
                                    {
                                        Serie = SAPmRs.Fields.Item("Series").Value;
                                        SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                        Delivery.Series = Serie;
                                    }

                                }
                                else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoEntrega, out Serie))
                                    Delivery.Series = Serie;

                            Delivery.CardCode = Trans.CardCode;
                            Delivery.DocDate = Trans.DocDate;
                            Delivery.DocDueDate = Trans.DocDueDate;
                            Delivery.Address = Trans.Address;
                            Delivery.Comments = Trans.Comments;
                            Delivery.Rounding = Trans.Rounding;
                            Delivery.Printed = PrintStatusEnum.psNo;
                            //Delivery.RelatedType = (Int32) Trans.DocType;
                            //Delivery.RelatedEntry = Trans.DocEntry;
                            Delivery.Reference2 = Trans.DocNum.ToString();

                            CamposOrg = Delivery.UserFields.Fields;

                            CamposOrg.Item("U_FACNIT").Value = Trans.UserFields.Fields.Item("U_FACNIT").Value;
                            CamposOrg.Item("U_FACNOM").Value = Trans.UserFields.Fields.Item("U_FACNOM").Value;
                            CamposOrg.Item("U_FACFECHA").Value = Trans.UserFields.Fields.Item("U_FACFECHA").Value;
                            CamposOrg.Item("U_TNEGOCIO").Value = Trans.UserFields.Fields.Item("U_TNEGOCIO").Value;
                            CamposOrg.Item("U_CGASTOS").Value = Trans.UserFields.Fields.Item("U_CGASTOS").Value;
                            CamposOrg.Item("U_FV6").Value = Trans.UserFields.Fields.Item("U_FV6").Value;

                            Document_Lines SrcLine = Trans.Lines;
                            Document_Lines Line = Delivery.Lines;

                            CamposOrg = Line.UserFields.Fields;

                            Int32 PendLnIndex = -1;

                            LineRelation = new Dictionary<String, String[]>();

                            Int32 LineComboStart = -1;
                            String CurrentComboCode = String.Empty;
                            Double CurrentComboLineQty = 0;
                            Double CurrentComboDeliveryQty = 0;

                            Line = Delivery.Lines;

                            for (Int32 i = 0; i < Trans.Lines.Count; i++)
                            {

                                SrcLine.SetCurrentLine(i);

                                Object[] SrcLineData;
                                Double mCantEntregar;

                                Boolean EsIngredienteDeCombo = false;
                                
                                StellarItemCode = SrcLine.ItemCode;

                                if (SrcLine.TreeType == BoItemTreeTypes.iSalesTree && SerialComboParentItems.ContainsKey(SrcLine.ItemCode))
                                {

                                    LineComboStart = i;
                                    CurrentComboCode = SrcLine.ItemCode;

                                }
                                else if (SrcLine.TreeType == BoItemTreeTypes.iIngredient)
                                {
                                    EsIngredienteDeCombo = true;
                                }
                                else
                                {

                                    LineComboStart = -1;
                                    CurrentComboCode = String.Empty;
                                    CurrentComboLineQty = 0;
                                    CurrentComboDeliveryQty = 0;
                                        
                                }

                                if (EsIngredienteDeCombo)
                                {

                                    /*
                                    // Ignorar los hijos de combos que la factura de SAP incluyo posteriormente, 
                                    // pero que la factura de Stellar no tiene.
                                    continue;
                                    */

                                    SrcLineData = ListaPendXEntrega[CurrentComboCode];

                                    Double mCantPend = Math.Round(Convert.ToDouble(SrcLineData[1]) - Convert.ToDouble(SrcLineData[3]), 
                                    Convert.ToInt32(SrcLineData[4]), MidpointRounding.AwayFromZero);

                                    mCantEntregar = Math.Round(SrcLine.Quantity * (CurrentComboDeliveryQty / CurrentComboLineQty), 6, MidpointRounding.AwayFromZero);

                                    mDatoTemp =
                                    "SELECT IfNULL(\"OITM\".\"U_TIPOMONTO\", 'B') AS \"TipoMonto\", " + "\n" +
                                    "IfNULL(\"OITM\".\"U_DIVISION\", NULL) AS \"Division\", " + "\n" +
                                    "IfNULL(\"OITM\".\"U_NUMERODECIMALES\", 0) AS \"CantDec\", " + "\n" +
                                    "\"OITM\".\"ManSerNum\" AS \"ManejaSerial\" " + "\n" +
                                    "FROM \"OITM\" " + "\n" +
                                    "WHERE \"OITM\".\"ItemCode\" = '" + StellarItemCode + "' " + "\n" +
                                    "";

                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                    SAPmRs.DoQuery(mDatoTemp);

                                    if (!SAPmRs.EoF)
                                    {
                                        mDatoTemp = SAPmRs.Fields.Item("TipoMonto").Value;
                                        CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;
                                        mDatoTemp = SAPmRs.Fields.Item("Division").Value;
                                        Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                        Line.CostingCode2 = mDatoTemp;
                                        Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                    }
                                    else
                                    {
                                        CamposOrg.Item("U_TIPOMONTO").Value = "B";
                                        Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                                        Line.CostingCode2 = String.Empty;
                                        Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                                    }

                                }
                                else
                                {

                                    SrcLineData = ListaPendXEntrega[SrcLine.ItemCode];

                                    mCantEntregar = Convert.ToDouble(SrcLineData[3]);

                                    if (mCantEntregar > SrcLine.Quantity)
                                        mCantEntregar = SrcLine.Quantity;

                                    ListaPendXEntrega[SrcLine.ItemCode][3] = Math.Round(Convert.ToDouble(SrcLineData[3]) - mCantEntregar,
                                    Convert.ToInt32(SrcLineData[4]), MidpointRounding.AwayFromZero);

                                    if (LineComboStart > -1)
                                    {
                                        CurrentComboLineQty = SrcLine.Quantity;
                                        CurrentComboDeliveryQty = mCantEntregar;
                                    }

                                }

                                if (mCantEntregar > 0)
                                {

                                    PendLnIndex++;

                                    if (PendLnIndex > 0)
                                    {
                                        Line.Add();
                                        Line.SetCurrentLine(PendLnIndex);
                                    }

                                    StellarItemCode = SrcLine.ItemCode;

                                    Line.ItemCode = StellarItemCode;
                                    Line.Quantity = mCantEntregar;
                                    Line.UnitPrice = SrcLine.UnitPrice;
                                    Line.DiscountPercent = SrcLine.DiscountPercent;
                                    Line.TaxCode = SrcLine.TaxCode;

                                    if (Program.ListaProductosCupon.ContainsValue(Line.ItemCode))
                                    {
                                        // QUE NO GENERE UTILIDAD
                                        Line.GrossBase = -10;
                                        Line.GrossBuyPrice = SrcLine.GrossBuyPrice;
                                    }

                                    LineRelation.Add((TransID + "|" + (PendLnIndex + 1).ToString()),
                                    new String[] { Line.ItemCode, Line.Quantity.ToString() });

                                    Line.BaseEntry = DocEntry;
                                    Line.BaseLine = SrcLine.LineNum;
                                    Line.BaseType = (Int32)BoObjectTypes.oInvoices;

                                    Line.CostingCode = SrcLine.CostingCode;
                                    Line.CostingCode2 = SrcLine.CostingCode2;
                                    Line.WarehouseCode = SrcLine.WarehouseCode;
                                    CamposOrg.Item("U_TIPOMONTO").Value = SrcLine.UserFields.Fields.Item("U_TIPOMONTO").Value;

                                    Boolean ManejaSerial = false;

                                    if (EsIngredienteDeCombo)
                                    {
                                        if (SerialComboParentItems.ContainsKey(CurrentComboCode))
                                            if (SerialComboParentItems[CurrentComboCode].ContainsKey(Line.ItemCode))
                                            {
                                                ManejaSerial = true;
                                            }
                                    }
                                    else
                                    {
                                        ManejaSerial = Convert.ToBoolean(SrcLineData[5]);
                                    }
                                    
                                    Double CantAsignarRestante = 0;

                                    if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                                    {

                                        //if (!TmpDraftDeliveryDoc && ManejaSerial)
                                        if (ManejaSerial)
                                        {

                                            mEtapa = "Verificando / Asignando Seriales. Producto " + Line.ItemCode;
                                            Console.WriteLine(mEtapa);
                                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                            Int64 MTS_ID = 0;

                                            if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                                ListaControlSeriales.Add(Line.ItemCode,
                                                new Object[] { Line.ItemCode, MTS_ID });
                                            else
                                                MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                            RsSeriales.Filter = "TransID = '" + TransID + "' " +
                                            "AND Cod_Principal = '" + Line.ItemCode + "' " +
                                            "AND ID > " + MTS_ID.ToString();

                                            if (!RsSeriales.EOF)
                                            {

                                                SerialNumbers LnSr = Line.SerialNumbers;

                                                Int32 SerIndex = -1;
                                                Int32 mUltNum = 0;
                                                String UsedSN = String.Empty;

                                                CantAsignarRestante = Line.Quantity;

                                                while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                                {

                                                    SerIndex++;

                                                    LnSr.SetCurrentLine(SerIndex);

                                                    MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                                    ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                                    UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                                    //LnSr.ManufacturerSerialNumber = UsedSN;
                                                    LnSr.InternalSerialNumber = UsedSN;

                                                    mDatoTemp = String.Empty; //"SELECT * FROM (" + "\n";

                                                    mDatoTemp = mDatoTemp +
                                                    "SELECT * FROM (SELECT TOP 5 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 1 AS \"TmpPrioridad\" " + "\n" +
                                                    "FROM \"OSRN\" " + "\n" +
                                                    "LEFT JOIN \"OSRI\" " + "\n" +
                                                    "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                                    "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                                    "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                                    "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                                    "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                                    "--AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                                    "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                                    "ORDER BY CASE WHEN IfNULL(\"OSRI\".\"WhsCode\", '') = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') THEN 1 ELSE 0 END DESC, \"OSRN\".\"SysNumber\" DESC" + "\n" +
                                                    ")" + "\n";

                                                    mDatoTemp = mDatoTemp + "UNION ALL" + "\n";

                                                    mDatoTemp = mDatoTemp +
                                                    "SELECT * FROM (SELECT TOP 3 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 2 AS \"TmpPrioridad\" " + "\n" +
                                                    "FROM \"OSRN\" " + "\n" +
                                                    "LEFT JOIN \"OSRI\" " + "\n" +
                                                    "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                                    "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                                    "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                                    "AND (IfNULL(\"OSRN\".\"MnfSerial\", '') = '' AND IfNULL(\"OSRN\".\"DistNumber\", '') = '') " + "\n" +
                                                    "AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                                    "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                                    "AND UPPER(IfNULL(\"OSRI\".\"WhsCode\", '')) = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') " + "\n" +
                                                    "ORDER BY \"OSRN\".\"SysNumber\" ASC" + "\n" +
                                                    ")" + "\n";

                                                    //mDatoTemp = mDatoTemp + ") TB ORDER BY \"TmpPrioridad\", CASE WHEN \"TmpPrioridad\" = 1 THEN \"SysNumber\" ELSE 0 END DESC, CASE WHEN \"TmpPrioridad\" = 2 THEN \"SysNumber\" ELSE 0 END ASC" + "\n";

                                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                                    SAPmRs.DoQuery(mDatoTemp);

                                                    if (!SAPmRs.EoF)
                                                    {

                                                        /*
                                                        mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                                        if (!mDatoTemp.isUndefined())
                                                        {
                                                            LnSr.ManufacturerSerialNumber = mDatoTemp;
                                                        }*/
                                                        mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                                        mDatoTemp2 = SAPmRs.Fields.Item("MnfSerial").Value;

                                                        //LnSr.SystemSerialNumber = SAPmRs.Fields.Item("SysSerial").Value;
                                                        //LnSr.InternalSerialNumber = SAPmRs.Fields.Item("IntrSerial").Value;

                                                        //LnSr.ExpiryDate = SAPmRs.Fields.Item("ExpDate").Value;
                                                        //LnSr.ReceptionDate = SAPmRs.Fields.Item("InDate").Value;
                                                        //LnSr.ManufactureDate = SAPmRs.Fields.Item("MnfDate").Value;
                                                        //LnSr.WarrantyStart = SAPmRs.Fields.Item("GrntStart").Value;
                                                        //LnSr.WarrantyEnd = SAPmRs.Fields.Item("GrntExp").Value;

                                                        mUltNum = LnSr.SystemSerialNumber;

                                                        if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(0))
                                                        && Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase)
                                                        && mDatoTemp.isUndefined() && mDatoTemp2.isUndefined()) // Hay un slot disponible pero el serial no estaba pre creado.
                                                        {

                                                            SAPbobsCOM.CompanyService SNCompanyService;
                                                            SNCompanyService = SAP.GetCompanyService();

                                                            SAPbobsCOM.SerialNumberDetailsService SNDetailService;
                                                            SNDetailService = SNCompanyService.GetBusinessService(ServiceTypes.SerialNumberDetailsService);

                                                            SAPbobsCOM.SerialNumberDetailParams SNDP;
                                                            SNDP = SNDetailService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                                                            SNDP.DocEntry = SAPmRs.Fields.Item("AbsEntry").Value; //'Put here the actual key of the serialnumber record

                                                            SAPbobsCOM.SerialNumberDetail SNDetails;
                                                            SNDetails = SNDetailService.Get(SNDP);

                                                            //SNDetails.MfrSerialNo = UsedSN; //"STELLAR11";
                                                            SNDetails.SerialNumber = UsedSN;

                                                            try
                                                            {
                                                                SNDetailService.Update(SNDetails);
                                                            }
                                                            catch (Exception SNDS2)
                                                            {

                                                                // No se pudo actualizar el Serial

                                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                                "Error [" + SNDS2.Message + "] al completar slot para el serial [" +
                                                                UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.");

                                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                                "Error [" + SNDS2.Message + "] al completar slot para el serial [" +
                                                                UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.",
                                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                                TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                                Trans = null; //Trans.Cancel();
                                                                Delivery = null;

                                                                if (SAP.InTransaction)
                                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                                tsFinTrans = DateTime.Now;

                                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                                RsVentas.MoveNext();

                                                                goto Continue;

                                                            }

                                                            //LnSr.ManufacturerSerialNumber = UsedSN;
                                                            LnSr.InternalSerialNumber = UsedSN;
                                                            LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                        }
                                                        else
                                                        {

                                                            if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(1)) ||
                                                            !Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase))
                                                            {

                                                                // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                                "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " + 
                                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                                "El serial [" + UsedSN + "] " +
                                                                "no existe ni hay un slot disponible en el Almacen " + 
                                                                Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                                TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                                Trans = null; //Trans.Cancel();
                                                                if (SAP.InTransaction)
                                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                                tsFinTrans = DateTime.Now;

                                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                                RsVentas.MoveNext();

                                                                goto Continue;

                                                            }

                                                            LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                        } // El serial ya existía asi que simplemente lo utilizamos.

                                                    }
                                                    else
                                                    {

                                                        // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " + 
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "El serial [" + UsedSN + "] " +
                                                        "no existe ni hay un slot disponible en el Almacen " + 
                                                        Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }

                                                    //LnSr.BaseLineNumber = Line.LineNum;
                                                    //LnSr.InternalSerialNumber = Convert.ToString(SerIndex + 1);

                                                    //LnSr.ExpiryDate = DateTime.Today.AddMonths(3);
                                                    //LnSr.ReceptionDate = DateTime.Today;
                                                    //LnSr.ManufactureDate = DateTime.Today;
                                                    //LnSr.WarrantyStart = DateTime.Today;
                                                    //LnSr.WarrantyEnd = DateTime.Today.AddMonths(3);

                                                    LnSr.Quantity = 1;

                                                    CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                                    RsSeriales.MoveNext();

                                                    if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                                        LnSr.Add();

                                                }

                                            }

                                        }

                                    }

                                }

                            } // End For Lines.

                            // Grabar el Delivery.

                            mEtapa = "Procediendo a grabar (registrar) la entrega parcial de " + StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            tsInicioProc = DateTime.Now;

                            TmpResult = Delivery.Add();

                            tsFinProc = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Delivery.Add(): "));

                            if (TmpResult != 0)
                            {

                                Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                                {
                                    String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                    if (Step1.Length > 1)
                                    {
                                        String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                        if (Step2.Length > 1)
                                        {
                                            try
                                            {
                                                //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                                Int32 mLinea = Convert.ToInt32(Step2[0]);
                                                //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                                //IDocument_Lines x;
                                                //Trans.Lines.SetCurrentLine(mLinea);
                                                String mKey = (TransID + "|" + mLinea.ToString());

                                                //Program.Logger.EscribirLog("mKey:" + mKey);

                                                String[] mLineData = LineRelation[mKey];

                                                //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                                mErrorDesc += ". Datos de la línea (Cod|Cant): " + "(" +
                                                mLineData[0] + "|" + mLineData[1] + ")";

                                            }
                                            catch (Exception ItemEx)
                                            {
                                                Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
                                            }
                                        }
                                    }

                                }

                                Program.Logger.EscribirLog("Error ingresando Nota de Entrega POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Nota de Entrega para documento [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, mErrorNumber.ToString(), "Documents.Add()", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                Delivery = null;

                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();
                                goto Continue;

                            }

                            if (false) //(TmpDraftDeliveryDoc) // Asi no era. Esto ya no va pal baile. Ya se le buscó otra solución. 
                            {

                                Line = Delivery.Lines;

                                LineComboStart = -1;
                                CurrentComboCode = String.Empty;
                                Double CurrentComboQty = 0;

                                for (Int32 i = 0; i < Line.Count; i++)
                                {

                                    Boolean AplicaSeriales = false;
                                    Object[] CurrentComboItemData = null;

                                    Line.SetCurrentLine(i);
                                    StellarItemCode = Line.ItemCode;

                                    if (Line.TreeType == BoItemTreeTypes.iSalesTree && SerialComboParentItems.ContainsKey(Line.ItemCode))
                                    {

                                        LineComboStart = i;
                                        CurrentComboCode = Line.ItemCode;
                                        CurrentComboQty = Line.Quantity;

                                    }
                                    else if (Line.TreeType == BoItemTreeTypes.iIngredient)
                                    {
                                        if (SerialComboParentItems.ContainsKey(CurrentComboCode))
                                            if (SerialComboParentItems[CurrentComboCode].ContainsKey(Line.ItemCode))
                                            {
                                                // Es un ingrediente del combo que maneja seriales. Aplicar lógica de seriales
                                                AplicaSeriales = true;
                                                CurrentComboItemData = SerialComboParentItems[CurrentComboCode][Line.ItemCode];
                                            }
                                    }
                                    else
                                    {

                                        LineComboStart = -1;
                                        CurrentComboCode = String.Empty;
                                        CurrentComboQty = 0;

                                        AplicaSeriales = (ListaControlSeriales.ContainsKey(Line.ItemCode));

                                        /*if (Line.SerialNumbers.Count > Line.Quantity)
                                        {
                                            for (Int32 k = 0; k <= Line.Quantity - 1; k++)
                                            {
                                                Line.SerialNumbers.SetCurrentLine(Convert.ToInt32(Line.Quantity) + k);
                                                Line.SerialNumbers.Quantity = 0;
                                                Line.SerialNumbers.SystemSerialNumber = 0;
                                                Line.SerialNumbers.InternalSerialNumber = String.Empty;
                                            }

                                        }*/

                                    }

                                    if (AplicaSeriales)
                                    {

                                        Double CantAsignarRestante = 0;

                                        mEtapa = "Verificando / Asignando Seriales. Producto " + Line.ItemCode;
                                        Console.WriteLine(mEtapa);
                                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                        Int64 MTS_ID = 0;

                                        if (!ListaControlSeriales.ContainsKey(Line.ItemCode))
                                            ListaControlSeriales.Add(Line.ItemCode,
                                            new Object[] { Line.ItemCode, MTS_ID });
                                        else
                                            MTS_ID = Convert.ToInt64(ListaControlSeriales[Line.ItemCode][1]);

                                        RsSeriales.Filter = "TransID = '" + TransID + "' " +
                                        "AND Cod_Principal = '" + Line.ItemCode + "' " +
                                        "AND ID > " + MTS_ID.ToString();

                                        if (!RsSeriales.EOF)
                                        {

                                            SerialNumbers LnSr = Line.SerialNumbers;

                                            Int32 SerIndex = -1;
                                            Int32 mUltNum = 0;
                                            String UsedSN = String.Empty;

                                            CantAsignarRestante = Line.Quantity; //(Line.Quantity * Convert.ToDouble(CurrentComboItemData[2]));

                                            while (!RsSeriales.EOF && CantAsignarRestante > 0)
                                            {

                                                SerIndex++;

                                                LnSr.SetCurrentLine(SerIndex);

                                                MTS_ID = Convert.ToInt64(RsSeriales.Fields["ID"].Value);

                                                ListaControlSeriales[Line.ItemCode][1] = MTS_ID;

                                                UsedSN = RsSeriales.Fields["c_Serial"].Value.ToString();

                                                //LnSr.ManufacturerSerialNumber = UsedSN;
                                                LnSr.InternalSerialNumber = UsedSN;

                                                mDatoTemp = String.Empty; //"SELECT * FROM (" + "\n";

                                                mDatoTemp = mDatoTemp +
                                                "SELECT * FROM (SELECT TOP 5 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 1 AS \"TmpPrioridad\" " + "\n" +
                                                "FROM \"OSRN\" " + "\n" +
                                                "LEFT JOIN \"OSRI\" " + "\n" +
                                                "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                                "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                                "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                                "AND (UPPER(IfNULL(\"OSRN\".\"DistNumber\", '')) = UPPER('" + UsedSN + "') " + "\n" +
                                                "OR UPPER(IfNULL(\"OSRN\".\"MnfSerial\", '')) = UPPER('" + UsedSN + "')) " + "\n" +
                                                "--AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                                "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                                "ORDER BY CASE WHEN IfNULL(\"OSRI\".\"WhsCode\", '') = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') THEN 1 ELSE 0 END DESC, \"OSRN\".\"SysNumber\" DESC" + "\n" +
                                                ")" + "\n";

                                                mDatoTemp = mDatoTemp + "UNION ALL" + "\n";

                                                mDatoTemp = mDatoTemp +
                                                "SELECT * FROM (SELECT TOP 3 \"OSRN\".*, \"OSRI\".\"Status\" AS \"AvailabilityStatus\", IfNULL(\"OSRI\".\"WhsCode\", '') AS \"WhsCode\", 2 AS \"TmpPrioridad\" " + "\n" +
                                                "FROM \"OSRN\" " + "\n" +
                                                "LEFT JOIN \"OSRI\" " + "\n" +
                                                "ON UPPER(\"OSRN\".\"ItemCode\") = UPPER(\"OSRI\".\"ItemCode\") " + "\n" +
                                                "AND \"OSRN\".\"SysNumber\" = \"OSRI\".\"SysSerial\" " + "\n" +
                                                "WHERE UPPER(\"OSRN\".\"ItemCode\") = UPPER('" + Line.ItemCode + "') " + "\n" +
                                                "AND (IfNULL(\"OSRN\".\"MnfSerial\", '') = '' AND IfNULL(\"OSRN\".\"DistNumber\", '') = '') " + "\n" +
                                                "AND IfNULL(\"OSRI\".\"Status\", 0) = 0 " + "\n" +
                                                "AND \"OSRN\".\"SysNumber\" > " + mUltNum.ToString() + " " + "\n" +
                                                "AND UPPER(IfNULL(\"OSRI\".\"WhsCode\", '')) = UPPER('" + Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + "') " + "\n" +
                                                "ORDER BY \"OSRN\".\"SysNumber\" ASC" + "\n" +
                                                ")" + "\n";

                                                //mDatoTemp = mDatoTemp + ") TB ORDER BY \"TmpPrioridad\", CASE WHEN \"TmpPrioridad\" = 1 THEN \"SysNumber\" ELSE 0 END DESC, CASE WHEN \"TmpPrioridad\" = 2 THEN \"SysNumber\" ELSE 0 END ASC" + "\n";

                                                SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                                SAPmRs.DoQuery(mDatoTemp);

                                                if (!SAPmRs.EoF)
                                                {

                                                    /*
                                                    mDatoTemp = SAPmRs.Fields.Item("MnfSerial").Value;
                                                    if (!mDatoTemp.isUndefined())
                                                    {
                                                        LnSr.ManufacturerSerialNumber = mDatoTemp;
                                                    }*/
                                                    mDatoTemp = SAPmRs.Fields.Item("DistNumber").Value;
                                                    mDatoTemp2 = SAPmRs.Fields.Item("MnfSerial").Value;

                                                    //LnSr.SystemSerialNumber = SAPmRs.Fields.Item("SysSerial").Value;
                                                    //LnSr.InternalSerialNumber = SAPmRs.Fields.Item("IntrSerial").Value;

                                                    //LnSr.ExpiryDate = SAPmRs.Fields.Item("ExpDate").Value;
                                                    //LnSr.ReceptionDate = SAPmRs.Fields.Item("InDate").Value;
                                                    //LnSr.ManufactureDate = SAPmRs.Fields.Item("MnfDate").Value;
                                                    //LnSr.WarrantyStart = SAPmRs.Fields.Item("GrntStart").Value;
                                                    //LnSr.WarrantyEnd = SAPmRs.Fields.Item("GrntExp").Value;

                                                    //mUltNum = LnSr.SystemSerialNumber;

                                                    if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(0))
                                                    && Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase)
                                                    && mDatoTemp.isUndefined() && mDatoTemp2.isUndefined()) // Hay un slot disponible pero el serial no estaba pre creado.
                                                    {

                                                        SAPbobsCOM.CompanyService SNCompanyService;
                                                        SNCompanyService = SAP.GetCompanyService();

                                                        SAPbobsCOM.SerialNumberDetailsService SNDetailService;
                                                        SNDetailService = SNCompanyService.GetBusinessService(ServiceTypes.SerialNumberDetailsService);

                                                        SAPbobsCOM.SerialNumberDetailParams SNDP;
                                                        SNDP = SNDetailService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                                                        SNDP.DocEntry = SAPmRs.Fields.Item("AbsEntry").Value; //'Put here the actual key of the serialnumber record

                                                        SAPbobsCOM.SerialNumberDetail SNDetails;
                                                        SNDetails = SNDetailService.Get(SNDP);

                                                        //SNDetails.MfrSerialNo = UsedSN; //"STELLAR11";
                                                        SNDetails.SerialNumber = UsedSN;

                                                        try
                                                        {
                                                            SNDetailService.Update(SNDetails);
                                                        }
                                                        catch (Exception SNDS1)
                                                        {

                                                            // No se pudo actualizar el Serial

                                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                            "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                            UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.");

                                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                            "Error [" + SNDS1.Message + "] al completar slot para el serial [" +
                                                            UsedSN + "] para el producto [" + StellarItemCode + "], posiblemente esté repetido o usado en SAP.",
                                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                            TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                            Trans = null; //Trans.Cancel();
                                                            if (SAP.InTransaction)
                                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                            tsFinTrans = DateTime.Now;

                                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                            RsVentas.MoveNext();

                                                            goto Continue;

                                                        }

                                                        //LnSr.ManufacturerSerialNumber = UsedSN;
                                                        LnSr.InternalSerialNumber = UsedSN;
                                                        LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                        //Line.WarehouseCode = "1001"; // SOLO TEST.... Ni modo. Tendran que trasladar los seriales antes
                                                        // de poderlos vender.

                                                    }
                                                    else
                                                    {

                                                        if ((Convert.ToInt32(SAPmRs.Fields.Item("AvailabilityStatus").Value) == Convert.ToInt32(1)) ||
                                                        !Convert.ToString(SAPmRs.Fields.Item("WhsCode").Value).Equals(Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad, StringComparison.OrdinalIgnoreCase))
                                                        {

                                                            // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                            "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " +
                                                            Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                            "El serial [" + UsedSN + "] " +
                                                            "no existe ni hay un slot disponible en el Almacen " +
                                                            Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                            TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                            Trans = null; //Trans.Cancel();
                                                            if (SAP.InTransaction)
                                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                            tsFinTrans = DateTime.Now;

                                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                            RsVentas.MoveNext();

                                                            goto Continue;

                                                        }

                                                        LnSr.SystemSerialNumber = Convert.ToInt32(SAPmRs.Fields.Item("SysNumber").Value);

                                                    } // El serial ya existía asi que simplemente lo utilizamos.

                                                }
                                                else
                                                {

                                                    // No existe el serial ni tampoco hay un slot para autocompletarlo. Error de Integridad.

                                                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "Error de integridad. El serial [" + UsedSN + "] no existe ni hay un slot disponible en el Almacen " +
                                                    Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]");

                                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                    "El serial [" + UsedSN + "] " +
                                                    "no existe ni hay un slot disponible en el Almacen " +
                                                    Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad + ", Producto [" + Line.ItemCode + "]." +
                                                    "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP.",
                                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                    TransID, "Null-Serial", "Validacion Stellar", Program.mCnLocal);

                                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                    Trans = null; //Trans.Cancel();
                                                    if (SAP.InTransaction)
                                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                    tsFinTrans = DateTime.Now;

                                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                    RsVentas.MoveNext();

                                                    goto Continue;

                                                }

                                                //LnSr.BaseLineNumber = Line.LineNum;
                                                //LnSr.InternalSerialNumber = Convert.ToString(SerIndex + 1);

                                                //LnSr.ExpiryDate = DateTime.Today.AddMonths(3);
                                                //LnSr.ReceptionDate = DateTime.Today;
                                                //LnSr.ManufactureDate = DateTime.Today;
                                                //LnSr.WarrantyStart = DateTime.Today;
                                                //LnSr.WarrantyEnd = DateTime.Today.AddMonths(3);

                                                LnSr.Quantity = 1;

                                                CantAsignarRestante = Math.Round(CantAsignarRestante - 1, 0, MidpointRounding.AwayFromZero);

                                                RsSeriales.MoveNext();

                                                if (!RsSeriales.EOF && CantAsignarRestante > 0)
                                                    LnSr.Add();

                                            }

                                        }

                                    }

                                }

                                // Ahora que hemos colocado los seriales faltantes de los combos, procedemos a convertir el 
                                // Draft en Doc.

                                TmpResult = Delivery.Update();

                                if (TmpResult != 0)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                    if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                                    {
                                        String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                        if (Step1.Length > 1)
                                        {
                                            String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                            if (Step2.Length > 1)
                                            {
                                                try
                                                {
                                                    //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                                    Int32 mLinea = Convert.ToInt32(Step2[0]);
                                                    //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                                    //IDocument_Lines x;
                                                    //Trans.Lines.SetCurrentLine(mLinea);
                                                    String mKey = (TransID + "|" + mLinea.ToString());

                                                    //Program.Logger.EscribirLog("mKey:" + mKey);

                                                    String[] mLineData = LineRelation[mKey];

                                                    //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                                    mErrorDesc += ". Datos de la línea (Cod|Cant): " + "(" +
                                                    mLineData[0] + "|" + mLineData[1] + ")";

                                                }
                                                catch (Exception ItemEx)
                                                {
                                                    Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
                                                }
                                            }
                                        }

                                    }

                                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error Actualizando entrega preliminar Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "Documents.Update()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();

                                    goto Continue;

                                }

                                TmpResult = Trans.SaveDraftToDocument();

                                if (TmpResult != 0)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                    if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                                    {
                                        String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                        if (Step1.Length > 1)
                                        {
                                            String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                            if (Step2.Length > 1)
                                            {
                                                try
                                                {
                                                    //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                                    Int32 mLinea = Convert.ToInt32(Step2[0]);
                                                    //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                                    //IDocument_Lines x;
                                                    //Trans.Lines.SetCurrentLine(mLinea);
                                                    String mKey = (TransID + "|" + mLinea.ToString());

                                                    //Program.Logger.EscribirLog("mKey:" + mKey);

                                                    String[] mLineData = LineRelation[mKey];

                                                    //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                                    mErrorDesc += ". Datos de la línea (Cod|Cant): " + "(" +
                                                    mLineData[0] + "|" + mLineData[1] + ")";

                                                }
                                                catch (Exception ItemEx)
                                                {
                                                    Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
                                                }
                                            }
                                        }

                                    }

                                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error Completando entrega preliminar Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "Documents.SaveDraftToDocument()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();
                                    goto Continue;

                                }

                                //SAP.GetNewObjectCode(out LastDocEntry);
                                //DocEntry = Convert.ToInt32(LastDocEntry);

                                //tsInicioProc = DateTime.Now;

                                //FoundTrans = Delivery.GetByKey(DocEntry);

                            }

                            // Documento Registrado.

                            //SAP.GetNewObjectCode(out mDatoTemp);

                        }

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {

                            // Registrar pagos.

                            mEtapa = "Iniciando etapa de aplicación de pagos. Documento " + StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            // Buscar Pagos del Documento.

                            RsDetPagos.Filter = "TransID = '" + TransID + "'";

                            if (RsDetPagos.EOF)
                            {

                                // No hay pagos. Error de Integridad.

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "Error de integridad. El documento no posee pagos registrados.");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                "El documento no posee pagos registrados.",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, "Null-DetPag", "Validacion Stellar", Program.mCnLocal);

                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                tsFinTrans = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                RsVentas.MoveNext();
                                goto Continue;

                            }

                            // Registrar los pagos de la transacción.

                            if (Properties.Settings.Default.SAP_FormaPagoFactura_SinDetallePago)
                            {

                                mEtapa = "Registrando un solo pago general para el documento " + StellarDocID;
                                Console.WriteLine(mEtapa);
                                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                                
                                Double mMonto = Convert.ToDouble(RsVentas.Fields["n_Total"].Value);

                                SAPbobsCOM.Payments DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                                //dynamic DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                DetPag.DocType = BoRcptTypes.rCustomer;
                                DetPag.CardCode = Trans.CardCode;
                                DetPag.DocDate = Trans.DocDate;
                                //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                                //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                                //DetPag.VatDate = Trans.VatDate; // ????
                                DetPag.Remarks = Trans.Comments;
                                //DetPag.JournalRemarks = String.Empty; // No es necesario.
                                
                                Int32 Serie = 0; Int32 SerieNextNum = 0;

                                if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                    if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                                    {

                                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                        mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                                        Console.WriteLine(mEtapa);
                                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                        mDatoTemp =
                                        "SELECT * FROM \"NNM1\" " + "\n" +
                                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                        "AND \"ObjectCode\" = " + ((Int32) BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                        SAPmRs.DoQuery(mDatoTemp);

                                        if (SAPmRs.EoF)
                                        {

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }
                                        else
                                        {
                                            Serie = SAPmRs.Fields.Item("Series").Value;
                                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                            DetPag.Series = Serie;
                                        }

                                    }
                                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                        DetPag.Series = Serie;

                                DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                DetPag.Invoices.DocEntry = DocEntry;
                                DetPag.Invoices.SumApplied = Math.Round(mTotalDocSAP, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                DetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                DetPag.CashSum = Math.Round(mTotalDocSAP, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                tsInicioProc = DateTime.Now;

                                TmpResult = DetPag.Add();

                                tsFinProc = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                                if (TmpResult != 0)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                    Program.Logger.EscribirLog("Error ingresando pago en modo resumido en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error ingresando pago en modo resumido en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "[Summarized]Payments.Add()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    DetPag = null;
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();
                                    goto Continue;

                                }

                            }
                            else
                            {

                                mEtapa = "Iniciando Registro de pagos para venta " + StellarDocID;
                                Console.WriteLine(mEtapa);
                                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                Boolean PasoPrimerPagoEnEfectivo = false;
                                Int32 DetPagRowCount = RsDetPagos.RecordCount;
                                Int32 CurrentDetPagRow = 0;
                                Int32 CurrentCardPaymentRow = 0;
                                Int32 CurrentCheckPaymentRow = 0;

                                Double mVueltoRestante = Convert.ToDouble(RsVentas.Fields["n_Vuelto"].Value);

                                if (Convert.ToDouble(RsVentas.Fields["n_Cancelado"].Value) <= mTotalDocStellar) mVueltoRestante = 0;

                                Double mVueltoLn = 0;
                                Double mPagoLn = 0;
                                //Double mRestanteDonacion = RsVentas.Fields["n_MontoDonacion"].Value;

                                Double mTotalPagado = 0;
                                Double mTotalDifRestante = mDiferenciaTotales;
                                Double DiferenciaPorNC_DEV = 0;

                                SAPbobsCOM.Payments UniquePayment = null;
                                //dynamic UniquePayment = null;

                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                {

                                    // Montos agrupados por forma de pago.

                                    mEtapa = "Estableciendo parámetros generales para un registro único de pagos. Documento " + StellarDocID;
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    //UniquePayment = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                                    UniquePayment = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                    UniquePayment.DocType = BoRcptTypes.rCustomer;
                                    UniquePayment.CardCode = Trans.CardCode;
                                    UniquePayment.DocDate = Trans.DocDate;
                                    //UniquePayment.DueDate = Trans.DocDueDate; // Para Cheques?
                                    //UniquePayment.TaxDate = Trans.TaxDate; // Default System Date
                                    //UniquePayment.VatDate = Trans.VatDate; // ????
                                    UniquePayment.Remarks = Trans.Comments;
                                    //UniquePayment.JournalRemarks = String.Empty; // No es necesario.

                                    Int32 Serie = 0; Int32 SerieNextNum = 0;

                                    if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                        if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                                        {

                                            // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                            mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                                            Console.WriteLine(mEtapa);
                                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                            mDatoTemp =
                                            "SELECT * FROM \"NNM1\" " + "\n" +
                                            "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                            "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                            "AND \"ObjectCode\" = " + ((Int32) BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                            SAPmRs.DoQuery(mDatoTemp);

                                            if (SAPmRs.EoF)
                                            {

                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                tsFinTrans = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                RsVentas.MoveNext();

                                                goto Continue;

                                            }
                                            else
                                            {
                                                Serie = SAPmRs.Fields.Item("Series").Value;
                                                SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                                UniquePayment.Series = Serie;
                                            }

                                        }
                                        else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                            UniquePayment.Series = Serie;

                                    UniquePayment.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                    UniquePayment.Invoices.DocEntry = DocEntry;

                                }

                                while (!RsDetPagos.EOF)
                                {

                                    mEtapa = "Registrando detalles de pago. ID Detalle Pago " + RsDetPagos.Fields["ID"].Value;
                                    Console.WriteLine(mEtapa);
                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                    CurrentDetPagRow++;

                                    String mMoneda = RsDetPagos.Fields["c_CodMoneda"].Value.ToString();
                                    String mFormaPago = RsDetPagos.Fields["c_CodDenominacion"].Value.ToString();
                                    String LlaveFormaPago = mMoneda + ";" + mFormaPago;
                                    String mRefPago = RsDetPagos.Fields["c_Numero"].Value.ToString();
                                    String DescFormaPago = "N/A";

                                    SAPbobsCOM.Documents DocNC = null; Int32 NCDocEntry = -1; Int32 NCJournalRowID = 0;
                                    Boolean HizoReconciliacion = false; Boolean CerrandoNotaCredito = false;

                                    if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada) 
                                    && ((!Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago.isUndefined() 
                                    && Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago.Equals(mFormaPago)) 
                                    || (!Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago_CambioTaxID.isUndefined()
                                    && Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago_CambioTaxID.Equals(mFormaPago)))
                                    && !Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                    {

                                        CerrandoNotaCredito = true;

                                        String mCampoTmp = Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CampoCuponNC;

                                        mDatoTemp =
                                        "SELECT \"DocEntry\" FROM \"ORIN\" " +
                                        "WHERE \"" + mCampoTmp +  "\" = '" + mRefPago + "' " +
                                        "AND \"DocStatus\" = 'O' " + "\n" +
                                        "ORDER BY \"DocEntry\" DESC";

                                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                        SAPmRs.DoQuery(mDatoTemp);

                                        if (!SAPmRs.EoF)
                                        {
                                            NCDocEntry = Convert.ToInt32(SAPmRs.Fields.Item("DocEntry").Value);
                                        } 
                                        else
                                        {

                                            Boolean mRechazoTmp = Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_RechazarFacturaEnCasoDeReferenciaErronea;

                                            mRechazoTmp = (mRechazoTmp
                                            || (Properties.Settings.Default.Stellar_LongitudMinimaEnSerialCuponNC > 0
                                            && mRefPago.Length >= Properties.Settings.Default.Stellar_LongitudMinimaEnSerialCuponNC));

                                            if (mRechazoTmp)
                                            {

                                                //goto N_C_NotFound;

                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error en forma de pago. No se encontró el enlace a una nota de crédito abierta con el Serial [" + mRefPago + "].");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error en forma de pago. No se encontró el enlace a una nota de crédito abierta con el Serial [" + mRefPago + "].",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, "N_C_NotFound", "Validacion Stellar", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                tsFinTrans = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                RsVentas.MoveNext();

                                                goto Continue;

                                            } else
                                            {
                                                // Hacer Fallback y grabar como pago.
                                                CerrandoNotaCredito = false;
                                            }

                                        }

                                        if (CerrandoNotaCredito )
                                        {

                                            DocNC = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);

                                            if (!DocNC.GetByKey(NCDocEntry))
                                            {

                                                //N_C_NotFound:

                                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error en forma de pago. No se encontró el enlace a una nota de crédito abierta con el Serial [" + mRefPago + "].");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                "Error en forma de pago. No se encontró el enlace a una nota de crédito abierta con el Serial [" + mRefPago + "].",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, "N_C_NotFound", "Validacion Stellar", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                tsFinTrans = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                RsVentas.MoveNext();

                                                goto Continue;

                                            }

                                            mDatoTemp =
                                            "SELECT TOP 1 \"JDT1\".\"Line_ID\" FROM \"ORIN\" " + "\n" +
                                            "INNER JOIN \"JDT1\" " + "\n" +
                                            "ON \"ORIN\".\"TransId\" = \"JDT1\".\"TransId\" " + "\n" +
                                            "AND UPPER(\"ORIN\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\") " + "\n" +
                                            "WHERE \"ORIN\".\"DocEntry\" = " + NCDocEntry + " " + "\n" +
                                            "AND UPPER(\"ORIN\".\"CardCode\") = UPPER(\"JDT1\".\"ShortName\") ";

                                            SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                            SAPmRs.DoQuery(mDatoTemp);

                                            if (!SAPmRs.EoF)
                                            {
                                                NCJournalRowID = SAPmRs.Fields.Item("Line_ID").Value;
                                            }

                                            SAPmRs = null;

                                        }

                                    }

                                    Double mDifDocVsPago = 0; String mComentarioDifFormaPago = String.Empty;

                                    if (Properties.Settings.Default.SAP_AjustarDiferenciaDocumentosEnFormaDePago)
                                    {
                                        if (CurrentDetPagRow == DetPagRowCount)
                                        {
                                            // Si la diferencia maxima es 0, acompletamos cualquier diferencia. Si la diferencia maxima es mayor a 0 (positiva o negativa) 
                                            // en todo caso solo acompletamos si la diferencia esta dentro del rango.
                                            if ((Properties.Settings.Default.SAP_DiferenciaMaximaDocumentosEnFormaDePago > 0 
                                            && Math.Abs(mDiferenciaTotales) <= Properties.Settings.Default.SAP_DiferenciaMaximaDocumentosEnFormaDePago) 
                                            || Properties.Settings.Default.SAP_DiferenciaMaximaDocumentosEnFormaDePago <= 0)
                                            { 
                                                mDifDocVsPago = mTotalDifRestante;
                                            }
                                        }
                                    }

                                    Double mMontoLn;

                                    if (CerrandoNotaCredito)
                                    {
                                        mMontoLn = Math.Round(Convert.ToDouble(RsDetPagos.Fields["n_Monto"].Value),
                                        Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                        mPagoLn = mMontoLn;
                                    }
                                    else
                                    {
                                        mMontoLn = Math.Round(Convert.ToDouble(RsDetPagos.Fields["n_Monto"].Value) +
                                        mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                    }

                                    Double mFactor = Convert.ToDouble(RsDetPagos.Fields["n_Factor"].Value);
                                    Double mCantidad = Math.Round(mMontoLn / mFactor, 12, MidpointRounding.AwayFromZero);  //RsDetPagos.Fields["n_Cantidad"].Value;

                                    String MonedaPagoSAP = null;
                                    String TmpTenderCurrency = null;

                                    if (Program.ListaAsociacionMonedas != null)
                                        if (Program.ListaAsociacionMonedas.ContainsKey(mMoneda))
                                        {
                                            Program.ListaAsociacionMonedas.TryGetValue(mMoneda, out TmpTenderCurrency);
                                        }

                                    if (TmpTenderCurrency == null) TmpTenderCurrency = String.Empty;

                                    if (mVueltoRestante > 0 && !CerrandoNotaCredito)
                                    {

                                        if ((TmpTenderCurrency.isUndefined() || TmpTenderCurrency.Equals(Properties.Settings.Default.SAP_CodigoMonedaPredeterminada,
                                        StringComparison.OrdinalIgnoreCase)) && 
                                        mFormaPago.Equals("Efectivo", StringComparison.OrdinalIgnoreCase))
                                        {
                                            
                                            // Se determinó que el POS le resta el vuelto al Efectivo de la
                                            // moneda predeterminada. En estos casos entonces, no restamos.
                                            // Esto da pie a muchas inconsistencias pero bueno, asi esta hecho.
                                            // Hay que resolver de esta manera.

                                            mVueltoLn = 0;
                                            mPagoLn = Math.Round(mMontoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                            // Es MAS!, en casos extraños pudiera quedar el Row con monto negativo...
                                            // Lo que significa que hay un vuelto que habría que restarle a otra línea.
                                            // entonces corregimos de la siguiente manera:

                                            if (mPagoLn < 0)
                                            {   // Modificamos el vuelto original por el vuelto Restante (el monto negativo)
                                                mVueltoRestante = Math.Abs(mPagoLn);
                                                mPagoLn = 0;
                                                PasoPrimerPagoEnEfectivo = true;
                                            }
                                            else
                                            {

                                                // Si el monto quedo positivo entonces no quedo mas vuelto por restar a ninguna otra forma de pago.
                                                if (!PasoPrimerPagoEnEfectivo)
                                                {
                                                    // Al primer pago en efectivo no se le resta vuelto, ya esta restado.
                                                    mPagoLn = Math.Round(mMontoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    PasoPrimerPagoEnEfectivo = true;
                                                    mVueltoRestante = 0;
                                                }
                                                else
                                                {

                                                    // A un segundo o posterior pago en efectivo si le restamos dado que el vuelto lo asumió el primer pago...

                                                    mVueltoLn = (Math.Round(mMontoLn - mVueltoRestante, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) >= 0 ?
                                                    Math.Round(mVueltoRestante, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) : Math.Round(mMontoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero));

                                                    // En este caso podriamos tener que ir restando varias veces.

                                                    mPagoLn = Math.Round(mMontoLn - mVueltoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    mVueltoRestante = Math.Round(mVueltoRestante - mVueltoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                }
                                                
                                            }

                                        }
                                        else
                                        {   

                                            // Si es cualquier otra cosa pero hay vuelto, restarsela
                                            // ya que el POS no se la resto. Caso Efectivo en Otras Monedas u otros tipos de denominaciones que pudieran permitir vuelto...
                                            mVueltoLn = (Math.Round(mMontoLn - mVueltoRestante, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) >= 0 ?
                                            Math.Round(mVueltoRestante, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) : Math.Round(mMontoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero));

                                            // En este caso podriamos tener que ir restando varias veces.

                                            mPagoLn = Math.Round(mMontoLn - mVueltoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                            mVueltoRestante = Math.Round(mVueltoRestante - mVueltoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                        }

                                    }
                                    else
                                    {
                                        mVueltoLn = 0;
                                        mPagoLn = Math.Round(mMontoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                    }

                                    /*if (mRestanteDonacion > 0)
                                    {
                                        if (mRestanteDonacion > mPagoLn)
                                        {
                                            mRestanteDonacion = Math.Round(mRestanteDonacion - mPagoLn, 8);
                                            mPagoLn = 0;
                                        }
                                        else
                                        {
                                            mPagoLn = Math.Round(mPagoLn - mRestanteDonacion, 8);
                                            mRestanteDonacion = 0;
                                        }
                                    }*/

                                    if (mPagoLn > 0) // Monto final del pago - Vuelto - Donaciones.
                                    {

                                        SAPbobsCOM.Payments DetPag = null;

                                        if (!Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                        {

                                            /*DescFormaPago = Functions.isDBNull( Functions.getParameterizedCommand(
                                            "SELECT c_Denominacion FROM VAD10.DBO.MA_DENOMINACIONES " +
                                            "WHERE c_CodMoneda = @CodMoneda " +
                                            "AND c_CodDenomina = @CodDen ", mCn, new SqlParameter[]
                                            {
                                                new SqlParameter("@CodMoneda", mMoneda),
                                                new SqlParameter("@CodDen", mFormaPago)
                                            }).ExecuteScalar(), "N/A").ToString();*/

                                            TmpRs = mCn.Execute(
                                            "SELECT c_Denominacion FROM VAD10.DBO.MA_DENOMINACIONES " +
                                            "WHERE c_CodMoneda = '" + mMoneda + "' " +
                                            "AND c_CodDenomina = '" + mFormaPago + "' ", out RecordsAffected);
                                            
                                            if (!TmpRs.EOF)
                                                DescFormaPago = Functions.isDBNull(TmpRs.Fields["c_Denominacion"].Value, "N/A").ToString();

                                            DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                                            
                                            DetPag.DocType = BoRcptTypes.rCustomer;
                                            DetPag.CardCode = Trans.CardCode;
                                            DetPag.DocDate = Trans.DocDate;
                                            DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                                            //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                                            //DetPag.VatDate = Trans.VatDate; // ????
                                            //DetPag.Remarks = Trans.Comments;
                                            DetPag.Remarks = Trans.Comments + "\n" +
                                            "Forma de Pago: " + DescFormaPago + " [" + mFormaPago + "]" + "\n" +
                                            (mRefPago.Trim().isUndefined() ? String.Empty : "REF " + mRefPago + "\n") +
                                            "Monto: " + mMontoLn.ToString("#,##0.00") + "   " +
                                            ((!TmpTenderCurrency.isUndefined() &&
                                            !TmpTenderCurrency.Equals(Properties.Settings.Default.SAP_CodigoMonedaPredeterminada, StringComparison.OrdinalIgnoreCase)) ?
                                            "Tasa: " + mFactor.ToString("#,##0.00") + "   Cant.: " + mCantidad.ToString() : String.Empty) + 
                                            (mDifDocVsPago != 0 && !CerrandoNotaCredito ? "\n\nPago con ajuste de (" + Math.Round(mDifDocVsPago, 
                                            Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero).ToString() + 
                                            ") por diferencia entre totales de documento." : String.Empty);
                                            //DetPag.JournalRemarks = String.Empty; // No es necesario.

                                            Int32 Serie = 0; Int32 SerieNextNum = 0;

                                            if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                                if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                                                {

                                                    // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                                    mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                                                    Console.WriteLine(mEtapa);
                                                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                                    mDatoTemp =
                                                    "SELECT * FROM \"NNM1\" " + "\n" +
                                                    "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                                    "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                                    "AND \"ObjectCode\" = " + ((Int32) BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                                                    SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                                    SAPmRs.DoQuery(mDatoTemp);

                                                    if (SAPmRs.EoF)
                                                    {

                                                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                        "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                        "No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.",
                                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                        TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                        Trans = null; //Trans.Cancel();
                                                        if (SAP.InTransaction)
                                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                        tsFinTrans = DateTime.Now;

                                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                        RsVentas.MoveNext();

                                                        goto Continue;

                                                    }
                                                    else
                                                    {
                                                        Serie = SAPmRs.Fields.Item("Series").Value;
                                                        SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                                        DetPag.Series = Serie;
                                                    }

                                                }
                                                else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                                    DetPag.Series = Serie;

                                            DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                            DetPag.Invoices.DocEntry = DocEntry;
                                            //DetPag.Invoices.SumApplied = TotalDoc;

                                        }

                                        // Ahora Categorizar el Tipo de Pago.

                                        String mCodBanco = RsDetPagos.Fields["c_CodBanco"].Value.ToString();
                                        String mDesBanco = RsDetPagos.Fields["c_Banco"].Value.ToString();
                                        String mRef = RsDetPagos.Fields["c_Numero"].Value.ToString();

                                        MonedaPagoSAP = TmpTenderCurrency;

                                        String FormaPagoSAP = String.Empty;
                                        String FPCat2 = String.Empty;
                                        String[] FormaPagoSAPMultiValores;

                                        if (Program.ListaAsociacionFormaPago != null)
                                            if (Program.ListaAsociacionFormaPago.ContainsKey(LlaveFormaPago))
                                            {
                                                Program.ListaAsociacionFormaPago.TryGetValue(LlaveFormaPago, out FormaPagoSAP);
                                            }

                                        FormaPagoSAPMultiValores = FormaPagoSAP.Split(new Char[] { ':' }, 2, StringSplitOptions.None);
                                        if (FormaPagoSAPMultiValores.Length > 0) FormaPagoSAP = FormaPagoSAPMultiValores[0];
                                        if (FormaPagoSAPMultiValores.Length > 1) FPCat2 = FormaPagoSAPMultiValores[1];

                                        String BancoSAP = String.Empty;

                                        if (Program.ListaAsociacionBancos != null)
                                            if (Program.ListaAsociacionBancos.ContainsKey(mCodBanco))
                                            {
                                                Program.ListaAsociacionBancos.TryGetValue(mCodBanco, out BancoSAP);
                                            }

                                        if (BancoSAP == null) BancoSAP = String.Empty;

                                        Int64 BancoSAPNum = 1;

                                        if (!BancoSAP.isUndefined())
                                            if (Int64.TryParse(BancoSAP, out BancoSAPNum))

                                        if ((!Properties.Settings.Default.SAP_FormasPagoFallbackEfectivo &&
                                        FormaPagoSAP.isUndefined()) || 
                                        (!Properties.Settings.Default.SAP_FormasPagoFallbackMoneda &&
                                        MonedaPagoSAP.isUndefined()))
                                        {

                                            // Forma de pago no categorizada. Arreglar Setup

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Error de integridad. El documento posee una forma de pago que no esta " + 
                                            "categorizada para su envio a SAP => [" + mMoneda + "][" + mFormaPago + "]");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "El documento posee una forma de pago que no esta " +
                                            "categorizada para su envio a SAP => [" + mMoneda + "][" + mFormaPago + "]",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Invalid_DetPag", "Validacion Stellar", Program.mCnLocal);
                                            
                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            DetPag = null;
                                            UniquePayment = null;
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);
                                            
                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        if ((!mFormaPago.Equals("Efectivo", StringComparison.OrdinalIgnoreCase)) &&
                                        (!Properties.Settings.Default.SAP_FormasPagoFallbackBanco) 
                                        && BancoSAP.isUndefined()
                                        && !Properties.Settings.Default.SAP_PagosEfectivoAMultiplesCuentas)
                                        {

                                            // Forma de pago no categorizada. Arreglar Setup

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Error de integridad. El pago posee un codigo de banco que no esta " +
                                            "categorizado para su envio a SAP => [" + mCodBanco + "][" + mDesBanco + "]");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "El pago posee un codigo de banco que no esta " +
                                            "categorizado para su envio a SAP => [" + mCodBanco + "][" + mDesBanco + "]",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Invalid_Bank", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            DetPag = null;
                                            UniquePayment = null;
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        if (Properties.Settings.Default.SAP_Llenar_Campos_UDF_Detalle_Pago)
                                        {
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_DOC_AFECTADO").Value = TransID;
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_COD_MONEDA").Value = mMoneda;
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_DESC_MONEDA").Value = Functions.BuscarValorBD("c_Descripcion", 
                                            "SELECT c_Descripcion FROM VAD10.DBO.MA_MONEDAS WHERE c_CodMoneda = '" + mMoneda + "'", ref mCn, "N/A");
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_COD_FORMA_PAGO").Value = mFormaPago;
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_DESC_FORMA_PAGO").Value = DescFormaPago; /*Functions.BuscarValorBD("c_Denominacion",
                                            "SELECT c_Denominacion FROM VAD10.DBO.MA_MONEDAS WHERE c_CodMoneda = '" + mMoneda + "' " +
                                            "AND c_CodDenomina = '" + mFormaPago + "' ", ref mCn, "N/A");*/
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_REFERENCIA").Value = mRefPago;
                                            DetPag.UserFields.Fields.Item("U_SISTEMA_TASA_CAMBIO").Value = mFactor;
                                        }

                                        String mCodigoCuentaContable = String.Empty;

                                        if (Properties.Settings.Default.SAP_PagosEfectivoAMultiplesCuentas)
                                        {
                                            // En este caso usamos el valor de la tabla de asociación para albergar la distribución de
                                            // cuenta contable por denominación, pero en definitiva todo va a llegar a SAP
                                            // como efectivo, en vez de usar el valor de asociación para identificar el tipo de pago.
                                            
                                            if (!Properties.Settings.Default.SAP_CodFormaPago_NotaCreditoSistemaAnterior.isUndefined() 
                                            && !Properties.Settings.Default.SAP_CuentaContable_NotaCreditoSistemaAnterior.isUndefined() 
                                            && mFormaPago.Equals(Properties.Settings.Default.SAP_CodFormaPago_NotaCreditoSistemaAnterior, StringComparison.OrdinalIgnoreCase))
                                            {
                                                mCodigoCuentaContable = Properties.Settings.Default.SAP_CuentaContable_NotaCreditoSistemaAnterior;
                                            } else
                                            {
                                                if (FormaPagoSAP.isUndefined())
                                                    mCodigoCuentaContable = Properties.Settings.Default.SAP_CashAccount;
                                                else
                                                    mCodigoCuentaContable = FormaPagoSAP;
                                            }

                                            FormaPagoSAP = "EFECTIVO";

                                        }

                                        switch (FormaPagoSAP.ToUpper())
                                        {

                                            case "CHECK": case "CHQ": case "CHEQUE": case "CHK":

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {

                                                    if (CurrentCheckPaymentRow > 1) UniquePayment.Checks.Add();

                                                    UniquePayment.CheckAccount = Properties.Settings.Default.SAP_ChecksAccount;
                                                    UniquePayment.Checks.BankCode = BancoSAP;

                                                    Int32 mCheckNum = 1;

                                                    if (Int32.TryParse(mRef, out mCheckNum))
                                                        UniquePayment.Checks.CheckNumber = mCheckNum;
                                                    else
                                                        UniquePayment.Checks.CheckNumber = 1;

                                                    UniquePayment.Checks.CheckSum += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    UniquePayment.Invoices.SumApplied += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                }
                                                else
                                                {

                                                    if (CurrentCheckPaymentRow > 1) DetPag.Checks.Add();

                                                    DetPag.CheckAccount = Properties.Settings.Default.SAP_ChecksAccount;
                                                    DetPag.Checks.BankCode = BancoSAP;

                                                    Int32 mCheckNum = 1;

                                                    if (Int32.TryParse(mRef, out mCheckNum))
                                                        DetPag.Checks.CheckNumber = mCheckNum;
                                                    else
                                                        DetPag.Checks.CheckNumber = 1;

                                                    DetPag.Checks.CheckSum = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    DetPag.Invoices.SumApplied = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                }

                                                mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                mTotalDifRestante = Math.Round(mTotalDifRestante - mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                break;

                                            //* NO USAR: ESTO ES SOLO PARA OUTGOING PAYMENTS COMO INDICA EL SDK (PAGO A PROVEEDORES).
                                            
                                            case "CREDIT CARD":
                                            case "TDC":
                                            case "TC":
                                            case "TARJETA":
                                            case "TARJETA CREDITO":
                                            case "CREDITCARD":
                                            case "TARJETACREDITO":
                                            case "CC":

                                                CurrentCardPaymentRow++;

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {
                                                    if (CurrentCardPaymentRow > 1) UniquePayment.CreditCards.Add();
                                                    UniquePayment.CreditCards.CreditCard = ((Int32)BancoSAPNum);
                                                    UniquePayment.CreditCards.CreditCardNumber = "12340000123400001234";
                                                    UniquePayment.CreditCards.CardValidUntil = Trans.DocDueDate;
                                                    UniquePayment.CreditCards.CreditSum += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    UniquePayment.CreditCards.VoucherNum = mRef;
                                                    UniquePayment.Invoices.SumApplied += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                }
                                                else
                                                {
                                                    if (CurrentCardPaymentRow > 1) DetPag.CreditCards.Add();
                                                    DetPag.CreditCards.CreditCard = ((Int32)BancoSAPNum);
                                                    DetPag.CreditCards.CreditCardNumber = "12340000123400001234";
                                                    DetPag.CreditCards.CardValidUntil = Trans.DocDueDate;
                                                    DetPag.CreditCards.CreditSum = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    DetPag.CreditCards.VoucherNum = mRef;
                                                    DetPag.Invoices.SumApplied = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                }

                                                mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                mTotalDifRestante = Math.Round(mTotalDifRestante - mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                break;

                                            case "TRANSFER":
                                            case "BANK TRANSFER":
                                            case "BANKTRANSFER":
                                            case "TRANSFERENCIA":
                                            case "TRANSFERENCIA BANCARIA":
                                            case "TRANSFERENCIABANCARIA":
                                            case "TRANS":
                                            case "TRS":
                                            case "WIRE":
                                                
                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {
                                                    UniquePayment.TransferAccount = Properties.Settings.Default.SAP_TransfersAccount;
                                                    UniquePayment.TransferDate = Convert.ToDateTime(RsDetPagos.Fields["d_Fecha"].Value);
                                                    UniquePayment.TransferReference = mRef;
                                                    UniquePayment.TransferSum += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    UniquePayment.Invoices.SumApplied += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                }
                                                else
                                                {
                                                    DetPag.TransferAccount = Properties.Settings.Default.SAP_TransfersAccount;
                                                    DetPag.TransferDate = Convert.ToDateTime(RsDetPagos.Fields["d_Fecha"].Value);
                                                    DetPag.TransferReference = mRef;
                                                    DetPag.TransferSum = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    DetPag.Invoices.SumApplied = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                }

                                                mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                mTotalDifRestante = Math.Round(mTotalDifRestante - mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                break;

                                            case "CASH":
                                            case "EFECTIVO":
                                            case "EFEC":
                                            case "BILL":
                                            case "BILLETE":
                                            case "CONTADO":
                                            case "MONEDA":
                                            case "COIN":
                                            default:

                                                if (!Properties.Settings.Default.SAP_PagosEfectivoAMultiplesCuentas)
                                                    mCodigoCuentaContable = Properties.Settings.Default.SAP_CashAccount;

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {

                                                    UniquePayment.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                                    UniquePayment.CashSum += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    UniquePayment.Invoices.SumApplied += Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                    mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                    mTotalDifRestante = Math.Round(mTotalDifRestante - mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                }
                                                else
                                                {

                                                    if (CerrandoNotaCredito)
                                                    {

                                                        Double mTotalPorPagar = Math.Round(mTotalDocSAP - mTotalPagado,
                                                        Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                        DiferenciaPorNC_DEV = Math.Round(mMontoLn - DocNC.DocTotal,
                                                        Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                        Double OpenNCAmount = Math.Round(DocNC.DocTotal - DocNC.PaidToDate,
                                                        Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                        if (Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada 
                                                        && Properties.Settings.Default.SAP_CruzarNotaCreditoConPago)
                                                        {

                                                            // Intentar primero la reconciliación. Si esta falla, grabar el cruce como un pago.

                                                            tsInicioProc = DateTime.Now;

                                                            try
                                                            {

                                                                InternalReconciliationsService PayLinkService = SAP.GetCompanyService().GetBusinessService(ServiceTypes.InternalReconciliationsService);
                                                                InternalReconciliationParams PayLinkParams = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationParams);
                                                                InternalReconciliationOpenTransParams PayLinkOpenParams = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTransParams);
                                                                InternalReconciliationOpenTrans PayLinkObj = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTrans);
                                                                InternalReconciliationOpenTransRows PayLinkRows = PayLinkObj.InternalReconciliationOpenTransRows;
                                                                InternalReconciliationOpenTransRow PayLinkCurrentRow;

                                                                DateTime TmpReconDate = (DocNC.DocDate > Trans.DocDate ? DocNC.DocDate : Trans.DocDate);

                                                                PayLinkObj.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : TmpReconDate);
                                                                PayLinkObj.CardOrAccount = CardOrAccountEnum.coaCard;

                                                                PayLinkOpenParams.CardOrAccount = CardOrAccountEnum.coaCard;
                                                                PayLinkOpenParams.DateType = ReconSelectDateTypeEnum.rsdtPostDate;
                                                                PayLinkOpenParams.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : TmpReconDate);
                                                                PayLinkOpenParams.InternalReconciliationBPs.Add();
                                                                PayLinkOpenParams.InternalReconciliationBPs.Item(0).BPCode = Trans.CardCode; //Properties.Settings.Default.SAP_ClienteContado;
                                                                if (!Trans.CardCode.Equals(DocNC.CardCode, StringComparison.OrdinalIgnoreCase))
                                                                {
                                                                    PayLinkOpenParams.InternalReconciliationBPs.Add();
                                                                    PayLinkOpenParams.InternalReconciliationBPs.Item(1).BPCode = DocNC.CardCode;
                                                                }

                                                                PayLinkRows.Add();

                                                                PayLinkCurrentRow = PayLinkRows.Item(0);
                                                                PayLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                                                PayLinkCurrentRow.TransId = Trans.TransNum;
                                                                PayLinkCurrentRow.TransRowId = TransJournalRowId; //0;
                                                                PayLinkCurrentRow.ReconcileAmount = DocNC.DocTotal;

                                                                PayLinkRows.Add();

                                                                PayLinkCurrentRow = PayLinkRows.Item(1);
                                                                PayLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                                                PayLinkCurrentRow.TransId = DocNC.TransNum;
                                                                PayLinkCurrentRow.TransRowId = NCJournalRowID; //0;
                                                                PayLinkCurrentRow.ReconcileAmount = DocNC.DocTotal;

                                                                PayLinkParams = PayLinkService.Add(PayLinkObj);
                                                                
                                                                HizoReconciliacion = true;

                                                                if (!DiferenciaPorNC_DEV.Equals(0))
                                                                    mTotalDifRestante = Math.Round(mTotalDifRestante - DiferenciaPorNC_DEV,
                                                                    Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                                mTotalPagado = Math.Round(mTotalPagado + DocNC.DocTotal, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                                DetPag = null;

                                                            }
                                                            catch (Exception AnyPayLinkEx)
                                                            {

                                                                Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                                                mErrorNumber = AnyPayLinkEx.HResult; mErrorDesc = AnyPayLinkEx.Message;

                                                                Program.Logger.EscribirLog("Error reconciliando NC vs Factura Pagada en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                                "NC [" + DocNC.DocNum.ToString() + "][" + Convert.ToString(DocNC.DocTotal.ToString("#,##0.00")) + "]. " +
                                                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                                            }

                                                        }

                                                        if (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago && !HizoReconciliacion)
                                                        {

                                                            mPagoLn = (OpenNCAmount <= mTotalPorPagar ? OpenNCAmount : mTotalPorPagar);

                                                            DetPag.Invoices.SumApplied = mPagoLn; //DocNC.DocTotal;
                                                            DetPag.Invoices.Add();
                                                            DetPag.Invoices.SetCurrentLine(1);
                                                            DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_CredItnote;
                                                            DetPag.Invoices.DocEntry = DocNC.DocEntry;
                                                            DetPag.Invoices.SumApplied = (-1 * mPagoLn); //(-1 * DocNC.DocTotal);

                                                            if (Math.Round(DiferenciaPorNC_DEV, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada,
                                                            MidpointRounding.AwayFromZero) >= 0.01)
                                                            {
                                                                DiferenciaPorNC_DEV = Math.Round(DiferenciaPorNC_DEV - 0.01, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada,
                                                                MidpointRounding.AwayFromZero);
                                                                DetPag.CashSum = Math.Round(0.01 + DiferenciaPorNC_DEV, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada,
                                                                MidpointRounding.AwayFromZero);
                                                                DiferenciaPorNC_DEV = 0;
                                                            } else
                                                            {
                                                                DetPag.CashSum = Math.Round(0.01, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada,
                                                                MidpointRounding.AwayFromZero);
                                                            }
                                                            
                                                            DetPag.CashAccount = mCodigoCuentaContable;

                                                            if (!DiferenciaPorNC_DEV.Equals(0))
                                                                mTotalDifRestante = Math.Round(mTotalDifRestante - DiferenciaPorNC_DEV,
                                                                Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                            mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                        }

                                                    }
                                                    else
                                                    {

                                                        DetPag.CashAccount = mCodigoCuentaContable;
                                                        DetPag.CashSum = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                        DetPag.Invoices.SumApplied = Math.Round(mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                        mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                        mTotalDifRestante = Math.Round(mTotalDifRestante - mDifDocVsPago, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                    }

                                                }

                                                break;

                                        }

                                        if (!HizoReconciliacion && Properties.Settings.Default.SAP_LlenarCategoriaFiscalPagos 
                                        && !Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                        {

                                            switch (FPCat2.ToUpper())
                                            {
                                                case "CH":
                                                case "CT":
                                                case "TR":
                                                case "OR":
                                                    FPCat2 = FPCat2.ToUpper();
                                                    break;
                                                case "EF":
                                                default:
                                                    FPCat2 = "EF";
                                                    break;
                                            }

                                            DetPag.UserFields.Fields.Item("U_FP").Value = FPCat2;

                                        }

                                        if (!Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                        {
                                            
                                            if (CurrentDetPagRow > 1)
                                            {

                                                /*
                                                
                                                Trans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);

                                                FoundTrans = Trans.GetByKey(DocEntry);

                                                Int32 ReOpenPay;

                                                if (Trans.DocumentStatus != BoStatus.bost_Open)
                                                {
                                                    ReOpenPay = Trans.Reopen();
                                                }
                                                
                                                */

                                            }
                                            
                                            if ((Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada && 
                                            !Properties.Settings.Default.SAP_CruzarNotaCreditoConPago) 
                                            && CerrandoNotaCredito && !HizoReconciliacion)
                                            {

                                                DetPag = null;

                                                // Ubicar el monto restante por pagar.

                                                Double mTotalPorPagar = Math.Round(mTotalDocSAP - mTotalPagado,
                                                Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                // Si utilizaron la nota de credito por un monto mayor o menor... trasladaremos la diferencia a otros pagos.
                                                // Pero no dejaremos la nota de crédito abierta al intentar pagar un monto parcial. A toda costa se intenta cerrar.

                                                DiferenciaPorNC_DEV = Math.Round(mMontoLn - DocNC.DocTotal,
                                                Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                // Buscar el monto que le queda disponible, en caso de que por alguna otra vía le hayan consumido parcialmente.
                                                // De otra forma daria error y la factura quedaria atascada si se intentara aplicar el monto total de la nota.

                                                Double OpenNCAmount = Math.Round(DocNC.DocTotal - DocNC.PaidToDate,
                                                Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                // El monto a pagar es el de la factura. Si esta NC se pasa, entonces quedará abierta por diferencia pero ni modo...
                                                // En la mayoria de los casos el monto restante deberia ser mayor o igual al de la NC, es solo como contingencia 
                                                // para no dejar la diferencia del lado de la factura como un "pago extra" o "vuelto".
                                                mPagoLn = (OpenNCAmount <= mTotalPorPagar ? OpenNCAmount : mTotalPorPagar);

                                                try
                                                {

                                                    InternalReconciliationsService PayLinkService = SAP.GetCompanyService().GetBusinessService(ServiceTypes.InternalReconciliationsService);
                                                    InternalReconciliationParams PayLinkParams = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationParams);
                                                    InternalReconciliationOpenTransParams PayLinkOpenParams = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTransParams);
                                                    InternalReconciliationOpenTrans PayLinkObj = PayLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTrans);
                                                    InternalReconciliationOpenTransRows PayLinkRows = PayLinkObj.InternalReconciliationOpenTransRows;
                                                    InternalReconciliationOpenTransRow PayLinkCurrentRow;

                                                    DateTime TmpReconDate = (DocNC.DocDate > Trans.DocDate ? DocNC.DocDate : Trans.DocDate);

                                                    PayLinkObj.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : TmpReconDate);
                                                    PayLinkObj.CardOrAccount = CardOrAccountEnum.coaCard;

                                                    PayLinkOpenParams.CardOrAccount = CardOrAccountEnum.coaCard;
                                                    PayLinkOpenParams.DateType = ReconSelectDateTypeEnum.rsdtPostDate;
                                                    PayLinkOpenParams.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : TmpReconDate);
                                                    PayLinkOpenParams.InternalReconciliationBPs.Add();
                                                    PayLinkOpenParams.InternalReconciliationBPs.Item(0).BPCode = Trans.CardCode; //Properties.Settings.Default.SAP_ClienteContado;
                                                    if (!Trans.CardCode.Equals(DocNC.CardCode, StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        PayLinkOpenParams.InternalReconciliationBPs.Add();
                                                        PayLinkOpenParams.InternalReconciliationBPs.Item(1).BPCode = DocNC.CardCode;
                                                    }

                                                    PayLinkRows.Add();

                                                    PayLinkCurrentRow = PayLinkRows.Item(0);
                                                    PayLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                                    PayLinkCurrentRow.TransId = Trans.TransNum;
                                                    PayLinkCurrentRow.TransRowId = TransJournalRowId; //0;
                                                    PayLinkCurrentRow.ReconcileAmount = mPagoLn; //DocNC.DocTotal;

                                                    PayLinkRows.Add();

                                                    PayLinkCurrentRow = PayLinkRows.Item(1);
                                                    PayLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                                    PayLinkCurrentRow.TransId = DocNC.TransNum;
                                                    PayLinkCurrentRow.TransRowId = NCJournalRowID; //0;
                                                    PayLinkCurrentRow.ReconcileAmount = mPagoLn; //DocNC.DocTotal;

                                                    PayLinkParams = PayLinkService.Add(PayLinkObj);

                                                    if (!DiferenciaPorNC_DEV.Equals(0))
                                                        if (mDiferenciaTotales.Equals(0)) 
                                                            mTotalDifRestante = Math.Round(mTotalDifRestante + DiferenciaPorNC_DEV,
                                                            Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);
                                                        else
                                                            mTotalDifRestante = Math.Round(mTotalDifRestante - DiferenciaPorNC_DEV,
                                                            Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                    mTotalPagado = Math.Round(mTotalPagado + mPagoLn, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                                }
                                                catch (Exception AnyPayLinkEx)
                                                {

                                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                                    mErrorNumber = AnyPayLinkEx.HResult; mErrorDesc = AnyPayLinkEx.Message;

                                                    Program.Logger.EscribirLog("Error reconciliando NC vs Factura Pagada en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "NC [" + DocNC.DocNum.ToString() + "][" + Convert.ToString(DocNC.DocTotal.ToString("#,##0.00")) + "]. " +
                                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                    "Error reconciliando NC vs Factura Pagada en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "NC [" + DocNC.DocNum.ToString() + "][" + Convert.ToString(DocNC.DocTotal.ToString("#,##0.00")) + "]. " +
                                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                    TransID, mErrorNumber.ToString(), "InternalReconciliationService.Add()", Program.mCnLocal);

                                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                    Trans = null; //Trans.Cancel();
                                                    DetPag = null;
                                                    if (SAP.InTransaction)
                                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                    tsFinTrans = DateTime.Now;

                                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                    RsVentas.MoveNext();

                                                    goto Continue;

                                                }

                                                tsFinProc = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                                            }
                                            else if (!CerrandoNotaCredito || (CerrandoNotaCredito && !HizoReconciliacion))
                                            {

                                                tsInicioProc = DateTime.Now;

                                                TmpResult = DetPag.Add();

                                                tsFinProc = DateTime.Now;

                                                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                                                if (TmpResult != 0)
                                                {

                                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                                    Program.Logger.EscribirLog("Error ingresando pago en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                    "Error ingresando pago en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                    TransID, mErrorNumber.ToString(), "Payments.Add()", Program.mCnLocal);

                                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                    Trans = null; //Trans.Cancel();
                                                    DetPag = null;
                                                    if (SAP.InTransaction)
                                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                    tsFinTrans = DateTime.Now;

                                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                    RsVentas.MoveNext();
                                                    goto Continue;

                                                }

                                            }

                                        } // Fin pagos detallados.

                                    }

                                    RsDetPagos.MoveNext();

                                } // End While NOT RsDetPag.EOF

                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                {

                                    tsInicioProc = DateTime.Now;

                                    TmpResult = UniquePayment.Add();

                                    tsFinProc = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                                    if (TmpResult != 0)
                                    {

                                        Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                        SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                        Program.Logger.EscribirLog("Error ingresando pagos de la factura en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error ingresando pago único de factura en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, mErrorNumber.ToString(), "[Unique]Payments.Add()", Program.mCnLocal);

                                        ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                        Trans = null; //Trans.Cancel();
                                        UniquePayment = null;
                                        if (SAP.InTransaction)
                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                        tsFinTrans = DateTime.Now;

                                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                        RsVentas.MoveNext();
                                        goto Continue;

                                    }

                                } // Fin Condicional UniquePayment
                                else
                                {

                                    if (mTotalDifRestante > 0 && (mTotalDocSAP > mTotalPagado))
                                    {

                                        SAPbobsCOM.Payments DetPag = null;

                                        DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                        DetPag.DocType = BoRcptTypes.rCustomer;
                                        DetPag.CardCode = Trans.CardCode;
                                        DetPag.DocDate = Trans.DocDate;
                                        DetPag.DueDate = Trans.DocDueDate;

                                        DetPag.Remarks = Trans.Comments + "\n" +
                                        "Pago por diferencia restante entre totales de documento.";

                                        Int32 Serie = 0; Int32 SerieNextNum = 0;

                                        if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                            if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                                            {

                                                // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                                                mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                                                Console.WriteLine(mEtapa);
                                                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                                                mDatoTemp =
                                                "SELECT * FROM \"NNM1\" " + "\n" +
                                                "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                                                "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                                                "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                                                SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                                                SAPmRs.DoQuery(mDatoTemp);

                                                if (SAPmRs.EoF)
                                                {

                                                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                    "Error ingresando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                                    "No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.",
                                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                    TransID, "Null-Series", "Validacion Stellar", Program.mCnLocal);

                                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                                    Trans = null; //Trans.Cancel();
                                                    if (SAP.InTransaction)
                                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                    tsFinTrans = DateTime.Now;

                                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                                    RsVentas.MoveNext();

                                                    goto Continue;

                                                }
                                                else
                                                {
                                                    Serie = SAPmRs.Fields.Item("Series").Value;
                                                    SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                                                    DetPag.Series = Serie;
                                                }

                                            }
                                            else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                                DetPag.Series = Serie;

                                        DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                        DetPag.Invoices.DocEntry = DocEntry;

                                        DetPag.Invoices.SumApplied = mTotalDifRestante;
                                        DetPag.CashSum = mTotalDifRestante;
                                        if (Properties.Settings.Default.SAP_CuentaContableDiferenciaEnPagos.isUndefined())
                                            DetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                        else
                                            DetPag.CashAccount = Properties.Settings.Default.SAP_CuentaContableDiferenciaEnPagos;

                                        tsInicioProc = DateTime.Now;

                                        TmpResult = DetPag.Add();

                                        tsFinProc = DateTime.Now;

                                        Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                                        if (TmpResult != 0)
                                        {

                                            Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                            SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                            Program.Logger.EscribirLog("Error ingresando pago en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando pago en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, mErrorNumber.ToString(), "Payments.Add()", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            DetPag = null;
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            tsFinTrans = DateTime.Now;

                                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                            RsVentas.MoveNext();
                                            goto Continue;

                                        }

                                    }

                                }

                            } // Si hay pagos

                        } // Si es venta y aplica pagos.

                        if (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase))
                        {

                            if (Properties.Settings.Default.SAP_AjustarDiferenciaPositivaNC_Reconciliacion &&
                            Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) > 0)
                            {

                                SAPbobsCOM.JournalEntries jNC = SAP.GetBusinessObject(BoObjectTypes.oJournalEntries);

                                jNC.Series = Properties.Settings.Default.SAP_AjustarDiferenciaPositivaNC_Reconciliacion_SerieJEntry;

                                jNC.ReferenceDate = DateTime.Now;
                                jNC.DueDate = DateTime.Now;
                                jNC.TaxDate = DateTime.Now;

                                jNC.Lines.ShortName = Properties.Settings.Default.SAP_ClienteContado;
                                jNC.Lines.Debit = mDiferenciaTotales;
                                jNC.Lines.Credit = 0;
                                jNC.Lines.ReferenceDate1 = jNC.ReferenceDate;
                                jNC.Lines.DueDate = jNC.DueDate;
                                jNC.Lines.TaxDate = jNC.TaxDate;

                                jNC.Lines.Add();

                                jNC.Lines.ShortName = Properties.Settings.Default.SAP_AjustarDiferenciaPositivaNC_Reconciliacion_Cuenta; //GLAccountDev;
                                jNC.Lines.Credit = mDiferenciaTotales;
                                jNC.Lines.Debit = 0;
                                jNC.Lines.ReferenceDate1 = jNC.ReferenceDate;
                                jNC.Lines.DueDate = jNC.DueDate;
                                jNC.Lines.TaxDate = jNC.TaxDate;

                                tsInicioProc = DateTime.Now;

                                TmpResult = jNC.Add();

                                if (TmpResult != 0)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                    Program.Logger.EscribirLog("Error ingresando ajuste en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error ingresando ajuste en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "JournalEntries.Add()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    jNC = null;
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();
                                    goto Continue;

                                }

                                // Documento Registrado. Proceder a registrar los pagos.

                                String jDocEntryStr; Int32 jDocEntry; Int32 jTID;

                                SAP.GetNewObjectCode(out jDocEntryStr);
                                jDocEntry = Convert.ToInt32(jDocEntryStr);

                                tsInicioProc = DateTime.Now;

                                Boolean FoundjNC = jNC.GetByKey(DocEntry);

                                jTID = jNC.Number;

                                try
                                {

                                    InternalReconciliationsService jLinkService = SAP.GetCompanyService().GetBusinessService(ServiceTypes.InternalReconciliationsService);
                                    InternalReconciliationParams jLinkParams = jLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationParams);
                                    InternalReconciliationOpenTransParams jLinkOpenParams = jLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTransParams);
                                    InternalReconciliationOpenTrans jLinkObj = jLinkService.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTrans);
                                    InternalReconciliationOpenTransRows jLinkRows = jLinkObj.InternalReconciliationOpenTransRows;
                                    InternalReconciliationOpenTransRow jLinkCurrentRow;

                                    jLinkObj.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : Trans.DocDate);
                                    jLinkObj.CardOrAccount = CardOrAccountEnum.coaCard;

                                    jLinkOpenParams.CardOrAccount = CardOrAccountEnum.coaCard;
                                    jLinkOpenParams.DateType = ReconSelectDateTypeEnum.rsdtPostDate;
                                    jLinkOpenParams.ReconDate = (Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_ModalidadFechaCruce.Equals(0) ? DateTime.Today : Trans.DocDate);
                                    jLinkOpenParams.InternalReconciliationBPs.Add();
                                    jLinkOpenParams.InternalReconciliationBPs.Item(0).BPCode = Properties.Settings.Default.SAP_ClienteContado;

                                    jLinkRows.Add();

                                    jLinkCurrentRow = jLinkRows.Item(0);
                                    jLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                    jLinkCurrentRow.TransId = jDocEntry;
                                    jLinkCurrentRow.TransRowId = 0;
                                    jLinkCurrentRow.ReconcileAmount = Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);

                                    jLinkRows.Add();

                                    jLinkCurrentRow = jLinkRows.Item(1);
                                    jLinkCurrentRow.Selected = BoYesNoEnum.tYES;
                                    jLinkCurrentRow.TransId = Trans.TransNum;
                                    jLinkCurrentRow.TransRowId = 0;
                                    jLinkCurrentRow.ReconcileAmount = Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero);  //DocNC.DocTotal;

                                    jLinkParams = jLinkService.Add(jLinkObj);

                                }
                                catch (Exception AnyJLinkEx)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    mErrorNumber = AnyJLinkEx.HResult; mErrorDesc = AnyJLinkEx.Message;

                                    Program.Logger.EscribirLog("Error reconciliando ajuste en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error reconciliando ajuste en Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "InternalReconciliationService.Add()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    jNC = null;
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    tsFinTrans = DateTime.Now;

                                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                                    RsVentas.MoveNext();

                                    goto Continue;

                                }

                                tsFinProc = DateTime.Now;

                                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo InternalReconciliationsService.Add(): "));

                            }

                        }

                        // Si todo salio bien, grabar.

                        if (SAP.InTransaction)
                        {

                            mEtapa = "Finalizando transacción de SAP. Registrando envío del Documento " + StellarDocID;
                            Console.WriteLine(mEtapa);

                            SAP.EndTransaction(BoWfTransOpt.wf_Commit);

                            mEtapa = "CommitTrans Exitoso. Procediendo a marcar como enviado Documento " + StellarDocID;
                            Console.WriteLine(mEtapa);

                            Program.mCnLocal.Execute(
                            "UPDATE [VAD20].[DBO].[MA_PAGOS] SET cs_Sync_SxS = '" + Program.gCorrelativo + "|" + LastDocEntry + "|" + LastDocNum + "' " +
                            "WHERE (c_Sucursal + c_Concepto + c_Numero) = '" + TransID + "'", out RecordsAffected);

                            InsertarAuditoria(Program.LogIDAuditoria, Program.InfoLog, 
                            "Transaccion POS [" + TransID + "] enviada con exito. " +
                            "SAP Num. Doc. [" + LastDocNum + "] ID Interno [" + LastDocEntry + "] ", "DBSync.ConstruirRegistrosDeVentas()", 
                            "POSTransaccionEnviada", TransID, "0", "EnviarTransaccionPOS", Program.mCnLocal);

                            mEtapa = "Documento procesado exitosamente en su totalidad: " + StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Tiempo total de transacción: "));

                        }
                        else
                        {

                            Program.Logger.EscribirLog("Error en SAP. Transaccion Inactiva / Rollback forzoso. Procesando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "]");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Transaccion BD SAP Inactiva / Rollback forzoso. Procesando Transaccion POS [" + TransID + "][" + mUltDocDateOnly.ToString("yyyy-MM-dd") + "]", 
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS", TransID, String.Empty, "Validacion Stellar", Program.mCnLocal);

                            ControlarErrorRegistroVentas(RegistrosFallidos, TransID, Loc, Concepto, NumDoc);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                        }

                        RsVentas.MoveNext();

                    }

                }

                MarcarFallidos(ref RegistrosFallidos);

                // Proceso Finalizado.

                return;

	        }
	        catch (Exception Any)
	        {

                if (SAP.InTransaction)
                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

		        Program.Logger.EscribirLog(Any, "ConstruirRegistrosDeVentas();" + "\n\n" +
                "mEtapa=" + mEtapa + "\n" +
                " TransaccionPOS " + mUltDoc + "\n" +
                " Fecha " + mUltFec.ToString() + "\n" +
                " Total " + mUltTot.ToString() + "\n" + 
                "\n");

                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error durante el procesamiento de Transacciones POS de ventas y devoluciones." + "\t" + 
                " TransaccionPOS " + mUltDoc + "\t" +
                " Fecha " + mUltFec.ToString() + "\t" +
                " Total " + mUltTot.ToString() + "\t",
                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS", mUltDoc, 
                ("[" + Any.HResult + "][" + Any.Message + "]"), "Procesando_Excepcion", Program.mCnLocal);

                if (Program.gDebugMode) {
                    MessageBox.Show(Any.Message + "\n" +
                    "mEtapa=" + mEtapa + "\n" +
                    "mUltDoc=" + mUltDoc + "\n" +
                    "mUltFec=" + mUltFec.ToString() + "\n" +
                    "mUltTot=" + mUltTot.ToString() + "\n");
                }

                tsFinTrans = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                if (!SAP.Connected)
                {

                    // Intentar recuperar la conexión.

                    Program.mCnSAP = ConexionSAP();
                    SAP = Program.mCnSAP;

                    if (SAP == null)
                        // La conexión está rota.
                        System.Environment.Exit(999);

                    if (!SAP.Connected)
                        // La conexión está rota.
                        System.Environment.Exit(998);

                }

            }

        }

        private static Boolean ObtenerDatosVentas(out ADODB.Recordset pRsVentas, out ADODB.Recordset pRsItems,
        out ADODB.Recordset pRsDetPagos, out ADODB.Recordset pRsImpuestos, out ADODB.Recordset pRsDatosFiscales, 
        out ADODB.Recordset pRsPendXEntregaMA, out ADODB.Recordset pRsPendXEntregaTR, 
        out ADODB.Recordset pRsSeriales, 
        Boolean pReprocesarFallidos)
        {

            Boolean ObtenerDatosVentas = false;

            try 
	        {

                String mSQL;
                
                String ExcluirVNF = String.Empty;

                String mEstatus = String.Empty;
                String OtrosFiltros = String.Empty;
                String TopNDoc = String.Empty;

                //if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal))
                    //ExcluirVNF = " AND bDocNoFiscal = 0";

                if (pReprocesarFallidos)
                    mEstatus = RegistrosPendientesFallidos;
                else
                    mEstatus = RegistrosPendientesPorLote;

                if ((!Properties.Settings.Default.Stellar_ControlSync_FechaIni.isUndefined() 
                && !Properties.Settings.Default.Stellar_ControlSync_FechaFin.isUndefined())
                || !Properties.Settings.Default.Stellar_ControlSync_Transacciones.isUndefined())
                {

                    if (Properties.Settings.Default.Stellar_ControlSync_Estatus.isUndefined()) // PENDIENTES Y FALLIDOS
                        OtrosFiltros = "DOC.cs_Sync_SxS IN ('" + RegistrosNuevos + "', '" + RegistrosPendientesPorLote + "', '" + RegistrosPendientesPorCorrida + "', '" + RegistrosPendientesFallidos + "')";
                    else if (Properties.Settings.Default.Stellar_ControlSync_Estatus.Equals("0")) // SOLO PENDIENTES
                        OtrosFiltros = "DOC.cs_Sync_SxS IN ('" + RegistrosNuevos + "', '" + RegistrosPendientesPorLote + "', '" + RegistrosPendientesPorCorrida + "')";
                    else if (Properties.Settings.Default.Stellar_ControlSync_Estatus.Equals("1")) // SOLO FALLLIDOS
                        OtrosFiltros = "DOC.cs_Sync_SxS IN ('" + RegistrosPendientesFallidos + "')";

                    if (!Properties.Settings.Default.Stellar_ControlSync_FechaIni.isUndefined()
                    && !Properties.Settings.Default.Stellar_ControlSync_FechaFin.isUndefined())
                    {
                        if (Properties.Settings.Default.Stellar_ControlSync_FechaIni.Length == 8
                        && Properties.Settings.Default.Stellar_ControlSync_FechaFin.Length == 8)
                        {
                            OtrosFiltros += " AND CAST(DOC.f_Fecha AS DATE) BETWEEN '" +
                            Properties.Settings.Default.Stellar_ControlSync_FechaIni.Replace("'", String.Empty) + "' AND '" +
                            Properties.Settings.Default.Stellar_ControlSync_FechaFin.Replace("'", String.Empty) + "'";
                        } else
                        {
                            OtrosFiltros += " AND (CAST(CAST(DOC.f_Fecha AS DATE) AS DATETIME) + CAST(CAST(DOC.f_Hora AS TIME) AS DATETIME)) BETWEEN '" +
                            Properties.Settings.Default.Stellar_ControlSync_FechaIni.Replace("'", String.Empty) + "' AND '" +
                            Properties.Settings.Default.Stellar_ControlSync_FechaFin.Replace("'", String.Empty) + "'";
                        }
                        
                    }

                    if (!Properties.Settings.Default.Stellar_ControlSync_Transacciones.isUndefined())
                    {

                        List<String> ListaTransacciones = Properties.Settings.Default.Stellar_ControlSync_Transacciones.Replace("'", String.Empty).Split('|').ToList();
                        String mInListaTransacciones = String.Join(", ", ListaTransacciones.Select(x => String.Format("'{0}'", x)).ToList());

                        OtrosFiltros += " AND (DOC.c_Sucursal + DOC.c_Concepto + DOC.c_Numero) IN (" + mInListaTransacciones + ")";
                    }

                    Double TopNTemp = 0; String TopNPercent = String.Empty;

                    Double.TryParse(Properties.Settings.Default.Stellar_ControlSync_TopNTransacciones.Replace("%", String.Empty), out TopNTemp);
                    if (Properties.Settings.Default.Stellar_ControlSync_TopNTransacciones.Contains("%"))
                        TopNPercent = " PERCENT";

                    if (TopNTemp > 0)
                        TopNDoc = "TOP (" + TopNTemp.ToString("################0") + ")" + TopNPercent + " ";
                    else
                        TopNDoc = "TOP (100) PERCENT";

                }
                else
                {
                    // Para no tener que quitar el Order BY en este caso. Traer todo.
                    TopNDoc = "TOP (100) PERCENT";
                }
                //

                String FiltroPrimarioTransacciones = (OtrosFiltros.isUndefined() ? "(DOC.cs_Sync_SxS = '" + mEstatus + "')" : OtrosFiltros);

                mSQL = "SELECT " + TopNDoc + " *, " + "\n" +
                "(DOC.c_Sucursal + DOC.c_Concepto + DOC.c_Numero) AS TransID " + "\n" + 
                "FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID";
                
                pRsVentas = new ADODB.Recordset();
                
                pRsVentas.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsVentas.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsVentas.ActiveConnection = null;
                
                if (Properties.Settings.Default.EnviarVentaAgrupada)
                {
                    mSQL = String.Empty +
                    "SELECT c_Localidad, c_Concepto, c_Numero, MIN(n_Linea) AS n_Linea, Cod_Principal, Cod_Principal AS Codigo, " + "\n" +
                    "SUM(Cantidad) AS Cantidad, ABS(ROUND(Precio, 3, 0)) AS Precio, " + "\n" +
                    "ABS(ROUND(n_PrecioReal, 3, 0)) AS n_PrecioReal, " + "\n" +
                    "SUM(Subtotal) AS Subtotal, SUM(Impuesto) AS Impuesto, SUM(Total) AS Total, " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, SUM(Descuento) AS Descuento, n_TipoPrecio, " + "\n" +
                    "MIN(ID) AS ID, (c_Localidad + c_Concepto + c_Numero) AS TransID " + "\n" +
                    "FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                    "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                    "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " + "\n" +
                    "GROUP BY Cod_Principal, ABS(ROUND(Precio, 3, 0)), ABS(ROUND(n_PrecioReal, 3, 0)), " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, n_TipoPrecio, c_Localidad, c_Concepto, c_Numero " + "\n" +
                    "HAVING ABS(ROUND(SUM(Cantidad), 8, 0)) >= 0.0001 " + "\n" +
                    "ORDER BY MIN(n_Linea), MIN(ID) "; // NO ENVIAR PRODUCTOS REINTEGRADOS CUYA CANTIDAD QUEDO EN CERO.
                }
                else
                {
                    mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                    "FROM[VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                    "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                    "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " + "\n" +
                    "ORDER BY n_Linea, ID ";
                }

                //

                pRsItems = new ADODB.Recordset();
                
                pRsItems.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsItems.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsItems.ActiveConnection = null;

                mSQL = "SELECT *, (c_Localidad + 'VEN' + c_Factura) AS TransID " + "\n" +
                "FROM [VAD20].[DBO].[MA_DETALLEPAGO] WHERE (c_Localidad + 'VEN' + c_Factura) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " +
                "ORDER BY c_Factura, " + "\n";
                
                if ((Properties.Settings.Default.SAP_CruzarNotaCreditoConPago || Properties.Settings.Default.SAP_ReconciliarNotaCreditoConFacturaPagada)
                && !Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago.isUndefined())
                {
                    mSQL += "(CASE WHEN c_CodDenominacion IN ('" + Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago.Replace("'", "") + "'" + 
                    (!Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago_CambioTaxID.isUndefined() ? 
                    ", '" + Properties.Settings.Default.SAP_CruzarNotaCreditoConPago_CodFormaPago_CambioTaxID.Replace("'", "") + "'" : String.Empty) + ") THEN 2 ";
                } else
                {
                    mSQL += "(CASE ";
                }

                mSQL += 
                "WHEN c_CodDenominacion = 'Efectivo' THEN 1 ELSE 0 END) DESC, ID";
                
                pRsDetPagos = new ADODB.Recordset();
                
                pRsDetPagos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsDetPagos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsDetPagos.ActiveConnection = null;

                //

                mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_PAGOS_IMPUESTOS] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) ";
                
                pRsImpuestos = new ADODB.Recordset();
                
                pRsImpuestos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsImpuestos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsImpuestos.ActiveConnection = null;
                
                //

                mSQL = "SELECT *, (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_DOCUMENTOS_FISCAL] WHERE (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) ";

                pRsDatosFiscales = new ADODB.Recordset();

                pRsDatosFiscales.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsDatosFiscales.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsDatosFiscales.ActiveConnection = null;

                //

                mSQL = "SELECT *, (CodLocalidad + Concepto + Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_PLAN_PENDIENTE_X_ENTREGA] WHERE (CodLocalidad + Concepto + Numero) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " + "\n" +
                "ORDER BY ID "; 

                pRsPendXEntregaMA = new ADODB.Recordset();

                pRsPendXEntregaMA.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsPendXEntregaMA.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsPendXEntregaMA.ActiveConnection = null;

                //

                mSQL = "SELECT *, (CodLocalidad + Concepto + Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_PENDIENTE_X_ENTREGA] WHERE (CodLocalidad + Concepto + Numero) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " + "\n" +
                "ORDER BY ID ";

                pRsPendXEntregaTR = new ADODB.Recordset();

                pRsPendXEntregaTR.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsPendXEntregaTR.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsPendXEntregaTR.ActiveConnection = null;

                //

                mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_SERIALES] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                "SELECT " + TopNDoc + " (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE " + FiltroPrimarioTransacciones + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, CAST(DOC.f_Hora AS TIME), DOC.ID) " + "\n" +
                "ORDER BY ID ";

                pRsSeriales = new ADODB.Recordset();

                pRsSeriales.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsSeriales.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsSeriales.ActiveConnection = null;

                //

                ObtenerDatosVentas = (pRsVentas.RecordCount > 0);

                return ObtenerDatosVentas;

	        }
	        catch (Exception Any)
	        {

                Program.Logger.EscribirLog(Any, "ObtenerDatosVentas();");

                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Buscando informacion de las Transacciones POS pendientes por procesar.", "DBSync.ObtenerDatosVentas()",
                "Ejecucion Agente", Program.gCorrelativo, (Any.HResult + "|" + Any.Message), "BuscarDatos", Program.mCnLocal);

                pRsVentas = null;
                pRsItems = null;
                pRsDetPagos = null;
                pRsImpuestos = null;
                pRsPendXEntregaMA = null;
                pRsPendXEntregaTR = null;
                pRsDatosFiscales = null;
                pRsSeriales = null;

                return ObtenerDatosVentas;
	        }

        }

        private static void ControlarErrorRegistroVentas(System.Collections.ArrayList pFallidos, 
        String pTransID, String pLocalidad, String pConcepto, String pNumero)
        {

            pFallidos.Add( new String[] {  pTransID, pLocalidad, pConcepto, pNumero } );
            
        }
        
        private static Boolean MarcarFallidos(ref System.Collections.ArrayList pListaFallidos)
        {

            try 
	        {

                Console.WriteLine("Finalizando lote de transacciones... Verificando y marcando fallidos (" + pListaFallidos.Count.ToString() + ")");

                Object Records; Int64 Contador = 0;

                foreach (String[] Item in pListaFallidos)
                {

                    Contador++;
                    
                    Console.WriteLine("Marcando fallidos... " + Contador + " / " + pListaFallidos.Count.ToString());
                    
                    Program.mCnLocal.Execute("UPDATE [VAD20].[DBO].[MA_PAGOS] " + 
                    "SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + "\n" +
                    "WHERE c_Sucursal = '" + Item[1] + "' " +
                    "AND c_Concepto = '" + Item[2] + "' " + 
                    "AND c_Numero = '" + Item[3] + "' ", 
                    out Records);

                }

                return true;

	        }
	        catch (Exception Any)
	        {
                Program.Logger.EscribirLog (Any, "Error marcando registros de ventas fallidos.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error marcando registros de ventas o devoluciones fallidos.",
                "DBSync.MarcarFallidos()", "Ejecución Agente",
                Program.gCorrelativo, Program.mCnLocal);
                return false;
	        }
            
        }

        //   ACA SE AGREGA ESTA FUNCIÓN CON EL FIN DE GENERAR LAS AUDITORIAS QUE SE REQUIERAN PARA TODOS LOS CASOS.

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria,
        String DescripcionAuditoria,
        String DescripcionEvento,
        String Ventana,
        String TipoObjeto,
        String CodigoAfectado,
        String CodigoRetorno,
        String AccionRealizada,
        ADODB.Connection Conexion)
        {

            try
            {

                ADODB.Connection DB = null;
                //DB = new ADODB.Connection();
                DB = Conexion;

                ADODB.Recordset mRs = null; String SQL = String.Empty;

                mRs = new ADODB.Recordset();

                SQL = "SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS] WHERE 1 = 2";

                //DB.ConnectionString = Conexion.ConnectionString;
                //DB.Open();

                mRs.Open(SQL, DB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                mRs.AddNew();

                mRs.Fields["Cod_Prod"].Value = Program.gCodProducto;
                mRs.Fields["Nom_Prod"].Value = Program.ApplicationName;
                mRs.Fields["Tipo"].Value = TipoAuditoria;
                mRs.Fields["Descripcion"].Value = DescripcionAuditoria;
                mRs.Fields["Evento"].Value = DescripcionEvento;
                //mRs.Fields["Fecha"].Value = DateTime.Now; // Posee Valor Default GetDate()
                mRs.Fields["CodUsuario"].Value = "9999999999";
                mRs.Fields["Usuario"].Value = mRs.Fields["Nom_Prod"].Value;
                mRs.Fields["Ventana"].Value = Ventana;
                mRs.Fields["TipoObjAuditado"].Value = TipoObjeto;
                mRs.Fields["CodigoAuditado"].Value = CodigoAfectado;
                if (Functions.ExisteCampoRs(mRs, "CodigoRetorno"))
                    mRs.Fields["CodigoRetorno"].Value = CodigoRetorno;
                if (Functions.ExisteCampoRs(mRs, "AccionRealizada"))
                    mRs.Fields["AccionRealizada"].Value = AccionRealizada;

                mRs.Update();

                mRs.Close();

                // DB.Close();

                return true;

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "InsertarAuditoria()" + "\n" +
                "Cod_Prod=" + Program.gCodProducto + "\n" +
                "Nom_Prod=" + Program.ApplicationName + "\n" +
                "Tipo=" + TipoAuditoria + "\n" +
                "Descripcion=" + DescripcionAuditoria + "\n" +
                "Evento=" + DescripcionEvento + "\n" +
                "Ventana=" + Ventana + "\n" +
                "TipoObjAuditado=" + TipoObjeto + "\n" +
                "CodigoAuditado=" + CodigoAfectado + "\n" +
                "CodigoRetorno=" + CodigoRetorno + "\n" +
                "AccionRealizada=" + AccionRealizada + "\n");
                return false;
            }
            
        }

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria, 
        String DescripcionAuditoria, 
        String DescripcionEvento, 
        String Ventana, 
        String TipoObjeto, 
        String CodigoAfectado, 
        ADODB.Connection Conexion)
        {
            return InsertarAuditoria(TipoAuditoria, DescripcionAuditoria, DescripcionEvento, 
            Ventana, TipoObjeto, CodigoAfectado, String.Empty, String.Empty, Conexion);
        }

        // Apartado de Modalidad de Sincronizacion de Datos Maestros SAP - Stellar

        private static void ActualizarDatosMaestrosPendientes()
        {

            ActualizarMotivosDevolucion();

            ActualizarCategorias();

            ActualizarProductosYCodigos();

            String mEtapa = String.Empty;

            if (Properties.Settings.Default.Stellar_AutoProcessMasterData)
            {

                try
                {

                    mEtapa = "Ejecutando Llamada a URL de Procesamiento de Pendientes.";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    String SxS_POS_Dir = Path.GetFullPath(Path.Combine(Program.gPath, Properties.Settings.Default.Stellar_MasterData_ProcessURL));

                    mEtapa = "URL:\n\n" + SxS_POS_Dir;
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    using (System.Diagnostics.Process tmpExe = new System.Diagnostics.Process())
                    {
                        tmpExe.StartInfo.FileName = SxS_POS_Dir;
                        tmpExe.Start();
                    }

                }
                catch (Exception Any)
                {

                    Console.WriteLine("Error: " + mEtapa);

                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    Program.Logger.EscribirLog(Any, "ActualizarDatosMaestrosPendientes(); Etapa: " + mEtapa);

                }

            }

        }

        private static void ActualizarProductosYCodigos()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_CODIGOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_PRODUCTOS_SAP", out Records);
                }
                catch { }
                
                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems) 
                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_INVENTARIO_ITEM", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, " +
                "CAST('' AS NVARCHAR(MAX)) AS DefaultCode, CAST(0 AS INT) AS SAPUserChange " + 
                "INTO #TMP_CODIGOS_SAP FROM MA_CODIGOS WHERE 1 = 2", out Records);

                LocalDB.Execute("SELECT c_Codigo, CAST('' AS NVARCHAR(MAX)) AS c_Descri, " +
                "CAST('' AS NVARCHAR(MAX)) AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS c_Grupo, " +
                "CAST('' AS NVARCHAR(MAX)) AS c_Subgrupo, CAST('' AS NVARCHAR(MAX)) AS c_Marca, " +
                "n_CostoAct, n_CostoAnt, n_CostoPro, n_CostoRep, n_Precio1, n_Precio2, n_Precio3, " +
                "c_Seriales, CAST('' AS NVARCHAR(MAX)) AS c_Presenta, n_Cantibul, n_Impuesto1, n_Activo, " +
                "n_TipoPeso, CAST('' AS NVARCHAR(MAX)) AS cu_Descripcion_Corta, Cant_Decimales, " +
                "Add_Date, Update_Date, c_CodMoneda, CAST('' AS NVARCHAR(MAX)) AS c_Observacio, " +
                "CAST('' AS NVARCHAR(MAX)) AS WarrntTmpl, CAST('' AS NVARCHAR(MAX)) AS U_pltgrt, " +
                "nu_NivelClave, CAST('' AS NVARCHAR(MAX)) AS EOF " + 
                "INTO #TMP_PRODUCTOS_SAP FROM MA_PRODUCTOS WHERE 1 = 2", out Records);

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                    LocalDB.Execute("SELECT * " +
                    "INTO #TMP_INVENTARIO_ITEM FROM MA_DEPOPROD_ITEMS WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String mJoinBitacora = String.Empty; String MarcaInicioBitacora;

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    
                    // Hacer un CROSS JOIN para que se traiga todos los registros, sin tomar en cuenta log de cambios.

                    mJoinBitacora = String.Empty +
                    "CROSS JOIN (SELECT TOP 1 NULL AS \"ItemCode\", NULL AS \"CHDATE\", " + "\n" +
                    "NULL AS \"CHTIME\", NULL AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"OITM\" " + "\n" +
                    ") AS \"PEND\" " + "\n";
                    
                else
                {

                    // Si maneja multi localidad, tomar solo lo que le corresponda a la localidad de SAP definida en el Setup.
                    // Sino, hacer la consulta asumiendo que solo se trata de una instancia de BD autonoma sin localidades.

                    mJoinBitacora = String.Empty +
                    (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD ?
                    "INNER JOIN ( " + "\n" +
                    "SELECT \"BI\".\"LIST_OF_COLS_VAL_TAB_DEL\" AS \"ItemCode\", MAX(\"BI\".\"DATE\") AS \"CHDATE\", " + "\n" +
                    "MAX(\"BI\".\"HORA\") AS \"CHTIME\", MAX(\"BI\".\"LOGINSTAC\") AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"BITACORA\" AS \"BI\" " + "\n" +
                    "WHERE \"BI\".\"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "' " + "\n" +
                    "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                    "AND \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"LECTURA_ST\" = 1 " + "\n" +
                    "GROUP BY \"BI\".\"LIST_OF_COLS_VAL_TAB_DEL\" " + "\n" +
                    ") AS \"PEND\" " + "\n" +
                    "ON \"OITM\".\"ItemCode\" = \"PEND\".\"ItemCode\" " + "\n" +
                    "AND \"PEND\".\"PROCESADO_ST\" = 0 " + "\n"
                    :
                    "INNER JOIN( " + "\n" +
                    "SELECT \"LIST_OF_COLS_VAL_TAB_DEL\" AS \"ItemCode\", MAX(\"DATE\") AS \"CHDATE\", " + "\n" +
                    "MAX(\"HORA\") AS \"CHTIME\", MAX(\"LOGINSTAC\") AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"BITACORA\" WHERE \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                    "AND \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"LECTURA_ST\" = 1 " + "\n" +
                    "GROUP BY \"LIST_OF_COLS_VAL_TAB_DEL\" " + "\n" +
                    ") AS \"PEND\" " + "\n" +
                    "ON \"OITM\".\"ItemCode\" = \"PEND\".\"ItemCode\" " + "\n" +
                    "AND \"PEND\".\"PROCESADO_ST\" = 0 " + "\n");

                    if (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD)
                    {

                        MarcaInicioBitacora = "UPDATE \"BITACORA\" SET " + "\n" +
                        "\"LECTURA_ST\" = 1 " + "\n" +
                        "WHERE  \"PROCESADO_ST\" = 0 " + "\n" +
                        "AND \"LECTURA_ST\" = 0 " + "\n" +
                        "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                        "AND  \"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "' " + "\n";

                    }    
                    else
                    {

                        MarcaInicioBitacora = "UPDATE \"BITACORA\" SET " + "\n" +
                        "\"LECTURA_ST\" = 1 " + "\n" +
                        "WHERE  \"PROCESADO_ST\" = 0 " + "\n" +
                        "AND \"LECTURA_ST\" = 0 " + "\n" +
                        "AND \"OBJECT_TYPE\" = 4 " + "\n";

                    }

                    mEtapa = "Marcando Actualizaciones Pendientes de Productos por Procesar";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    SAPbobsCOM.Recordset mRsMarca;
                    mRsMarca = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);
                    mRsMarca.DoQuery(MarcaInicioBitacora);

                }

                // Query:
                //Buscar los codigos de productos que hayan tenido cambios y con codigos de 15 digitos compatibles con Stellar
                //Unido a
                //Obtener registro de Codigo Maestro igual para los productos que hayan cambiado.
                //Unido a
                //Obtener codigos PLU para productos que hagan match con la matriz de codigo de productos pesados en SAP.

                String mCodigoEDI1 = String.Empty;
                String mCodigoEDI2 = String.Empty;
                String mCodigoEDI3 = String.Empty;

                if (Properties.Settings.Default.Stellar_CodigoEDIAutomatico)
                {

                    // Asigna el Codigo Maestro automaticamente como el EDI Siempre

                    mCodigoEDI1 = String.Empty +
                    "0 AS \"nu_Intercambio\", ";

                    mCodigoEDI2 = String.Empty +
                    "1 AS \"nu_Intercambio\", ";

                    mCodigoEDI3 = String.Empty +
                    "0 AS \"nu_Intercambio\", ";

                }
                else
                {

                    // Desde SAP controlan la seleccion de un codigo especifico como Estandar (Campo Codebars de OITM)
                    // Si el Campo Codebars esta vacío se toma como Fallback el Maestro.
                    // Para evitar errores de Integridad desde SAP deberan validar que en el campo Codebars no se ingrese 
                    // un codigo distinto a cualquiera de los codigos de barra registrados para cada producto.

                    mCodigoEDI1 = String.Empty +
                    "CASE WHEN \"OBCD\".\"BcdCode\" = \"OITM\".\"CodeBars\" THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";

                    mCodigoEDI2 = String.Empty +
                    "CASE WHEN (\"OITM\".\"ItemCode\" = \"OITM\".\"CodeBars\" OR TRIM(IfNULL(\"OITM\".\"CodeBars\", '')) = '') THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";

                    mCodigoEDI3 = String.Empty +
                    "CASE WHEN SUBSTRING(\"OITM\".\"ItemCode\", 1, 7) = \"OITM\".\"CodeBars\" THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";
                        
                }
                
                String SQLCodigos = String.Empty +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", \"OBCD\".\"BcdCode\" AS \"c_Codigo\", " + "\n" +
                "SUBSTRING(IfNULL(\"OBCD\".\"BcdName\", ''), 1, 20) AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI1 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 1 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "INNER JOIN \"OBCD\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"OBCD\".\"ItemCode\" " + "\n" +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 AND LENGTH(\"OBCD\".\"BcdCode\") <= 15 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", \"OITM\".\"ItemCode\" AS \"c_Codigo\", " + "\n" +
                "'CODIGO MAESTRO' AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI2 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 0 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", SUBSTRING(\"OITM\".\"ItemCode\", 1, 7) AS \"c_Codigo\", " + "\n" +
                "'PLU' AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI3 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 0 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "WHERE SUBSTRING(\"OITM\".\"ItemCode\", 1, 2) = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " + "\n" +
                "AND LENGTH(\"OITM\".\"ItemCode\") >= " + Properties.Settings.Default.SAP_LongitudMinimaCodigoProductosPesados + " " + "\n" +
                "AND LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n";

                //

                List<String> mUndMedidaPesable; String mCriterioPesableIN;

                if (Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.ContainsIgnoreCase("*;"))
                    mCriterioPesableIN = "NOT IN ";
                else
                    mCriterioPesableIN = "IN ";

                if (Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.isUndefined())
                    mUndMedidaPesable = "|[NULL]|".Split(';').ToList();
                else
                    mUndMedidaPesable = Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.Split(';').ToList();

                String mInTipoPesable = String.Join(", ", mUndMedidaPesable.Select(x => String.Format("'{0}'", x)).ToList());

                String mInProductosCupon = String.Join(", ", Program.ListaProductosCupon.Values.ToList().Select(x => String.Format("'{0}'", x)).ToList());

                if (!mInProductosCupon.isUndefined()) mInProductosCupon = " OR \"OITM\".\"ItemCode\" IN (" + mInProductosCupon + ")";

                String mProductosInformativos = " OR (\"OITM\".\"InvntItem\" = 'N' AND IfNULL(\"PRC1\".\"Price\", 0) = 0) ";

                String mCampoDescripcion = (Properties.Settings.Default.Stellar_SoloDescripcionCorta ? "U_NOMBREPOS" : "ItemName");
                // Sacar datos de productos. Los precios dependen de la lista de precios que se indique en el Setup.
                // EL tipo de producto lo determina: Si el codigo hace match con la matriz de codigo de SAP son pesados, 
                // Si la unidad de medida es alguna de las definidas en una lista de Setup, son pesables. 
                // Si la lista de precios tiene definida la bandera de Entrada Manual del precio son informativos.
                // Ante cualquier otro caso distinto son productos por unidad.
                // Manejo de bandera para especificar si un producto se maneja por Lotes o por Numeros de Serie.

                String SQLProductos = String.Empty +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_Codigo\", IfNULL(\"" + mCampoDescripcion + "\", '') AS \"c_Descri\", " + "\n" +
                "IfNULL(\"OITB\".\"ItmsGrpCod\", '0') AS \"c_Departamento\", " + "\n" +
                "IfNULL(\"@GRUPO_INV\".\"Code\", '') AS \"c_Grupo\", " + "\n" +
                "IfNULL(\"@CLASES_INV\".\"Code\", '') AS \"c_Subgrupo\", " + "\n" +
                "IfNULL(\"OMRC\".\"FirmName\", '') AS \"c_Marca\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoAct\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoAnt\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoPro\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoRep\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC1\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC1\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio1\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC2\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC2\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio2\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC3\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC3\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio3\", " + "\n" +
                "CASE WHEN \"OITM\".\"ManSerNum\" = 'Y' THEN '2' WHEN \"OITM\".\"ManBtchNum\" = 'Y' THEN '3' ELSE '1' END AS \"c_Seriales\", " + "\n" +
                "IfNULL(\"SalUnitMsr\", '') AS \"c_Presenta\", " + "\n" +
                "IfNULL(\"NumInSale\", 1) AS \"n_Cantibul\", " + "\n" +
                "IfNULL(\"OSTC\".\"Rate\", 0) AS \"n_Impuesto1\", " + "\n" +
                "CASE WHEN(\"frozenFor\" = 'Y' OR \"OITM\".\"Deleted\" = 'Y') THEN 0 ELSE 1 END AS \"n_Activo\", " + "\n" +
                "CASE " + "\n" +
                "   WHEN (\"OITM\".\"InvntItem\" = 'N' AND IfNULL(\"PRC1\".\"Price\", 0) = 0) THEN " +
                "       4-- INFORMATIVO / SERVICIO " + "\n" +
                "   WHEN \"U_NUMERODECIMALES\" > 0 THEN " + "\n" +
                "   CASE WHEN SUBSTRING(\"OITM\".\"ItemCode\", 1, 2) = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " +
                "   AND LENGTH(\"OITM\".\"ItemCode\") >= " + Properties.Settings.Default.SAP_LongitudMinimaCodigoProductosPesados + " THEN " + "\n" +
                "       2-- PESADO / ETIQUETA PERO TAMBIEN LO VAN A PESAR EN CAJA " + "\n" +
                "   ELSE " + "\n" +
                "       CASE WHEN UPPER(\"SalUnitMsr\") " + mCriterioPesableIN +"(" + mInTipoPesable + ") THEN " + "\n" +
                "           2-- PESABLE " + "\n" +
                "       ELSE " + "\n" +
                "           CASE WHEN IfNULL(\"PRC1\".\"Ovrwritten\", '') = 'Y' THEN " + "\n" +
                "               4-- INFORMATIVO / SERVICIO " + "\n" +
                "           ELSE " + "\n" +
                "               0-- UNIDAD " + "\n" +
                "           END " + "\n" +
                "        END " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "   0-- UNIDAD " + "\n" +
                "END AS \"n_TipoPeso\", " + "\n" +
                "IfNULL(\"U_NOMBREPOS\", '') AS \"cu_Descripcion_Corta\", " + "\n" +
                "IfNULL(\"U_NUMERODECIMALES\", 0) AS \"Cant_Decimales\", " + "\n" +
                "\"OITM\".\"CreateDate\" AS \"Add_Date\", " + "\n" +
                "CASE WHEN \"PEND\".\"CHDATE\" IS NULL THEN " + "\n" +
                "\"OITM\".\"UpdateDate\" " + "\n" +
                "ELSE " + "\n" +
                "TO_TIMESTAMP(LEFT(TO_VARCHAR(\"PEND\".\"CHDATE\"), 10) || ' ' || TO_VARCHAR(\"PEND\".\"CHTIME\")) " + "\n" +
                "END AS \"Update_Date\", " + "\n" +
                "IfNULL(\"PRC1\".\"Currency\", '') AS \"c_CodMoneda\", " + "\n" +
                "IfNULL(\"OITM\".\"UserText\", '') AS \"c_Observacio\", " + "\n" +
                "IfNULL(\"OITM\".\"WarrntTmpl\", '') AS \"WarrntTmpl\", " + "\n" +
                "IfNULL(\"OCTT\".\"Remark\", '') AS \"U_pltgrt\", " + "\n" +
                "1 AS nu_NivelClave, " + "\n" +
                "'' AS \"U_EOF\" " + "\n" +
                "--, \"OITM\".* " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "LEFT JOIN \"OITB\" " + "\n" +
                "ON \"OITM\".\"ItmsGrpCod\" = \"OITB\".\"ItmsGrpCod\" " + "\n" +
                "LEFT JOIN \"@GRUPO_INV\" " + "\n" +
                "ON \"OITM\".\"U_GRUPO\" = \"@GRUPO_INV\".\"Code\" " + "\n" +
                "LEFT JOIN \"@CLASES_INV\" " + "\n" +
                "ON \"OITM\".\"U_CLASE\" = \"@CLASES_INV\".\"Code\" " + "\n" +
                "LEFT JOIN \"OMRC\" " + "\n" +
                "ON \"OITM\".\"FirmCode\" = \"OMRC\".\"FirmCode\" " + "\n" +
                "LEFT JOIN \"OSTC\" " + "\n" +
                "ON \"OITM\".\"TaxCodeAR\" = \"OSTC\".\"Code\" " + "\n" +
                "LEFT JOIN \"OCTT\" " + "\n" +
                "ON \"OITM\".\"WarrntTmpl\" = \"OCTT\".\"TmpltName\" " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC1\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC1\".\"ItemCode\" " + "\n" +
                "AND \"PRC1\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceListNo + ") " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC2\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC2\".\"ItemCode\" " + "\n" +
                "AND \"PRC2\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceList2No + ") " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC3\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC3\".\"ItemCode\" " + "\n" +
                "AND \"PRC3\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceList3No + ") " + "\n" +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n" +
                "AND \"OITM\".\"ManBtchNum\" = 'N' " + "\n" +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio ? "AND ((IfNULL(\"PRC1\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty) +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio2 ? "AND ((IfNULL(\"PRC2\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty) +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio3 ? "AND ((IfNULL(\"PRC3\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty);

                String SQLInventarioxItems = String.Empty;

                Boolean ActualizarTimeStampUltimaActualizacionInventarioXItems = false;

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                {

                    Boolean mSoloCambios = true; String mCambiosS = String.Empty; String mCambiosL = String.Empty;

                    if (Properties.Settings.Default.SAP_SincronizarInventarioXItems_CadaCuantosMinutos > 0)
                        if (DateTime.Compare(DateTime.Now, Properties.Settings.Default.SAP_SincronizarInventarioXItems_UltimaActualizacion.AddMinutes(
                        Properties.Settings.Default.SAP_SincronizarInventarioXItems_CadaCuantosMinutos)) > 0)
                        {
                            mSoloCambios = false; // Si ya han pasado mas de X minutos desde la ultima actualizacion, traerse todo.
                            ActualizarTimeStampUltimaActualizacionInventarioXItems = true;
                        }
                    
                    if (mSoloCambios)
                    {
                        mCambiosS = mJoinBitacora.Replace("ON \"OITM\"", "ON \"OSRI\"");
                        mCambiosL = mJoinBitacora.Replace("ON \"OITM\"", "ON \"OIBT\"");
                    }

                    SQLInventarioxItems = String.Empty +
                    "SELECT \"WhsCode\", \"OSRI\".\"ItemCode\", \"SysSerial\", \"IntrSerial\", " + "\n" +
                    "\"IntrSerial\", \"BatchId\", IfNULL(\"InDate\", CURRENT_DATE) AS \"InDate\", \"PrdDate\", " + "\n" +
                    "\"ExpDate\", 1 AS \"RegQty\", CASE WHEN \"Status\" = 0 THEN 1 ELSE 0 END AS \"AvlQty\", 0 AS \"BlqQty\", " + "\n" +
                    "IfNULL(\"Notes\", '') AS \"Notes\", 1 AS \"Status\", \"GrntStart\", \"GrntExp\", 'S' AS RegType " + "\n" +
                    "FROM \"OSRI\" " + "\n" +
                    mCambiosS +
                    "WHERE LENGTH(\"OSRI\".\"ItemCode\") <= 15 " + "\n" +
                    "UNION ALL " + "\n" +
                    "SELECT \"WhsCode\", \"OIBT\".\"ItemCode\", \"SysNumber\" AS \"SysSerial\", \"SuppSerial\" AS \"IntrSerial\", " + "\n" +
                    "'' AS \"SuppSerial\", \"BatchNum\" AS \"BatchId\", IfNULL(\"InDate\", CURRENT_DATE) AS \"InDate\", \"PrdDate\", " + "\n" +
                    "\"ExpDate\", \"Quantity\" AS \"RegQty\", \"Quantity\" AS \"AvlQty\", CASE WHEN \"Status\" = 0 THEN 0 ELSE \"Quantity\" END AS \"BlqQty\", " + "\n" +
                    "IfNULL(\"Notes\", '') AS \"Notes\", CASE WHEN \"Status\" = 0 THEN 1 ELSE 2 END AS \"Status\", NULL AS \"GrntStart\", NULL AS \"GrntExp\", 'L' AS RegType " + "\n" +
                    "FROM \"OIBT\" " + "\n" +
                    mCambiosL +
                    "WHERE LENGTH(\"OIBT\".\"ItemCode\") <= 15 " + "\n";

                }

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp; Boolean mBoolTemp;

                //

                mEtapa = "Consultando Codigos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLCodigos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Codigos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_CODIGOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        LocalRs.Fields["c_CodNasa"].Value = mRs.Fields.Item("c_CodNasa").Value;
                        LocalRs.Fields["c_Codigo"].Value = mRs.Fields.Item("c_Codigo").Value;
                        LocalRs.Fields["c_Descripcion"].Value = mRs.Fields.Item("c_Descripcion").Value;
                        LocalRs.Fields["nu_Intercambio"].Value = mRs.Fields.Item("nu_Intercambio").Value;
                        LocalRs.Fields["n_Cantidad"].Value = mRs.Fields.Item("n_Cantidad").Value;
                        LocalRs.Fields["nu_TipoPrecio"].Value = mRs.Fields.Item("nu_TipoPrecio").Value;
                        LocalRs.Fields["DefaultCode"].Value = mRs.Fields.Item("DefaultCode").Value;
                        LocalRs.Fields["SAPUserChange"].Value = mRs.Fields.Item("SAPUserChange").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                if (Properties.Settings.Default.Stellar_AutoGenerarMatrizCodigo)
                {

                    //

                    mEtapa = "Validando Matriz de Codigo SBO";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute("DELETE FROM VAD20.DBO.MA_ETIQUETAS " + "\n" +
                    "WHERE c_Longitud = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " + "\n" +
                    "AND n_Longitud = 13 " + "\n" +
                    "AND c_Codigo <> 'SBO'", out Records);

                    if (Convert.ToDouble(LocalDB.Execute("SELECT isNULL(COUNT(*), 0) AS Existe " + "\n" +
                    "FROM VAD20.DBO.MA_ETIQUETAS " + "\n" +
                    "WHERE c_Codigo = 'SBO'", out Records).Fields[0].Value) == 0)
                    {

                        ADODB.Recordset LocalRs = new ADODB.Recordset();

                        LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                        LocalRs.Open("SELECT * FROM VAD20.DBO.MA_ETIQUETAS WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                        LocalRs.ActiveConnection = null;

                        //while (!mRs.EoF)
                        {

                            LocalRs.AddNew();

                            LocalRs.Fields["c_Codigo"].Value = "SBO";
                            LocalRs.Fields["c_Descripcio"].Value = "ETIQUETA BALANZA SBO";
                            LocalRs.Fields["c_Longitud"].Value = Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo;
                            LocalRs.Fields["n_Longitud"].Value = 13;
                            LocalRs.Fields["n_Pos_Ini_1"].Value = 1;
                            LocalRs.Fields["n_Caracteres_1"].Value = 7;
                            LocalRs.Fields["c_Campo"].Value = 1;
                            LocalRs.Fields["n_Pos_Ini_2"].Value = 8;
                            LocalRs.Fields["n_Caracteres_2"].Value = 2;
                            LocalRs.Fields["b_Maneja_Decimal"].Value = true;
                            LocalRs.Fields["n_Pos_Ini_3"].Value = 10;
                            LocalRs.Fields["n_Caracteres_3"].Value = 3;
                            LocalRs.Fields["b_Activa"].Value = true;
                            LocalRs.Fields["b_VerificaDigito"].Value = Properties.Settings.Default.Stellar_AutoGenerarMatrizCodigo_DigitoVerificador;

                            //mRs.MoveNext();

                        }

                        LocalRs.ActiveConnection = LocalDB;

                        LocalRs.UpdateBatch();

                    }

                }

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                {

                    //

                    mEtapa = "Consultando Seriales y Lotes en SAP";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                    mRs.DoQuery(SQLInventarioxItems);

                    if (!mRs.EoF)
                    {

                        mEtapa = "Insertando Registros Temp InvxItems";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        ADODB.Recordset LocalRs = new ADODB.Recordset();

                        LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                        LocalRs.Open("SELECT * FROM #TMP_INVENTARIO_ITEM", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                        LocalRs.ActiveConnection = null;

                        while (!mRs.EoF)
                        {

                            LocalRs.AddNew();
                            
                            LocalRs.Fields["CodDeposito"].Value = mRs.Fields.Item("WhsCode").Value;
                            LocalRs.Fields["CodProducto"].Value = mRs.Fields.Item("ItemCode").Value;

                            LocalRs.Fields["IDSistema"].Value = mRs.Fields.Item("SysSerial").Value;
                            LocalRs.Fields["IDInterno"].Value = mRs.Fields.Item("IntrSerial").Value;
                            LocalRs.Fields["IDSerialFabricante"].Value = mRs.Fields.Item("SuppSerial").Value;
                            LocalRs.Fields["IDLoteFabricante"].Value = mRs.Fields.Item("BatchId").Value;

                            LocalRs.Fields["FechaEntrada"].Value = mRs.Fields.Item("InDate").Value;
                            LocalRs.Fields["FechaCreacion"].Value = mRs.Fields.Item("PrdDate").Value;
                            LocalRs.Fields["FechaVencimiento"].Value = mRs.Fields.Item("ExpDate").Value;

                            LocalRs.Fields["CantidadRegistrada"].Value = mRs.Fields.Item("RegQty").Value;
                            LocalRs.Fields["CantidadDisponible"].Value = mRs.Fields.Item("AvlQty").Value;
                            LocalRs.Fields["CantidadComprometida"].Value = mRs.Fields.Item("BlqQty").Value;

                            if (Convert.ToDouble(LocalRs.Fields["CantidadComprometida"].Value) > 0)
                            {

                                // Este campo es para uso interno de Stellar. Si el ERP externo tiene la funcion de comprometer cantidades
                                // Entonces lo que haremos es que no esten disponibles.

                                LocalRs.Fields["CantidadDisponible"].Value = Math.Round(
                                (Convert.ToDouble(LocalRs.Fields["CantidadDisponible"].Value) - Convert.ToDouble(LocalRs.Fields["CantidadComprometida"].Value)), 8, MidpointRounding.AwayFromZero);

                                LocalRs.Fields["CantidadComprometida"].Value = 0;

                            }

                            String mTempLnFix = String.Empty;

                            mTempLnFix = mRs.Fields.Item("Notes").Value;
                            LocalRs.Fields["Comentarios"].Value = mTempLnFix.Replace("\r\r", System.Environment.NewLine); //  ASCIIEncoding.ASCII.GetString(new byte[] { 13, 10 })

                            LocalRs.Fields["Status"].Value = mRs.Fields.Item("Status").Value;
                            LocalRs.Fields["FechaInicioGarantia"].Value = mRs.Fields.Item("GrntStart").Value;
                            LocalRs.Fields["FechaFinGarantia"].Value = mRs.Fields.Item("GrntExp").Value;
                            LocalRs.Fields["TipoRegistro"].Value = mRs.Fields.Item("RegType").Value;
                            
                            mRs.MoveNext();

                        }

                        LocalRs.ActiveConnection = LocalDB;

                        LocalRs.UpdateBatch();

                    }

                }

                //

                mEtapa = "Consultando Productos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLProductos);

                ADODB.Recordset SchemaRs = null;
                ADODB.Fields mLngCamposProd = null;

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Productos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    SchemaRs = LocalDB.Execute("SELECT * FROM MA_PRODUCTOS WHERE 1 = 2", out Records);

                    mLngCamposProd = SchemaRs.Fields;

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_PRODUCTOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        LocalRs.Fields["c_Codigo"].Value = mRs.Fields.Item("c_Codigo").Value;

                        mDatoTemp = mRs.Fields.Item("c_Descri").Value.ToString();
                        LocalRs.Fields["c_Descri"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descri"].DefinedSize);

                        // Validar que el departamento exista, debido a que ellos no tienen integridad de datos a nivel de jerarquia de categorias.
                        // Si el departamento no existiera, lo categorizamos en 0 (Departamento "No Categorizado") creado por el agente como fallback.

                        mBoolTemp = false;

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();

                        mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Departamento), 0) = 0 " +
                        "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_DEPARTAMENTOS_SAP WHERE c_Departamento = '" + mDatoTemp + "'", out Records)
                        .Fields["Existe"].Value);

                        if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                        "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_DEPARTAMENTOS WHERE c_Codigo = '" + mDatoTemp + "'", out Records)
                        .Fields["Existe"].Value);

                        if (!mBoolTemp) mDatoTemp = "0";

                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);

                        // Validar que el grupo sea valido. Si esta en blanco se graba asi, quiere decir que no esta categorizado.
                        // Si no esta en blanco, entonces se valida que el grupo exista dentro de ese departamento.
                        // Si no existe, lo categorizamos en 0 (Grupo "No Categorizado") creado por el agente como falllback.

                        if (mDatoTemp != "0") mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();

                        mBoolTemp = mDatoTemp.isUndefined();

                        if (mDatoTemp != "0" && !mBoolTemp)
                        {
                            mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Grupo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_GRUPOS_SAP WHERE c_Grupo = '" + mDatoTemp + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);

                            if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_GRUPOS WHERE c_Codigo = '" + mDatoTemp + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);
                        }

                        if (!mBoolTemp)
                        {
                            mDatoTemp = "0";
                            LocalRs.Fields["c_Departamento"].Value = mDatoTemp;
                        }

                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);

                        // Validar que el subgrupo sea valido. Si esta en blanco se graba asi, quiere decir que no esta categorizado.
                        // Si no esta en blanco, entonces se valida que el subgrupo exista dentro de ese grupo y departamento.
                        // Si no existe, lo categorizamos en 0 (Subgrupo "No Categorizado") creado por el agente como falllback.

                        if (mDatoTemp != "0") mDatoTemp = mRs.Fields.Item("c_Subgrupo").Value.ToString();

                        mBoolTemp = mDatoTemp.isUndefined();

                        if (mDatoTemp != "0" && !mBoolTemp)
                        {
                            mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Subgrupo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_SUBGRUPOS_SAP WHERE c_Subgrupo = '" + mDatoTemp + "' " +
                            "AND c_Grupo = '" + LocalRs.Fields["c_Grupo"].Value + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);

                            if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_SUBGRUPOS WHERE c_Codigo = '" + mDatoTemp + "' " +
                            "AND c_In_Grupo = '" + LocalRs.Fields["c_Grupo"].Value + "' " +
                            "AND c_In_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);
                        }

                        if (!mBoolTemp)
                        {
                            mDatoTemp = "0";
                            LocalRs.Fields["c_Grupo"].Value = mDatoTemp;
                            LocalRs.Fields["c_Departamento"].Value = mDatoTemp;
                        }

                        LocalRs.Fields["c_Subgrupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Subgrupo"].DefinedSize);

                        mDatoTemp = mRs.Fields.Item("c_Marca").Value.ToString();
                        LocalRs.Fields["c_Marca"].Value = mDatoTemp.Left(LocalRs.Fields["c_Marca"].DefinedSize);

                        LocalRs.Fields["nu_NivelClave"].Value = 1;

                        LocalRs.Fields["n_Activo"].Value = mRs.Fields.Item("n_Activo").Value;

                        LocalRs.Fields["n_CostoAct"].Value = mRs.Fields.Item("n_CostoAct").Value;
                        LocalRs.Fields["n_CostoAnt"].Value = mRs.Fields.Item("n_CostoAnt").Value;
                        LocalRs.Fields["n_CostoPro"].Value = mRs.Fields.Item("n_CostoPro").Value;
                        LocalRs.Fields["n_CostoRep"].Value = mRs.Fields.Item("n_CostoRep").Value;

                        LocalRs.Fields["n_Precio1"].Value = mRs.Fields.Item("n_Precio1").Value;
                        LocalRs.Fields["n_Precio2"].Value = mRs.Fields.Item("n_Precio2").Value;
                        LocalRs.Fields["n_Precio3"].Value = mRs.Fields.Item("n_Precio3").Value;

                        if (!Convert.ToBoolean(LocalRs.Fields["n_Activo"].Value))
                        {
                            if (Properties.Settings.Default.Stellar_NivelVentaProductosInactivos <= 0)
                            {
                                LocalRs.Fields["n_Precio1"].Value = 0;
                                LocalRs.Fields["n_Precio2"].Value = 0;
                                LocalRs.Fields["n_Precio3"].Value = 0;
                            }
                            else
                            {
                                LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.Stellar_NivelVentaProductosInactivos;
                            }
                        }

                        LocalRs.Fields["c_Seriales"].Value = mRs.Fields.Item("c_Seriales").Value;

                        mDatoTemp = mRs.Fields.Item("c_Presenta").Value.ToString();
                        LocalRs.Fields["c_Presenta"].Value = mDatoTemp.Left(LocalRs.Fields["c_Presenta"].DefinedSize);

                        LocalRs.Fields["n_Cantibul"].Value = mRs.Fields.Item("n_Cantibul").Value;
                        LocalRs.Fields["n_Impuesto1"].Value = mRs.Fields.Item("n_Impuesto1").Value;
                        LocalRs.Fields["n_TipoPeso"].Value = mRs.Fields.Item("n_TipoPeso").Value;

                        mDatoTemp = mRs.Fields.Item("cu_Descripcion_Corta").Value.ToString();
                        LocalRs.Fields["cu_Descripcion_Corta"].Value = mDatoTemp.Left(LocalRs.Fields["cu_Descripcion_Corta"].DefinedSize);

                        if (Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 0
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 3
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 4
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 5)
                            // Los unitarios, variantes (Car. Ext), informativos y compuestos no pueden manejar decimales.
                            LocalRs.Fields["Cant_Decimales"].Value = 0;
                        else
                            // Decimales solo en Pesados y Pesables. (2, 1)
                            // Si no tienen los decimales correctos, entonces el POS no lo permitira vender.
                            LocalRs.Fields["Cant_Decimales"].Value = mRs.Fields.Item("Cant_Decimales").Value;

                        if (Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 4)
                            LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.SAP_NivelClaveProductosInformativos;

                        LocalRs.Fields["Add_Date"].Value = mRs.Fields.Item("Add_Date").Value;
                        LocalRs.Fields["Update_Date"].Value = mRs.Fields.Item("Update_Date").Value;

                        // Esta moneda solo es referencial. No manejamos multimoneda aun.
                        // A pesar de lo que venga aqui igual tomaremos los valores en la moneda predeterminada.
                        // Si algun dia fueramos a controlar multimoneda especificado desde SAP, tendriamos que crear
                        // una lista de asociacion de codigos entre Moneda de Stellar y Moneda SAP.
                        mDatoTemp = mRs.Fields.Item("c_CodMoneda").Value;
                        LocalRs.Fields["c_CodMoneda"].Value = mDatoTemp.Left(LocalRs.Fields["c_CodMoneda"].DefinedSize);

                        mDatoTemp = mRs.Fields.Item("c_Observacio").Value;
                        LocalRs.Fields["c_Observacio"].Value = mDatoTemp.Replace("\r\r", System.Environment.NewLine);

                        LocalRs.Fields["WarrntTmpl"].Value = mRs.Fields.Item("WarrntTmpl").Value;

                        mDatoTemp = mRs.Fields.Item("U_pltgrt").Value;
                        LocalRs.Fields["U_pltgrt"].Value = mDatoTemp.Replace("\r\r", System.Environment.NewLine);

                        if (Program.ListaProductosCupon.Count > 0)
                            if(Program.ListaProductosCupon.ContainsValue(LocalRs.Fields["c_Codigo"].Value.ToString()))
                            {
                                LocalRs.Fields["c_Seriales"].Value = 1; // No maneja seriales a lo interno pero en el POS para la funcionalidad los va a pedir.
                                LocalRs.Fields["n_TipoPeso"].Value = 0; // Al ser un producto que pida seriales solo puede ser unitario.
                                LocalRs.Fields["Cant_Decimales"].Value = 0; // Y de igual manera no puede manejar decimales.
                                LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.SAP_NivelClaveProductosInformativos;
                            }

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros TR_PEND a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                mEtapa = "Insertando Actualizaciones de Códigos Alternos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                // PRIMERO REGISTRAR LAS ELIMINACIONES DE CODIGOS QUE YA NO VAN

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT COD.c_CodNasa, COD.c_Codigo, COD.c_Descripcion, COD.nu_Intercambio, " +
                    "COD.n_Cantidad, COD.nu_TipoPrecio, 1 AS TipoCambio \n" +
                    "FROM MA_CODIGOS COD LEFT JOIN #TMP_CODIGOS_SAP TMP \n" +
                    "ON COD.c_CodNasa = TMP.c_CodNasa \n" +
                    "AND COD.c_Codigo = TMP.c_Codigo \n" +
                    "WHERE TMP.c_Descripcion IS NULL \n"
                    , out Records);
                else
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT COD.c_CodNasa, COD.c_Codigo, COD.c_Descripcion, COD.nu_Intercambio, " +
                    "COD.n_Cantidad, COD.nu_TipoPrecio, 1 AS TipoCambio \n" +
                    "FROM MA_CODIGOS COD LEFT JOIN #TMP_CODIGOS_SAP TMP \n" +
                    "ON COD.c_CodNasa = TMP.c_CodNasa \n" +
                    "AND COD.c_Codigo = TMP.c_Codigo \n" +
                    "WHERE TMP.c_Descripcion IS NULL \n" + 
                    "AND COD.c_CodNasa IN (SELECT DISTINCT c_CodNasa FROM #TMP_CODIGOS_SAP) \n"
                    , out Records);

                // Y AHORA LOS CAMBIOS

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT TMP.c_CodNasa, TMP.c_Codigo, TMP.c_Descripcion, TMP.nu_Intercambio, " +
                    "TMP.n_Cantidad, TMP.nu_TipoPrecio, 0 AS TipoCambio \n" +
                    "FROM #TMP_CODIGOS_SAP TMP \n"
                    , out Records);
                else
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT TMP.c_CodNasa, TMP.c_Codigo, TMP.c_Descripcion, TMP.nu_Intercambio, " +
                    "TMP.n_Cantidad, TMP.nu_TipoPrecio, 0 AS TipoCambio \n" +
                    "FROM #TMP_CODIGOS_SAP TMP LEFT JOIN MA_CODIGOS COD \n" +
                    "ON TMP.c_CodNasa = COD.c_CodNasa \n" +
                    "AND TMP.c_Codigo = COD.c_Codigo \n" +
                    "WHERE isNULL(COD.c_Descripcion, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcion COLLATE MODERN_SPANISH_CI_AS \n"
                    , out Records);

                mEtapa = "Insertando Actualizaciones de Productos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                if (mLngCamposProd != null)
                {
                    
                    Boolean CambiosPoliticaGarantia = false;

                    // Insertar cualquier politica de garantia nueva.

                    mEtapa = "Insertando Nuevas Politicas de Garantia";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute(
                    "INSERT INTO MA_PRODUCTOS_DATOS_GARANTIA (AliasPlantillaGarantia, TextoCompleto) \n" +
                    "SELECT WarrntTmpl, U_pltgrt \n" +
                    "FROM #TMP_PRODUCTOS_SAP TPS \n" +
                    "LEFT JOIN MA_PRODUCTOS_DATOS_GARANTIA MPDG \n" +
                    "ON TPS.WarrntTmpl = MPDG.AliasPlantillaGarantia \n" +
                    "WHERE MPDG.AliasPlantillaGarantia IS NULL \n" +
                    "AND isNULL(TPS.WarrntTmpl, '') <> '' \n" +
                    "AND isNULL(TPS.U_pltgrt, '') <> '' \n" +
                    "GROUP BY WarrntTmpl, U_pltgrt", out Records);

                    CambiosPoliticaGarantia = CambiosPoliticaGarantia || ((Int32) Records > 0);

                    // Actualizar el texto de cualquier politica de garantia que haya cambiado.

                    mEtapa = "Insertando Actualizaciones de Politicas de Garantia";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute(
                    "UPDATE MA_PRODUCTOS_DATOS_GARANTIA SET \n" +
                    "TextoCompleto = TPS.U_pltgrt \n" +
                    "FROM (\n" +
                    "SELECT WarrntTmpl, U_pltgrt \n" +
                    "FROM #TMP_PRODUCTOS_SAP \n" +
                    "WHERE 1 = 1 \n" +
                    "AND isNULL(WarrntTmpl, '') <> '' \n" +
                    "AND isNULL(U_pltgrt, '') <> '' \n" +
                    "GROUP BY WarrntTmpl, U_pltgrt \n" +
                    ") TPS \n" +
                    "INNER JOIN MA_PRODUCTOS_DATOS_GARANTIA MPDG \n" +
                    "ON TPS.WarrntTmpl = MPDG.AliasPlantillaGarantia \n" +
                    "WHERE 1 = 1 \n" +
                    "AND TPS.U_pltgrt <> MPDG.TextoCompleto \n" +
                    "", out Records);

                    CambiosPoliticaGarantia = CambiosPoliticaGarantia || ((Int32)Records > 0);

                    if (CambiosPoliticaGarantia) // Mandar notificaciones de actualizacion a las cajas.
                    {

                        mEtapa = "Insertando Notificaciones Parametros de Caja Faltantes";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "INSERT INTO VAD20.DBO.MA_CAJA_PARAMETROS \n" +
                        "(IDGrupoConfiguracion, cCodigoCaja, cSeccionIni, cVariableIni, cValor, bActivo, cDefaultValue)  \n" +
                        "SELECT 'SYSTEM INTERNAL UPDATE' AS IDGrupoConfiguracion, C.c_Codigo AS cCodigoCaja, " + 
                        "'INTERNAL_USE' AS cSeccionIni, 'ReSync_MPDG' AS cVariableIni, " + 
                        "'1' AS cValor, 1 AS bActivo, '0' AS cDefaultValue " +
                        "FROM VAD20.DBO.MA_CAJA C \n" +
                        "LEFT JOIN VAD20.DBO.MA_CAJA_PARAMETROS P \n" +
                        "ON C.c_Codigo = P.cCodigoCaja \n" +
                        "AND P.cSeccionIni = 'INTERNAL_USE' \n" +
                        "AND P.cVariableIni = 'ReSync_MPDG' \n" +
                        "WHERE P.cCodigoCaja IS NULL \n" +
                        "", out Records);

                        mEtapa = "Actualizando Notificaciones de Parametros de Caja";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "UPDATE VAD20.DBO.MA_CAJA_PARAMETROS SET \n" +
                        "cValor = '1', bActivo = 1 \n" +
                        "WHERE 1 = 1 \n" +
                        "AND cSeccionIni = 'INTERNAL_USE' \n" +
                        "AND cVariableIni = 'ReSync_MPDG' \n" +
                        "", out Records);

                    }

                    if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                    {

                        Boolean CambiosInvXItems = false;

                        mEtapa = "Insertando Registros de Inventario X Items Nuevos";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "INSERT INTO MA_DEPOPROD_ITEMS (CodDeposito, CodProducto, IDSistema, IDInterno, \n" +
                        "IDSerialFabricante, IDLoteFabricante, FechaEntrada, FechaCreacion, \n" +
                        "FechaVencimiento, CantidadRegistrada, CantidadDisponible, CantidadComprometida, \n" +
                        "Comentarios, Status, FechaInicioGarantia, FechaFinGarantia, TipoRegistro) \n" +
                        "SELECT TMP.CodDeposito, TMP.CodProducto, TMP.IDSistema, TMP.IDInterno, \n" +
                        "TMP.IDSerialFabricante, TMP.IDLoteFabricante, TMP.FechaEntrada, TMP.FechaCreacion, \n" +
                        "TMP.FechaVencimiento, TMP.CantidadRegistrada, TMP.CantidadDisponible, TMP.CantidadComprometida, \n" +
                        "TMP.Comentarios, TMP.Status, TMP.FechaInicioGarantia, TMP.FechaFinGarantia, TMP.TipoRegistro \n" +
                        "FROM #TMP_INVENTARIO_ITEM TMP \n" +
                        "LEFT JOIN MA_DEPOPROD_ITEMS MDI \n" +
                        "ON TMP.CodDeposito = MDI.CodDeposito \n" +
                        "AND TMP.CodProducto = MDI.CodProducto \n" +
                        "AND TMP.IDSerialFabricante = MDI.IDSerialFabricante \n" +
                        "AND TMP.IDLoteFabricante = MDI.IDLoteFabricante \n" +
                        "AND TMP.TipoRegistro = MDI.TipoRegistro \n" +
                        "WHERE MDI.ID IS NULL \n" +
                        "", out Records);

                        CambiosInvXItems = CambiosInvXItems || ((Int32) Records > 0);

                        mEtapa = "Insertando Actualizaciones de Registro de Inventario X Items";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "UPDATE MA_DEPOPROD_ITEMS SET \n" +
                        "CodDeposito = TMP.CodDeposito, \n" +
                        "CodProducto = TMP.CodProducto, \n" +
                        "IDSistema = TMP.IDSistema, \n" +
                        "IDInterno = TMP.IDInterno, \n" +
                        "IDSerialFabricante = TMP.IDSerialFabricante, \n" +
                        "IDLoteFabricante = TMP.IDLoteFabricante, \n" +
                        "FechaEntrada = TMP.FechaEntrada, \n" +
                        "FechaCreacion = TMP.FechaCreacion, \n" +
                        "FechaVencimiento = TMP.FechaVencimiento, \n" +
                        "CantidadRegistrada = TMP.CantidadRegistrada, \n" +
                        "CantidadDisponible = TMP.CantidadDisponible, \n" +
                        "--CantidadComprometida = TMP.CantidadComprometida, -- AQUI no vamos a afectar por que es un campo de uso interno. \n" +
                        "Comentarios = TMP.Comentarios, \n" +
                        "Status = TMP.Status, \n" +
                        "FechaInicioGarantia = TMP.FechaInicioGarantia, \n" +
                        "FechaFinGarantia = TMP.FechaFinGarantia, \n" +
                        "TipoRegistro = TMP.TipoRegistro \n" +
                        "FROM #TMP_INVENTARIO_ITEM TMP \n" +
                        "INNER JOIN MA_DEPOPROD_ITEMS MDI \n" +
                        "ON TMP.CodDeposito = MDI.CodDeposito \n" +
                        "AND TMP.CodProducto = MDI.CodProducto \n" +
                        "AND TMP.IDSerialFabricante = MDI.IDSerialFabricante \n" +
                        "AND TMP.IDLoteFabricante = MDI.IDLoteFabricante \n" +
                        "AND TMP.TipoRegistro = MDI.TipoRegistro \n" +
                        "--WHERE MDI.ID IS NOT NULL \n" +
                        "", out Records);

                        CambiosInvXItems = CambiosInvXItems || ((Int32)Records > 0);

                    }

                    // Insertar Pendientes de Productos.

                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_PROD \n" +
                    "(c_Codigo, c_Descri, c_Departamento, c_Grupo, c_Subgrupo, c_Marca, n_CostoAct, n_CostoAnt, n_CostoPro, n_CostoRep, \n" +
                    "n_Precio1, n_Precio2, n_Precio3, c_Seriales, c_Presenta, n_Cantibul, n_Impuesto1, n_Activo, n_TipoPeso, \n" +
                    "cu_Descripcion_Corta, Cant_Decimales, c_CodMoneda, Add_Date, Update_Date, c_Observacio, " + 
                    Properties.Settings.Default.Stellar_CampoAliasTextoGarantia + ", nu_NivelClave, TipoCambio) \n" +
                    "SELECT T.c_Codigo, LEFT(T.c_Descri, " + mLngCamposProd["c_Descri"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Departamento, " + mLngCamposProd["c_Departamento"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Grupo, " + mLngCamposProd["c_Grupo"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Subgrupo, " + mLngCamposProd["c_Subgrupo"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Marca, " + mLngCamposProd["c_Marca"].DefinedSize.ToString() + "), " +
                    "T.n_CostoAct, T.n_CostoAnt, T.n_CostoPro, T.n_CostoRep, T.n_Precio1, T.n_Precio2, T.n_Precio3, T.c_Seriales, " +
                    "LEFT(T.c_Presenta, " + mLngCamposProd["c_Presenta"].DefinedSize.ToString() + "), T.n_Cantibul, T.n_Impuesto1, " +
                    "T.n_Activo, T.n_TipoPeso, LEFT(T.cu_Descripcion_Corta, " + mLngCamposProd["cu_Descripcion_Corta"].DefinedSize.ToString() + "), " +
                    "T.Cant_Decimales, MON.CodMonedaProd, T.Add_Date, T.Update_date, T.c_Observacio, T.WarrntTmpl, T.nu_NivelClave, 0 AS TipoCambio \n" +
                    "FROM #TMP_PRODUCTOS_SAP T CROSS JOIN (SELECT TOP 1 c_CodMoneda AS CodMonedaProd FROM MA_MONEDAS WHERE b_Preferencia = 1) MON \n" +
                    "--ON TMP.c_Departamento = GRU.c_Departamento \n" +
                    "--AND TMP.c_Grupo = GRU.c_Codigo \n" +
                    "--WHERE isNULL(GRU.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                    , out Records);

                    SchemaRs.Close();

                }

                SchemaRs = null;

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Marcar Cambios como procesados en SAP.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String mQueryBase = "UPDATE \"BITACORA\" SET \n" +
                "\"PROCESADO_ST\" = 1 \n" +
                "WHERE \"OBJECT_TYPE\" = 4 \n" +
                "AND \"LECTURA_ST\" = 1 \n" +
                "AND \"PROCESADO_ST\" = 0";

                if (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD)
                    mRs.DoQuery(mQueryBase + " AND \"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "'");
                else
                    mRs.DoQuery(mQueryBase);

                mEtapa = "Insercion de Pendientes de Productos finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                if (ActualizarTimeStampUltimaActualizacionInventarioXItems)
                {
                    Properties.Settings.Default.SAP_SincronizarInventarioXItems_UltimaActualizacion = DateTime.Now;
                    Properties.Settings.Default.Save();
                }

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                {
                    Properties.Settings.Default.Stellar_MasterData_CargaInicial = false;
                    Properties.Settings.Default.Save();
                }

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);

                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                Program.Logger.EscribirLog(Any, "ActualizarProductosYCodigos(); Etapa: " + mEtapa);

            }

        }

        private static void ActualizarCategorias()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_DEPARTAMENTOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_GRUPOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_SUBGRUPOS_SAP", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT c_Codigo AS c_Departamento, c_Descripcio, " + 
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_DEPARTAMENTOS_SAP FROM MA_DEPARTAMENTOS WHERE 1 = 2", out Records);
                
                LocalDB.Execute("SELECT c_Departamento AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS Dpto, " +
                "c_Codigo AS c_Grupo, c_Descripcio, " + 
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_GRUPOS_SAP FROM MA_GRUPOS WHERE 1 = 2", out Records);
                
                LocalDB.Execute("SELECT c_In_Departamento AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS Dpto, " +
                "c_In_Grupo AS c_Grupo, CAST('' AS NVARCHAR(MAX)) AS Grupo, " +
                "c_Codigo AS c_Subgrupo, c_Descripcio, " +
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_SUBGRUPOS_SAP FROM MA_SUBGRUPOS WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String SQLDepartamentos = String.Empty +
                "SELECT * FROM ( " + "\n" + 
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"c_Descripcio\", COUNT(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"DPTMP\" LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"DPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"DPTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", " + "\n" +
                "\"DPTO\".\"ItmsGrpNam\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITB\" AS \"DPTO\" LEFT JOIN \"OITM\" AS \"DPTMP\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"DPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"DPTMP\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") \"TB\" ORDER BY \"c_Departamento\" " + "\n";

                String SQLGrupos = String.Empty +
                "SELECT * FROM ( " + "\n" +
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"Dpto\",  " + "\n" +
                "0 AS \"c_Grupo\", 'No Categorizado' AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"GRPTMP\" LEFT JOIN \"@GRUPO_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"GRPTMP\".\"U_GRUPO\" " + "\n" +
                "LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"GRPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL OR \"GRUPO\".\"Code\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"GRPTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", \"DPTO\".\"ItmsGrpNam\" AS \"Dpto\",  " + "\n" +
                "\"GRUPO\".\"Code\" AS \"c_Grupo\", \"GRUPO\".\"Name\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"@GRUPO_INV\" AS \"GRUPO\" INNER JOIN \"OITM\" AS \"GRPTMP\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"GRPTMP\".\"U_GRUPO\" " + "\n" +
                "INNER JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"GRPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"GRPTMP\".\"U_GRUPO\" IS NOT NULL AND \"GRPTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"GRPTMP\".\"ItmsGrpCod\", \"GRPTMP\".\"U_GRUPO\", " + "\n" +
                "\"GRUPO\".\"Code\", \"GRUPO\".\"Name\", " + "\n" +
                "\"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") TB ORDER BY \"c_Departamento\", \"c_Grupo\" " + "\n";

                String SQLSubgrupos = String.Empty +
                "SELECT * FROM ( " + "\n" +
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"Dpto\", " + "\n" +
                "0 AS \"c_Grupo\", 'No Categorizado' AS \"Grupo\", " + "\n" +
                "0 AS \"c_Subgrupo\", 'No Categorizado' AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"SUBTMP\" LEFT JOIN \"@CLASES_INV\" AS \"SUBGRUPO\" " + "\n" +
                "ON \"SUBGRUPO\".\"Code\" = \"SUBTMP\".\"U_CLASE\" " + "\n" +
                "LEFT JOIN \"@CLASES_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"SUBTMP\".\"U_GRUPO\" " + "\n" +
                "LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"SUBTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL OR \"GRUPO\".\"Code\" IS NULL OR \"SUBGRUPO\".\"Code\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"SUBTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", \"DPTO\".\"ItmsGrpNam\" AS \"Dpto\", " + "\n" +
                "\"GRUPO\".\"Code\" AS \"c_Grupo\", \"GRUPO\".\"Name\" AS \"Grupo\", " + "\n" +
                "\"SUBGRUPO\".\"Code\" AS \"c_Subgrupo\", \"SUBGRUPO\".\"Name\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"@CLASES_INV\" AS \"SUBGRUPO\" INNER JOIN \"OITM\" AS \"SUBTMP\" " + "\n" +
                "ON \"SUBGRUPO\".\"Code\" = \"SUBTMP\".\"U_CLASE\" " + "\n" +
                "INNER JOIN \"@GRUPO_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"SUBTMP\".\"U_GRUPO\" " + "\n" +
                "INNER JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"SUBTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"SUBTMP\".\"U_CLASE\" IS NOT NULL AND \"SUBTMP\".\"U_GRUPO\" IS NOT NULL AND \"SUBTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"SUBTMP\".\"ItmsGrpCod\", \"SUBTMP\".\"U_GRUPO\", \"SUBTMP\".\"U_CLASE\", " + "\n" +
                "\"SUBGRUPO\".\"Code\", \"SUBGRUPO\".\"Name\", " + "\n" +
                "\"GRUPO\".\"Code\", \"GRUPO\".\"Name\", " + "\n" +
                "\"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") TB ORDER BY \"c_Departamento\", \"c_Grupo\", \"c_Subgrupo\" " + "\n";

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp;

                //

                mEtapa = "Consultando Departamentos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLDepartamentos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Departamentos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_DEPARTAMENTOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Grupos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLGrupos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Grupos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_GRUPOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        LocalRs.Fields["Dpto"].Value = mRs.Fields.Item("Dpto").Value;
                        mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();
                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Subgrupos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLSubgrupos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Subgrupos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_SUBGRUPOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        LocalRs.Fields["Dpto"].Value = mRs.Fields.Item("Dpto").Value;
                        mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();
                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);
                        LocalRs.Fields["Grupo"].Value = mRs.Fields.Item("Grupo").Value;
                        mDatoTemp = mRs.Fields.Item("c_Subgrupo").Value.ToString();
                        LocalRs.Fields["c_Subgrupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Subgrupo"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros TR_PEND a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                mEtapa = "Insertando Actualizaciones de Departamentos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_DEPARTAMENTOS \n" +
                "(c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_DEPARTAMENTOS_SAP TMP LEFT JOIN MA_DEPARTAMENTOS DEP \n" +
                "ON TMP.c_Departamento = DEP.c_Codigo \n" +
                "WHERE IsNULL(DEP.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                mEtapa = "Insertando Actualizaciones de Grupos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_GRUPOS \n" +
                "(c_Departamento, c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Grupo, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_GRUPOS_SAP TMP LEFT JOIN MA_GRUPOS GRU \n" +
                "ON TMP.c_Departamento = GRU.c_Departamento \n" +
                "AND TMP.c_Grupo = GRU.c_Codigo \n" +
                "WHERE isNULL(GRU.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                mEtapa = "Insertando Actualizaciones de Subgrupos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_SUBGRUPOS \n" +
                "(c_In_Departamento, c_In_Grupo, c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Grupo, TMP.c_Subgrupo, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_SUBGRUPOS_SAP TMP LEFT JOIN MA_SUBGRUPOS SUB \n" +
                "ON TMP.c_Departamento = SUB.c_In_Departamento \n" +
                "AND TMP.c_Grupo = SUB.c_In_Grupo \n" +
                "AND TMP.c_Subgrupo = SUB.c_Codigo \n" +
                "WHERE isNULL(SUB.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Insercion de Pendientes de Categorias finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);
                
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                
                Program.Logger.EscribirLog(Any, "ActualizarCategorias(); Etapa: " + mEtapa);

            }

        }

        private static void ActualizarMotivosDevolucion()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_MA_AUX_GRUPO", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT cs_Grupo, cs_Tipo, cs_Codigo_Opcion " +
                "INTO #TMP_MA_AUX_GRUPO FROM MA_AUX_GRUPO WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String SQLTiposDevolucion = String.Empty +
                "SELECT * FROM \"@TIPOS_ANULACION\" ";

                String SQLMotivosDevolucion = String.Empty +
                "SELECT * FROM \"@TIPO_DEV_VTA\" ";

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp;

                //

                mEtapa = "Consultando Tipos Devolucion en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLTiposDevolucion);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Tipos Devolucion";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_MA_AUX_GRUPO WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("Code").Value.ToString();
                        LocalRs.Fields["cs_Codigo_Opcion"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Codigo_Opcion"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("Name").Value.ToString();
                        LocalRs.Fields["cs_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Grupo"].DefinedSize);
                        LocalRs.Fields["cs_Tipo"].Value = "TIPO_DEV_SBO";

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Motivos de Devolucion en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLMotivosDevolucion);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Motivos Devolucion";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_MA_AUX_GRUPO WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("Code").Value.ToString();
                        LocalRs.Fields["cs_Codigo_Opcion"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Codigo_Opcion"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("Name").Value.ToString();
                        LocalRs.Fields["cs_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Grupo"].DefinedSize);
                        LocalRs.Fields["cs_Tipo"].Value = "MOTIVOS_DEV";

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros MA_AUX_GRUPO a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                LocalDB.Execute("DELETE FROM MA_AUX_GRUPO WHERE cs_Tipo = 'TIPO_DEV_SBO' ", out Records);
                LocalDB.Execute("DELETE FROM MA_AUX_GRUPO WHERE cs_Tipo = 'MOTIVOS_DEV' ", out Records);

                LocalDB.Execute(
                "INSERT INTO MA_AUX_GRUPO \n" +
                "(cs_Tipo, cs_Grupo, cs_Codigo_Opcion) \n" +
                "SELECT cs_Tipo, cs_Grupo, cs_Codigo_Opcion \n" +
                "FROM #TMP_MA_AUX_GRUPO \n" +
                "", out Records);

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Insercion de Datos de Grupo finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);

                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                
                Program.Logger.EscribirLog(Any, "ActualizarMotivosDevolucion(); Etapa: " + mEtapa);

            }

        }

    }

}
