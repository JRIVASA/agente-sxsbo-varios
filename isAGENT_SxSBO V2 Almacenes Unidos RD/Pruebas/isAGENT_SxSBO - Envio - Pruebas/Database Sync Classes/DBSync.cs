﻿using isAGENT_SxSBO.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SAPbobsCOM;
using Sap.Data.Hana;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.Compatibility.VB6;
using System.IO;
using System.Diagnostics;

namespace isAGENT_SxSBO.Database_Sync_Classes
{
    class DBSync
    {
        // SELECT * FROM VAD20..MA_PAGOS WHERE cs_Sync_SxS IN ('', '!!!!!!!!!!', '..........', '-_-_-_-_-')

        public const String RegistrosPendientesPorCorrida = "-_-_-_-_-";
        public const String RegistrosPendientesPorLote = "..........";
        public const String RegistrosPendientesFallidos = "!!!!!!!!!!";
        public const String RegistrosNuevos = "";

        public static long nLotesMaxEjecucion = 0;
        public static long nLotesProcesados = 0;
        public static long nRegistrosLote = 0;

        private static void RecuperacionInicialDeCredenciales()
        {
            
            dynamic mClsTmp = null;

            Program.mStellarDBUser = Properties.Settings.Default.Stellar_SQLUser;
            Program.mStellarDBPass = Properties.Settings.Default.Stellar_SQLPass;

            if (!(String.Equals(Program.mStellarDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mStellarDBPass.Length == 0))
            {

                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mStellarDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBUser);
                    Program.mStellarDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPDBUser = Properties.Settings.Default.SAP_DBUser;
            Program.mSAPDBPass = Properties.Settings.Default.SAP_DBPass;

            if (!(String.Equals(Program.mSAPDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mSAPDBPass.Length == 0))
            {
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBUser);
                    Program.mSAPDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPUserLogin = Properties.Settings.Default.SAP_CompanyUser;
            Program.mSAPUserPwd = Properties.Settings.Default.SAP_CompanyPass;

            //if (!(String.Equals(Program.mUserFTP, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassFTP.Length == 0))
            //{
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPUserLogin = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserLogin);
                    Program.mSAPUserPwd = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserPwd);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            //}

        }

        private static SqlConnection ConexionStellar()
        {

            SqlConnection mCnLocal = null;

        Retry:

            try
            {
                mCnLocal = (Properties.Settings.Default.Stellar_TrustedConnection ?
                Functions.getAlternateTrustedConnection(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, 30) :
                Functions.getAlternateConnection(Properties.Settings.Default.Stellar_SQLServerName,
                Properties.Settings.Default.Stellar_SQLDBName, 
                Program.mStellarDBUser, Program.mStellarDBPass, 30));
                mCnLocal.Open();
            }
            catch (SqlException SQLAny)
            {

                if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                {
                    
                    dynamic mClsTmp = null;
                    
                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");
                    
                    if (mClsTmp == null) goto Otros;
                    
                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");
                    
                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);
                    
                    if  (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog( "Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }
                    
                }

                Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            if (mCnLocal.State != ConnectionState.Open)
            {
                Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor Stellar.");
                System.Environment.Exit(0);
            }

            return mCnLocal;

        }

        private static String CadenaConexionADO(String pServidor, String pBD, String pUser = "SA", String pPassword = "", 
        Boolean pTrustedConnection = false){
            
            String mCadenaConexion = String.Empty;

            if (pTrustedConnection)
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                "Integrated Security=SSPI;";
            else
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                (pUser.isUndefined() || pPassword.isUndefined() ? "Persist Security Info=False;User ID=" + pUser + ";"
                : "Persist Security Info=True;User ID=" + pUser + ";Password=" + pPassword + ";");

            return mCadenaConexion;

        }

        private static ADODB.Connection ConexionStellar(Boolean pADO = true)
        {

            ADODB.Connection mCnLocal = null;

        Retry:

            try
            {

                mCnLocal = new ADODB.Connection();

                mCnLocal.ConnectionTimeout = Properties.Settings.Default.Stellar_ConnectionTimeout;
                //mCnLocal.Provider = "SQLOLEDB.1";
                mCnLocal.ConnectionString = CadenaConexionADO(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, Program.mStellarDBUser, 
                Program.mStellarDBPass, Properties.Settings.Default.Stellar_TrustedConnection);

                mCnLocal.Open();

            }
            catch (System.Runtime.InteropServices.COMException SQLAny)
            {

                //if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                if (SQLAny.HResult == (-2147217843))
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            return mCnLocal;

        }

        private static SAPbobsCOM.Company ConexionSAP(Boolean ManagedObject)
        {
            SAPbobsCOM.Company Obj = ConexionSAP();
            return Obj;
        }

        private static dynamic ConexionSAP()
        {

            dynamic mCnSAP = null;

            Int32 ErrorNumber; String ErrorDesc;

        Retry:

            try
            {

                //mCnSAP = new Company();
                mCnSAP = Functions.SafeCreateObject("SAPbobsCOM.Company" + Program.DI_API_Version);

	            mCnSAP.Server = Properties.Settings.Default.SAP_Server;
	            mCnSAP.LicenseServer = Properties.Settings.Default.SAP_LicenseServer;

                mCnSAP.DbServerType = (BoDataServerTypes)Properties.Settings.Default.SAP_ServerTypeID;  //BoDataServerTypes.dst_MSSQL2014;
	            mCnSAP.DbUserName = Program.mSAPDBUser;
	            mCnSAP.DbPassword = Program.mSAPDBPass;
	            mCnSAP.language = (BoSuppLangs) Properties.Settings.Default.SAP_LangID; //BoSuppLangs.ln_Spanish_La;
                mCnSAP.UseTrusted = false;

                mCnSAP.CompanyDB = Properties.Settings.Default.SAP_DBName;
                mCnSAP.UserName = Program.mSAPUserLogin;
                mCnSAP.Password = Program.mSAPUserPwd;
	        
	            if(mCnSAP.Connect() != 0) 
                {
                
	                mCnSAP.GetLastError(out ErrorNumber, out ErrorDesc);
                    mCnSAP.Disconnect();

                    throw new Exception(ErrorDesc) { HelpLink = ErrorNumber.ToString() };

	            }

            }
            catch (Exception SQLAny)
            {

                if (SQLAny.HelpLink == "-4008" || SQLAny.HelpLink == "-132")
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPDBUser = TmpVar[0];
                        Program.mSAPDBPass = TmpVar[1];
                        Properties.Settings.Default.SAP_DBUser = TmpVar[2];
                        Properties.Settings.Default.SAP_DBPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        //goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server, 
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP",  Program.mCnLocal);
                    }

                /*}
                else if (SQLAny.HelpLink == "-8023")
                {*/

                    //dynamic mClsTmp = null;
                    mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión a la Compañia SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    //dynamic 
                    TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPUserLogin = TmpVar[0];
                        Program.mSAPUserPwd = TmpVar[1];
                        Properties.Settings.Default.SAP_CompanyUser = TmpVar[2];
                        Properties.Settings.Default.SAP_CompanyPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server,
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor SAP.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, 
                "Estableciendo conexión al Servidor SAP.", "DBSync.ConexionSAP()", "Servidor SAP",
                Properties.Settings.Default.SAP_Server,
                (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                Console.WriteLine(SQLAny);

            }

            if (mCnSAP != null)
            {
                if (!mCnSAP.Connected)
                {
                    Marshal.ReleaseComObject(mCnSAP);
                    Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor SAP.");
                    System.Environment.Exit(0);
                }
            }
            else
            {
                Program.Logger.EscribirLog("Verifique que SAP BUSINESS ONE y la DI API esten correctamente instalados.");
                System.Environment.Exit(0);
            }

            return mCnSAP;

        }

        public static void IniciarAgente()
        {

            ADODB.Connection mCnLocal = null;
            Company SAPCon = null;
            //dynamic SAPCon = null;
            //HanaConnection HanaDB = null;
            //dynamic HanaDB = null;

            try
            {

                //Functions.LoadSettings();

                RecuperacionInicialDeCredenciales();

                DateTime tsInicioProc, tsFinProc;

                tsInicioProc = DateTime.Now;

                mCnLocal = ConexionStellar(true);
                Program.mCnLocal = mCnLocal;

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo de Conexión Stellar: "));

                if (mCnLocal == null)
                {
                    Program.Logger.EscribirLog("Conexión al Servidor Stellar no disponible / Timeout.");
                    System.Environment.Exit(500);
                }

                if(mCnLocal.State != Convert.ToInt32(ADODB.ObjectStateEnum.adStateOpen))
                {
                    Program.Logger.EscribirLog("Conexión al Servidor Stellar en estado no abierto.");
                    System.Environment.Exit(500);
                }

                Program.DI_API_Version = Properties.Settings.Default.APP_DEFINED_SAP_DI_API_VERSION;

                tsInicioProc = DateTime.Now;

                SAPCon = ConexionSAP();
                Program.mCnSAP = SAPCon;

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo de Conexión SAP: "));

                if (SAPCon == null)
                {
                    Program.Logger.EscribirLog("Conexión al Servidor SAP nula.");
                    System.Environment.Exit(999);
                }

                if (!SAPCon.Connected)
                {
                    Program.Logger.EscribirLog("Warning: Conexión al Servidor SAP no establecida.");
                    System.Environment.Exit(998);
                }

                // Comun para ambos

                Program.ListaProductosCupon = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.Stellar_ListaProductosCupon);

                if (Program.ListaProductosCupon == null) Program.ListaProductosCupon = new Dictionary<String, String>();

                // Iniciar Modalidad.

                if (Properties.Settings.Default.ModalidadAgente.Left(1).Equals("1"))
                {
                    ActualizarDatosMaestrosPendientes();
                }
                else if (Properties.Settings.Default.ModalidadAgente.Equals("2"))
                {

                    if (!Properties.Settings.Default.ObtenerListasAsociacionBD)
                    {

                        Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionMonedas);
                        Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionBancos);
                        Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionFormaPago);

                        if (Program.ListaAsociacionMonedas == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos.");
                            return;
                        }

                        if (Program.ListaAsociacionFormaPago == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos.");
                            return;
                        }

                        if (Program.ListaAsociacionBancos == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos.");
                            return;
                        }

                    }

                    if (Properties.Settings.Default.SAP_CodLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Codigo de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    if (Properties.Settings.Default.SAP_AliasLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Alias de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    /*if (Properties.Settings.Default.SAP_ClaveOpcionesLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Clave de Localidad de SAP no definida. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }*/

                    if (Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad.isUndefined())
                    {
                        Program.Logger.EscribirLog("Almacen de SAP no definido. Verifique los parámetros en el archivo Stellar.Config");
                        return;
                    }

                    Program.gCorrelativo = Functions.Correlativo(mCnLocal, "Corrida_Agente_SAP_Trans");

                    if (!Program.gCorrelativo.isUndefined())
                    {

                        Program.ListaAsociacionTipoPrecio = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.SAP_Linea_ListaTipoPrecio);

                        Program.TaxList1 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList1);
                        Program.TaxList2 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList2);
                        Program.TaxList3 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList3);

                        if (Program.TaxList1 == null && Program.TaxList2 == null && Program.TaxList3 == null)
                        {
                            Program.Logger.EscribirLog("Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos");
                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos",
                            "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                            return;
                        }

                        if (Properties.Settings.Default.ObtenerListasAsociacionBD)
                        {

                            String mLista;
                            ADODB.Recordset x; Object Records = null;

                            try
                            {

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_Codigo + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_BANCOS WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionBancos == null && !Properties.Settings.Default.SAP_FormasPagoFallbackBanco)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Bancos inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ';' + c_CodDenomina + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_DENOMINACIONES WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionFormaPago == null && !Properties.Settings.Default.SAP_FormasPagoFallbackEfectivo)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Formas de Pago inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                                mLista = "";

                                x = Program.mCnLocal.Execute(
                                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ':' + CodigoExterno AS 'data()' FROM\n" +
                                "VAD10.DBO.MA_MONEDAS WHERE CodigoExterno <> ''\n" +
                                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                                );

                                if (!x.EOF)
                                {
                                    mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                                }

                                Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(mLista);

                                if (Program.ListaAsociacionMonedas == null && !Properties.Settings.Default.SAP_FormasPagoFallbackMoneda)
                                {
                                    Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos");
                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Monedas inválidos o no establecidos",
                                    "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                    return;
                                }

                            }
                            catch (Exception ex)
                            {
                                Program.Logger.EscribirLog(ex, "Error al obtener datos de asociaciones por BD.");
                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Error al obtener datos de asociaciones por BD.",
                                "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, (ex.HResult + "|" + ex.Message), "Validacion Stellar", Program.mCnLocal);
                                return;
                            }

                        }

                        nRegistrosLote = Properties.Settings.Default.nRegistrosLote;
                        nLotesMaxEjecucion = Properties.Settings.Default.nLotesCorrida;

                        Boolean result = PrepararBD(ref mCnLocal);

                        SincronizarVentas();

                    }
                    else
                    {
                        throw new Exception("Falla al obtener correlativo de ejecución de Agente.");
                    }

                }

            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Ejecutando Agente.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Ejecutando Agente.", "DBSync.IniciarAgente()", "Corrida Agente",
                Program.gCorrelativo, Program.mCnLocal);
            }
            finally
            {

                if (SAPCon != null)
                if (SAPCon.Connected)
                    SAPCon.Disconnect();

                if (mCnLocal != null)
                if (mCnLocal.State != Convert.ToInt32(ADODB.ObjectStateEnum.adStateClosed))
                    mCnLocal.Close();

            }

        }

        public static Boolean PrepararBD(ref ADODB.Connection pCn)
        {

            Boolean result = true; Object Records;
            long mCmdTOut_Original = 0; Boolean Reintentando = false;

            mCmdTOut_Original = Properties.Settings.Default.Stellar_CommandTimeout;

        Retry:
            
            for (int i = 0; i <= 0; i++) {
                
                try {

                    result = true;

                    Properties.Settings.Default.Stellar_CommandTimeout = 0;

                    if (!Functions.ExisteCampoTabla("cs_Sync_SxS", "MA_PAGOS", ref pCn, "VAD20")) {
                        pCn.Execute("ALTER TABLE [VAD20].[DBO].[MA_PAGOS]" + "\n" + 
                        "ADD cs_Sync_SxS NVARCHAR(50) NOT NULL CONSTRAINT" + "\n" +
                        "[MA_PAGOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')", out Records);
            
                        result = result && true;
                    }

                    pCn.Execute("IF NOT EXISTS(SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] WHERE ID = " + Program.LogIDAuditoria + ")" + "\n" +
                    "INSERT INTO [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] (ID, Descripcion, DescResourceID)" + "\n" +
                    "SELECT " + Program.LogIDAuditoria  + " AS ID, 'Log Error / Evento isAGENT_SxSBO', 0 AS DescResourceID" + "\n" +
                    "", out Records);

                    if (!Functions.ExisteCampoTabla("CodigoRetorno", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD CodigoRetorno NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_CodigoRetorno] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    if (!Functions.ExisteCampoTabla("AccionRealizada", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD AccionRealizada NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_AccionRealizada] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    /*if (gTransferirCierres) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_CIERRES", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_CIERRES]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_CIERRES_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    /*if (gTransferirDepositosCP) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_DEPOSITOS", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_DEPOSITOS]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_DEPOSITOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    return result;

                }
                catch (Exception SQLAny)
                {
                    Program.Logger.EscribirLog(SQLAny, "PrepararBD:");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                        "Ejecutando validaciones y requerimientos de Base de Datos.", "DBSync.PrepararBD()", "Servidor Stellar",
                        Properties.Settings.Default.Stellar_SQLServerName,
                        (SQLAny.HResult + "|" + SQLAny.Message), "PrepararBD", Program.mCnLocal);
                        if (!Reintentando) {
                            Reintentando = true;
                            goto Retry;
                        } else {
                        // Sin Exito.
                    }
                    
                }

            }

            return result;
            
        }

        private static void SincronizarVentas()
        {
            /*
            if (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
            HayDatosPendientes(RegistrosPendientesPorLote) ||
            HayDatosPendientes(RegistrosPendientesFallidos))
            {
                ProcesarCorrida(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
            }

            if (nLotesProcesados < nLotesMaxEjecucion)
                if (MarcarRegistros(RegistrosPendientesPorCorrida))
                { // Procesar registros nuevos.
                    ProcesarCorrida();
                }
                */

            // IR DIRECTO AL METODO DE PROBAR. SE PROBARAN TRANSACCIONES CON DATOS A PIE
            ConstruirRegistrosDeVentas(false);
        }

        private static Boolean HayDatosPendientes(String pEstatus)
        {

            try
            {

                ADODB.Recordset RsDatosPendientes; Object Records;

                String ExcluirVNF = String.Empty;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal, "VAD20"))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                RsDatosPendientes = Program.mCnLocal.Execute("SELECT COUNT(*) AS Registros " +
                "FROM [VAD20].[DBO].[MA_PAGOS] WHERE cs_Sync_SxS = " +
                "'" + pEstatus + "'" + ExcluirVNF, out Records);

                return (Convert.ToDouble(RsDatosPendientes.Fields["Registros"].Value) > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ProcesarCorrida()
        {

            if (HayDatosPendientes(RegistrosPendientesFallidos))
                ConstruirRegistrosDeVentas(true); // Intentar procesar los Fallidos una sola vez.

            while (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
            HayDatosPendientes(RegistrosPendientesPorLote))
            {
                if (nLotesProcesados < nLotesMaxEjecucion)
                    ProcesarLote();
                else
                    break;
            }

        }

        private static void ProcesarLote()
        {

            if (HayDatosPendientes(RegistrosPendientesPorLote))
            {
                // Procesar primero los pendientes
                // Antes de marcar el próximo Lote.
                ConstruirRegistrosDeVentas();
                nLotesProcesados++;
            }

            if (nLotesProcesados < nLotesMaxEjecucion)
                if (MarcarRegistros(RegistrosPendientesPorLote))
                {
                    ConstruirRegistrosDeVentas();
                    nLotesProcesados++;
                }

        }

        private static Boolean MarcarRegistros(String pEstatus, Boolean pFallidos = false)
        {

            try
            {

                long RegistrosAfectados = 0; String pEstatusPrevio = String.Empty; String mRegistrosXLote = String.Empty;

                String ExcluirVNF = String.Empty; String SQLMarcaje = String.Empty; Object Records;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal, "VAD20"))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                switch (pEstatus)
                {
                    case RegistrosPendientesPorCorrida:
                        pEstatusPrevio = "'" + RegistrosNuevos + "'";
                        break;
                    case RegistrosPendientesPorLote:
                        if (pFallidos)
                            pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                        else
                        {
                            pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                            mRegistrosXLote = "AND (c_Sucursal + c_Concepto + c_Numero) IN (" + "\n" +
                            "SELECT TOP (" + nRegistrosLote + ") (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                            "WHERE cs_Sync_SxS = " + pEstatusPrevio + ExcluirVNF + " ORDER BY f_Fecha, f_Hora, ID)";
                        }
                        break;
                    default: // Guardados con Exito.
                        pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                        break;
                }

                SQLMarcaje = "UPDATE [VAD20].[DBO].[MA_PAGOS] SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" +
                "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

                if (Program.gDebugMode) Program.Logger.EscribirLog(SQLMarcaje);

                Program.mCnLocal.Execute(SQLMarcaje, out Records);

                RegistrosAfectados = Convert.ToInt64(Records);

                return (RegistrosAfectados > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ConstruirRegistrosDeVentas(Boolean pReprocesarFallidos = false)
        {

            ADODB.Recordset RsVentas = null; ADODB.Recordset RsItems = null; ADODB.Recordset RsDetPagos = null;
            ADODB.Recordset RsImpuestos = null; ADODB.Recordset RsDatosFiscales;
            ADODB.Recordset RsPendXEntregaMA = null; ADODB.Recordset RsPendXEntregaTR = null;
            ADODB.Recordset RsSeriales = null;
            ADODB.Recordset RsDocRel = null;
            ADODB.Recordset RsDocRelDatosFiscales = null;

            System.Collections.ArrayList RegistrosFallidos = new System.Collections.ArrayList();

            Double Cont = 0; Int32 ItmCount = 0;
            String mUltDoc = String.Empty; Double mUltTot = 0; DateTime mUltFec = DateTime.Now;
            String mDocRel = String.Empty; String Related_StellarDocID = String.Empty;

            Dictionary<String, String[]> LineRelation = new Dictionary<String, String[]>();
            String StellarItemID = String.Empty; String StellarItemLn = String.Empty;

            Dictionary<String, Object[]> ListaPendXEntrega = null;

            String mEtapa = String.Empty;
            SAPbobsCOM.Recordset SAPmRs; ADODB.Recordset TmpRs;
            String mDatoTemp;

            Object RecordsAffected;

            DateTime tsInicioTrans = DateTime.Now, tsFinTrans;
            DateTime tsInicioProc, tsFinProc;

            SAPbobsCOM.Company SAP = Program.mCnSAP;
            //dynamic SAP = Program.mCnSAP;
            ADODB.Connection mCn = Program.mCnLocal;

            //Double PrcTest = 500, PrcFull = 590;
            //Double PrcTest = 800.85, PrcFull = 945.00;
            //Double PrcTest = 0.47, PrcFull = 0.5546;
            // PrcTest = 61.86, PrcFull = 72.9948;
            Double PrcTest = 4310.17, PrcFull = 5086;
            //Double PrcTest = 121528.81, PrcFull = 143403.9958;
            
            try
            {

                tsInicioTrans = DateTime.Now;

                mEtapa = "Iniciando Procesamiento de Documentos Ventas / Devoluciones POS. Iniciando Transaccion SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                SAP.StartTransaction();

                SAPbobsCOM.Documents Trans; SAPbobsCOM.Documents AffectedTrans;

                String TransID = "000133";
                String StellarDocID = TransID;

                Double mTotalDocStellar = 0, mTotalDocSAP = 0;

                ProductTrees oTree;
                Boolean PCCargado;
                Int32 PCCUpdated;
                
                oTree = (ProductTrees)SAP.GetBusinessObject(BoObjectTypes.oProductTrees);
                
                //PCCargado = oTree.GetByKey("128361");
                PCCargado = oTree.GetByKey("C1500");
                PCCargado = false;
                if (PCCargado)
                {
                    
                    oTree.TreeType = BoItemTreeTypes.iSalesTree;
                    oTree.HideBOMComponentsInPrintout = BoYesNoEnum.tNO;
                    PCCUpdated = oTree.Update();
                    if (PCCUpdated != 0)
                    {

                        Int32 mErrorNumber; String mErrorDesc = String.Empty;

                        SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                        Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                        Trans = null; //Trans.Cancel();
                        if (SAP.InTransaction)
                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                        tsFinTrans = DateTime.Now;

                        Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                        goto Continue;

                    }
                }
                


                Trans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);
                Trans = SAP.GetBusinessObject(BoObjectTypes.oDrafts);
                Trans.DocObjectCode = BoObjectTypes.oInvoices;

                Int32 Serie = 0; Int32 SerieNextNum = 0;

                if (!Properties.Settings.Default.SAP_SerieDocumentoFactura.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoFactura.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento de Venta";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oInvoices).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Venta para esta localidad.");

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            Trans.Series = Serie;
                        }

                    }
                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoFactura, out Serie))
                        Trans.Series = Serie;


                Trans.Comments = "Stellar test" + ". " + TransID;

                // Asignar datos generales de cabecero de transacción.

                mEtapa = "Asignando datos generales de cabecero de transacción";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                Trans.CardCode = Properties.Settings.Default.SAP_ClienteContado;

                //mTotalDocStellar = 3 * PrcFull;
                //mTotalDocStellar = 0.04 * PrcFull;
                mTotalDocStellar = 1 * PrcFull;

                SAPbobsCOM.Fields CamposOrg;

                CamposOrg = Trans.UserFields.Fields;

                CamposOrg.Item("U_FACNIT").Value = "123456";
                CamposOrg.Item("U_FACNOM").Value = "Prueba Generica";

                Trans.Address = "Prueba Dir";

                Trans.DocDate = new DateTime(2020, 10, 05); //DateTime.Today.AddDays(-1);
                Trans.DocDueDate = Trans.DocDate;
                //Trans.TaxDate = Trans.DocDate; // Autocolocado en SAP

                CamposOrg.Item("U_FACFECHA").Value = Trans.DocDate;
                /*
                switch (RsVentas.Fields["c_Rif"].Value.ToString().Length)
                {
                    case 9:
                        CamposOrg.Item("U_TNEGOCIO").Value = "1";
                        break;
                    case 11:
                        CamposOrg.Item("U_TNEGOCIO").Value = "2";
                        break;
                    default:
                        CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;
                        break;
                };
                */

                CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;

                CamposOrg.Item("U_CGASTOS").Value = "09";

                CamposOrg.Item("U_CAJA").Value = "999";

                CamposOrg.Item("U_CUADRE").Value = 0;
                CamposOrg.Item("U_FACSERIE").Value = "02";

                //

                DateTime fv6 = new DateTime(DateTime.Today.Year, 12, 31);

                CamposOrg.Item("U_FV6").Value = fv6;

                //

                Trans.TrackingNumber = TransID;

                //Trans.JournalMemo = String.Empty; // Autocolocado en SAP
                Trans.Rounding = BoYesNoEnum.tNO;

                Trans.POSCashierNumber = 123;

                //Trans.ControlAccount = "1110101";
                //Trans.ControlAccount = "1110101";

                mEtapa = "Ingresando Items del Documento";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                // Registrar los productos de la transacción.

                ItmCount = 0;

                IDocument_Lines Line = Trans.Lines;
                //dynamic Line = Trans.Lines;

                /*
                ItmCount++;

                //

                //Line.ItemCode = "106902";
                Line.ItemCode = "RT000003";

                Line.Quantity = 0.1;

                Line.UnitPrice = 0;

                Line.DiscountPercent = 0;
                //Line.TaxCode = "EXE";
                Line.TaxCode = "ITBIS18";

                CamposOrg = Line.UserFields.Fields;

                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;

                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                Line.CostingCode2 = "L01";
                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                Line.Add();
                Line.SetCurrentLine(1);
                */

                Trans.DocTotal = Math.Round( mTotalDocStellar,2, MidpointRounding.AwayFromZero);

                ItmCount++;

                //Line.ItemCode = "106902";
                //Line.ItemCode = "RT000003";
                //Line.ItemCode = "028391";
                Line.ItemCode = "C1500";

                //Line.Quantity = 3;
                Line.Quantity = 1;
                //Line.Quantity = 28500;

                Line.UnitPrice = PrcTest;
                //Line.UnitPrice = 550.08;
                
                //Line.DiscountPercent = 24.5648;

                //Line.TaxCode = "EXE";
                Line.TaxCode = "ITBIS18";

                //Line.PriceAfterVAT = PrcFull;


                CamposOrg = Line.UserFields.Fields;

                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;

                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                Line.CostingCode2 = "L01";
                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                /*
                Line.Add();

                //Line.ItemCode = "028285";
                Line.ItemCode = "087921";
                Line.Quantity = 1;
                Line.Price = 0;
                Line.InventoryQuantity = 0;
                //Line.WithoutInventoryMovement = BoYesNoEnum.tYES;
                Line.SerialNumbers.SystemSerialNumber = 3;
                Line.SerialNumbers.Quantity = 1;
                */

                //Line.Add();
                //Line.SetCurrentLine(1);
                //Line.Delete();
                //Line.SetCurrentLine(0);

                Console.WriteLine(Line.Count);

                /*Line.Add();
                Line.ItemCode = "000687";
                Line.Quantity = 1;
                Line.UnitPrice = 0;
                Line.TaxCode = "ITBIS18";
                */
                /*
                Line.Add();
                //Line.ItemCode = "C0968";
                Line.ItemCode = "C1500";
                Line.Quantity = 1;
                Line.UnitPrice = PrcTest;
                Line.DiscountPercent = 50;
                Line.TaxCode = "ITBIS18";
                CamposOrg = Line.UserFields.Fields;
                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;
                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                Line.CostingCode2 = "L01";
                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;
                */
                // Fin Productos.



                // Grabar la transacción.

                mEtapa = "Procediendo a grabar (registrar) la transacción " + StellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                tsInicioProc = DateTime.Now;

                String TXML = String.Empty;
                TXML = Trans.GetAsXML();

                Int32 TmpResult = Trans.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.Add(): "));

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                // Documento Registrado. Proceder a registrar los pagos.

                String LastDocEntry; Int32 DocEntry; Double TotalDoc; String LastDocNum; Int32 DocNum;

                SAP.GetNewObjectCode(out LastDocEntry);
                DocEntry = Convert.ToInt32(LastDocEntry);

                tsInicioProc = DateTime.Now;

                Boolean FoundTrans = Trans.GetByKey(DocEntry);

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.GetByKey(): "));

                Double mDiferenciaTotales = 0;

                if (FoundTrans)
                {

                    TXML = Trans.GetAsXML();

                    DocNum = Trans.DocNum;
                    LastDocNum = Convert.ToString(DocNum);

                    TotalDoc = Trans.DocTotal;
                    mTotalDocSAP = TotalDoc;

                    if (Math.Round(mTotalDocStellar, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) !=
                    Math.Round(mTotalDocSAP, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero))
                        // Para calcular la diferencia exacta sin redondeo
                        mDiferenciaTotales = Math.Round((mTotalDocSAP - mTotalDocStellar), 8, MidpointRounding.AwayFromZero);

                    if (Math.Abs(Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero)) >= 0.01)
                    {
                        Console.WriteLine("Diferencia entre totales: " + "\n\n" +
                        Convert.ToString(mTotalDocStellar) + "\n" +
                        Convert.ToString(mTotalDocSAP) + "\n" +
                        "Diferencia: " + mDiferenciaTotales.ToString());

                        if (Properties.Settings.Default.DebugMode)
                        {
                            MessageBox.Show("Diferencia entre totales: " + "\n\n" +
                            Convert.ToString(mTotalDocStellar) + "\n" +
                            Convert.ToString(mTotalDocSAP) + "\n" +
                            "Diferencia: " + mDiferenciaTotales.ToString());
                        }
                    }
                    else
                    {
                        // Diferencia no significativa. No afectá el pago.
                        mDiferenciaTotales = 0;
                    }

                }
                else
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error buscando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                Trans.Lines.SetCurrentLine(2);
                //Trans.Lines.SetCurrentLine(0);
                //Trans.Lines.SetCurrentLine(1);
                Trans.Lines.SerialNumbers.Quantity = 1;
                Trans.Lines.SerialNumbers.SystemSerialNumber = 185;

                TmpResult = Trans.Update();

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                TmpResult = Trans.SaveDraftToDocument();

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }


                mEtapa = "Documento grabado exitosamente. " + StellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                SAPbobsCOM.Payments DetPag;

                DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                //DetPag = SAP.GetBusinessObject(BoObjectTypes.oPaymentsDrafts);
                //DetPag.DocObjectCode = BoPaymentsObjectType.bopot_IncomingPayments;
                
                
                DetPag.DocType = BoRcptTypes.rCustomer;
                DetPag.CardCode = Trans.CardCode;
                DetPag.DocDate = Trans.DocDate;
                //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                //DetPag.VatDate = Trans.VatDate; // ????
                DetPag.Remarks = Trans.Comments;
                //DetPag.JournalRemarks = String.Empty; // No es necesario.

                if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            DetPag.Series = Serie;
                        }

                    }
                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                        DetPag.Series = Serie;


                DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                DetPag.Invoices.DocEntry = DocEntry;
                //DetPag.Invoices.SumApplied = 3 * PrcFull;
                //DetPag.Invoices.SumApplied = 1.5 * PrcFull;
                DetPag.Invoices.SumApplied = 1 * PrcFull;

                DetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                //DetPag.CashSum = 3 * PrcFull;
                //DetPag.CashSum = 1.5 * PrcFull;
                DetPag.CashSum = 1 * PrcFull;

                tsInicioProc = DateTime.Now;

                TmpResult = DetPag.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando pago en modo resumido en Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    DetPag = null;
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                SAPbobsCOM.Documents DevTrans;
                
                DevTrans = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);
                DevTrans = SAP.GetBusinessObject(BoObjectTypes.oDrafts);
                DevTrans.DocObjectCode = BoObjectTypes.oCreditNotes;

                if (!Properties.Settings.Default.SAP_SerieDocumentoNC.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoNC.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento Nota de Crédito";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oCreditNotes).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Nota de Crédito para esta localidad.");

                            DevTrans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            DevTrans.Series = Serie;
                        }

                    }
                if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoNC, out Serie))
                    DevTrans.Series = Serie;

                DevTrans.Reference1 = TransID;

                DevTrans.Reference2 = Trans.DocNum.ToString();

                DevTrans.Comments = "Stellar TEST. " + TransID + " -> VEN " + TransID + " (SBO No. " + Trans.DocNum.ToString() + ")";

                DevTrans.CardCode = Properties.Settings.Default.SAP_ClienteContado;

                SAPbobsCOM.Fields CamposOrgDev;

                CamposOrgDev = DevTrans.UserFields.Fields;

                CamposOrgDev.Item("U_FACNIT").Value = "123456";
                CamposOrgDev.Item("U_FACNOM").Value = "Prueba Generica";

                //DevTrans.Address = "Prueba Dir";

                DevTrans.DocDate = Trans.DocDate; // DateTime.Today.AddDays(-1);
                DevTrans.DocDueDate = DevTrans.DocDate;// DateTime.Today.AddDays(-1);
                //Trans.TaxDate = Trans.DocDate; // Autocolocado en SAP

                CamposOrgDev.Item("U_FACFECHA").Value = DevTrans.DocDate;
                /*
                switch (RsVentas.Fields["c_Rif"].Value.ToString().Length)
                {
                    case 9:
                        CamposOrg.Item("U_TNEGOCIO").Value = "1";
                        break;
                    case 11:
                        CamposOrg.Item("U_TNEGOCIO").Value = "2";
                        break;
                    default:
                        CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;
                        break;
                };
                */

                CamposOrgDev.Item("U_TNEGOCIO").Value = String.Empty;

                CamposOrgDev.Item("U_CGASTOS").Value = "09";

                CamposOrgDev.Item("U_CAJA").Value = "999";

                CamposOrgDev.Item("U_CUADRE").Value = 0;
                CamposOrgDev.Item("U_FACSERIE").Value = "02";

                //

                fv6 = new DateTime(DateTime.Today.Year, 12, 31);

                CamposOrgDev.Item("U_FV6").Value = fv6;

                //

                DevTrans.TrackingNumber = TransID;

                // PRUEBA CAMPOS.

                //DevTrans.TrackingNumber = "01010099000001456712";
                DevTrans.NumAtCard = "000131"; //DevTrans.TrackingNumber;
                //DevTrans.PaymentReference = "01010099000001456712"; //DevTrans.TrackingNumber;

                // PRUEBA CAMPOS.

                //DevTrans.JournalMemo = String.Empty; // Autocolocado en SAP
                DevTrans.Rounding = BoYesNoEnum.tNO;

                DevTrans.POSCashierNumber = 123;

                //DevTrans.ControlAccount = "2120305";
                //DevTrans.ControlAccount = "2120306";

                /*
                FoundTrans = Trans.GetByKey(DocEntry);

                int ReOpenFac1 = Trans.Reopen();

                if (ReOpenFac1 != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }
                */

                // Registrar los productos de la transacción.

                ItmCount = 0;

                IDocument_Lines DevLine = DevTrans.Lines;
                //dynamic Line = Trans.Lines;

                /*
                ItmCount++;

                //DevLine.ItemCode = "106902";
                DevLine.ItemCode = "RT000003";

                DevLine.Quantity = 0.1;

                DevLine.UnitPrice = 0;

                DevLine.DiscountPercent = 0;
                //DevLine.TaxCode = "EXE";
                DevLine.TaxCode = "ITBIS18";

                CamposOrgDev = DevLine.UserFields.Fields;

                mDatoTemp = "B";
                CamposOrgDev.Item("U_TIPOMONTO").Value = mDatoTemp;

                DevLine.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                DevLine.CostingCode2 = "L01";
                DevLine.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                DevLine.Add();
                DevLine.SetCurrentLine(1);
                
                */

                ItmCount++;

                /*
                DevLine.BaseLine = 0;
                DevLine.BaseEntry = Trans.DocEntry;
                DevLine.BaseType = Convert.ToInt32(BoObjectTypes.oInvoices);
                */

                //DevLine.ItemCode = "106902";
                //DevLine.ItemCode = "RT000003";
                DevLine.ItemCode = "C1500";

                DevLine.Quantity = 1;
                //DevLine.Quantity = 0.5;

                DevLine.UnitPrice = PrcTest;

                DevLine.DiscountPercent = 0;

                //DevLine.TaxCode = "EXE";
                DevLine.TaxCode = "ITBIS18";

                //CamposOrgDev = DevLine.UserFields.Fields;

                //DevTrans.DocTotal = 0.5 * PrcFull;
                DevTrans.DocTotal = 1 * PrcFull;

                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;

                Line.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                Line.CostingCode2 = "L01";
                Line.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                // Fin Productos.

                // Grabar la transacción.

                mEtapa = "Procediendo a grabar (registrar) la transacción " + StellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                tsInicioProc = DateTime.Now;

                TmpResult = DevTrans.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.Add(): "));

                //PCCargado = oTree.GetByKey("C0968");
                PCCargado = oTree.GetByKey("C1500");
                if (PCCargado)
                {
                    PCCUpdated = 0;
                    oTree.TreeType = BoItemTreeTypes.iAssemblyTree;
                    oTree.HideBOMComponentsInPrintout = BoYesNoEnum.tNO;
                    PCCUpdated = oTree.Update();
                }

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                // Documento Registrado. Proceder a registrar los pagos.

                String DevLastDocEntry; Int32 DevDocEntry; Double DevTotalDoc; String DevLastDocNum; Int32 DevDocNum;

                SAP.GetNewObjectCode(out DevLastDocEntry);
                DevDocEntry = Convert.ToInt32(DevLastDocEntry);

                tsInicioProc = DateTime.Now;

                FoundTrans = DevTrans.GetByKey(DevDocEntry);

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.GetByKey(): "));

                mDiferenciaTotales = 0;

                mEtapa = "Documento grabado exitosamente. " + StellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);


                //////////////////////

                String NewTransID = TransID + "-1";
                String NewStellarDocID = TransID;

                mTotalDocStellar = 0; mTotalDocSAP = DevTrans.DocTotal;

                AffectedTrans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);

                if (!Properties.Settings.Default.SAP_SerieDocumentoFactura.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoFactura.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento de Venta";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oInvoices).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            // No hay datos fiscales. Documento no Impreso. Error de Integridad.

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Venta para esta localidad.");

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            AffectedTrans.Series = Serie;
                        }

                    }
                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoFactura, out Serie))
                        AffectedTrans.Series = Serie;


                AffectedTrans.Comments = "Stellar test" + ". " + NewTransID;

                // Asignar datos generales de cabecero de transacción.

                mEtapa = "Asignando datos generales de cabecero de transacción";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                AffectedTrans.CardCode = Properties.Settings.Default.SAP_ClienteContado;
                //AffectedTrans.CardCode = "C999998";

                //mTotalDocStellar = 3 * PrcFull;
                mTotalDocStellar = 1.5 * PrcFull;

                CamposOrg = AffectedTrans.UserFields.Fields;

                CamposOrg.Item("U_FACNIT").Value = "123456";
                CamposOrg.Item("U_FACNOM").Value = "Prueba Generica";

                //AffectedTrans.Address = "Prueba Dir";

                AffectedTrans.DocDate = DateTime.Today.AddDays(-1);
                AffectedTrans.DocDueDate = DateTime.Today.AddDays(-1);
                //AffectedTrans.TaxDate = Trans.DocDate; // Autocolocado en SAP

                CamposOrg.Item("U_FACFECHA").Value = AffectedTrans.DocDate;
                /*
                switch (RsVentas.Fields["c_Rif"].Value.ToString().Length)
                {
                    case 9:
                        CamposOrg.Item("U_TNEGOCIO").Value = "1";
                        break;
                    case 11:
                        CamposOrg.Item("U_TNEGOCIO").Value = "2";
                        break;
                    default:
                        CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;
                        break;
                };
                */

                CamposOrg.Item("U_TNEGOCIO").Value = String.Empty;

                CamposOrg.Item("U_CGASTOS").Value = "09";

                CamposOrg.Item("U_CAJA").Value = "999";

                CamposOrg.Item("U_CUADRE").Value = 0;
                CamposOrg.Item("U_FACSERIE").Value = "02";

                //

                fv6 = new DateTime(DateTime.Today.Year, 12, 31);

                CamposOrg.Item("U_FV6").Value = fv6;

                //

                AffectedTrans.TrackingNumber = NewTransID;

                //Trans.JournalMemo = String.Empty; // Autocolocado en SAP
                AffectedTrans.Rounding = BoYesNoEnum.tNO;

                AffectedTrans.POSCashierNumber = 123;

                //AffectedTrans.ControlAccount = "1110101";
                //AffectedTrans.ControlAccount = "1110101";

                mEtapa = "Ingresando Items del Documento";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                // Registrar los productos de la transacción.

                ItmCount = 0;

                IDocument_Lines NewLine = AffectedTrans.Lines;
                //dynamic NewLine = AffectedTrans.Lines;
                /*
                ItmCount++;

                //

                //NewLine.ItemCode = "106902";
                NewLine.ItemCode = "RT000003";

                NewLine.Quantity = 0.1;

                NewLine.UnitPrice = 0;

                NewLine.DiscountPercent = 0;
                //NewLine.TaxCode = "EXE";
                NewLine.TaxCode = "ITBIS18";

                CamposOrg = NewLine.UserFields.Fields;

                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;

                NewLine.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                NewLine.CostingCode2 = "L01";
                NewLine.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                NewLine.Add();
                NewLine.SetCurrentLine(1);
                */
                ItmCount++;

                //NewLine.ItemCode = "106902";
                NewLine.ItemCode = "RT000003";

                //NewLine.Quantity = 1;
                NewLine.Quantity = 0.5;

                NewLine.UnitPrice = PrcTest;

                NewLine.DiscountPercent = 0;

                //NewLine.TaxCode = "EXE";
                NewLine.TaxCode = "ITBIS18";

                CamposOrg = NewLine.UserFields.Fields;

                mDatoTemp = "B";
                CamposOrg.Item("U_TIPOMONTO").Value = mDatoTemp;

                NewLine.CostingCode = Properties.Settings.Default.SAP_AliasLocalidad;
                NewLine.CostingCode2 = "L01";
                NewLine.WarehouseCode = Properties.Settings.Default.SAP_CodigoAlmacenPorDefectoLocalidad;

                // Fin Productos.

                // Grabar la transacción.

                mEtapa = "Procediendo a grabar (registrar) la transacción " + NewStellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                tsInicioProc = DateTime.Now;

                TmpResult = AffectedTrans.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.Add(): "));

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + NewTransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                // Documento Registrado.

                String NewLastDocEntry; Int32 NewDocEntry; Double NewTotalDoc; String NewLastDocNum; Int32 NewDocNum;

                SAP.GetNewObjectCode(out NewLastDocEntry);
                NewDocEntry = Convert.ToInt32(NewLastDocEntry);

                tsInicioProc = DateTime.Now;

                FoundTrans = AffectedTrans.GetByKey(NewDocEntry);

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Documents.GetByKey(): "));

                mDiferenciaTotales = 0;

                if (FoundTrans)
                {

                    NewDocNum = AffectedTrans.DocNum;
                    NewLastDocNum = Convert.ToString(NewDocNum);

                    TotalDoc = AffectedTrans.DocTotal;
                    mTotalDocSAP = TotalDoc;

                    /*
                    if (Math.Round(mTotalDocStellar, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero) !=
                    Math.Round(mTotalDocSAP, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero))
                        // Para calcular la diferencia exacta sin redondeo
                        mDiferenciaTotales = Math.Round((mTotalDocSAP - mTotalDocStellar), 8, MidpointRounding.AwayFromZero);

                    if (Math.Abs(Math.Round(mDiferenciaTotales, Properties.Settings.Default.SAP_DecimalesMonedaPredeterminada, MidpointRounding.AwayFromZero)) >= 0.01)
                    {
                        Console.WriteLine("Diferencia entre totales: " + "\n\n" +
                        Convert.ToString(mTotalDocStellar) + "\n" +
                        Convert.ToString(mTotalDocSAP) + "\n" +
                        "Diferencia: " + mDiferenciaTotales.ToString());

                        if (Properties.Settings.Default.DebugMode)
                        {
                            MessageBox.Show("Diferencia entre totales: " + "\n\n" +
                            Convert.ToString(mTotalDocStellar) + "\n" +
                            Convert.ToString(mTotalDocSAP) + "\n" +
                            "Diferencia: " + mDiferenciaTotales.ToString());
                        }
                    }
                    else
                    {
                        // Diferencia no significativa. No afectá el pago.
                        mDiferenciaTotales = 0;
                    }
                    */

                    mDiferenciaTotales = 0;

                }
                else
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error buscando Transaccion POS [" + NewTransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                mEtapa = "Documento grabado exitosamente. " + NewStellarDocID;
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                /*

                SAPbobsCOM.Payments NewDetPag;

                NewDetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                //dynamic DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                NewDetPag.DocType = BoRcptTypes.rCustomer;
                //NewDetPag.DocType = BoRcptTypes.rAccount;
                NewDetPag.CardCode = AffectedTrans.CardCode;
                NewDetPag.DocDate = AffectedTrans.DocDate;
                //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                //DetPag.VatDate = Trans.VatDate; // ????
                NewDetPag.Remarks = AffectedTrans.Comments;
                //DetPag.JournalRemarks = String.Empty; // No es necesario.

                if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + NewTransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            NewDetPag.Series = Serie;
                        }

                    }
                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                        NewDetPag.Series = Serie;

                //NewDetPag.Invoices.SumApplied = -590;
                //NewDetPag.Invoices.SumApplied = 590;

                //NewDetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;

                //NewDetPag.CashAccount = "2120305";
                //NewDetPag.CashSum = 590;

                //NewDetPag.Invoices.InvoiceType = BoRcptInvTypes.it_CredItnote;
                //NewDetPag.Invoices.DocEntry = DevDocEntry;

                //NewDetPag.Invoices.Add();

                //NewDetPag.Invoices.SumApplied = -590;
                NewDetPag.Invoices.SumApplied = 1 * PrcFull;

                // NewDetPag es el Objeto de tipo oIncomingPayments

                NewDetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                NewDetPag.Invoices.DocEntry = AffectedTrans.DocEntry;

                //NewDetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                //NewDetPag.CashAccount = "2120305";
                //NewDetPag.CashSum = 1.5*PrcFull;

                // Aplicar Nota

                NewDetPag.Invoices.Add();
                NewDetPag.Invoices.SetCurrentLine(1);

                NewDetPag.Invoices.SumApplied = -1 * 1 * PrcFull;
                //NewDetPag.Invoices.SumApplied = 590;

                NewDetPag.Invoices.InvoiceType = BoRcptInvTypes.it_CredItnote;
                NewDetPag.Invoices.DocEntry = DevTrans.DocEntry;

                NewDetPag.CashAccount = "2120305";
                //NewDetPag.CashSum = 1*1*PrcFull;
                NewDetPag.CashSum = 0.01;

                // Aplicar Nota

                tsInicioProc = DateTime.Now;

                TmpResult = NewDetPag.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando pago en modo resumido en Transaccion POS [" + NewTransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    DetPag = null;
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }


                // PAGO RESTANTE

                SAPbobsCOM.Payments NewDetPag2;

                NewDetPag2 = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                //dynamic DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                NewDetPag2.DocType = BoRcptTypes.rCustomer;
                //NewDetPag2.DocType = BoRcptTypes.rAccount;
                NewDetPag2.CardCode = AffectedTrans.CardCode;
                NewDetPag2.DocDate = AffectedTrans.DocDate;
                //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                //DetPag.VatDate = Trans.VatDate; // ????
                NewDetPag2.Remarks = AffectedTrans.Comments;
                //DetPag.JournalRemarks = String.Empty; // No es necesario.

                if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                    if (Properties.Settings.Default.SAP_SerieDocumentoPago.Equals("*"))
                    {

                        // IR A BUSCAR LA SERIE EN LAS TABLAS DE SAP

                        mEtapa = "Buscando la Serie correspondiente a Documento de Entrada de Pagos";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        mDatoTemp =
                        "SELECT * FROM \"NNM1\" " + "\n" +
                        "WHERE \"GroupCode\" = '" + Properties.Settings.Default.SAP_CodLocalidad + "' " + "\n" +
                        "AND LEFT(\"SeriesName\", 2) = 'PV' " + "\n" +
                        "AND \"ObjectCode\" = " + ((Int32)BoObjectTypes.oIncomingPayments).ToString() + " " + "\n";

                        SAPmRs = SAP.GetBusinessObject(BoObjectTypes.BoRecordset);

                        SAPmRs.DoQuery(mDatoTemp);

                        if (SAPmRs.EoF)
                        {

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + NewTransID + "] en SAP. " +
                            "Error de integridad. No esta definida en SAP la Serie de Documentos Entrada de Pagos para esta localidad.");

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                            goto Continue;

                        }
                        else
                        {
                            Serie = SAPmRs.Fields.Item("Series").Value;
                            SerieNextNum = SAPmRs.Fields.Item("NextNumber").Value;
                            NewDetPag2.Series = Serie;
                        }

                    }
                    else if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                        NewDetPag2.Series = Serie;


                NewDetPag2.Invoices.SumApplied = 1 * PrcFull;

                // NewDetPag es el Objeto de tipo oIncomingPayments

                NewDetPag2.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                NewDetPag2.Invoices.DocEntry = AffectedTrans.DocEntry;

                NewDetPag2.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                NewDetPag2.CashSum = 1 * PrcFull;

                // Aplicar Nota

                tsInicioProc = DateTime.Now;

                TmpResult = NewDetPag2.Add();

                tsFinProc = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinProc - tsInicioProc), "Tiempo Payments.Add(): "));

                if (TmpResult != 0)
                {

                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                    Program.Logger.EscribirLog("Error ingresando pago en modo resumido en Transaccion POS [" + NewTransID + "] en SAP. " +
                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                    Trans = null; //Trans.Cancel();
                    DetPag = null;
                    if (SAP.InTransaction)
                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                    tsFinTrans = DateTime.Now;

                    Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                    goto Continue;

                }

                // PAGO RESTANTE

                */

                // PRUEBA RECONCILIACION

                try
                {

                    InternalReconciliationsService riservice = SAP.GetCompanyService().GetBusinessService(ServiceTypes.InternalReconciliationsService);
                    InternalReconciliationParams riparams = riservice.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationParams);
                    InternalReconciliationOpenTransParams rioparams = riservice.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTransParams);
                    InternalReconciliationOpenTrans riobj = riservice.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTrans);
                    InternalReconciliationOpenTransRows rirows = riobj.InternalReconciliationOpenTransRows;
                    InternalReconciliationOpenTransRow currirow;

                    /*
                    riobj.CardOrAccount = CardOrAccountEnum.coaCard;
                    riobj.ReconDate = DateTime.Today;
                    riobj.ReconType = ReconTypeEnum.rtManual;
                    */

                    riobj.ReconDate = DateTime.Today.AddDays(-1);
                    riobj.CardOrAccount = CardOrAccountEnum.coaCard;
                    

                    rioparams.CardOrAccount = CardOrAccountEnum.coaCard;
                    rioparams.DateType = ReconSelectDateTypeEnum.rsdtPostDate;
                    rioparams.ReconDate = DateTime.Today;
                    rioparams.InternalReconciliationBPs.Add();
                    rioparams.InternalReconciliationBPs.Item(0).BPCode = AffectedTrans.CardCode;
                    rioparams.InternalReconciliationBPs.Item(0).BPCode = AffectedTrans.CardCode;
                    if (!AffectedTrans.CardCode.Equals(DevTrans.CardCode, StringComparison.OrdinalIgnoreCase))
                    {
                        rioparams.InternalReconciliationBPs.Add();
                        rioparams.InternalReconciliationBPs.Item(1).BPCode = DevTrans.CardCode;
                    }


                    if (rirows.Count <= 0) rirows.Add();
                    currirow = rirows.Item(0);
                    currirow.Selected = BoYesNoEnum.tYES;
                    currirow.TransId = AffectedTrans.TransNum;
                    currirow.TransRowId = 0;
                    //currirow.CreditOrDebit = CreditOrDebitEnum.codDebit;
                    currirow.ReconcileAmount = DevTrans.DocTotal;
                    currirow.CashDiscount = 0;

                    rirows.Add();

                    currirow = rirows.Item(1);
                    currirow.Selected = BoYesNoEnum.tYES;
                    currirow.TransId = DevTrans.TransNum;
                    currirow.TransRowId = 0;
                    //currirow.CreditOrDebit = CreditOrDebitEnum.codDebit;
                    currirow.ReconcileAmount = DevTrans.DocTotal;
                    currirow.CashDiscount = 0;

                    riparams = riservice.Add(riobj);

                }


                catch (Exception AnyRec)
                {
                    Console.WriteLine(AnyRec.Message);
                }

                // PRUEBA RECONCILIACION

                

                if (SAP.InTransaction)
                        {

                            mEtapa = "Finalizando transacción de SAP. Registrando envío del Documento " + StellarDocID;
                            Console.WriteLine(mEtapa);

                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);
                    System.Environment.Exit(0);
                            SAP.EndTransaction(BoWfTransOpt.wf_Commit);

                            mEtapa = "Documento procesado exitosamente en su totalidad: " + StellarDocID;
                            Console.WriteLine(mEtapa);
                            if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                            tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Tiempo total de transacción: "));

                        }
                        else
                        {

                            Program.Logger.EscribirLog("Error en SAP. Transaccion Inactiva / Rollback forzoso. Procesando Transaccion POS [" + TransID + "]");

        tsFinTrans = DateTime.Now;

                            Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                        }

                Continue:

                // Proceso Finalizado.

                return;

	        }
	        catch (Exception Any)
	        {

                if (SAP.InTransaction)
                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

		        Program.Logger.EscribirLog(Any, "ConstruirRegistrosDeVentas();" + "\n\n" +
                "mEtapa=" + mEtapa + "\n" +
                " TransaccionPOS " + mUltDoc + "\n" +
                " Fecha " + mUltFec.ToString() + "\n" +
                " Total " + mUltTot.ToString() + "\n" + 
                "\n");

                if (Program.gDebugMode) {
                    MessageBox.Show(Any.Message + "\n" +
                    "mEtapa=" + mEtapa + "\n" +
                    "mUltDoc=" + mUltDoc + "\n" +
                    "mUltFec=" + mUltFec.ToString() + "\n" +
                    "mUltTot=" + mUltTot.ToString() + "\n");
                }

                tsFinTrans = DateTime.Now;

                Console.WriteLine(Functions.CustomDurationES((tsFinTrans - tsInicioTrans), "Documento Fallido. Tiempo total de transacción: "));

                if (!SAP.Connected)
                {

                    // Intentar recuperar la conexión.

                    Program.mCnSAP = ConexionSAP();
                    SAP = Program.mCnSAP;

                    if (SAP == null)
                        // La conexión está rota.
                        System.Environment.Exit(999);

                    if (!SAP.Connected)
                        // La conexión está rota.
                        System.Environment.Exit(998);

                }

            }

        }

        private static Boolean ObtenerDatosVentas(out ADODB.Recordset pRsVentas, out ADODB.Recordset pRsItems,
        out ADODB.Recordset pRsDetPagos, out ADODB.Recordset pRsImpuestos, out ADODB.Recordset pRsDatosFiscales, 
        out ADODB.Recordset pRsPendXEntregaMA, out ADODB.Recordset pRsPendXEntregaTR, 
        out ADODB.Recordset pRsSeriales, 
        Boolean pReprocesarFallidos)
        {

            Boolean ObtenerDatosVentas = false;

            try 
	        {

                String mSQL;
                
                String ExcluirVNF = String.Empty;

                String mEstatus = String.Empty;

                //if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal))
                    //ExcluirVNF = " AND bDocNoFiscal = 0";

                if (pReprocesarFallidos)
                    mEstatus = RegistrosPendientesFallidos;
                else
                    mEstatus = RegistrosPendientesPorLote;

                //

                mSQL = "SELECT *, " + "\n" +
                "(DOC.c_Sucursal + DOC.c_Concepto + DOC.c_Numero) AS TransID " + "\n" + 
                "FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE (DOC.cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + " " +
                "ORDER BY DOC.f_Fecha, DOC.f_Hora, DOC.ID";
                
                pRsVentas = new ADODB.Recordset();
                
                pRsVentas.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsVentas.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsVentas.ActiveConnection = null;
                
                if (Properties.Settings.Default.EnviarVentaAgrupada)
                {
                    mSQL = String.Empty +
                    "SELECT c_Localidad, c_Concepto, c_Numero, MIN(n_Linea) AS n_Linea, Cod_Principal, Cod_Principal AS Codigo, " + "\n" +
                    "SUM(Cantidad) AS Cantidad, ABS(ROUND(Precio, 3, 0)) AS Precio, " + "\n" +
                    "ABS(ROUND(n_PrecioReal, 3, 0)) AS n_PrecioReal, " + "\n" +
                    "SUM(Subtotal) AS Subtotal, SUM(Impuesto) AS Impuesto, SUM(Total) AS Total, " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, SUM(Descuento) AS Descuento, n_TipoPrecio, " + "\n" +
                    "MIN(ID) AS ID, (c_Localidad + c_Concepto + c_Numero) AS TransID " + "\n" +
                    "FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                    "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ") " + "\n" +
                    "GROUP BY Cod_Principal, ABS(ROUND(Precio, 3, 0)), ABS(ROUND(n_PrecioReal, 3, 0)), " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, n_TipoPrecio, c_Localidad, c_Concepto, c_Numero " + "\n" +
                    "HAVING ABS(ROUND(SUM(Cantidad), 8, 0)) >= 0.0001 " + "\n" +
                    "ORDER BY MIN(n_Linea), MIN(ID) "; // NO ENVIAR PRODUCTOS REINTEGRADOS CUYA CANTIDAD QUEDO EN CERO.
                }
                else
                {
                    mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                    "FROM[VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                    "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ") " + "\n" +
                    "ORDER BY n_Linea, ID ";
                }

                //

                pRsItems = new ADODB.Recordset();
                
                pRsItems.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsItems.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsItems.ActiveConnection = null;

                mSQL = "SELECT *, (c_Localidad + 'VEN' + c_Factura) AS TransID " + "\n" +
                "FROM [VAD20].[DBO].[MA_DETALLEPAGO] WHERE (c_Localidad + 'VEN' + c_Factura) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
                "ORDER BY c_Factura, " + "\n" +
                "(CASE WHEN c_CodDenominacion = 'Efectivo' THEN 1 ELSE 0 END) DESC, ID";
                
                pRsDetPagos = new ADODB.Recordset();
                
                pRsDetPagos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsDetPagos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsDetPagos.ActiveConnection = null;

                //

                mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_PAGOS_IMPUESTOS] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";
                
                pRsImpuestos = new ADODB.Recordset();
                
                pRsImpuestos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                
                pRsImpuestos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
                
                pRsImpuestos.ActiveConnection = null;
                
                //

                mSQL = "SELECT *, (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_DOCUMENTOS_FISCAL] WHERE (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";

                pRsDatosFiscales = new ADODB.Recordset();

                pRsDatosFiscales.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsDatosFiscales.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsDatosFiscales.ActiveConnection = null;

                //

                mSQL = "SELECT *, (CodLocalidad + Concepto + Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_PLAN_PENDIENTE_X_ENTREGA] WHERE (CodLocalidad + Concepto + Numero) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";

                pRsPendXEntregaMA = new ADODB.Recordset();

                pRsPendXEntregaMA.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsPendXEntregaMA.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsPendXEntregaMA.ActiveConnection = null;

                //

                mSQL = "SELECT *, (CodLocalidad + Concepto + Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_PENDIENTE_X_ENTREGA] WHERE (CodLocalidad + Concepto + Numero) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";

                pRsPendXEntregaTR = new ADODB.Recordset();

                pRsPendXEntregaTR.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsPendXEntregaTR.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsPendXEntregaTR.ActiveConnection = null;

                //

                mSQL = "SELECT *, (c_Localidad + c_Concepto + c_Numero) AS TransID" + "\n" +
                "FROM [VAD20].[DBO].[MA_TRANSACCION_SERIALES] WHERE (c_Localidad + c_Concepto + c_Numero) IN (" + "\n" +
                "SELECT (c_Sucursal + c_Concepto + c_Numero) FROM [VAD20].[DBO].[MA_PAGOS]" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";

                pRsSeriales = new ADODB.Recordset();

                pRsSeriales.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                pRsSeriales.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);

                pRsSeriales.ActiveConnection = null;

                //

                ObtenerDatosVentas = (pRsVentas.RecordCount > 0);

                return ObtenerDatosVentas;

	        }
	        catch (Exception Any)
	        {

                Program.Logger.EscribirLog(Any, "ObtenerDatosVentas();");

                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Buscando informacion de las Transacciones POS pendientes por procesar.", "DBSync.ObtenerDatosVentas()",
                "Ejecucion Agente", Program.gCorrelativo, (Any.HResult + "|" + Any.Message), "BuscarDatos", Program.mCnLocal);

                pRsVentas = null;
                pRsItems = null;
                pRsDetPagos = null;
                pRsImpuestos = null;
                pRsPendXEntregaMA = null;
                pRsPendXEntregaTR = null;
                pRsDatosFiscales = null;
                pRsSeriales = null;

                return ObtenerDatosVentas;
	        }

        }

        private static void ControlarErrorRegistroVentas(System.Collections.ArrayList pFallidos, 
        String pTransID, String pLocalidad, String pConcepto, String pNumero)
        {

            pFallidos.Add( new String[] {  pTransID, pLocalidad, pConcepto, pNumero } );
            
        }
        
        private static Boolean MarcarFallidos(ref System.Collections.ArrayList pListaFallidos)
        {

            try 
	        {

                Object Records;

                foreach (String[] Item in pListaFallidos)
                {
                    Program.mCnLocal.Execute("UPDATE [VAD20].[DBO].[MA_PAGOS] " + 
                    "SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + "\n" +
                    "WHERE c_Sucursal = '" + Item[1] + "' " +
                    "AND c_Concepto = '" + Item[2] + "' " + 
                    "AND c_Numero = '" + Item[3] + "' ", 
                    out Records);
                }

                return true;

	        }
	        catch (Exception Any)
	        {
                Program.Logger.EscribirLog (Any, "Error marcando registros de ventas fallidos.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error marcando registros de ventas o devoluciones fallidos.",
                "DBSync.MarcarFallidos()", "Ejecución Agente",
                Program.gCorrelativo, Program.mCnLocal);
                return false;
	        }
            
        }

        //   ACA SE AGREGA ESTA FUNCIÓN CON EL FIN DE GENERAR LAS AUDITORIAS QUE SE REQUIERAN PARA TODOS LOS CASOS.

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria,
        String DescripcionAuditoria,
        String DescripcionEvento,
        String Ventana,
        String TipoObjeto,
        String CodigoAfectado,
        String CodigoRetorno,
        String AccionRealizada,
        ADODB.Connection Conexion)
        {

            try
            {

                ADODB.Connection DB = null;
                //DB = new ADODB.Connection();
                DB = Conexion;

                ADODB.Recordset mRs = null; String SQL = String.Empty;

                mRs = new ADODB.Recordset();

                SQL = "SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS] WHERE 1 = 2";

                //DB.ConnectionString = Conexion.ConnectionString;
                //DB.Open();

                mRs.Open(SQL, DB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                mRs.AddNew();

                mRs.Fields["Cod_Prod"].Value = Program.gCodProducto;
                mRs.Fields["Nom_Prod"].Value = Program.ApplicationName;
                mRs.Fields["Tipo"].Value = TipoAuditoria;
                mRs.Fields["Descripcion"].Value = DescripcionAuditoria;
                mRs.Fields["Evento"].Value = DescripcionEvento;
                //mRs.Fields["Fecha"].Value = DateTime.Now; // Posee Valor Default GetDate()
                mRs.Fields["CodUsuario"].Value = "9999999999";
                mRs.Fields["Usuario"].Value = mRs.Fields["Nom_Prod"].Value;
                mRs.Fields["Ventana"].Value = Ventana;
                mRs.Fields["TipoObjAuditado"].Value = TipoObjeto;
                mRs.Fields["CodigoAuditado"].Value = CodigoAfectado;
                if (Functions.ExisteCampoRs(mRs, "CodigoRetorno"))
                    mRs.Fields["CodigoRetorno"].Value = CodigoRetorno;
                if (Functions.ExisteCampoRs(mRs, "AccionRealizada"))
                    mRs.Fields["AccionRealizada"].Value = AccionRealizada;

                mRs.Update();

                mRs.Close();

                // DB.Close();

                return true;

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "InsertarAuditoria()" + "\n" +
                "Cod_Prod=" + Program.gCodProducto + "\n" +
                "Nom_Prod=" + Program.ApplicationName + "\n" +
                "Tipo=" + TipoAuditoria + "\n" +
                "Descripcion=" + DescripcionAuditoria + "\n" +
                "Evento=" + DescripcionEvento + "\n" +
                "Ventana=" + Ventana + "\n" +
                "TipoObjAuditado=" + TipoObjeto + "\n" +
                "CodigoAuditado=" + CodigoAfectado + "\n" +
                "CodigoRetorno=" + CodigoRetorno + "\n" +
                "AccionRealizada=" + AccionRealizada + "\n");
                return false;
            }
            
        }

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria, 
        String DescripcionAuditoria, 
        String DescripcionEvento, 
        String Ventana, 
        String TipoObjeto, 
        String CodigoAfectado, 
        ADODB.Connection Conexion)
        {
            return InsertarAuditoria(TipoAuditoria, DescripcionAuditoria, DescripcionEvento, 
            Ventana, TipoObjeto, CodigoAfectado, String.Empty, String.Empty, Conexion);
        }

        // Apartado de Modalidad de Sincronizacion de Datos Maestros SAP - Stellar

        private static void ActualizarDatosMaestrosPendientes()
        {

            ActualizarMotivosDevolucion();

            ActualizarCategorias();

            ActualizarProductosYCodigos();

            String mEtapa = String.Empty;

            if (Properties.Settings.Default.Stellar_AutoProcessMasterData)
            {

                try
                {

                    mEtapa = "Ejecutando Llamada a URL de Procesamiento de Pendientes.";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    String SxS_POS_Dir = Path.GetFullPath(Path.Combine(Program.gPath, Properties.Settings.Default.Stellar_MasterData_ProcessURL));

                    mEtapa = "URL:\n\n" + SxS_POS_Dir;
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    using (System.Diagnostics.Process tmpExe = new System.Diagnostics.Process())
                    {
                        tmpExe.StartInfo.FileName = SxS_POS_Dir;
                        tmpExe.Start();
                    }

                }
                catch (Exception Any)
                {

                    Console.WriteLine("Error: " + mEtapa);

                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    Program.Logger.EscribirLog(Any, "ActualizarDatosMaestrosPendientes(); Etapa: " + mEtapa);

                }

            }

        }

        private static void ActualizarProductosYCodigos()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_CODIGOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_PRODUCTOS_SAP", out Records);
                }
                catch { }
                
                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems) 
                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_INVENTARIO_ITEM", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, " +
                "CAST('' AS NVARCHAR(MAX)) AS DefaultCode, CAST(0 AS INT) AS SAPUserChange " + 
                "INTO #TMP_CODIGOS_SAP FROM MA_CODIGOS WHERE 1 = 2", out Records);

                LocalDB.Execute("SELECT c_Codigo, CAST('' AS NVARCHAR(MAX)) AS c_Descri, " +
                "CAST('' AS NVARCHAR(MAX)) AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS c_Grupo, " +
                "CAST('' AS NVARCHAR(MAX)) AS c_Subgrupo, CAST('' AS NVARCHAR(MAX)) AS c_Marca, " +
                "n_CostoAct, n_CostoAnt, n_CostoPro, n_CostoRep, n_Precio1, n_Precio2, n_Precio3, " +
                "c_Seriales, CAST('' AS NVARCHAR(MAX)) AS c_Presenta, n_Cantibul, n_Impuesto1, n_Activo, " +
                "n_TipoPeso, CAST('' AS NVARCHAR(MAX)) AS cu_Descripcion_Corta, Cant_Decimales, " +
                "Add_Date, Update_Date, c_CodMoneda, CAST('' AS NVARCHAR(MAX)) AS c_Observacio, " +
                "CAST('' AS NVARCHAR(MAX)) AS WarrntTmpl, CAST('' AS NVARCHAR(MAX)) AS U_pltgrt, " +
                "nu_NivelClave, CAST('' AS NVARCHAR(MAX)) AS EOF " + 
                "INTO #TMP_PRODUCTOS_SAP FROM MA_PRODUCTOS WHERE 1 = 2", out Records);

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                    LocalDB.Execute("SELECT * " +
                    "INTO #TMP_INVENTARIO_ITEM FROM MA_DEPOPROD_ITEMS WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String mJoinBitacora = String.Empty; String MarcaInicioBitacora;

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    
                    // Hacer un CROSS JOIN para que se traiga todos los registros, sin tomar en cuenta log de cambios.

                    mJoinBitacora = String.Empty +
                    "CROSS JOIN (SELECT TOP 1 NULL AS \"ItemCode\", NULL AS \"CHDATE\", " + "\n" +
                    "NULL AS \"CHTIME\", NULL AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"OITM\" " + "\n" +
                    ") AS \"PEND\" " + "\n";
                    
                else
                {

                    // Si maneja multi localidad, tomar solo lo que le corresponda a la localidad de SAP definida en el Setup.
                    // Sino, hacer la consulta asumiendo que solo se trata de una instancia de BD autonoma sin localidades.

                    mJoinBitacora = String.Empty +
                    (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD ?
                    "INNER JOIN ( " + "\n" +
                    "SELECT \"BI\".\"LIST_OF_COLS_VAL_TAB_DEL\" AS \"ItemCode\", MAX(\"BI\".\"DATE\") AS \"CHDATE\", " + "\n" +
                    "MAX(\"BI\".\"HORA\") AS \"CHTIME\", MAX(\"BI\".\"LOGINSTAC\") AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"BITACORA\" AS \"BI\" " + "\n" +
                    "WHERE \"BI\".\"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "' " + "\n" +
                    "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                    "AND \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"LECTURA_ST\" = 1 " + "\n" +
                    "GROUP BY \"BI\".\"LIST_OF_COLS_VAL_TAB_DEL\" " + "\n" +
                    ") AS \"PEND\" " + "\n" +
                    "ON \"OITM\".\"ItemCode\" = \"PEND\".\"ItemCode\" " + "\n" +
                    "AND \"PEND\".\"PROCESADO_ST\" = 0 " + "\n"
                    :
                    "INNER JOIN( " + "\n" +
                    "SELECT \"LIST_OF_COLS_VAL_TAB_DEL\" AS \"ItemCode\", MAX(\"DATE\") AS \"CHDATE\", " + "\n" +
                    "MAX(\"HORA\") AS \"CHTIME\", MAX(\"LOGINSTAC\") AS \"LOG_ID\", 0 AS \"PROCESADO_ST\" " + "\n" +
                    "FROM \"BITACORA\" WHERE \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                    "AND \"PROCESADO_ST\" = 0 " + "\n" +
                    "AND \"LECTURA_ST\" = 1 " + "\n" +
                    "GROUP BY \"LIST_OF_COLS_VAL_TAB_DEL\" " + "\n" +
                    ") AS \"PEND\" " + "\n" +
                    "ON \"OITM\".\"ItemCode\" = \"PEND\".\"ItemCode\" " + "\n" +
                    "AND \"PEND\".\"PROCESADO_ST\" = 0 " + "\n");

                    if (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD)
                    {

                        MarcaInicioBitacora = "UPDATE \"BITACORA\" SET " + "\n" +
                        "\"LECTURA_ST\" = 1 " + "\n" +
                        "WHERE  \"PROCESADO_ST\" = 0 " + "\n" +
                        "AND \"LECTURA_ST\" = 0 " + "\n" +
                        "AND \"OBJECT_TYPE\" = 4 " + "\n" +
                        "AND  \"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "' " + "\n";

                    }    
                    else
                    {

                        MarcaInicioBitacora = "UPDATE \"BITACORA\" SET " + "\n" +
                        "\"LECTURA_ST\" = 1 " + "\n" +
                        "WHERE  \"PROCESADO_ST\" = 0 " + "\n" +
                        "AND \"LECTURA_ST\" = 0 " + "\n" +
                        "AND \"OBJECT_TYPE\" = 4 " + "\n";

                    }

                    mEtapa = "Marcando Actualizaciones Pendientes de Productos por Procesar";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    SAPbobsCOM.Recordset mRsMarca;
                    mRsMarca = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);
                    mRsMarca.DoQuery(MarcaInicioBitacora);

                }

                // Query:
                //Buscar los codigos de productos que hayan tenido cambios y con codigos de 15 digitos compatibles con Stellar
                //Unido a
                //Obtener registro de Codigo Maestro igual para los productos que hayan cambiado.
                //Unido a
                //Obtener codigos PLU para productos que hagan match con la matriz de codigo de productos pesados en SAP.

                String mCodigoEDI1 = String.Empty;
                String mCodigoEDI2 = String.Empty;
                String mCodigoEDI3 = String.Empty;

                if (Properties.Settings.Default.Stellar_CodigoEDIAutomatico)
                {

                    // Asigna el Codigo Maestro automaticamente como el EDI Siempre

                    mCodigoEDI1 = String.Empty +
                    "0 AS \"nu_Intercambio\", ";

                    mCodigoEDI2 = String.Empty +
                    "1 AS \"nu_Intercambio\", ";

                    mCodigoEDI3 = String.Empty +
                    "0 AS \"nu_Intercambio\", ";

                }
                else
                {

                    // Desde SAP controlan la seleccion de un codigo especifico como Estandar (Campo Codebars de OITM)
                    // Si el Campo Codebars esta vacío se toma como Fallback el Maestro.
                    // Para evitar errores de Integridad desde SAP deberan validar que en el campo Codebars no se ingrese 
                    // un codigo distinto a cualquiera de los codigos de barra registrados para cada producto.

                    mCodigoEDI1 = String.Empty +
                    "CASE WHEN \"OBCD\".\"BcdCode\" = \"OITM\".\"CodeBars\" THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";

                    mCodigoEDI2 = String.Empty +
                    "CASE WHEN (\"OITM\".\"ItemCode\" = \"OITM\".\"CodeBars\" OR TRIM(IfNULL(\"OITM\".\"CodeBars\", '')) = '') THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";

                    mCodigoEDI3 = String.Empty +
                    "CASE WHEN SUBSTRING(\"OITM\".\"ItemCode\", 1, 7) = \"OITM\".\"CodeBars\" THEN " + "\n" +
                    "    1 " + "\n" +
                    "ELSE " + "\n" +
                    "    0 " + "\n" +
                    "END AS \"nu_Intercambio\", ";
                        
                }
                
                String SQLCodigos = String.Empty +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", \"OBCD\".\"BcdCode\" AS \"c_Codigo\", " + "\n" +
                "SUBSTRING(IfNULL(\"OBCD\".\"BcdName\", ''), 1, 20) AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI1 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 1 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "INNER JOIN \"OBCD\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"OBCD\".\"ItemCode\" " + "\n" +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 AND LENGTH(\"OBCD\".\"BcdCode\") <= 15 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", \"OITM\".\"ItemCode\" AS \"c_Codigo\", " + "\n" +
                "'CODIGO MAESTRO' AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI2 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 0 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_CodNasa\", SUBSTRING(\"OITM\".\"ItemCode\", 1, 7) AS \"c_Codigo\", " + "\n" +
                "'PLU' AS \"c_Descripcion\", " + "\n" +
                mCodigoEDI3 + "1 AS \"n_Cantidad\", 0 AS \"nu_TipoPrecio\", " + "\n" +
                "IfNULL(\"OITM\".\"CodeBars\", '') AS \"DefaultCode\", 0 AS \"SAPUserChange\" " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "WHERE SUBSTRING(\"OITM\".\"ItemCode\", 1, 2) = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " + "\n" +
                "AND LENGTH(\"OITM\".\"ItemCode\") >= " + Properties.Settings.Default.SAP_LongitudMinimaCodigoProductosPesados + " " + "\n" +
                "AND LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n";

                //

                List<String> mUndMedidaPesable; String mCriterioPesableIN;

                if (Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.ContainsIgnoreCase("*;"))
                    mCriterioPesableIN = "NOT IN ";
                else
                    mCriterioPesableIN = "IN ";

                if (Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.isUndefined())
                    mUndMedidaPesable = "|[NULL]|".Split(';').ToList();
                else
                    mUndMedidaPesable = Properties.Settings.Default.SAP_ListaUnidadesDeMedida_ProductosPesables.Split(';').ToList();

                String mInTipoPesable = String.Join(", ", mUndMedidaPesable.Select(x => String.Format("'{0}'", x)).ToList());

                String mInProductosCupon = String.Join(", ", Program.ListaProductosCupon.Values.ToList().Select(x => String.Format("'{0}'", x)).ToList());

                if (!mInProductosCupon.isUndefined()) mInProductosCupon = " OR \"OITM\".\"ItemCode\" IN (" + mInProductosCupon + ")";

                String mProductosInformativos = " OR (\"OITM\".\"InvntItem\" = 'N' AND IfNULL(\"PRC1\".\"Price\", 0) = 0) ";

                String mCampoDescripcion = (Properties.Settings.Default.Stellar_SoloDescripcionCorta ? "U_NOMBREPOS" : "ItemName");
                // Sacar datos de productos. Los precios dependen de la lista de precios que se indique en el Setup.
                // EL tipo de producto lo determina: Si el codigo hace match con la matriz de codigo de SAP son pesados, 
                // Si la unidad de medida es alguna de las definidas en una lista de Setup, son pesables. 
                // Si la lista de precios tiene definida la bandera de Entrada Manual del precio son informativos.
                // Ante cualquier otro caso distinto son productos por unidad.
                // Manejo de bandera para especificar si un producto se maneja por Lotes o por Numeros de Serie.

                String SQLProductos = String.Empty +
                "SELECT " + "\n" +
                "\"OITM\".\"ItemCode\" AS \"c_Codigo\", IfNULL(\"" + mCampoDescripcion + "\", '') AS \"c_Descri\", " + "\n" +
                "IfNULL(\"OITB\".\"ItmsGrpCod\", '0') AS \"c_Departamento\", " + "\n" +
                "IfNULL(\"@GRUPO_INV\".\"Code\", '') AS \"c_Grupo\", " + "\n" +
                "IfNULL(\"@CLASES_INV\".\"Code\", '') AS \"c_Subgrupo\", " + "\n" +
                "IfNULL(\"OMRC\".\"FirmName\", '') AS \"c_Marca\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoAct\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoAnt\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoPro\", " + "\n" +
                "IfNULL(\"AvgPrice\", 0) AS \"n_CostoRep\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC1\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC1\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio1\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC2\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC2\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio2\", " + "\n" +
                "CASE WHEN(\"U_PARPOS\" = 'S' OR \"OITM\".\"Deleted\" = 'Y') THEN " + "\n" +
                "   CASE WHEN \"PRC3\".\"Ovrwritten\" = 'Y' THEN " + "\n" +
                "        0 " + "\n" +
                "    ELSE " + "\n" +
                "        IfNULL(\"PRC3\".\"Price\", 0) " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "    0 " + "\n" +
                "END AS \"n_Precio3\", " + "\n" +
                "CASE WHEN \"OITM\".\"ManSerNum\" = 'Y' THEN '2' WHEN \"OITM\".\"ManBtchNum\" = 'Y' THEN '3' ELSE '1' END AS \"c_Seriales\", " + "\n" +
                "IfNULL(\"SalUnitMsr\", '') AS \"c_Presenta\", " + "\n" +
                "IfNULL(\"NumInSale\", 1) AS \"n_Cantibul\", " + "\n" +
                "IfNULL(\"OSTC\".\"Rate\", 0) AS \"n_Impuesto1\", " + "\n" +
                "CASE WHEN(\"frozenFor\" = 'Y' OR \"OITM\".\"Deleted\" = 'Y') THEN 0 ELSE 1 END AS \"n_Activo\", " + "\n" +
                "CASE " + "\n" +
                "   WHEN (\"OITM\".\"InvntItem\" = 'N' AND IfNULL(\"PRC1\".\"Price\", 0) = 0) THEN " +
                "       4-- INFORMATIVO / SERVICIO " + "\n" +
                "   WHEN \"U_NUMERODECIMALES\" > 0 THEN " + "\n" +
                "   CASE WHEN SUBSTRING(\"OITM\".\"ItemCode\", 1, 2) = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " +
                "   AND LENGTH(\"OITM\".\"ItemCode\") >= " + Properties.Settings.Default.SAP_LongitudMinimaCodigoProductosPesados + " THEN " + "\n" +
                "       2-- PESADO / ETIQUETA PERO TAMBIEN LO VAN A PESAR EN CAJA " + "\n" +
                "   ELSE " + "\n" +
                "       CASE WHEN UPPER(\"SalUnitMsr\") " + mCriterioPesableIN +"(" + mInTipoPesable + ") THEN " + "\n" +
                "           2-- PESABLE " + "\n" +
                "       ELSE " + "\n" +
                "           CASE WHEN IfNULL(\"PRC1\".\"Ovrwritten\", '') = 'Y' THEN " + "\n" +
                "               4-- INFORMATIVO / SERVICIO " + "\n" +
                "           ELSE " + "\n" +
                "               0-- UNIDAD " + "\n" +
                "           END " + "\n" +
                "        END " + "\n" +
                "    END " + "\n" +
                "ELSE " + "\n" +
                "   0-- UNIDAD " + "\n" +
                "END AS \"n_TipoPeso\", " + "\n" +
                "IfNULL(\"U_NOMBREPOS\", '') AS \"cu_Descripcion_Corta\", " + "\n" +
                "IfNULL(\"U_NUMERODECIMALES\", 0) AS \"Cant_Decimales\", " + "\n" +
                "\"OITM\".\"CreateDate\" AS \"Add_Date\", " + "\n" +
                "CASE WHEN \"PEND\".\"CHDATE\" IS NULL THEN " + "\n" +
                "\"OITM\".\"UpdateDate\" " + "\n" +
                "ELSE " + "\n" +
                "TO_TIMESTAMP(LEFT(TO_VARCHAR(\"PEND\".\"CHDATE\"), 10) || ' ' || TO_VARCHAR(\"PEND\".\"CHTIME\")) " + "\n" +
                "END AS \"Update_Date\", " + "\n" +
                "IfNULL(\"PRC1\".\"Currency\", '') AS \"c_CodMoneda\", " + "\n" +
                "IfNULL(\"OITM\".\"UserText\", '') AS \"c_Observacio\", " + "\n" +
                "IfNULL(\"OITM\".\"WarrntTmpl\", '') AS \"WarrntTmpl\", " + "\n" +
                "IfNULL(\"OCTT\".\"Remark\", '') AS \"U_pltgrt\", " + "\n" +
                "1 AS nu_NivelClave, " + "\n" +
                "'' AS \"U_EOF\" " + "\n" +
                "--, \"OITM\".* " + "\n" +
                "FROM \"OITM\" " + "\n" +
                mJoinBitacora +
                "LEFT JOIN \"OITB\" " + "\n" +
                "ON \"OITM\".\"ItmsGrpCod\" = \"OITB\".\"ItmsGrpCod\" " + "\n" +
                "LEFT JOIN \"@GRUPO_INV\" " + "\n" +
                "ON \"OITM\".\"U_GRUPO\" = \"@GRUPO_INV\".\"Code\" " + "\n" +
                "LEFT JOIN \"@CLASES_INV\" " + "\n" +
                "ON \"OITM\".\"U_CLASE\" = \"@CLASES_INV\".\"Code\" " + "\n" +
                "LEFT JOIN \"OMRC\" " + "\n" +
                "ON \"OITM\".\"FirmCode\" = \"OMRC\".\"FirmCode\" " + "\n" +
                "LEFT JOIN \"OSTC\" " + "\n" +
                "ON \"OITM\".\"TaxCodeAR\" = \"OSTC\".\"Code\" " + "\n" +
                "LEFT JOIN \"OCTT\" " + "\n" +
                "ON \"OITM\".\"WarrntTmpl\" = \"OCTT\".\"TmpltName\" " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC1\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC1\".\"ItemCode\" " + "\n" +
                "AND \"PRC1\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceListNo + ") " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC2\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC2\".\"ItemCode\" " + "\n" +
                "AND \"PRC2\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceList2No + ") " + "\n" +
                "LEFT JOIN \"ITM1\" AS \"PRC3\" " + "\n" +
                "ON \"OITM\".\"ItemCode\" = \"PRC3\".\"ItemCode\" " + "\n" +
                "AND \"PRC3\".\"PriceList\" = (" + Properties.Settings.Default.SAP_SyncPriceList3No + ") " + "\n" +
                "WHERE LENGTH(\"OITM\".\"ItemCode\") <= 15 " + "\n" +
                "AND \"OITM\".\"ManBtchNum\" = 'N' " + "\n" +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio ? "AND ((IfNULL(\"PRC1\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty) +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio2 ? "AND ((IfNULL(\"PRC2\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty) +
                (Properties.Settings.Default.SAP_OmitirProductosSinPrecio3 ? "AND ((IfNULL(\"PRC3\".\"Price\", 0) > 0) " + mInProductosCupon + mProductosInformativos + ") " + "\n" : String.Empty);

                String SQLInventarioxItems = String.Empty;

                Boolean ActualizarTimeStampUltimaActualizacionInventarioXItems = false;

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                {

                    Boolean mSoloCambios = true; String mCambiosS = String.Empty; String mCambiosL = String.Empty;

                    if (Properties.Settings.Default.SAP_SincronizarInventarioXItems_CadaCuantosMinutos > 0)
                        if (DateTime.Compare(DateTime.Now, Properties.Settings.Default.SAP_SincronizarInventarioXItems_UltimaActualizacion.AddMinutes(
                        Properties.Settings.Default.SAP_SincronizarInventarioXItems_CadaCuantosMinutos)) > 0)
                        {
                            mSoloCambios = false; // Si ya han pasado mas de X minutos desde la ultima actualizacion, traerse todo.
                            ActualizarTimeStampUltimaActualizacionInventarioXItems = true;
                        }
                    
                    if (mSoloCambios)
                    {
                        mCambiosS = mJoinBitacora.Replace("ON \"OITM\"", "ON \"OSRI\"");
                        mCambiosL = mJoinBitacora.Replace("ON \"OITM\"", "ON \"OIBT\"");
                    }

                    SQLInventarioxItems = String.Empty +
                    "SELECT \"WhsCode\", \"OSRI\".\"ItemCode\", \"SysSerial\", \"IntrSerial\", " + "\n" +
                    "\"SuppSerial\", \"BatchId\", IfNULL(\"InDate\", CURRENT_DATE) AS \"InDate\", \"PrdDate\", " + "\n" +
                    "\"ExpDate\", 1 AS \"RegQty\", CASE WHEN \"Status\" = 0 THEN 1 ELSE 0 END AS \"AvlQty\", 0 AS \"BlqQty\", " + "\n" +
                    "IfNULL(\"Notes\", '') AS \"Notes\", 1 AS \"Status\", \"GrntStart\", \"GrntExp\", 'S' AS RegType " + "\n" +
                    "FROM \"OSRI\" " + "\n" +
                    mCambiosS +
                    "WHERE LENGTH(\"OSRI\".\"ItemCode\") <= 15 " + "\n" +
                    "UNION ALL " + "\n" +
                    "SELECT \"WhsCode\", \"OIBT\".\"ItemCode\", \"SysNumber\" AS \"SysSerial\", \"SuppSerial\" AS \"IntrSerial\", " + "\n" +
                    "'' AS \"SuppSerial\", \"BatchNum\" AS \"BatchId\", IfNULL(\"InDate\", CURRENT_DATE) AS \"InDate\", \"PrdDate\", " + "\n" +
                    "\"ExpDate\", \"Quantity\" AS \"RegQty\", \"Quantity\" AS \"AvlQty\", CASE WHEN \"Status\" = 0 THEN 0 ELSE \"Quantity\" END AS \"BlqQty\", " + "\n" +
                    "IfNULL(\"Notes\", '') AS \"Notes\", CASE WHEN \"Status\" = 0 THEN 1 ELSE 2 END AS \"Status\", NULL AS \"GrntStart\", NULL AS \"GrntExp\", 'L' AS RegType " + "\n" +
                    "FROM \"OIBT\" " + "\n" +
                    mCambiosL +
                    "WHERE LENGTH(\"OIBT\".\"ItemCode\") <= 15 " + "\n";

                }

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp; Boolean mBoolTemp;

                //

                mEtapa = "Consultando Codigos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLCodigos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Codigos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_CODIGOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        LocalRs.Fields["c_CodNasa"].Value = mRs.Fields.Item("c_CodNasa").Value;
                        LocalRs.Fields["c_Codigo"].Value = mRs.Fields.Item("c_Codigo").Value;
                        LocalRs.Fields["c_Descripcion"].Value = mRs.Fields.Item("c_Descripcion").Value;
                        LocalRs.Fields["nu_Intercambio"].Value = mRs.Fields.Item("nu_Intercambio").Value;
                        LocalRs.Fields["n_Cantidad"].Value = mRs.Fields.Item("n_Cantidad").Value;
                        LocalRs.Fields["nu_TipoPrecio"].Value = mRs.Fields.Item("nu_TipoPrecio").Value;
                        LocalRs.Fields["DefaultCode"].Value = mRs.Fields.Item("DefaultCode").Value;
                        LocalRs.Fields["SAPUserChange"].Value = mRs.Fields.Item("SAPUserChange").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                if (Properties.Settings.Default.Stellar_AutoGenerarMatrizCodigo)
                {

                    //

                    mEtapa = "Validando Matriz de Codigo SBO";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute("DELETE FROM VAD20.DBO.MA_ETIQUETAS " + "\n" +
                    "WHERE c_Longitud = '" + Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo + "' " + "\n" +
                    "AND n_Longitud = 13 " + "\n" +
                    "AND c_Codigo <> 'SBO'", out Records);

                    if (Convert.ToDouble(LocalDB.Execute("SELECT isNULL(COUNT(*), 0) AS Existe " + "\n" +
                    "FROM VAD20.DBO.MA_ETIQUETAS " + "\n" +
                    "WHERE c_Codigo = 'SBO'", out Records).Fields[0].Value) == 0)
                    {

                        ADODB.Recordset LocalRs = new ADODB.Recordset();

                        LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                        LocalRs.Open("SELECT * FROM VAD20.DBO.MA_ETIQUETAS WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                        LocalRs.ActiveConnection = null;

                        //while (!mRs.EoF)
                        {

                            LocalRs.AddNew();

                            LocalRs.Fields["c_Codigo"].Value = "SBO";
                            LocalRs.Fields["c_Descripcio"].Value = "ETIQUETA BALANZA SBO";
                            LocalRs.Fields["c_Longitud"].Value = Properties.Settings.Default.SAP_SecuenciaInicioMatrizCodigo;
                            LocalRs.Fields["n_Longitud"].Value = 13;
                            LocalRs.Fields["n_Pos_Ini_1"].Value = 1;
                            LocalRs.Fields["n_Caracteres_1"].Value = 7;
                            LocalRs.Fields["c_Campo"].Value = 1;
                            LocalRs.Fields["n_Pos_Ini_2"].Value = 8;
                            LocalRs.Fields["n_Caracteres_2"].Value = 2;
                            LocalRs.Fields["b_Maneja_Decimal"].Value = true;
                            LocalRs.Fields["n_Pos_Ini_3"].Value = 10;
                            LocalRs.Fields["n_Caracteres_3"].Value = 3;
                            LocalRs.Fields["b_Activa"].Value = true;
                            LocalRs.Fields["b_VerificaDigito"].Value = Properties.Settings.Default.Stellar_AutoGenerarMatrizCodigo_DigitoVerificador;

                            //mRs.MoveNext();

                        }

                        LocalRs.ActiveConnection = LocalDB;

                        LocalRs.UpdateBatch();

                    }

                }

                if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                {

                    //

                    mEtapa = "Consultando Seriales y Lotes en SAP";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                    mRs.DoQuery(SQLInventarioxItems);

                    if (!mRs.EoF)
                    {

                        mEtapa = "Insertando Registros Temp InvxItems";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        ADODB.Recordset LocalRs = new ADODB.Recordset();

                        LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                        LocalRs.Open("SELECT * FROM #TMP_INVENTARIO_ITEM", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                        LocalRs.ActiveConnection = null;

                        while (!mRs.EoF)
                        {

                            LocalRs.AddNew();
                            
                            LocalRs.Fields["CodDeposito"].Value = mRs.Fields.Item("WhsCode").Value;
                            LocalRs.Fields["CodProducto"].Value = mRs.Fields.Item("ItemCode").Value;

                            LocalRs.Fields["IDSistema"].Value = mRs.Fields.Item("SysSerial").Value;
                            LocalRs.Fields["IDInterno"].Value = mRs.Fields.Item("IntrSerial").Value;
                            LocalRs.Fields["IDSerialFabricante"].Value = mRs.Fields.Item("SuppSerial").Value;
                            LocalRs.Fields["IDLoteFabricante"].Value = mRs.Fields.Item("BatchId").Value;

                            LocalRs.Fields["FechaEntrada"].Value = mRs.Fields.Item("InDate").Value;
                            LocalRs.Fields["FechaCreacion"].Value = mRs.Fields.Item("PrdDate").Value;
                            LocalRs.Fields["FechaVencimiento"].Value = mRs.Fields.Item("ExpDate").Value;

                            LocalRs.Fields["CantidadRegistrada"].Value = mRs.Fields.Item("RegQty").Value;
                            LocalRs.Fields["CantidadDisponible"].Value = mRs.Fields.Item("AvlQty").Value;
                            LocalRs.Fields["CantidadComprometida"].Value = mRs.Fields.Item("BlqQty").Value;

                            if (Convert.ToDouble(LocalRs.Fields["CantidadComprometida"].Value) > 0)
                            {

                                // Este campo es para uso interno de Stellar. Si el ERP externo tiene la funcion de comprometer cantidades
                                // Entonces lo que haremos es que no esten disponibles.

                                LocalRs.Fields["CantidadDisponible"].Value = Math.Round(
                                (Convert.ToDouble(LocalRs.Fields["CantidadDisponible"].Value) - Convert.ToDouble(LocalRs.Fields["CantidadComprometida"].Value)), 8, MidpointRounding.AwayFromZero);

                                LocalRs.Fields["CantidadComprometida"].Value = 0;

                            }

                            String mTempLnFix = String.Empty;

                            mTempLnFix = mRs.Fields.Item("Notes").Value;
                            LocalRs.Fields["Comentarios"].Value = mTempLnFix.Replace("\r\r", System.Environment.NewLine); //  ASCIIEncoding.ASCII.GetString(new byte[] { 13, 10 })

                            LocalRs.Fields["Status"].Value = mRs.Fields.Item("Status").Value;
                            LocalRs.Fields["FechaInicioGarantia"].Value = mRs.Fields.Item("GrntStart").Value;
                            LocalRs.Fields["FechaFinGarantia"].Value = mRs.Fields.Item("GrntExp").Value;
                            LocalRs.Fields["TipoRegistro"].Value = mRs.Fields.Item("RegType").Value;
                            
                            mRs.MoveNext();

                        }

                        LocalRs.ActiveConnection = LocalDB;

                        LocalRs.UpdateBatch();

                    }

                }

                //

                mEtapa = "Consultando Productos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLProductos);

                ADODB.Recordset SchemaRs = null;
                ADODB.Fields mLngCamposProd = null;

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Productos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    SchemaRs = LocalDB.Execute("SELECT * FROM MA_PRODUCTOS WHERE 1 = 2", out Records);

                    mLngCamposProd = SchemaRs.Fields;

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_PRODUCTOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        LocalRs.Fields["c_Codigo"].Value = mRs.Fields.Item("c_Codigo").Value;

                        mDatoTemp = mRs.Fields.Item("c_Descri").Value.ToString();
                        LocalRs.Fields["c_Descri"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descri"].DefinedSize);

                        // Validar que el departamento exista, debido a que ellos no tienen integridad de datos a nivel de jerarquia de categorias.
                        // Si el departamento no existiera, lo categorizamos en 0 (Departamento "No Categorizado") creado por el agente como fallback.

                        mBoolTemp = false;

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();

                        mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Departamento), 0) = 0 " +
                        "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_DEPARTAMENTOS_SAP WHERE c_Departamento = '" + mDatoTemp + "'", out Records)
                        .Fields["Existe"].Value);

                        if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                        "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_DEPARTAMENTOS WHERE c_Codigo = '" + mDatoTemp + "'", out Records)
                        .Fields["Existe"].Value);

                        if (!mBoolTemp) mDatoTemp = "0";

                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);

                        // Validar que el grupo sea valido. Si esta en blanco se graba asi, quiere decir que no esta categorizado.
                        // Si no esta en blanco, entonces se valida que el grupo exista dentro de ese departamento.
                        // Si no existe, lo categorizamos en 0 (Grupo "No Categorizado") creado por el agente como falllback.

                        if (mDatoTemp != "0") mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();

                        mBoolTemp = mDatoTemp.isUndefined();

                        if (mDatoTemp != "0" && !mBoolTemp)
                        {
                            mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Grupo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_GRUPOS_SAP WHERE c_Grupo = '" + mDatoTemp + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);

                            if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_GRUPOS WHERE c_Codigo = '" + mDatoTemp + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);
                        }

                        if (!mBoolTemp)
                        {
                            mDatoTemp = "0";
                            LocalRs.Fields["c_Departamento"].Value = mDatoTemp;
                        }

                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);

                        // Validar que el subgrupo sea valido. Si esta en blanco se graba asi, quiere decir que no esta categorizado.
                        // Si no esta en blanco, entonces se valida que el subgrupo exista dentro de ese grupo y departamento.
                        // Si no existe, lo categorizamos en 0 (Subgrupo "No Categorizado") creado por el agente como falllback.

                        if (mDatoTemp != "0") mDatoTemp = mRs.Fields.Item("c_Subgrupo").Value.ToString();

                        mBoolTemp = mDatoTemp.isUndefined();

                        if (mDatoTemp != "0" && !mBoolTemp)
                        {
                            mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Subgrupo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM #TMP_SUBGRUPOS_SAP WHERE c_Subgrupo = '" + mDatoTemp + "' " +
                            "AND c_Grupo = '" + LocalRs.Fields["c_Grupo"].Value + "' " +
                            "AND c_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);

                            if (!mBoolTemp) mBoolTemp = Convert.ToBoolean(LocalDB.Execute("SELECT CAST(CASE WHEN isNULL(COUNT(c_Codigo), 0) = 0 " +
                            "THEN 0 ELSE 1 END AS BIT) AS Existe FROM MA_SUBGRUPOS WHERE c_Codigo = '" + mDatoTemp + "' " +
                            "AND c_In_Grupo = '" + LocalRs.Fields["c_Grupo"].Value + "' " +
                            "AND c_In_Departamento = '" + LocalRs.Fields["c_Departamento"].Value + "' ", out Records)
                            .Fields["Existe"].Value);
                        }

                        if (!mBoolTemp)
                        {
                            mDatoTemp = "0";
                            LocalRs.Fields["c_Grupo"].Value = mDatoTemp;
                            LocalRs.Fields["c_Departamento"].Value = mDatoTemp;
                        }

                        LocalRs.Fields["c_Subgrupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Subgrupo"].DefinedSize);

                        mDatoTemp = mRs.Fields.Item("c_Marca").Value.ToString();
                        LocalRs.Fields["c_Marca"].Value = mDatoTemp.Left(LocalRs.Fields["c_Marca"].DefinedSize);

                        LocalRs.Fields["nu_NivelClave"].Value = 1;

                        LocalRs.Fields["n_Activo"].Value = mRs.Fields.Item("n_Activo").Value;

                        LocalRs.Fields["n_CostoAct"].Value = mRs.Fields.Item("n_CostoAct").Value;
                        LocalRs.Fields["n_CostoAnt"].Value = mRs.Fields.Item("n_CostoAnt").Value;
                        LocalRs.Fields["n_CostoPro"].Value = mRs.Fields.Item("n_CostoPro").Value;
                        LocalRs.Fields["n_CostoRep"].Value = mRs.Fields.Item("n_CostoRep").Value;

                        LocalRs.Fields["n_Precio1"].Value = mRs.Fields.Item("n_Precio1").Value;
                        LocalRs.Fields["n_Precio2"].Value = mRs.Fields.Item("n_Precio2").Value;
                        LocalRs.Fields["n_Precio3"].Value = mRs.Fields.Item("n_Precio3").Value;

                        if (!Convert.ToBoolean(LocalRs.Fields["n_Activo"].Value))
                        {
                            if (Properties.Settings.Default.Stellar_NivelVentaProductosInactivos <= 0)
                            {
                                LocalRs.Fields["n_Precio1"].Value = 0;
                                LocalRs.Fields["n_Precio2"].Value = 0;
                                LocalRs.Fields["n_Precio3"].Value = 0;
                            }
                            else
                            {
                                LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.Stellar_NivelVentaProductosInactivos;
                            }
                        }

                        LocalRs.Fields["c_Seriales"].Value = mRs.Fields.Item("c_Seriales").Value;

                        mDatoTemp = mRs.Fields.Item("c_Presenta").Value.ToString();
                        LocalRs.Fields["c_Presenta"].Value = mDatoTemp.Left(LocalRs.Fields["c_Presenta"].DefinedSize);

                        LocalRs.Fields["n_Cantibul"].Value = mRs.Fields.Item("n_Cantibul").Value;
                        LocalRs.Fields["n_Impuesto1"].Value = mRs.Fields.Item("n_Impuesto1").Value;
                        LocalRs.Fields["n_TipoPeso"].Value = mRs.Fields.Item("n_TipoPeso").Value;

                        mDatoTemp = mRs.Fields.Item("cu_Descripcion_Corta").Value.ToString();
                        LocalRs.Fields["cu_Descripcion_Corta"].Value = mDatoTemp.Left(LocalRs.Fields["cu_Descripcion_Corta"].DefinedSize);

                        if (Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 0
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 3
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 4
                        || Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 5)
                            // Los unitarios, variantes (Car. Ext), informativos y compuestos no pueden manejar decimales.
                            LocalRs.Fields["Cant_Decimales"].Value = 0;
                        else
                            // Decimales solo en Pesados y Pesables. (2, 1)
                            // Si no tienen los decimales correctos, entonces el POS no lo permitira vender.
                            LocalRs.Fields["Cant_Decimales"].Value = mRs.Fields.Item("Cant_Decimales").Value;

                        if (Convert.ToInt32(LocalRs.Fields["n_TipoPeso"].Value) == 4)
                            LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.SAP_NivelClaveProductosInformativos;

                        LocalRs.Fields["Add_Date"].Value = mRs.Fields.Item("Add_Date").Value;
                        LocalRs.Fields["Update_Date"].Value = mRs.Fields.Item("Update_Date").Value;

                        // Esta moneda solo es referencial. No manejamos multimoneda aun.
                        // A pesar de lo que venga aqui igual tomaremos los valores en la moneda predeterminada.
                        // Si algun dia fueramos a controlar multimoneda especificado desde SAP, tendriamos que crear
                        // una lista de asociacion de codigos entre Moneda de Stellar y Moneda SAP.
                        mDatoTemp = mRs.Fields.Item("c_CodMoneda").Value;
                        LocalRs.Fields["c_CodMoneda"].Value = mDatoTemp.Left(LocalRs.Fields["c_CodMoneda"].DefinedSize);

                        mDatoTemp = mRs.Fields.Item("c_Observacio").Value;
                        LocalRs.Fields["c_Observacio"].Value = mDatoTemp.Replace("\r\r", System.Environment.NewLine);

                        LocalRs.Fields["WarrntTmpl"].Value = mRs.Fields.Item("WarrntTmpl").Value;

                        mDatoTemp = mRs.Fields.Item("U_pltgrt").Value;
                        LocalRs.Fields["U_pltgrt"].Value = mDatoTemp.Replace("\r\r", System.Environment.NewLine);

                        if (Program.ListaProductosCupon.Count > 0)
                            if(Program.ListaProductosCupon.ContainsValue(LocalRs.Fields["c_Codigo"].Value.ToString()))
                            {
                                LocalRs.Fields["c_Seriales"].Value = 1; // No maneja seriales a lo interno pero en el POS para la funcionalidad los va a pedir.
                                LocalRs.Fields["n_TipoPeso"].Value = 0; // Al ser un producto que pida seriales solo puede ser unitario.
                                LocalRs.Fields["Cant_Decimales"].Value = 0; // Y de igual manera no puede manejar decimales.
                                LocalRs.Fields["nu_NivelClave"].Value = Properties.Settings.Default.SAP_NivelClaveProductosInformativos;
                            }

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros TR_PEND a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                mEtapa = "Insertando Actualizaciones de Códigos Alternos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                // PRIMERO REGISTRAR LAS ELIMINACIONES DE CODIGOS QUE YA NO VAN

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT COD.c_CodNasa, COD.c_Codigo, COD.c_Descripcion, COD.nu_Intercambio, " +
                    "COD.n_Cantidad, COD.nu_TipoPrecio, 1 AS TipoCambio \n" +
                    "FROM MA_CODIGOS COD LEFT JOIN #TMP_CODIGOS_SAP TMP \n" +
                    "ON COD.c_CodNasa = TMP.c_CodNasa \n" +
                    "AND COD.c_Codigo = TMP.c_Codigo \n" +
                    "WHERE TMP.c_Descripcion IS NULL \n"
                    , out Records);
                else
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT COD.c_CodNasa, COD.c_Codigo, COD.c_Descripcion, COD.nu_Intercambio, " +
                    "COD.n_Cantidad, COD.nu_TipoPrecio, 1 AS TipoCambio \n" +
                    "FROM MA_CODIGOS COD LEFT JOIN #TMP_CODIGOS_SAP TMP \n" +
                    "ON COD.c_CodNasa = TMP.c_CodNasa \n" +
                    "AND COD.c_Codigo = TMP.c_Codigo \n" +
                    "WHERE TMP.c_Descripcion IS NULL \n" + 
                    "AND COD.c_CodNasa IN (SELECT DISTINCT c_CodNasa FROM #TMP_CODIGOS_SAP) \n"
                    , out Records);

                // Y AHORA LOS CAMBIOS

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT TMP.c_CodNasa, TMP.c_Codigo, TMP.c_Descripcion, TMP.nu_Intercambio, " +
                    "TMP.n_Cantidad, TMP.nu_TipoPrecio, 0 AS TipoCambio \n" +
                    "FROM #TMP_CODIGOS_SAP TMP \n"
                    , out Records);
                else
                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_CODIGO \n" +
                    "(c_CodNasa, c_Codigo, c_Descripcion, nu_Intercambio, n_Cantidad, nu_TipoPrecio, TipoCambio) \n" +
                    "SELECT TMP.c_CodNasa, TMP.c_Codigo, TMP.c_Descripcion, TMP.nu_Intercambio, " +
                    "TMP.n_Cantidad, TMP.nu_TipoPrecio, 0 AS TipoCambio \n" +
                    "FROM #TMP_CODIGOS_SAP TMP LEFT JOIN MA_CODIGOS COD \n" +
                    "ON TMP.c_CodNasa = COD.c_CodNasa \n" +
                    "AND TMP.c_Codigo = COD.c_Codigo \n" +
                    "WHERE isNULL(COD.c_Descripcion, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcion COLLATE MODERN_SPANISH_CI_AS \n"
                    , out Records);

                mEtapa = "Insertando Actualizaciones de Productos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                if (mLngCamposProd != null)
                {
                    
                    Boolean CambiosPoliticaGarantia = false;

                    // Insertar cualquier politica de garantia nueva.

                    mEtapa = "Insertando Nuevas Politicas de Garantia";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute(
                    "INSERT INTO MA_PRODUCTOS_DATOS_GARANTIA (AliasPlantillaGarantia, TextoCompleto) \n" +
                    "SELECT WarrntTmpl, U_pltgrt \n" +
                    "FROM #TMP_PRODUCTOS_SAP TPS \n" +
                    "LEFT JOIN MA_PRODUCTOS_DATOS_GARANTIA MPDG \n" +
                    "ON TPS.WarrntTmpl = MPDG.AliasPlantillaGarantia \n" +
                    "WHERE MPDG.AliasPlantillaGarantia IS NULL \n" +
                    "AND isNULL(TPS.WarrntTmpl, '') <> '' \n" +
                    "AND isNULL(TPS.U_pltgrt, '') <> '' \n" +
                    "GROUP BY WarrntTmpl, U_pltgrt", out Records);

                    CambiosPoliticaGarantia = CambiosPoliticaGarantia || ((Int32) Records > 0);

                    // Actualizar el texto de cualquier politica de garantia que haya cambiado.

                    mEtapa = "Insertando Actualizaciones de Politicas de Garantia";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    LocalDB.Execute(
                    "UPDATE MA_PRODUCTOS_DATOS_GARANTIA SET \n" +
                    "TextoCompleto = TPS.U_pltgrt \n" +
                    "FROM (\n" +
                    "SELECT WarrntTmpl, U_pltgrt \n" +
                    "FROM #TMP_PRODUCTOS_SAP \n" +
                    "WHERE 1 = 1 \n" +
                    "AND isNULL(WarrntTmpl, '') <> '' \n" +
                    "AND isNULL(U_pltgrt, '') <> '' \n" +
                    "GROUP BY WarrntTmpl, U_pltgrt \n" +
                    ") TPS \n" +
                    "INNER JOIN MA_PRODUCTOS_DATOS_GARANTIA MPDG \n" +
                    "ON TPS.WarrntTmpl = MPDG.AliasPlantillaGarantia \n" +
                    "WHERE 1 = 1 \n" +
                    "AND TPS.U_pltgrt <> MPDG.TextoCompleto \n" +
                    "", out Records);

                    CambiosPoliticaGarantia = CambiosPoliticaGarantia || ((Int32)Records > 0);

                    if (CambiosPoliticaGarantia) // Mandar notificaciones de actualizacion a las cajas.
                    {

                        mEtapa = "Insertando Notificaciones Parametros de Caja Faltantes";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "INSERT INTO VAD20.DBO.MA_CAJA_PARAMETROS \n" +
                        "(IDGrupoConfiguracion, cCodigoCaja, cSeccionIni, cVariableIni, cValor, bActivo, cDefaultValue)  \n" +
                        "SELECT 'SYSTEM INTERNAL UPDATE' AS IDGrupoConfiguracion, C.c_Codigo AS cCodigoCaja, " + 
                        "'INTERNAL_USE' AS cSeccionIni, 'ReSync_MPDG' AS cVariableIni, " + 
                        "'1' AS cValor, 1 AS bActivo, '0' AS cDefaultValue " +
                        "FROM VAD20.DBO.MA_CAJA C \n" +
                        "LEFT JOIN VAD20.DBO.MA_CAJA_PARAMETROS P \n" +
                        "ON C.c_Codigo = P.cCodigoCaja \n" +
                        "AND P.cSeccionIni = 'INTERNAL_USE' \n" +
                        "AND P.cVariableIni = 'ReSync_MPDG' \n" +
                        "WHERE P.cCodigoCaja IS NULL \n" +
                        "", out Records);

                        mEtapa = "Actualizando Notificaciones de Parametros de Caja";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "UPDATE VAD20.DBO.MA_CAJA_PARAMETROS SET \n" +
                        "cValor = '1', bActivo = 1 \n" +
                        "WHERE 1 = 1 \n" +
                        "AND cSeccionIni = 'INTERNAL_USE' \n" +
                        "AND cVariableIni = 'ReSync_MPDG' \n" +
                        "", out Records);

                    }

                    if (Properties.Settings.Default.SAP_SincronizarInventarioXItems)
                    {

                        Boolean CambiosInvXItems = false;

                        mEtapa = "Insertando Registros de Inventario X Items Nuevos";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "INSERT INTO MA_DEPOPROD_ITEMS (CodDeposito, CodProducto, IDSistema, IDInterno, \n" +
                        "IDSerialFabricante, IDLoteFabricante, FechaEntrada, FechaCreacion, \n" +
                        "FechaVencimiento, CantidadRegistrada, CantidadDisponible, CantidadComprometida, \n" +
                        "Comentarios, Status, FechaInicioGarantia, FechaFinGarantia, TipoRegistro) \n" +
                        "SELECT TMP.CodDeposito, TMP.CodProducto, TMP.IDSistema, TMP.IDInterno, \n" +
                        "TMP.IDSerialFabricante, TMP.IDLoteFabricante, TMP.FechaEntrada, TMP.FechaCreacion, \n" +
                        "TMP.FechaVencimiento, TMP.CantidadRegistrada, TMP.CantidadDisponible, TMP.CantidadComprometida, \n" +
                        "TMP.Comentarios, TMP.Status, TMP.FechaInicioGarantia, TMP.FechaFinGarantia, TMP.TipoRegistro \n" +
                        "FROM #TMP_INVENTARIO_ITEM TMP \n" +
                        "LEFT JOIN MA_DEPOPROD_ITEMS MDI \n" +
                        "ON TMP.CodDeposito = MDI.CodDeposito \n" +
                        "AND TMP.CodProducto = MDI.CodProducto \n" +
                        "AND TMP.IDSerialFabricante = MDI.IDSerialFabricante \n" +
                        "AND TMP.IDLoteFabricante = MDI.IDLoteFabricante \n" +
                        "AND TMP.TipoRegistro = MDI.TipoRegistro \n" +
                        "WHERE MDI.ID IS NULL \n" +
                        "", out Records);

                        CambiosInvXItems = CambiosInvXItems || ((Int32) Records > 0);

                        mEtapa = "Insertando Actualizaciones de Registro de Inventario X Items";
                        Console.WriteLine(mEtapa);
                        if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                        LocalDB.Execute(
                        "UPDATE MA_DEPOPROD_ITEMS SET \n" +
                        "CodDeposito = TMP.CodDeposito, \n" +
                        "CodProducto = TMP.CodProducto, \n" +
                        "IDSistema = TMP.IDSistema, \n" +
                        "IDInterno = TMP.IDInterno, \n" +
                        "IDSerialFabricante = TMP.IDSerialFabricante, \n" +
                        "IDLoteFabricante = TMP.IDLoteFabricante, \n" +
                        "FechaEntrada = TMP.FechaEntrada, \n" +
                        "FechaCreacion = TMP.FechaCreacion, \n" +
                        "FechaVencimiento = TMP.FechaVencimiento, \n" +
                        "CantidadRegistrada = TMP.CantidadRegistrada, \n" +
                        "CantidadDisponible = TMP.CantidadDisponible, \n" +
                        "--CantidadComprometida = TMP.CantidadComprometida, -- AQUI no vamos a afectar por que es un campo de uso interno. \n" +
                        "Comentarios = TMP.Comentarios, \n" +
                        "Status = TMP.Status, \n" +
                        "FechaInicioGarantia = TMP.FechaInicioGarantia, \n" +
                        "FechaFinGarantia = TMP.FechaFinGarantia, \n" +
                        "TipoRegistro = TMP.TipoRegistro \n" +
                        "FROM #TMP_INVENTARIO_ITEM TMP \n" +
                        "INNER JOIN MA_DEPOPROD_ITEMS MDI \n" +
                        "ON TMP.CodDeposito = MDI.CodDeposito \n" +
                        "AND TMP.CodProducto = MDI.CodProducto \n" +
                        "AND TMP.IDSerialFabricante = MDI.IDSerialFabricante \n" +
                        "AND TMP.IDLoteFabricante = MDI.IDLoteFabricante \n" +
                        "AND TMP.TipoRegistro = MDI.TipoRegistro \n" +
                        "--WHERE MDI.ID IS NOT NULL \n" +
                        "", out Records);

                        CambiosInvXItems = CambiosInvXItems || ((Int32)Records > 0);

                    }

                    // Insertar Pendientes de Productos.

                    LocalDB.Execute(
                    "INSERT INTO TR_PENDIENTE_PROD \n" +
                    "(c_Codigo, c_Descri, c_Departamento, c_Grupo, c_Subgrupo, c_Marca, n_CostoAct, n_CostoAnt, n_CostoPro, n_CostoRep, \n" +
                    "n_Precio1, n_Precio2, n_Precio3, c_Seriales, c_Presenta, n_Cantibul, n_Impuesto1, n_Activo, n_TipoPeso, \n" +
                    "cu_Descripcion_Corta, Cant_Decimales, c_CodMoneda, Add_Date, Update_Date, c_Observacio, " + 
                    Properties.Settings.Default.Stellar_CampoAliasTextoGarantia + ", nu_NivelClave, TipoCambio) \n" +
                    "SELECT T.c_Codigo, LEFT(T.c_Descri, " + mLngCamposProd["c_Descri"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Departamento, " + mLngCamposProd["c_Departamento"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Grupo, " + mLngCamposProd["c_Grupo"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Subgrupo, " + mLngCamposProd["c_Subgrupo"].DefinedSize.ToString() + "), " +
                    "LEFT(T.c_Marca, " + mLngCamposProd["c_Marca"].DefinedSize.ToString() + "), " +
                    "T.n_CostoAct, T.n_CostoAnt, T.n_CostoPro, T.n_CostoRep, T.n_Precio1, T.n_Precio2, T.n_Precio3, T.c_Seriales, " +
                    "LEFT(T.c_Presenta, " + mLngCamposProd["c_Presenta"].DefinedSize.ToString() + "), T.n_Cantibul, T.n_Impuesto1, " +
                    "T.n_Activo, T.n_TipoPeso, LEFT(T.cu_Descripcion_Corta, " + mLngCamposProd["cu_Descripcion_Corta"].DefinedSize.ToString() + "), " +
                    "T.Cant_Decimales, MON.CodMonedaProd, T.Add_Date, T.Update_date, T.c_Observacio, T.WarrntTmpl, T.nu_NivelClave, 0 AS TipoCambio \n" +
                    "FROM #TMP_PRODUCTOS_SAP T CROSS JOIN (SELECT TOP 1 c_CodMoneda AS CodMonedaProd FROM MA_MONEDAS WHERE b_Preferencia = 1) MON \n" +
                    "--ON TMP.c_Departamento = GRU.c_Departamento \n" +
                    "--AND TMP.c_Grupo = GRU.c_Codigo \n" +
                    "--WHERE isNULL(GRU.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                    , out Records);

                    SchemaRs.Close();

                }

                SchemaRs = null;

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Marcar Cambios como procesados en SAP.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String mQueryBase = "UPDATE \"BITACORA\" SET \n" +
                "\"PROCESADO_ST\" = 1 \n" +
                "WHERE \"OBJECT_TYPE\" = 4 \n" +
                "AND \"LECTURA_ST\" = 1 \n" +
                "AND \"PROCESADO_ST\" = 0";

                if (Properties.Settings.Default.SAP_AmbienteMultiLocalidadPorBD)
                    mRs.DoQuery(mQueryBase + " AND \"LOCALIDAD\" = '" + Properties.Settings.Default.SAP_CodLocalidad.Replace("'", String.Empty) + "'");
                else
                    mRs.DoQuery(mQueryBase);

                mEtapa = "Insercion de Pendientes de Productos finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                if (ActualizarTimeStampUltimaActualizacionInventarioXItems)
                {
                    Properties.Settings.Default.SAP_SincronizarInventarioXItems_UltimaActualizacion = DateTime.Now;
                    Properties.Settings.Default.Save();
                }

                if (Properties.Settings.Default.Stellar_MasterData_CargaInicial)
                {
                    Properties.Settings.Default.Stellar_MasterData_CargaInicial = false;
                    Properties.Settings.Default.Save();
                }

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);

                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                Program.Logger.EscribirLog(Any, "ActualizarProductosYCodigos(); Etapa: " + mEtapa);

            }

        }

        private static void ActualizarCategorias()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_DEPARTAMENTOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_GRUPOS_SAP", out Records);
                }
                catch { }

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_SUBGRUPOS_SAP", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT c_Codigo AS c_Departamento, c_Descripcio, " + 
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_DEPARTAMENTOS_SAP FROM MA_DEPARTAMENTOS WHERE 1 = 2", out Records);
                
                LocalDB.Execute("SELECT c_Departamento AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS Dpto, " +
                "c_Codigo AS c_Grupo, c_Descripcio, " + 
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_GRUPOS_SAP FROM MA_GRUPOS WHERE 1 = 2", out Records);
                
                LocalDB.Execute("SELECT c_In_Departamento AS c_Departamento, CAST('' AS NVARCHAR(MAX)) AS Dpto, " +
                "c_In_Grupo AS c_Grupo, CAST('' AS NVARCHAR(MAX)) AS Grupo, " +
                "c_Codigo AS c_Subgrupo, c_Descripcio, " +
                "CAST(0 AS FLOAT) AS Cant INTO #TMP_SUBGRUPOS_SAP FROM MA_SUBGRUPOS WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String SQLDepartamentos = String.Empty +
                "SELECT * FROM ( " + "\n" + 
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"c_Descripcio\", COUNT(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"DPTMP\" LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"DPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"DPTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", " + "\n" +
                "\"DPTO\".\"ItmsGrpNam\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITB\" AS \"DPTO\" LEFT JOIN \"OITM\" AS \"DPTMP\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"DPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"DPTMP\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") \"TB\" ORDER BY \"c_Departamento\" " + "\n";

                String SQLGrupos = String.Empty +
                "SELECT * FROM ( " + "\n" +
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"Dpto\",  " + "\n" +
                "0 AS \"c_Grupo\", 'No Categorizado' AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"GRPTMP\" LEFT JOIN \"@GRUPO_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"GRPTMP\".\"U_GRUPO\" " + "\n" +
                "LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"GRPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL OR \"GRUPO\".\"Code\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"GRPTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", \"DPTO\".\"ItmsGrpNam\" AS \"Dpto\",  " + "\n" +
                "\"GRUPO\".\"Code\" AS \"c_Grupo\", \"GRUPO\".\"Name\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"@GRUPO_INV\" AS \"GRUPO\" INNER JOIN \"OITM\" AS \"GRPTMP\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"GRPTMP\".\"U_GRUPO\" " + "\n" +
                "INNER JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"GRPTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"GRPTMP\".\"U_GRUPO\" IS NOT NULL AND \"GRPTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"GRPTMP\".\"ItmsGrpCod\", \"GRPTMP\".\"U_GRUPO\", " + "\n" +
                "\"GRUPO\".\"Code\", \"GRUPO\".\"Name\", " + "\n" +
                "\"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") TB ORDER BY \"c_Departamento\", \"c_Grupo\" " + "\n";

                String SQLSubgrupos = String.Empty +
                "SELECT * FROM ( " + "\n" +
                "SELECT 0 AS \"c_Departamento\", 'No Categorizado' AS \"Dpto\", " + "\n" +
                "0 AS \"c_Grupo\", 'No Categorizado' AS \"Grupo\", " + "\n" +
                "0 AS \"c_Subgrupo\", 'No Categorizado' AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"OITM\" AS \"SUBTMP\" LEFT JOIN \"@CLASES_INV\" AS \"SUBGRUPO\" " + "\n" +
                "ON \"SUBGRUPO\".\"Code\" = \"SUBTMP\".\"U_CLASE\" " + "\n" +
                "LEFT JOIN \"@CLASES_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"SUBTMP\".\"U_GRUPO\" " + "\n" +
                "LEFT JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"SUBTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"DPTO\".\"ItmsGrpCod\" IS NULL OR \"GRUPO\".\"Code\" IS NULL OR \"SUBGRUPO\".\"Code\" IS NULL " + "\n" +
                "HAVING COUNT(*) > 0 " + "\n" +
                "UNION ALL " + "\n" +
                "SELECT \"SUBTMP\".\"ItmsGrpCod\" AS \"c_Departamento\", \"DPTO\".\"ItmsGrpNam\" AS \"Dpto\", " + "\n" +
                "\"GRUPO\".\"Code\" AS \"c_Grupo\", \"GRUPO\".\"Name\" AS \"Grupo\", " + "\n" +
                "\"SUBGRUPO\".\"Code\" AS \"c_Subgrupo\", \"SUBGRUPO\".\"Name\" AS \"c_Descripcio\", Count(*) AS \"Cant\" " + "\n" +
                "FROM \"@CLASES_INV\" AS \"SUBGRUPO\" INNER JOIN \"OITM\" AS \"SUBTMP\" " + "\n" +
                "ON \"SUBGRUPO\".\"Code\" = \"SUBTMP\".\"U_CLASE\" " + "\n" +
                "INNER JOIN \"@GRUPO_INV\" AS \"GRUPO\" " + "\n" +
                "ON \"GRUPO\".\"Code\" = \"SUBTMP\".\"U_GRUPO\" " + "\n" +
                "INNER JOIN \"OITB\" AS \"DPTO\" " + "\n" +
                "ON \"DPTO\".\"ItmsGrpCod\" = \"SUBTMP\".\"ItmsGrpCod\" " + "\n" +
                "WHERE \"SUBTMP\".\"U_CLASE\" IS NOT NULL AND \"SUBTMP\".\"U_GRUPO\" IS NOT NULL AND \"SUBTMP\".\"ItmsGrpCod\" IS NOT NULL " + "\n" +
                "GROUP BY \"SUBTMP\".\"ItmsGrpCod\", \"SUBTMP\".\"U_GRUPO\", \"SUBTMP\".\"U_CLASE\", " + "\n" +
                "\"SUBGRUPO\".\"Code\", \"SUBGRUPO\".\"Name\", " + "\n" +
                "\"GRUPO\".\"Code\", \"GRUPO\".\"Name\", " + "\n" +
                "\"DPTO\".\"ItmsGrpCod\", \"DPTO\".\"ItmsGrpNam\" " + "\n" +
                ") TB ORDER BY \"c_Departamento\", \"c_Grupo\", \"c_Subgrupo\" " + "\n";

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp;

                //

                mEtapa = "Consultando Departamentos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLDepartamentos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Departamentos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_DEPARTAMENTOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Grupos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLGrupos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Grupos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_GRUPOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        LocalRs.Fields["Dpto"].Value = mRs.Fields.Item("Dpto").Value;
                        mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();
                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Subgrupos en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLSubgrupos);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Subgrupos";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_SUBGRUPOS_SAP", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("c_Departamento").Value.ToString();
                        LocalRs.Fields["c_Departamento"].Value = mDatoTemp.Left(LocalRs.Fields["c_Departamento"].DefinedSize);
                        LocalRs.Fields["Dpto"].Value = mRs.Fields.Item("Dpto").Value;
                        mDatoTemp = mRs.Fields.Item("c_Grupo").Value.ToString();
                        LocalRs.Fields["c_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Grupo"].DefinedSize);
                        LocalRs.Fields["Grupo"].Value = mRs.Fields.Item("Grupo").Value;
                        mDatoTemp = mRs.Fields.Item("c_Subgrupo").Value.ToString();
                        LocalRs.Fields["c_Subgrupo"].Value = mDatoTemp.Left(LocalRs.Fields["c_Subgrupo"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("c_Descripcio").Value.ToString();
                        LocalRs.Fields["c_Descripcio"].Value = mDatoTemp.Left(LocalRs.Fields["c_Descripcio"].DefinedSize);
                        LocalRs.Fields["Cant"].Value = mRs.Fields.Item("Cant").Value;

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros TR_PEND a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                mEtapa = "Insertando Actualizaciones de Departamentos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_DEPARTAMENTOS \n" +
                "(c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_DEPARTAMENTOS_SAP TMP LEFT JOIN MA_DEPARTAMENTOS DEP \n" +
                "ON TMP.c_Departamento = DEP.c_Codigo \n" +
                "WHERE IsNULL(DEP.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                mEtapa = "Insertando Actualizaciones de Grupos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_GRUPOS \n" +
                "(c_Departamento, c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Grupo, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_GRUPOS_SAP TMP LEFT JOIN MA_GRUPOS GRU \n" +
                "ON TMP.c_Departamento = GRU.c_Departamento \n" +
                "AND TMP.c_Grupo = GRU.c_Codigo \n" +
                "WHERE isNULL(GRU.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                mEtapa = "Insertando Actualizaciones de Subgrupos";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute(
                "INSERT INTO TR_PEND_SUBGRUPOS \n" +
                "(c_In_Departamento, c_In_Grupo, c_Codigo, c_Descripcio, Tipo_Cambio) \n" +
                "SELECT TMP.c_Departamento, TMP.c_Grupo, TMP.c_Subgrupo, TMP.c_Descripcio, 0 AS Tipo_Cambio \n" +
                "FROM #TMP_SUBGRUPOS_SAP TMP LEFT JOIN MA_SUBGRUPOS SUB \n" +
                "ON TMP.c_Departamento = SUB.c_In_Departamento \n" +
                "AND TMP.c_Grupo = SUB.c_In_Grupo \n" +
                "AND TMP.c_Subgrupo = SUB.c_Codigo \n" +
                "WHERE isNULL(SUB.c_Descripcio, '**[NULL]**') COLLATE MODERN_SPANISH_CI_AS <> TMP.c_Descripcio COLLATE MODERN_SPANISH_CI_AS \n"
                , out Records);

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Insercion de Pendientes de Categorias finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);
                
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                
                Program.Logger.EscribirLog(Any, "ActualizarCategorias(); Etapa: " + mEtapa);

            }

        }

        private static void ActualizarMotivosDevolucion()
        {

            SAPbobsCOM.Company SAPDB = Program.mCnSAP;
            ADODB.Connection LocalDB = Program.mCnLocal;

            String mEtapa = String.Empty;

            Object Records; Boolean ActiveTrans = false;

            try
            {

                mEtapa = "Limpiando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                try
                {
                    LocalDB.Execute("DROP TABLE #TMP_MA_AUX_GRUPO", out Records);
                }
                catch { }

                mEtapa = "Creando tablas temporales";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.Execute("SELECT cs_Grupo, cs_Tipo, cs_Codigo_Opcion " +
                "INTO #TMP_MA_AUX_GRUPO FROM MA_AUX_GRUPO WHERE 1 = 2", out Records);

                mEtapa = "Creando Queries SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                String SQLTiposDevolucion = String.Empty +
                "SELECT * FROM \"@TIPOS_ANULACION\" ";

                String SQLMotivosDevolucion = String.Empty +
                "SELECT * FROM \"@TIPO_DEV_VTA\" ";

                SAPbobsCOM.Recordset mRs;
                String mDatoTemp;

                //

                mEtapa = "Consultando Tipos Devolucion en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLTiposDevolucion);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Tipos Devolucion";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_MA_AUX_GRUPO WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("Code").Value.ToString();
                        LocalRs.Fields["cs_Codigo_Opcion"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Codigo_Opcion"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("Name").Value.ToString();
                        LocalRs.Fields["cs_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Grupo"].DefinedSize);
                        LocalRs.Fields["cs_Tipo"].Value = "TIPO_DEV_SBO";

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                //

                mEtapa = "Consultando Motivos de Devolucion en SAP";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                mRs = SAPDB.GetBusinessObject(BoObjectTypes.BoRecordset);

                mRs.DoQuery(SQLMotivosDevolucion);

                if (!mRs.EoF)
                {

                    mEtapa = "Insertando Registros Temp Motivos Devolucion";
                    Console.WriteLine(mEtapa);
                    if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                    ADODB.Recordset LocalRs = new ADODB.Recordset();

                    LocalRs.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                    LocalRs.Open("SELECT * FROM #TMP_MA_AUX_GRUPO WHERE 1 = 2", LocalDB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                    LocalRs.ActiveConnection = null;

                    while (!mRs.EoF)
                    {

                        LocalRs.AddNew();

                        mDatoTemp = mRs.Fields.Item("Code").Value.ToString();
                        LocalRs.Fields["cs_Codigo_Opcion"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Codigo_Opcion"].DefinedSize);
                        mDatoTemp = mRs.Fields.Item("Name").Value.ToString();
                        LocalRs.Fields["cs_Grupo"].Value = mDatoTemp.Left(LocalRs.Fields["cs_Grupo"].DefinedSize);
                        LocalRs.Fields["cs_Tipo"].Value = "MOTIVOS_DEV";

                        mRs.MoveNext();

                    }

                    LocalRs.ActiveConnection = LocalDB;

                    LocalRs.UpdateBatch();

                }

                mEtapa = "Insertando registros MA_AUX_GRUPO a partir de tablas Tmp";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

                LocalDB.BeginTrans(); ActiveTrans = true;

                LocalDB.Execute("DELETE FROM MA_AUX_GRUPO WHERE cs_Tipo = 'TIPO_DEV_SBO' ", out Records);
                LocalDB.Execute("DELETE FROM MA_AUX_GRUPO WHERE cs_Tipo = 'MOTIVOS_DEV' ", out Records);

                LocalDB.Execute(
                "INSERT INTO MA_AUX_GRUPO \n" +
                "(cs_Tipo, cs_Grupo, cs_Codigo_Opcion) \n" +
                "SELECT cs_Tipo, cs_Grupo, cs_Codigo_Opcion \n" +
                "FROM #TMP_MA_AUX_GRUPO \n" +
                "", out Records);

                LocalDB.CommitTrans(); ActiveTrans = false;

                mEtapa = "Insercion de Datos de Grupo finalizado.";
                Console.WriteLine(mEtapa);
                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);

            }
            catch (Exception Any)
            {

                if (ActiveTrans) LocalDB.RollbackTrans();

                Console.WriteLine("Error: " + mEtapa);

                if (Properties.Settings.Default.DebugMode) MessageBox.Show(mEtapa);
                
                Program.Logger.EscribirLog(Any, "ActualizarMotivosDevolucion(); Etapa: " + mEtapa);

            }

        }

    }

}
