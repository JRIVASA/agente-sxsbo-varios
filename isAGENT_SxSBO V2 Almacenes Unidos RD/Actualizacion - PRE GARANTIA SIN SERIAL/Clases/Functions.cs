﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace isAGENT_SxSBO.Clases
{
    class Functions
    {
        public static string EncryptionKey;

        public Functions()
        {
            //LoadSettings();
        }

        static Functions()
        {
            EncryptionKey = Properties.Settings.Default.EncryptionKey;
        }

        public static String getAlternateTrustedConnectionString()
        {
            return
            "Server=" + Properties.Settings.Default.Stellar_SQLServerName +
            ";Database=" + Properties.Settings.Default.Stellar_SQLDBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString()
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : String.Empty)
            );
        }

        public static String getAlternateTrustedConnectionString(String ServerInstance, String DBName)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";Trusted_Connection=True;"
            ;
        }

        public static SqlConnection getAlternateTrustedConnection(String ServerInstance, String DBName, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateTrustedConnectionString(ServerInstance, DBName)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : String.Empty)
            );
        }

        public static String getAlternateConnectionString(String UserID, String Password)
        {
            return
            "Server=" + Properties.Settings.Default.Stellar_SQLServerName +
            ";Database=" + Properties.Settings.Default.Stellar_SQLDBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : String.Empty)
            );
        }

        public static String getAlternateConnectionString(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return
            "Server=" + ServerInstance +
            ";Database=" + DBName +
            ";User Id=" + UserID +
            ";Password=" + Password + ";"
            ;
        }

        public static SqlConnection getAlternateConnection(String ServerInstance, String DBName, String UserID, String Password, int ConnectionTimeout = -1)
        {
            return new SqlConnection(
            getAlternateConnectionString(ServerInstance, DBName, UserID, Password)
            + ((ConnectionTimeout != -1) ? "Connection Timeout = " + ConnectionTimeout + ";" : String.Empty)
            );
        }

        public static Boolean CheckDBConnection(SqlConnection pDBConnection)
        {
            Boolean Returns = false;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", pDBConnection);
                pDBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    Returns = true; // Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return Returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                pDBConnection.Close();
            }
        }

        public static void QuickSyncDB(SqlConnection pDBConnection, String Query)
        {
            try
            {
                SqlCommand command = new SqlCommand(Query, pDBConnection);
                pDBConnection.Open();

                command.CommandTimeout = 30000;

                int rows = command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                pDBConnection.Close();
            }
        }

        public DateTime getServerDatetime(SqlConnection DBConnection)
        {
            DateTime returns = DateTime.Now;

            try
            {
                SqlCommand command = new SqlCommand("SELECT GETDATE() AS ServerTime", DBConnection);
                DBConnection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    returns = Convert.ToDateTime(reader["ServerTime"]);
                }

                reader.Close();
                return returns;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return returns;
            }
            finally
            {
                DBConnection.Close();
            }
        }

        public static SqlCommand getParameterizedCommand(SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand();
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection);
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static SqlCommand getParameterizedCommand(String CommandText, SqlConnection Connection, SqlTransaction Transaction, SqlParameter[] Params)
        {
            SqlCommand returnCommand = new SqlCommand(CommandText, Connection, Transaction);
            // For some reason the constructor doesn't work, so the Transaction must be explicitly applied to the Command.
            returnCommand.Transaction = Transaction;             
            if (Params != null) returnCommand.Parameters.AddRange(Params);
            return returnCommand;
        }

        public static string ByteToHex2(byte[] bin)
        {
            string Result = String.Empty;

            for (int i = 0; i < bin.Length; i++)
            {
                Result += bin[i].ToString("X2"); // Ex: byte [1] -> "01" instead of "1"
            }
            return (Result);
        }

        public static byte[] Hex2ToByte(String Text)
        {
            if (Text == null)
                return null;

            //if (Text.Length % 2 == 1)
            //    Text = '0' + Text; // Up to you whether to pad the first or last byte

            byte[] bin = new byte[Text.Length / 2];

            for (int i = 0; i < bin.Length; i++)
                bin[i] = Convert.ToByte(Text.Substring(i * 2, 2), 16);

            return bin;
        }

        public static void LoadSettings()
        {

            // Load All Settings using the LoadValue function which chains a SettingsProperty with
            // a Section and a Key Name in the final .ini style File.

            try
            {

            if (!(System.IO.Directory.Exists(QuickSettings.FolderPath))) System.IO.Directory.CreateDirectory(QuickSettings.FolderPath);

            if (Properties.Settings.Default.PropertyValues.Count <= 0)
            {
                foreach (System.Configuration.SettingsProperty Property in Properties.Settings.Default.Properties.OfType<System.Configuration.SettingsProperty>())
                {
                    System.Configuration.SettingsPropertyValue Value = new System.Configuration.SettingsPropertyValue(Property);
                    Properties.Settings.Default.PropertyValues.Add(Value);
                }
            }

            Properties.Settings.Default.Properties["Stellar_TrustedConnection"].LoadValue("Stellar", "TrustedConnection");
            Properties.Settings.Default.Properties["Stellar_SQLServerName"].LoadValue("Stellar", "SQLServerName");
            Properties.Settings.Default.Properties["Stellar_SQLDBName"].LoadValue("Stellar", "SQLDBName");
            Properties.Settings.Default.Properties["Stellar_SQLUser"].LoadValue("Stellar", "SQLUser");
            Properties.Settings.Default.Properties["Stellar_SQLPass"].LoadValue("Stellar", "SQLPass");
            Properties.Settings.Default.Properties["Stellar_CommandTimeout"].LoadValue("Stellar", "CommandTimeout");
            Properties.Settings.Default.Properties["Stellar_ConnectionTimeout"].LoadValue("Stellar", "ConnectionTimeout");

            Properties.Settings.Default.Properties["SAP_Server"].LoadValue("SAP", "Server");
            Properties.Settings.Default.Properties["SAP_LicenseServer"].LoadValue("SAP", "LicenseServer");
            Properties.Settings.Default.Properties["SAP_DBName"].LoadValue("SAP", "DBName");
            Properties.Settings.Default.Properties["SAP_DBUser"].LoadValue("SAP", "DBUser");
            Properties.Settings.Default.Properties["SAP_DBPass"].LoadValue("SAP", "DBPass");
            Properties.Settings.Default.Properties["SAP_CompanyUser"].LoadValue("SAP", "CompanyUser");
            Properties.Settings.Default.Properties["SAP_CompanyPass"].LoadValue("SAP", "CompanyPass");
            Properties.Settings.Default.Properties["SAP_LangID"].LoadValue("SAP", "LangID");
            Properties.Settings.Default.Properties["SAP_ServerTypeID"].LoadValue("SAP", "ServerTypeID");
            Properties.Settings.Default.Properties["SAP_TrustedConnection"].LoadValue("SAP", "TrustedConnection");

            Properties.Settings.Default.Properties["EnviarVentaAgrupada"].LoadValue("Stellar", "EnviarVentaAgrupada");
            Properties.Settings.Default.Properties["ObtenerListasAsociacionBD"].LoadValue("Stellar", "ObtenerListasAsociacionBD");
            Properties.Settings.Default.Properties["ListaAsociacionBancos"].LoadValue("Stellar", "ListaAsociacionBancos");
            Properties.Settings.Default.Properties["ListaAsociacionFormaPago"].LoadValue("Stellar", "ListaAsociacionFormaPago");
            Properties.Settings.Default.Properties["ListaAsociacionMonedas"].LoadValue("Stellar", "ListaAsociacionMonedas");
            Properties.Settings.Default.Properties["ModalidadAgente"].LoadValue("Stellar", "ModalidadAgente");

            Properties.Settings.Default.Properties["nLotesCorrida"].LoadValue("Stellar", "nLotesCorrida");
            Properties.Settings.Default.Properties["nRegistrosLote"].LoadValue("Stellar", "nRegistrosLote");

            Properties.Settings.Default.Properties["SAP_CashAccount"].LoadValue("SAP", "CashAccount");
            Properties.Settings.Default.Properties["SAP_ChecksAccount"].LoadValue("SAP", "ChecksAccount");
            Properties.Settings.Default.Properties["SAP_TransfersAccount"].LoadValue("SAP", "TransfersAccount");

            Properties.Settings.Default.Properties["Stellar_ClienteContado"].LoadValue("Stellar", "CodigoClienteEventual");
            Properties.Settings.Default.Properties["SAP_ClienteContado"].LoadValue("SAP", "CodigoClienteEventual");

            Properties.Settings.Default.Properties["TaxList1"].LoadValue("Stellar", "TaxList1");
            Properties.Settings.Default.Properties["TaxList2"].LoadValue("Stellar", "TaxList2");
            Properties.Settings.Default.Properties["TaxList3"].LoadValue("Stellar", "TaxList3");

            Properties.Settings.Default.Properties["SAP_FormasPagoFallbackEfectivo"].LoadValue("SAP", "FormasPagoFallbackEfectivo");
            Properties.Settings.Default.Properties["SAP_FormasPagoFallbackBanco"].LoadValue("SAP", "FormasPagoFallbackBanco");
            Properties.Settings.Default.Properties["SAP_FormasPagoFallbackMoneda"].LoadValue("SAP", "FormasPagoFallbackMoneda");
            
            Properties.Settings.Default.Properties["SAP_UnSoloPaymentsPorFactura"].LoadValue("SAP", "UnSoloPaymentsPorFactura");
            Properties.Settings.Default.Properties["SAP_FormaPagoFactura_SinDetallePago"].LoadValue("SAP", "FormaPagoFactura_SinDetallePago");
            Properties.Settings.Default.Properties["SAP_PagosEfectivoAMultiplesCuentas"].LoadValue("SAP", "PagosEfectivoAMultiplesCuentas");
            Properties.Settings.Default.Properties["SAP_LlenarCategoriaFiscalPagos"].LoadValue("SAP", "LlenarCategoriaFiscalPagos");

            Properties.Settings.Default.Properties["SAP_CodigoMonedaPredeterminada"].LoadValue("SAP", "CodigoMonedaPredeterminada");

            Properties.Settings.Default.Properties["SAP_Linea_ListaTipoPrecio"].LoadValue("SAP", "Linea_ListaTipoPrecio");
            Properties.Settings.Default.Properties["SAP_Linea_DeterminarPorcentajeDescuentoItem"].LoadValue("SAP", "Linea_DeterminarPorcentajeDescuentoItem");

            Properties.Settings.Default.Properties["SAP_SerieDocumentoFactura"].LoadValue("SAP", "SerieDocumentoFactura");
            Properties.Settings.Default.Properties["SAP_SerieDocumentoNC"].LoadValue("SAP", "SerieDocumentoNC");
            Properties.Settings.Default.Properties["SAP_SerieDocumentoPago"].LoadValue("SAP", "SerieDocumentoPago");
            Properties.Settings.Default.Properties["SAP_SerieDocumentoEntrega"].LoadValue("SAP", "SerieDocumentoEntrega");

            Properties.Settings.Default.Properties["SAP_CuentaContableFactura"].LoadValue("SAP", "CuentaContableFactura");
            Properties.Settings.Default.Properties["SAP_CuentaContableNotaCredito"].LoadValue("SAP", "CuentaContableNotaCredito");

            Properties.Settings.Default.Properties["SAP_ValidarDocumentoFiscal"].LoadValue("SAP", "ValidarDocumentoFiscal");
            Properties.Settings.Default.Properties["SAP_FallbackVentaSinDatosFiscales_MinutosMax"].LoadValue("SAP", "FallbackVentaSinDatosFiscales_MinutosMax");

            Properties.Settings.Default.Properties["SAP_OmitirFichaClientes"].LoadValue("SAP", "OmitirFichaClientes");
            
            Properties.Settings.Default.Properties["Stellar_MasterData_ProcessURL"].LoadValue("Stellar", "MasterData_ProcessURL");
            Properties.Settings.Default.Properties["SAP_SyncPriceListNo"].LoadValue("SAP", "SyncPriceListNo");
            Properties.Settings.Default.Properties["SAP_SyncPriceList2No"].LoadValue("SAP", "SyncPriceList2No");
            Properties.Settings.Default.Properties["SAP_SyncPriceList3No"].LoadValue("SAP", "SyncPriceList3No");
            Properties.Settings.Default.Properties["SAP_OmitirProductosSinPrecio"].LoadValue("SAP", "OmitirProductosSinPrecio");
            Properties.Settings.Default.Properties["SAP_OmitirProductosSinPrecio2"].LoadValue("SAP", "OmitirProductosSinPrecio2");
            Properties.Settings.Default.Properties["SAP_OmitirProductosSinPrecio3"].LoadValue("SAP", "OmitirProductosSinPrecio3");
            Properties.Settings.Default.Properties["SAP_NivelClaveProductosInformativos"].LoadValue("SAP", "NivelClaveProductosInformativos");

            Properties.Settings.Default.Properties["SAP_SecuenciaInicioMatrizCodigo"].LoadValue("SAP", "SecuenciaInicioMatrizCodigo");
            Properties.Settings.Default.Properties["Stellar_AutoGenerarMatrizCodigo"].LoadValue("Stellar", "AutoGenerarMatrizCodigo");
            Properties.Settings.Default.Properties["Stellar_AutoGenerarMatrizCodigo_DigitoVerificador"].LoadValue("Stellar", "AutoGenerarMatrizCodigo_DigitoVerificador");
                
            Properties.Settings.Default.Properties["Stellar_HoraInicioResincronizarCategorias"].LoadValue("Stellar", "HoraInicioResincronizarCategorias");
            Properties.Settings.Default.Properties["Stellar_HoraFinResincronizarCategorias"].LoadValue("Stellar", "HoraFinResincronizarCategorias");
            Properties.Settings.Default.Properties["SAP_ListaUnidadesDeMedida_ProductosPesables"].LoadValue("SAP", "ListaUnidadesDeMedida_ProductosPesables");
            Properties.Settings.Default.Properties["SAP_LongitudMinimaCodigoProductosPesados"].LoadValue("SAP", "LongitudMinimaCodigoProductosPesados");
            Properties.Settings.Default.Properties["Stellar_CodigoEDIAutomatico"].LoadValue("Stellar", "CodigoEDIAutomatico");
            Properties.Settings.Default.Properties["Stellar_MasterData_CargaInicial"].LoadValue("Stellar", "MasterData_CargaInicial");
            Properties.Settings.Default.Properties["Stellar_CodLocalidad"].LoadValue("Stellar", "CodLocalidad");
            Properties.Settings.Default.Properties["SAP_AmbienteMultiLocalidadPorBD"].LoadValue("SAP", "AmbienteMultiLocalidadPorBD");
            Properties.Settings.Default.Properties["SAP_CodLocalidad"].LoadValue("SAP", "CodLocalidad");
            Properties.Settings.Default.Properties["SAP_AliasLocalidad"].LoadValue("SAP", "AliasLocalidad");
            Properties.Settings.Default.Properties["SAP_ClaveOpcionesLocalidad"].LoadValue("SAP", "ClaveOpcionesLocalidad");
            Properties.Settings.Default.Properties["SAP_CodigoAlmacenPorDefectoLocalidad"].LoadValue("SAP", "CodigoAlmacenPorDefectoLocalidad");
            Properties.Settings.Default.Properties["Stellar_AutoProcessMasterData"].LoadValue("Stellar", "AutoProcessMasterData");
            Properties.Settings.Default.Properties["Stellar_CampoAliasTextoGarantia"].LoadValue("Stellar", "CampoAliasTextoGarantia");
            Properties.Settings.Default.Properties["Stellar_CampoDivision"].LoadValue("Stellar", "CampoDivision");
            Properties.Settings.Default.Properties["Stellar_ListaProductosCupon"].LoadValue("Stellar", "ListaProductosCupon");
            Properties.Settings.Default.Properties["Stellar_NivelVentaProductosInactivos"].LoadValue("Stellar", "NivelVentaProductosInactivos");
            Properties.Settings.Default.Properties["Stellar_SoloDescripcionCorta"].LoadValue("Stellar", "SoloDescripcionCorta");

            Properties.Settings.Default.Properties["SAP_SincronizarInventarioXItems"].LoadValue("SAP", "SincronizarInventarioXItems");
            Properties.Settings.Default.Properties["SAP_SincronizarInventarioXItems_CadaCuantosMinutos"].LoadValue("SAP", "SincronizarInventarioXItems_CadaCuantosMinutos");
            Properties.Settings.Default.Properties["SAP_SincronizarInventarioXItems_UltimaActualizacion"].LoadValue("SAP", "SincronizarInventarioXItems_UltimaActualizacion");

            Properties.Settings.Default.Properties["SAP_TipoProductoPorCantidadDecimales"].LoadValue("SAP", "TipoProductoPorCantidadDecimales");

            Properties.Settings.Default.Properties["DebugMode"].LoadValue("Stellar", "DebugMode");

            Properties.Settings.Default.Properties["APP_DEFINED_SAP_DI_API_VERSION"].LoadValue("SAP", "DI_API_VERSION");
                
            // Loading values mark properties as "Changed", but we are essentially initializing them so we 
            // use this function to Remove "Changed" status for all settings.

            Properties.Settings.Default.Clean();

            // From now on, we access settings using the regular strongly-typed settings class which we define
            // using the Properties.Settings UI. Note that you should define all properties which won't
            // be ReadOnly as UserScoped. If you use some ApplicationScoped Settings, just can just use them
            // but you can't write any values to them, you would get a Runtime Exception by using LoadValue or a
            // Compiler Error if you intend to assign other values explicitly using the strongly-typed members.

            // So here is as we use them:

            // Properties.Settings.Default.Language = "en";

            // And here we will save all settings which were Changed.

            // Properties.Settings.Default.Save();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static String Tab()
        {
            return "\t";
        }

        public static String Tab(int HowMany)
        {
            String Tabs = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Tabs += Tab();
            }
            return Tabs;
        }

        public static String NewLine()
        {
            return System.Environment.NewLine;
        }

        public static String NewLine(int HowMany)
        {
            String Lines = String.Empty;
            for (int i = 1; i <= HowMany; i++)
            {
                Lines += System.Environment.NewLine;
            }
            return Lines;
        }

        public static System.Drawing.Color getColorOrEmpty(String ColorInput)
        {

            System.Drawing.Color MyColor = System.Drawing.Color.Empty; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = System.Drawing.Color.Empty; return MyColor; }
                }
            }

            return MyColor;
        }

        public static System.Drawing.Color getColorOrDefault(String ColorInput, System.Drawing.Color FallBackColor)
        {

            System.Drawing.Color MyColor = FallBackColor; int ColorNumber;

            String[] ColorDetails;

            try { ColorDetails = ColorInput.Split(','); }
            catch (Exception) { return MyColor; }

            if (ColorDetails.Length == 4)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]), Convert.ToInt32(ColorDetails[3]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 3)
                try
                {
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails[0]),
                    Convert.ToInt32(ColorDetails[1]), Convert.ToInt32(ColorDetails[2]));
                }
                catch (Exception) { MyColor = FallBackColor; return MyColor; }

            if (ColorDetails.Length == 1)
            {
                if (int.TryParse(ColorDetails[0], out ColorNumber))
                {
                    try { MyColor = System.Drawing.Color.FromArgb(ColorNumber); }
                    catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
                else
                {
                    if (ColorDetails[0].Contains("#"))
                        try { MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                    else
                        try { MyColor = System.Drawing.Color.FromName(ColorDetails[0]); }
                        catch (Exception) { MyColor = FallBackColor; return MyColor; }
                }
            }

            return MyColor;
        }

        //public static long GetZipExtractedSize(String FolderPath)
        //{

        //    try
        //    {
        //        System.IO.Compression.ZipArchive ZipFile = System.IO.Compression.ZipFile.OpenRead(FolderPath);

        //        long ExtractedBytes = 0;                

        //        foreach (System.IO.Compression.ZipArchiveEntry Item in ZipFile.Entries)
        //        {
        //            //Console.WriteLine(Item.Length);
        //            ExtractedBytes += Item.Length;
        //        }

        //        return ExtractedBytes;
        //    }
        //    catch (Exception Any)
        //    {
        //        Console.WriteLine(Any.Message);
        //        return 0;
        //    }

        //}

        //public static long GetZipCompressedSize(String FolderPath)
        //{

        //    try
        //    {
        //        System.IO.Compression.ZipArchive ZipFile = System.IO.Compression.ZipFile.OpenRead(FolderPath);

        //        long CompressedBytes = 0;

        //        foreach (System.IO.Compression.ZipArchiveEntry Item in ZipFile.Entries)
        //        {
        //            //Console.WriteLine(Item.Length);
        //            CompressedBytes += Item.Length;
        //        }

        //        return CompressedBytes;
        //    }
        //    catch (Exception Any)
        //    {
        //        Console.WriteLine(Any.Message);
        //        return 0;
        //    }

        //}

        public static long GetDirectorySize(String FolderPath)
        {

            try
            {
                String[] AllFilesWithinFolder = System.IO.Directory.GetFiles(FolderPath, "*", System.IO.SearchOption.AllDirectories);

                long TotalBytes = 0;

                foreach (String FilePath in AllFilesWithinFolder)
                {
                    System.IO.FileInfo FileData = new System.IO.FileInfo(FilePath);
                    TotalBytes += FileData.Length;
                }

                return TotalBytes;
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
                return 0;
            }

        }

        public static String PeopleFriendlyFormattedSize(long Bytes)
        {

            // This is for values between 1000 and 1023 so people won't have to read something like 1011 B or 0.98 KB, 
            // We know such Byte values are not exactly 1 KB, but the difference is minimal so we can treat it like that
            // since people commonly deal with values that go in the order of { 10, 100, 1000, 1000000 } and such.
            // Here we make the calculations so people don't have to read either too big or too small values.            

            // Bytes

            if (Bytes <= 0) return "0|B";

            if (Bytes > 1 && Bytes < 1000)
            {
                return Bytes.ToString() + "|" + "B";
            }
            else if (Bytes >= 1000 && Bytes <= 1024)
            {
                return "1|KB";
            }

            // KiloBytes

            double Result = ((double)Bytes / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "KB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|MB";
            }

            // MegaBytes

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|GB";
            }

            // GigaBytes

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "GB";
            }
            else if (Result >= 1000 && Result <= 1024)
            {
                return "1|TB";
            }

            // TeraBytes and Beyond...

            Result = (Result / 1024);

            if (Result > 1 && Result < 1000)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }

            return "0|B";

        }

        public static String RealFormattedSize(long Bytes)
        {

            if (Bytes <= 0) return "0|B";

            double Result = ((double)Bytes / 1024);

            // Bytes
            if (Result < 1)
            {
                return Bytes.ToString() + "|" + "B";
            }

            // KiloBytes
            if (Result > 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "KB";
            }

            // MegaBytes

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "MB";
            }

            // GigaBytes

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "GB";
            }

            // TeraBytes and Beyond...

            Result = (Result / 1024);

            if (Result >= 1 && Result < 1024)
            {
                return Math.Round(Result, 2).ToString() + "|" + "TB";
            }

            return "0|B";

        }

        public static String HttpPost(String URI, String Parameters)
        {

            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
                //Add these, as we're doing a POST
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = "POST";
                //We need to count how many bytes we're sending. 
                //Post'ed Faked Forms should be name=value&
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
                req.ContentLength = bytes.Length;
                System.IO.Stream os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();

                System.Net.WebResponse resp = req.GetResponse();
                if (resp == null) return null;
                System.IO.StreamReader sr =
                      new System.IO.StreamReader(resp.GetResponseStream());
                return sr.ReadToEnd().Trim();

            }
            catch (System.Exception e) { 
                Console.WriteLine(e.Message);
                return null;
            }

        }
            
        public static dynamic SafeCreateObject(String pClass, String pServerName = ""){

            try
            {

                Type ObjType = null;

                if (pServerName.Trim().Length == 0)
                {
                    ObjType = Type.GetTypeFromProgID(pClass, true);
                }
                else
                {
                    ObjType = Type.GetTypeFromProgID(pClass, pServerName, true);
                }

                dynamic ObjInstance = Activator.CreateInstance(ObjType);

                return ObjInstance;

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "No se pudo instanciar el objeto [" + pClass + "]");
                return null;
            }
            
        }

        public static Dictionary<String, String> ConvertirCadenadeAsociacion(String pCadena, Char pSeparador = '|'){

            try {
                
                Dictionary<String, String> Tmp = new Dictionary<String, String>();
                String[] ParClaveValor = null;
                
                String[] Split1 = pCadena.Split(new Char[] { pSeparador });
                
                foreach (var Item in Split1)
	            {
                    ParClaveValor = Item.ToString().Split(new Char[] { ':' }, 2);
                    if (!Tmp.ContainsKey(ParClaveValor[0])) Tmp.Add(ParClaveValor[0], ParClaveValor[1]);
	            }
                
                if (Tmp.Count <= 0) throw new Exception("NoData");

                return Tmp;
                
            } catch (Exception) {
                return null;
            }
            
        }

        public static Dictionary<String, String> ConvertirCadenadeAsociacion(String pCadena)
        {
            return ConvertirCadenadeAsociacion(pCadena, '|');
        }

        public static Boolean ExisteTabla(String pTabla, ref ADODB.Connection pCn,
        String pBD = "")
        {

            ADODB.Recordset pRs = null; String mBD = (!pBD.isUndefined() ? pBD + "." : pBD);
            Object Records; Boolean ExisteTabla = false;

            try
            {
                pRs = pCn.Execute("SELECT Tabla_Name FROM " + mBD + "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" + pTabla + "'", out Records);
                ExisteTabla = !(pRs.EOF && pRs.BOF);
            }
            catch (Exception)
            {
            }

            return ExisteTabla;

        }

        public static Boolean ExisteCampoTabla(String pColumna, String pTabla, ref ADODB.Connection pCn, 
        String pBD = "")
        {

            ADODB.Recordset pRs = null; String mBD = (!pBD.isUndefined() ? pBD + ".": pBD);
            Object Records; Boolean ExisteCampoTabla = false;

            try 
	        {	        
		        pRs = pCn.Execute("SELECT Column_Name FROM " + mBD + "INFORMATION_SCHEMA.COLUMNS WHERE Table_Name = '" + pTabla + "' AND Column_Name = '" + pColumna + "'", out Records);
                ExisteCampoTabla = !(pRs.EOF && pRs.BOF);
	        }
	        catch (Exception)
	        {
	        }

            return ExisteCampoTabla;

        }

        public static Boolean ExisteCampoRs(ADODB.Recordset pRs, String pCampo)
        {

            try 
	        {	        
		        dynamic ValorTmp = pRs.Fields[pCampo].Value;
                return true;
	        }
	        catch (Exception)
	        {
                return false;
	        }

        }

        public static String Correlativo(ADODB.Connection pCn, String pCampo)
        {

            ADODB.Recordset mRs = new ADODB.Recordset();
            String mSQL;
            String mValor;

            try
            {

                mSQL = "SELECT * FROM MA_CORRELATIVOS WHERE (CU_Campo = '" + pCampo + "')";

                mRs.CursorLocation = ADODB.CursorLocationEnum.adUseServer;

                mRs.Open(mSQL, pCn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic);

                if (!mRs.EOF)
                {
                    mRs.Fields["nu_Valor"].Value = Convert.ToInt64(mRs.Fields["nu_Valor"].Value) + 1;
                    mValor = Microsoft.VisualBasic.Strings.Format(mRs.Fields["nu_Valor"].Value, mRs.Fields["cu_Formato"].Value.ToString());
                    mRs.UpdateBatch();
                }
                else
                {
                    mRs.AddNew();
                    mRs.Fields["cu_Campo"].Value = pCampo;
                    mRs.Fields["cu_Formato"].Value = "0000000000";
                    mRs.Fields["nu_Valor"].Value = 1;
                    mValor = Microsoft.VisualBasic.Strings.Format(mRs.Fields["nu_Valor"].Value, mRs.Fields["cu_Formato"].Value.ToString());
                    mRs.UpdateBatch();
                }

                return mValor;

            }
            catch (Exception Ex)
            {
                Program.Logger.EscribirLog(Ex, "Functions.Correlativo()");
                isAGENT_SxSBO.Database_Sync_Classes.DBSync.InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error obteniendo correlativo de ejecución de Agente.",
                "Functions.Correlativo()", String.Empty,
                String.Empty, Program.mCnLocal);
                return String.Empty;
            }

        }

        public static Boolean isDBNull(Object pValue) { return (pValue == null); }

        public static Object isDBNull(Object pValue, Object pDefaultValueReturned)
        {
            if (isDBNull(pValue))
                return pDefaultValueReturned;
            else
                return pValue;
        }


        /// <param name="Duration">Un TimeSpan. Ejemplo: Dados 2 Datetimes A y B, cuando B es después de A, Duración puede ser pasada como (B - A)</param>
        /// <param name="Prefix">Cualquier prefijo de texto que le quieras añadir a la representación de la Duración a retornar</param>
        /// <param name="Mode">Valores Posibles: "SIMPLE" > Representación Humana, "PRECISE" > Retorna la duración en números con precisión hasta milisegundos, "FULL": Retorna ambos</param>
        public static String CustomDurationES(TimeSpan Duration,
        String Prefix = "Tiempo transcurrido: ",
        String Mode = "FULL")
        {

            if (Duration.TotalMilliseconds < 0)
            {
                return "Duración Negativa (Inválida)";
            }

            String SuperFormat = String.Empty;

            switch (Mode)
            {
                case "SIMPLE":
                case "PRECISE":
                    Mode = Mode.ToUpper();
                    break;
                default:
                    Mode = "FULL";
                    break;
            }

            if (Mode.Equals("SIMPLE") || Mode.Equals("FULL"))
            {

                if (Duration.Days > 0)
                {
                    SuperFormat = SuperFormat + @"\ d\ ";

                    if (Duration.Days == 1)
                        SuperFormat = SuperFormat + @"\D\í\a";
                    else
                        SuperFormat = SuperFormat + @"\D\í\a\s";
                }

                if (Duration.Hours > 0)
                {
                    SuperFormat = SuperFormat + @"\ h\ ";

                    if (Duration.Hours == 1)
                        SuperFormat = SuperFormat + @"\H\o\r\a";
                    else
                        SuperFormat = SuperFormat + @"\H\o\r\a\s";
                }

                if (Duration.Minutes > 0)
                {
                    SuperFormat = SuperFormat + @"\ m\ ";

                    if (Duration.Minutes == 1)
                        SuperFormat = SuperFormat + @"\M\i\n\u\t\o";
                    else
                        SuperFormat = SuperFormat + @"\M\i\n\u\t\o\s";
                }

                if (Duration.Seconds > 0)
                {
                    SuperFormat = SuperFormat + @"\ s\ ";

                    if (Duration.Seconds == 1)
                        SuperFormat = SuperFormat + @"\S\e\g\u\n\d\o";
                    else
                        SuperFormat = SuperFormat + @"\S\e\g\u\n\d\o\s";
                }

                if (Duration.TotalMilliseconds < 1000)
                {
                    SuperFormat = SuperFormat + @"\ ";

                    //if (Duration.Milliseconds > 99)
                    SuperFormat = SuperFormat + @"fff";
                    /*else if (Duration.Milliseconds > 9)
                        SuperFormat = SuperFormat + @"ff";
                    else
                        SuperFormat = SuperFormat + @"f";*/

                    SuperFormat = SuperFormat + @"\ \m\s";
                }

            }

            if (Mode.Equals("PRECISE") || Mode.Equals("FULL"))
            {

                if (Duration.TotalSeconds > 59)
                {

                    SuperFormat = SuperFormat + @"\ \(";

                    if (Duration.Days > 0)
                        SuperFormat = SuperFormat + @"dd\:";

                    if (Duration.TotalMinutes > 59)
                        SuperFormat = SuperFormat + @"hh\:";

                    if (Duration.TotalSeconds > 59)
                        SuperFormat = SuperFormat + @"mm\:";

                    SuperFormat = SuperFormat + @"ss\.fff";

                    SuperFormat = SuperFormat + @"\)";

                }
                else
                {

                    if (Duration.TotalMilliseconds > 1000)
                    {

                        SuperFormat = SuperFormat + @"\ \(";

                        SuperFormat = SuperFormat + @"ss\.fff";

                        SuperFormat = SuperFormat + @"\)";

                    }

                }

            }

            return Prefix + Duration.ToString(Microsoft.VisualBasic.Strings.Replace(SuperFormat,
            "\\ ", String.Empty, 1, 1, Microsoft.VisualBasic.CompareMethod.Text));

        }

        /// <param name="Duration">A TimeSpan. Example: Given Two Datetimes A and B, where B is later, Duration can be passed as (B - A)</param>
        /// <param name="Prefix">Any textual Prefix you would like to add to the Duration representation to be returned</param>
        /// <param name="Mode">Posible Values: "SIMPLE" > Returns Human representation, "PRECISE" > Returns Raw Date with precision up to Milliseconds, "FULL": Returns Both</param>
        public static String CustomDurationEN(TimeSpan Duration,
        String Prefix = "Time elapsed: ",
        String Mode = "FULL")
        {

            if (Duration.TotalMilliseconds < 0)
            {
                return "Negative Duration (invalid Params)";
            }

            String SuperFormat = String.Empty;

            switch (Mode)
            {
                case "SIMPLE":
                case "PRECISE":
                    Mode = Mode.ToUpper();
                    break;
                default:
                    Mode = "FULL";
                    break;
            }

            if (Mode.Equals("SIMPLE") || Mode.Equals("FULL"))
            {

                if (Duration.Days > 0)
                {
                    SuperFormat = SuperFormat + @"\ d\ ";

                    if (Duration.Days == 1)
                        SuperFormat = SuperFormat + @"\D\a\y";
                    else
                        SuperFormat = SuperFormat + @"\D\a\y\s";
                }

                if (Duration.Hours > 0)
                {
                    SuperFormat = SuperFormat + @"\ h\ ";

                    if (Duration.Hours == 1)
                        SuperFormat = SuperFormat + @"\H\o\u\r";
                    else
                        SuperFormat = SuperFormat + @"\H\o\u\r\s";
                }

                if (Duration.Minutes > 0)
                {
                    SuperFormat = SuperFormat + @"\ m\ ";

                    if (Duration.Minutes == 1)
                        SuperFormat = SuperFormat + @"\M\i\n\u\t\e";
                    else
                        SuperFormat = SuperFormat + @"\M\i\n\u\t\e\s";
                }

                if (Duration.Seconds > 0)
                {
                    SuperFormat = SuperFormat + @"\ s\ ";

                    if (Duration.Seconds == 1)
                        SuperFormat = SuperFormat + @"\S\e\c\o\n\d";
                    else
                        SuperFormat = SuperFormat + @"\S\e\c\o\n\d\s";
                }

                if (Duration.TotalMilliseconds < 1000)
                {
                    SuperFormat = SuperFormat + @"\ ";

                    //if (Duration.Milliseconds > 99)
                    SuperFormat = SuperFormat + @"fff";
                    /*else if (Duration.Milliseconds > 9)
                        SuperFormat = SuperFormat + @"ff";
                    else
                        SuperFormat = SuperFormat + @"f";*/

                    SuperFormat = SuperFormat + @"\ \m\s";
                }

            }

            if (Mode.Equals("PRECISE") || Mode.Equals("FULL"))
            {

                if (Duration.TotalSeconds > 59)
                {

                    SuperFormat = SuperFormat + @"\ \(";

                    if (Duration.Days > 0)
                        SuperFormat = SuperFormat + @"dd\:";

                    if (Duration.TotalMinutes > 59)
                        SuperFormat = SuperFormat + @"hh\:";

                    if (Duration.TotalSeconds > 59)
                        SuperFormat = SuperFormat + @"mm\:";

                    SuperFormat = SuperFormat + @"ss\.fff";

                    SuperFormat = SuperFormat + @"\)";

                }
                else
                {

                    if (Duration.TotalMilliseconds > 1000)
                    {

                        SuperFormat = SuperFormat + @"\ \(";

                        SuperFormat = SuperFormat + @"ss\.fff";

                        SuperFormat = SuperFormat + @"\)";

                    }

                }

            }

            return Prefix + Duration.ToString(Microsoft.VisualBasic.Strings.Replace(SuperFormat,
            "\\ ", String.Empty, 1, 1, Microsoft.VisualBasic.CompareMethod.Text));

        }

    }

}
