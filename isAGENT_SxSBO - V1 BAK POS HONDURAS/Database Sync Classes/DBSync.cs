﻿using isAGENT_SxSBO.Clases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SAPbobsCOM;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.Compatibility.VB6;

namespace isAGENT_SxSBO.Database_Sync_Classes
{
    class DBSync
    {

        public const String RegistrosPendientesPorCorrida = "-_-_-_-_-";
        public const String RegistrosPendientesPorLote = "..........";
        public const String RegistrosPendientesFallidos = "!!!!!!!!!!";
        public const String RegistrosNuevos = "";

        public static long nLotesMaxEjecucion = 0;
        public static long nLotesProcesados = 0;
        public static long nRegistrosLote = 0;

        private static void RecuperacionInicialDeCredenciales()
        {
            
            dynamic mClsTmp = null;

            Program.mStellarDBUser = Properties.Settings.Default.Stellar_SQLUser;
            Program.mStellarDBPass = Properties.Settings.Default.Stellar_SQLPass;

            if (!(String.Equals(Program.mStellarDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mStellarDBPass.Length == 0))
            {

                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mStellarDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBUser);
                    Program.mStellarDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mStellarDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPDBUser = Properties.Settings.Default.SAP_DBUser;
            Program.mSAPDBPass = Properties.Settings.Default.SAP_DBPass;

            if (!(String.Equals(Program.mSAPDBUser, "SA", StringComparison.OrdinalIgnoreCase)
            && Program.mSAPDBPass.Length == 0))
            {
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPDBUser = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBUser);
                    Program.mSAPDBPass = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPDBPass);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            }

            Program.mSAPUserLogin = Properties.Settings.Default.SAP_CompanyUser;
            Program.mSAPUserPwd = Properties.Settings.Default.SAP_CompanyPass;

            //if (!(String.Equals(Program.mUserFTP, "SA", StringComparison.OrdinalIgnoreCase) && Program.mPassFTP.Length == 0))
            //{
                mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                if (mClsTmp != null)
                {
                    Program.mSAPUserLogin = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserLogin);
                    Program.mSAPUserPwd = mClsTmp.Decode(Program.gCodProducto, Program.gNombreProducto,
                    Program.gPK, Program.mSAPUserPwd);
                }
                else
                {
                    Program.Logger.EscribirLog("Error de autenticación. Debe instalar y registrar el componente DLL SQLSafeGuard.");
                    System.Environment.Exit(0);
                }

            //}

        }

        private static SqlConnection ConexionStellar()
        {

            SqlConnection mCnLocal = null;

        Retry:

            try
            {
                mCnLocal = (Properties.Settings.Default.Stellar_TrustedConnection ?
                Functions.getAlternateTrustedConnection(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, 30) :
                Functions.getAlternateConnection(Properties.Settings.Default.Stellar_SQLServerName,
                Properties.Settings.Default.Stellar_SQLDBName, 
                Program.mStellarDBUser, Program.mStellarDBPass, 30));
                mCnLocal.Open();
            }
            catch (SqlException SQLAny)
            {

                if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                {
                    
                    dynamic mClsTmp = null;
                    
                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");
                    
                    if (mClsTmp == null) goto Otros;
                    
                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");
                    
                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);
                    
                    if  (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog( "Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }
                    
                }

                Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            if (mCnLocal.State != ConnectionState.Open)
            {
                Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor Stellar.");
                System.Environment.Exit(0);
            }

            return mCnLocal;

        }

        private static String CadenaConexionADO(String pServidor, String pBD, String pUser = "SA", String pPassword = "", 
        Boolean pTrustedConnection = false){
            
            String mCadenaConexion = String.Empty;

            if (pTrustedConnection)
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                "Trusted_Connection=true;";
            else
                mCadenaConexion = "Provider=SQLOLEDB.1;Initial Catalog=" + pBD + ";Data Source=" + pServidor + ";" +
                (pUser.isUndefined() || pPassword.isUndefined() ? "Persist Security Info=False;User ID=" + pUser + ";"
                : "Persist Security Info=True;User ID=" + pUser + ";Password=" + pPassword + ";");

            return mCadenaConexion;

        }

        private static ADODB.Connection ConexionStellar(Boolean pADO = true)
        {

            ADODB.Connection mCnLocal = null;

        Retry:

            try
            {

                mCnLocal = new ADODB.Connection();

                mCnLocal.ConnectionTimeout = Properties.Settings.Default.Stellar_ConnectionTimeout;
                //mCnLocal.Provider = "SQLOLEDB.1";
                mCnLocal.ConnectionString = CadenaConexionADO(Properties.Settings.Default.Stellar_SQLServerName, 
                Properties.Settings.Default.Stellar_SQLDBName, Program.mStellarDBUser, 
                Program.mStellarDBPass, Properties.Settings.Default.Stellar_TrustedConnection);

                mCnLocal.Open();

            }
            catch (System.Runtime.InteropServices.COMException SQLAny)
            {

                //if (SQLAny.Number == 18456 && SQLAny.ErrorCode == -2146232060)
                if (SQLAny.HResult == (-2147217843))
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión al Servidor de Stellar " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mStellarDBUser = TmpVar[0];
                        Program.mStellarDBPass = TmpVar[1];
                        Properties.Settings.Default.Stellar_SQLUser = TmpVar[2];
                        Properties.Settings.Default.Stellar_SQLPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión al servidor Stellar " +
                        "son incorrectos, Conectando a " + Properties.Settings.Default.Stellar_SQLServerName);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(SQLAny);

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Estableciendo conexión al Servidor Stellar.");
                Console.WriteLine(Any);
            }

            return mCnLocal;

        }

        private static SAPbobsCOM.Company ConexionSAP(Boolean ManagedObject)
        {
            SAPbobsCOM.Company Obj = ConexionSAP();
            return Obj;
        }

        private static dynamic ConexionSAP()
        {

            dynamic mCnSAP = null;

            Int32 ErrorNumber; String ErrorDesc;

        Retry:


            try
            {

                //mCnSAP = new Company();
                mCnSAP = Functions.SafeCreateObject("SAPbobsCOM.Company" + Program.DI_API_Version);

	            mCnSAP.Server = Properties.Settings.Default.SAP_Server;
	            mCnSAP.LicenseServer = Properties.Settings.Default.SAP_LicenseServer;

                mCnSAP.DbServerType = (BoDataServerTypes)Properties.Settings.Default.SAP_ServerTypeID;  //BoDataServerTypes.dst_MSSQL2014;
	            mCnSAP.DbUserName = Program.mSAPDBUser;
	            mCnSAP.DbPassword = Program.mSAPDBPass;
	            mCnSAP.language = (BoSuppLangs) Properties.Settings.Default.SAP_LangID; //BoSuppLangs.ln_Spanish_La;
                mCnSAP.UseTrusted = false;

                mCnSAP.CompanyDB = Properties.Settings.Default.SAP_DBName;
                mCnSAP.UserName = Program.mSAPUserLogin;
                mCnSAP.Password = Program.mSAPUserPwd;
	        
	            if(mCnSAP.Connect() != 0) 
                {
                
	                mCnSAP.GetLastError(out ErrorNumber, out ErrorDesc);
                    mCnSAP.Disconnect();

                    throw new Exception(ErrorDesc) { HelpLink = ErrorNumber.ToString() };

	            }

            }
            catch (Exception SQLAny)
            {

                if (SQLAny.HelpLink == "-4008")
                {

                    dynamic mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    dynamic TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPDBUser = TmpVar[0];
                        Program.mSAPDBPass = TmpVar[1];
                        Properties.Settings.Default.SAP_DBUser = TmpVar[2];
                        Properties.Settings.Default.SAP_DBPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        //goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión de base de datos al Servidor SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server, 
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP",  Program.mCnLocal);
                    }

                /*}
                else if (SQLAny.HelpLink == "-8023")
                {*/

                    //dynamic mClsTmp = null;
                    mClsTmp = null;

                    mClsTmp = Functions.SafeCreateObject("SQLSafeGuard.Service");

                    if (mClsTmp == null) goto Otros;

                    System.Windows.Forms.MessageBox.Show("Los datos de acceso para la conexión a la Compañia SAP " +
                    "no estan establecidos o son incorrectos. Se le solicitarán a continuación.");

                    //dynamic 
                    TmpVar = mClsTmp.RequestAccess(Program.gCodProducto, Program.gNombreProducto, Program.gPK);

                    if (TmpVar.Length > 0)
                    {
                        Program.mSAPUserLogin = TmpVar[0];
                        Program.mSAPUserPwd = TmpVar[1];
                        Properties.Settings.Default.SAP_CompanyUser = TmpVar[2];
                        Properties.Settings.Default.SAP_CompanyPass = TmpVar[3];
                        Properties.Settings.Default.Save();
                        goto Retry;
                    }
                    else
                    {
                        Program.Logger.EscribirLog("Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Los datos de acceso para la conexión a la Compañia SAP " +
                        "son incorrectos, Conectando a [" + Properties.Settings.Default.SAP_Server + "]", "DBSync.ConexionSAP()",
                        "Servidor SAP", Properties.Settings.Default.SAP_Server,
                        (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                    }

                }

            Otros:

                Program.Logger.EscribirLog(SQLAny, "Estableciendo conexión al Servidor SAP.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, 
                "Estableciendo conexión al Servidor SAP.", "DBSync.ConexionSAP()", "Servidor SAP",
                Properties.Settings.Default.SAP_Server,
                (SQLAny.HelpLink + "|" + SQLAny.Message), "ConexionSAP", Program.mCnLocal);
                Console.WriteLine(SQLAny);

            }

            if (mCnSAP != null)
            {
                if (!mCnSAP.Connected)
                {
                    Marshal.ReleaseComObject(mCnSAP);
                    Program.Logger.EscribirLog("No se pudo establecer conexión al Servidor SAP.");
                    System.Environment.Exit(0);
                }
            }
            else
            {
                Program.Logger.EscribirLog("Verifique que SAP BUSINESS ONE y la DI API esten correctamente instalados.");
                System.Environment.Exit(0);
            }

            return mCnSAP;

        }

        public static void IniciarAgente()
        {

            ADODB.Connection mCnLocal = null;
            //Company SAPCon = null;
            dynamic SAPCon = null;

            try
            {

                //Functions.LoadSettings();

                RecuperacionInicialDeCredenciales();

                if (!Properties.Settings.Default.ObtenerListasAsociacionBD)
                {

                    Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionMonedas);
                    Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionBancos);
                    Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.ListaAsociacionFormaPago);

                    if (Program.ListaAsociacionMonedas == null)
                    {
                        Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos.");
                        return;
                    }
                        
                    if (Program.ListaAsociacionFormaPago == null)
                    {
                        Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos.");
                        return;
                    }
                        
                    if (Program.ListaAsociacionBancos == null)
                    {
                        Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos.");
                        return;
                    }
                        

                }

                mCnLocal = ConexionStellar(true);
                Program.mCnLocal = mCnLocal;

                Program.DI_API_Version = Properties.Settings.Default.APP_DEFINED_SAP_DI_API_VERSION;

                SAPCon = ConexionSAP();
                Program.mCnSAP = SAPCon;
                
                Program.gCorrelativo = Functions.Correlativo(mCnLocal, "Corrida_Agente_SAP_Trans");

                if (!Program.gCorrelativo.isUndefined())
                {

                    Program.ListaAsociacionTipoPrecio = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.SAP_Linea_ListaTipoPrecio);

                    Program.TaxList1 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList1);
                    Program.TaxList2 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList2);
                    Program.TaxList3 = Functions.ConvertirCadenadeAsociacion(Properties.Settings.Default.TaxList3);

                    if (Program.TaxList1 == null && Program.TaxList2 == null && Program.TaxList3 == null)
                    {
                        Program.Logger.EscribirLog("Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Codigos de Impuestos inválidos o no establecidos",
                        "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                        return;
                    }

                    if (Properties.Settings.Default.ObtenerListasAsociacionBD)
                    {

                        String mLista;
                        ADODB.Recordset x; Object Records = null;

                        try
                        {

                            mLista = "";

                            x = Program.mCnLocal.Execute(
                            "SELECT REPLACE(SUBSTRING((SELECT '|' + c_Codigo + ':' + CodigoExterno AS 'data()' FROM\n" +
                            "VAD10.DBO.MA_BANCOS WHERE CodigoExterno <> ''\n" +
                            "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                            );

                            if (!x.EOF)
                            {
                                mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                            }

                            Program.ListaAsociacionBancos = Functions.ConvertirCadenadeAsociacion(mLista);

                            if (Program.ListaAsociacionBancos == null) { 
                                Program.Logger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos");
                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Bancos inválidos o no establecidos",
                                "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                return;
                            }

                            mLista = "";

                            x = Program.mCnLocal.Execute(
                            "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ';' + c_CodDenomina + ':' + CodigoExterno AS 'data()' FROM\n" +
                            "VAD10.DBO.MA_DENOMINACIONES WHERE CodigoExterno <> ''\n" +
                            "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                            );

                            if (!x.EOF)
                            {
                                mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                            }

                            Program.ListaAsociacionFormaPago = Functions.ConvertirCadenadeAsociacion(mLista);

                            if (Program.ListaAsociacionFormaPago == null) { 
                                Program.Logger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos");
                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Formas de Pago inválidos o no establecidos",
                                "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                return;
                            }

                            mLista = "";

                            x = Program.mCnLocal.Execute(
                            "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ':' + CodigoExterno AS 'data()' FROM\n" +
                            "VAD10.DBO.MA_MONEDAS WHERE CodigoExterno <> ''\n" +
                            "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista", out Records
                            );

                            if (!x.EOF)
                            {
                                mLista = Functions.isDBNull(x.Fields["Lista"].Value, String.Empty).ToString();
                            }

                            Program.ListaAsociacionMonedas = Functions.ConvertirCadenadeAsociacion(mLista);

                            if (Program.ListaAsociacionMonedas == null) { 
                                Program.Logger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos");
                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Datos de asociaciones de Monedas inválidos o no establecidos",
                                "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, String.Empty, "Validacion Stellar", Program.mCnLocal);
                                return;
                            }

                        }
                        catch (Exception ex)
                        {
                            Program.Logger.EscribirLog(ex, "Error al obtener datos de asociaciones por BD.");
                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Error al obtener datos de asociaciones por BD.",
                            "DBSync.IniciarAgente()", "Ejecucion Agente", Program.gCorrelativo, (ex.HResult + "|" + ex.Message), "Validacion Stellar", Program.mCnLocal);
                            return;
                        }

                    }

                    nRegistrosLote = Properties.Settings.Default.nRegistrosLote;
                    nLotesMaxEjecucion = Properties.Settings.Default.nLotesCorrida;

                    Boolean result = PrepararBD(ref mCnLocal);

                    SincronizarVentas();

                }
                else
                {
                    throw new Exception("Falla al obtener correlativo de ejecución de Agente.");
                }

            }
            catch (System.Exception Any)
            {
                Program.Logger.EscribirLog(Any, "Ejecutando Agente.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Ejecutando Agente.", "DBSync.IniciarAgente()", "Corrida Agente",
                Program.gCorrelativo, Program.mCnLocal);
            }
            finally
            {

                if (SAPCon != null)
                if (SAPCon.Connected)
                    SAPCon.Disconnect();

                if (mCnLocal != null)
                if (mCnLocal.State != Convert.ToInt32(ADODB.ObjectStateEnum.adStateClosed))
                    mCnLocal.Close();

            }

        }

        public static Boolean PrepararBD(ref ADODB.Connection pCn)
        {

            Boolean result = true; Object Records;
            long mCmdTOut_Original = 0; Boolean Reintentando = false;

            mCmdTOut_Original = Properties.Settings.Default.Stellar_CommandTimeout;

        Retry:
            
            for (int i = 0; i <= 0; i++) {
                
                try {

                    result = true;

                    Properties.Settings.Default.Stellar_CommandTimeout = 0;

                    if (!Functions.ExisteCampoTabla("cs_Sync_SxS", "MA_PAGOS", ref pCn, Properties.Settings.Default.Stellar_SQLDBName)) {
                        pCn.Execute("ALTER TABLE [VAD20].[DBO].[MA_PAGOS]" + "\n" + 
                        "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT" + "\n" +
                        "[MA_PAGOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')", out Records);
            
                        result = result && true;
                    }

                    pCn.Execute("IF NOT EXISTS(SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] WHERE ID = " + Program.LogIDAuditoria + ")" + "\n" +
                    "INSERT INTO [VAD10].[DBO].[MA_AUDITORIAS_TIPOS] (ID, Descripcion, DescResourceID)" + "\n" +
                    "SELECT " + Program.LogIDAuditoria  + " AS ID, 'Log Error / Evento isAGENT_SxSBO', 0 AS DescResourceID" + "\n" +
                    "", out Records);

                    if (!Functions.ExisteCampoTabla("CodigoRetorno", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD CodigoRetorno NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_CodigoRetorno] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    if (!Functions.ExisteCampoTabla("AccionRealizada", "MA_AUDITORIAS", ref pCn, "VAD10"))
                    {
                        pCn.Execute("ALTER TABLE [VAD10].[DBO].[MA_AUDITORIAS]" + "\n" +
                        "ADD AccionRealizada NVARCHAR(255) NOT NULL CONSTRAINT " + "\n" +
                        "[MA_AUDITORIAS_DEFAULT_AccionRealizada] DEFAULT ('')", out Records);

                        result = result && true;
                    }

                    /*if (gTransferirCierres) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_CIERRES", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_CIERRES]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_CIERRES_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    /*if (gTransferirDepositosCP) {
                        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_DEPOSITOS", gConexion, DB_POS)) {
                            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_DEPOSITOS]" + "\n" + 
                            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_DEPOSITOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                            result = result && true;
                        }
                    }*/
        
                    return result;

                }
                catch (Exception SQLAny)
                {
                    Program.Logger.EscribirLog(SQLAny, "PrepararBD:");
                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                        "Ejecutando validaciones y requerimientos de Base de Datos.", "DBSync.PrepararBD()", "Servidor Stellar",
                        Properties.Settings.Default.Stellar_SQLServerName,
                        (SQLAny.HResult + "|" + SQLAny.Message), "PrepararBD", Program.mCnLocal);
                        if (!Reintentando) {
                            Reintentando = true;
                            goto Retry;
                        } else {
                        // Sin Exito.
                    }
                    
                }

            }

            return result;
            
        }

        private static void SincronizarVentas()
        {

            if (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
            HayDatosPendientes(RegistrosPendientesPorLote) ||
            HayDatosPendientes(RegistrosPendientesFallidos))
            {
                ProcesarCorrida(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
            }

            if (nLotesProcesados < nLotesMaxEjecucion)
                if (MarcarRegistros(RegistrosPendientesPorCorrida))
                { // Procesar registros nuevos.
                    ProcesarCorrida();
                }

        }

        private static Boolean HayDatosPendientes(String pEstatus)
        {

            try
            {

                ADODB.Recordset RsDatosPendientes; Object Records;

                String ExcluirVNF = String.Empty;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                RsDatosPendientes = Program.mCnLocal.Execute("SELECT COUNT(*) AS Registros FROM MA_PAGOS WHERE cs_Sync_SxS = '"
                + pEstatus + "'" + ExcluirVNF, out Records);

                return (RsDatosPendientes.Fields["Registros"].Value > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ProcesarCorrida()
        {

            if (HayDatosPendientes(RegistrosPendientesFallidos))
                ConstruirRegistrosDeVentas(true); // Intentar procesar los Fallidos una sola vez.

            while (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
            HayDatosPendientes(RegistrosPendientesPorLote))
            {
                if (nLotesProcesados < nLotesMaxEjecucion)
                    ProcesarLote();
                else
                    break;
            }

        }

        private static void ProcesarLote()
        {

            if (HayDatosPendientes(RegistrosPendientesPorLote))
            {
                // Procesar primero los pendientes
                // Antes de marcar el próximo Lote.
                ConstruirRegistrosDeVentas();
                nLotesProcesados++;
            }

            if (nLotesProcesados < nLotesMaxEjecucion)
                if (MarcarRegistros(RegistrosPendientesPorLote))
                {
                    ConstruirRegistrosDeVentas();
                    nLotesProcesados++;
                }

        }

        private static Boolean MarcarRegistros(String pEstatus, Boolean pFallidos = false)
        {

            try
            {

                long RegistrosAfectados = 0; String pEstatusPrevio = String.Empty; String mRegistrosXLote = String.Empty;

                String ExcluirVNF = String.Empty; String SQLMarcaje = String.Empty; Object Records;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal))
                {
                    ExcluirVNF = " AND bDocNoFiscal = 0";
                }

                switch (pEstatus)
                {
                    case RegistrosPendientesPorCorrida:
                        pEstatusPrevio = "'" + RegistrosNuevos + "'";
                        break;
                    case RegistrosPendientesPorLote:
                        if (pFallidos)
                            pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                        else
                        {
                            pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                            mRegistrosXLote = "AND c_Concepto + c_Numero IN (" + "\n" +
                            "SELECT TOP (" + nRegistrosLote + ") c_Concepto + c_Numero FROM MA_PAGOS" + "\n" +
                            "WHERE cs_Sync_SxS = " + pEstatusPrevio + ExcluirVNF + " ORDER BY ID)";
                        }
                        break;
                    default: // Guardados con Exito.
                        pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                        break;
                }

                SQLMarcaje = "UPDATE MA_PAGOS SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" +
                "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

                if (Program.gDebugMode) Program.Logger.EscribirLog(SQLMarcaje);

                Program.mCnLocal.Execute(SQLMarcaje, out Records);

                RegistrosAfectados = Convert.ToInt64(Records);

                return (RegistrosAfectados > 0);

            }
            catch (Exception) { return false; }

        }

        private static void ConstruirRegistrosDeVentas(Boolean pReprocesarFallidos = false)
        {
            
            ADODB.Recordset RsVentas = null; ADODB.Recordset RsItems = null; ADODB.Recordset RsDetPagos = null;
            ADODB.Recordset RsImpuestos = null; ADODB.Recordset RsDocRel = null;

            System.Collections.ArrayList RegistrosFallidos = new System.Collections.ArrayList();

            Double Cont = 0; Int64 ItmCount = 0;
            String mUltDoc = String.Empty; Double mUltTot = 0; DateTime mUltFec = DateTime.Now;
            String mDocRel = String.Empty; String Related_StellarDocID = String.Empty;

            Dictionary<String, String[]> LineRelation = new Dictionary<String, String[]>();
            String StellarItemID = String.Empty; String StellarItemLn = String.Empty;

            Object RecordsAffected;

            //SAPbobsCOM.Company SAP = Program.mCnSAP;
            dynamic SAP = Program.mCnSAP;
            ADODB.Connection mCn = Program.mCnLocal;

            try 
	        {

                if (pReprocesarFallidos)
                    MarcarRegistros(RegistrosPendientesPorLote, true);

                if (ObtenerDatosVentas(out RsVentas, out RsItems, out RsDetPagos, out RsImpuestos, pReprocesarFallidos))
                {

                    Cont = 0;
                    
                    RsVentas.Sort = "ID";
                    RsVentas.MoveFirst();

                Continue: 

                    while (!RsVentas.EOF)
                    {

                        Cont = Cont + 1;

                        SAP.StartTransaction();

                        String Concepto = RsVentas.Fields["c_Concepto"].Value;
                        String NumDoc = RsVentas.Fields["c_Numero"].Value;

                        String TransID = Concepto + NumDoc;

                        String StellarDocID = "PR" + "_" + TransID;

                        mUltDoc = StellarDocID;

                        //SAPbobsCOM.Documents Trans;
                        dynamic Trans;

                        // Determinar si es Factura (VEN) o Devolucion (DEV).

                        Double Signo = 1;

                        RsDocRel = null;

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {
                            Trans = SAP.GetBusinessObject(BoObjectTypes.oInvoices);
                            Int32 Serie = 0;

                            if (!Properties.Settings.Default.SAP_SerieDocumentoFactura.isUndefined())
                                if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoFactura, out Serie))
                                    Trans.Series = Serie;
                        }
                        else
                        {

                            Signo = -1;

                            Trans = SAP.GetBusinessObject(BoObjectTypes.oCreditNotes);

                            mDocRel = RsVentas.Fields["cs_Documento_Rel"].Value;
                            Related_StellarDocID = "PR" + "_" + "VEN" + mDocRel;

                            RsDocRel = mCn.Execute("SELECT * FROM MA_PAGOS WHERE c_Concepto = 'VEN' " +
                            "AND c_Numero = '" + mDocRel + "'", out RecordsAffected);

                            Int32 Serie = 0;

                            if (!Properties.Settings.Default.SAP_SerieDocumentoNC.isUndefined())
                                if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoNC, out Serie))
                                    Trans.Series = Serie;

                        }

                        // Asignar datos generales de cabecero de transacción.

                        Trans.CardCode = RsVentas.Fields["c_Cliente"].Value;

                        if (Trans.CardCode.Equals(Properties.Settings.Default.Stellar_ClienteContado))
                            Trans.CardCode = Properties.Settings.Default.SAP_ClienteContado;

                        Trans.DocDate = RsVentas.Fields["f_Fecha"].Value;
                        Trans.DocDueDate = Trans.DocDate;
                        //Trans.TaxDate = Trans.DocDate; // Autocolocado en SAP

                        //Trans.Comments = "Stellar POS";

                        Trans.TrackingNumber = NumDoc + 
                        (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) ? 
                        " -> " + mDocRel: String.Empty);

                        Trans.Comments = "Stellar POS" + ". " + TransID;

                        //Trans.JournalMemo = String.Empty; // Autocolocado en SAP
                        Trans.Rounding = BoYesNoEnum.tNO;

                        Int32 POSCashierNumber = 0;

                        if (Int32.TryParse(RsVentas.Fields["c_Usuario"].Value, out POSCashierNumber))
                            Trans.POSCashierNumber = POSCashierNumber;

                        if (RsDocRel != null)
                        {

                            Trans.Reference1 = mDocRel;

                            Trans.Comments = Trans.Comments +
                            (Concepto.Equals("DEV", StringComparison.OrdinalIgnoreCase) ?
                            " -> " + "VEN" + mDocRel : String.Empty);

                            String[] mDatosSync = (RsDocRel.Fields["cs_Sync_SxS"].Value).Split(new Char[] { '|' });
                            if (mDatosSync.Length >= 2)
                            {

                                Int32 RelatedDocType = Convert.ToInt32(BoObjectTypes.oInvoices);
                                Int32 RelatedDocEntry;

                                if (Int32.TryParse(mDatosSync[1], out RelatedDocEntry))
                                {
                                    /*Trans.RelatedType = RelatedDocType;
                                    Trans.RelatedEntry = RelatedDocEntry;*/
                                    Trans.Reference2 = mDatosSync[1];
                                    Trans.Comments = Trans.Comments +
                                    " " + "(SBO No. " + mDatosSync[1] + ")";

                                }

                            }
                            
                        }

                        // Buscar Items del Documento.

                        RsItems.Filter = "c_Concepto = '" +
                        Concepto + "' AND c_Numero = '" + NumDoc + "'";

                        if (RsItems.EOF)
                        {

                            // No hay items. Error de Integridad.

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + StellarDocID + "] en SAP. " +
                            "Error de integridad. El documento no posee items registrados.");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Error de integridad. El documento no posee items registrados.", 
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, "Null-Items", "Validacion Stellar", Program.mCnLocal);

                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                            RegistrosFallidos, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            RsVentas.MoveNext();

                            goto Continue;

                        }

                        // Registrar los productos de la transacción.

                        ItmCount = 0;

                        while (!RsItems.EOF)
                        {

                            StellarItemID = RsItems.Fields["ID"].Value.ToString();
                            StellarItemLn = RsItems.Fields["n_Linea"].Value.ToString();

                            //IDocument_Lines Line = Trans.Lines;
                            dynamic Line = Trans.Lines;

                            if (ItmCount > 0){
                                Line.Add();
                            }

                            ItmCount++;

                            //Program.Logger.EscribirLog("TransID:" + TransID + "|LineNum:" + Line.LineNum.ToString());

                            LineRelation.Add((TransID + "|" + ItmCount.ToString()), 
                            new String[] { StellarItemID, StellarItemLn });

                            Line.ItemCode = RsItems.Fields["Cod_Principal"].Value;
                            Line.Quantity = (RsItems.Fields["Cantidad"].Value * Signo);

                            if (Properties.Settings.Default.SAP_Linea_DeterminarPorcentajeDescuentoItem)
                            {

                                Double mPrecioOrig = Math.Abs(RsItems.Fields["n_PrecioReal"].Value);
                                Double mPrecioFinal = Math.Abs(RsItems.Fields["Precio"].Value);
                                Double mDctoUnit = Math.Round((mPrecioOrig - mPrecioFinal), 6);
                                if ((mDctoUnit > 0) && (mPrecioOrig != 0))
                                {
                                    Double mPorcDescLn = (Math.Round(mDctoUnit / mPrecioOrig, 6) * 100);
                                    Line.UnitPrice = mPrecioOrig;
                                    Line.DiscountPercent = mPorcDescLn;
                                }
                                else
                                {
                                    Line.UnitPrice = mPrecioFinal;
                                    Line.DiscountPercent = 0;
                                }
                            }
                            else
                            {
                                Line.UnitPrice = Math.Abs(RsItems.Fields["Precio"].Value);
                                Line.DiscountPercent = 0;
                            }

                            // Categorizar Impuesto del producto.

                            String LnTax1 = String.Empty;
                            String LnTax2 = String.Empty;
                            String LnTax3 = String.Empty;
                            String LnTax = String.Empty;

                            Double ItemTax = Math.Abs(
                                RsItems.Fields["Impuesto1"].Value + 
                                RsItems.Fields["Impuesto2"].Value + 
                                RsItems.Fields["Impuesto3"].Value
                            );

                            if (Program.TaxList1 != null)
                            if (Program.TaxList1.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList1.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax1);
                            }

                            if (Program.TaxList2 != null)
                            if (Program.TaxList2.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList2.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax2);
                            }

                            if (Program.TaxList3 != null)
                            if (Program.TaxList3.ContainsKey(Microsoft.VisualBasic.Strings.Format(
                            ItemTax, "00.00")))
                            {
                                Program.TaxList3.TryGetValue(Microsoft.VisualBasic.Strings.Format(
                                ItemTax, "00.00"), out LnTax3);
                            }

                            if (LnTax.isUndefined() && !LnTax1.isUndefined()) LnTax = LnTax1;
                            if (LnTax.isUndefined() && !LnTax2.isUndefined()) LnTax = LnTax2;
                            if (LnTax.isUndefined() && !LnTax3.isUndefined()) LnTax = LnTax3;

                            Line.TaxCode = LnTax;

                            // Valor opcional de Tipo Lista de Precio.

                            if (Program.ListaAsociacionTipoPrecio != null)
                            {

                                String nTipoPrecio = RsItems.Fields["n_TipoPrecio"].Value;
                                String TipoPrecio = String.Empty;

                                if (Program.ListaAsociacionTipoPrecio.ContainsKey(nTipoPrecio))
                                {
                                    Program.ListaAsociacionTipoPrecio.TryGetValue(nTipoPrecio, out TipoPrecio);
                                }

                                Line.UserFields.Fields.Item("U_Listaprecio").Value = TipoPrecio;

                            }

                            RsItems.MoveNext();

                        }

                        // Fin Productos.

                        // Grabar la transacción.

                        Int32 tmpresult = Trans.Add();

                        if (tmpresult != 0)
                        {

                            Int32 mErrorNumber; String mErrorDesc = String.Empty;

                            SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                            if (mErrorNumber == (-10) && mErrorDesc.ContainsIgnoreCase(".ItemCode][line:"))
                            {
                                String[] Step1 = mErrorDesc.Split(new String[] { ".ItemCode][line: " }, StringSplitOptions.None);
                                if (Step1.Length > 1)
                                {
                                    String[] Step2 = Step1[1].Split(new String[] { "]" }, StringSplitOptions.None);
                                    if (Step2.Length > 1)
                                    {
                                        try 
	                                    {
                                            //Program.Logger.EscribirLog("Linea: " + Step2[0] + " | ... : " + Step2[1]);
                                            Int32 mLinea = Convert.ToInt32(Step2[0]);
                                            //Program.Logger.EscribirLog("Linea: " + mLinea.ToString());
                                            //IDocument_Lines x;
                                            //Trans.Lines.SetCurrentLine(mLinea);
                                            String mKey = (TransID + "|" + mLinea.ToString());

                                            //Program.Logger.EscribirLog("mKey:" + mKey);

                                            String[] mLineData = LineRelation[mKey];

                                            //Program.Logger.EscribirLog("mData_0_1:" + mLineData[0] + "_" + mLineData[1]);

                                            RsItems.Filter = "ID = " + "(" + mLineData[0] + ")";

                                            if (!RsItems.EOF)
                                            {
                                                String mDescripcion = mCn.Execute("SELECT c_Descri FROM VAD20.DBO.MA_PRODUCTOS WHERE c_Codigo = '" + RsItems.Fields["Cod_Principal"].Value.ToString() + "'", out RecordsAffected).Fields["c_Descri"].Value;
                                                //Program.Logger.EscribirLog("Datos de Linea Cargada");
                                                mErrorDesc += ". Datos de la línea (Cod|Desc|Cant|Prc): " + "(" +
                                                RsItems.Fields["Cod_Principal"].Value.ToString() + "|" + mDescripcion + "|" +
                                                RsItems.Fields["Cantidad"].Value.ToString() + "|" + RsItems.Fields["Precio"].Value.ToString() + ")";
                                            }

	                                    }
	                                    catch (Exception ItemEx)
	                                    {
                                            Program.Logger.EscribirLog(ItemEx, "Error buscando informacion de la línea afectada.");
	                                    }
                                    }
                                }

                            }

                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + StellarDocID + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, mErrorNumber.ToString(), "Documents.Add()", Program.mCnLocal);

                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                            RegistrosFallidos, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            RsVentas.MoveNext();
                            goto Continue;

                        }

                        // Documento Registrado. Proceder a registrar los pagos.

                        String LastDocEntry; Int32 DocEntryNum; Double TotalDoc;

                        SAP.GetNewObjectCode(out LastDocEntry);
                        DocEntryNum = Convert.ToInt32(LastDocEntry);

                        if (Trans.GetByKey(DocEntryNum))
                        {
                            TotalDoc = Trans.DocTotal;
                        }
                        else
                        {

                            Int32 mErrorNumber; String mErrorDesc = String.Empty;

                            SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                            Program.Logger.EscribirLog("Error buscando Transaccion POS [" + StellarDocID + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                            "Error buscando Transaccion POS [" + TransID + "] en SAP. " +
                            "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                            TransID, mErrorNumber.ToString(), "Documents.GetByKey(" + DocEntryNum + ")", Program.mCnLocal);

                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                            RegistrosFallidos, Concepto, NumDoc);

                            Trans = null; //Trans.Cancel();
                            if (SAP.InTransaction)
                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                            RsVentas.MoveNext();
                            goto Continue;

                        }

                        if (Concepto.Equals("VEN", StringComparison.OrdinalIgnoreCase))
                        {

                            // Registrar pagos.

                            // Buscar Pagos del Documento.

                            RsDetPagos.Filter = "c_Factura = '" + NumDoc + "'";

                            if (RsDetPagos.EOF)
                            {

                                // No hay pagos. Error de Integridad.

                                Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + StellarDocID + "] en SAP. " +
                                "Error de integridad. El documento no posee pagos registrados.");

                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                                "Error de integridad. El documento no posee pagos registrados.",
                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                TransID, "Null-DetPag", "Validacion Stellar", Program.mCnLocal);

                                ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                RegistrosFallidos, Concepto, NumDoc);

                                Trans = null; //Trans.Cancel();
                                if (SAP.InTransaction)
                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                RsVentas.MoveNext();
                                goto Continue;

                            }

                            // Registrar los pagos de la transacción.

                            if (Properties.Settings.Default.SAP_FormaPagoFactura_SinDetallePago)
                            {

                                Double mMonto = RsVentas.Fields["n_Total"].Value;

                                //SAPbobsCOM.Payments DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                                dynamic DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                DetPag.DocType = BoRcptTypes.rCustomer;
                                DetPag.CardCode = Trans.CardCode;
                                DetPag.DocDate = Trans.DocDate;
                                //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                                //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                                //DetPag.VatDate = Trans.VatDate; // ????
                                //DetPag.Remarks = "Pago de Contado"; // No es necesario.
                                //DetPag.JournalRemarks = String.Empty; // No es necesario.

                                Int32 Serie = 0;

                                if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                    if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                        DetPag.Series = Serie;

                                DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                DetPag.Invoices.DocEntry = DocEntryNum;
                                DetPag.Invoices.SumApplied = TotalDoc;

                                DetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                DetPag.CashSum = TotalDoc;

                                tmpresult = DetPag.Add();

                                if (tmpresult != 0)
                                {

                                    Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                    SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                    Program.Logger.EscribirLog("Error ingresando pago en modo resumido en Transaccion POS [" + StellarDocID + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                    InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                    "Error ingresando pago en modo resumido en Transaccion POS [" + TransID + "] en SAP. " +
                                    "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                    "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                    TransID, mErrorNumber.ToString(), "[Summarized]Payments.Add()", Program.mCnLocal);

                                    ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                    RegistrosFallidos, Concepto, NumDoc);

                                    Trans = null; //Trans.Cancel();
                                    DetPag = null;
                                    if (SAP.InTransaction)
                                        SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                    RsVentas.MoveNext();
                                    goto Continue;

                                }

                            }
                            else
                            {

                                Int32 DetPagRowCount = RsDetPagos.RecordCount;
                                Int32 CurrentDetPagRow = 0;
                                Int32 CurrentCardPaymentRow = 0;
                                //Int32 CurrentCheckPaymentRow = 0;

                                Double mVueltoRestante = RsVentas.Fields["n_Vuelto"].Value;
                                Double mVueltoLn = 0;
                                Double mPagoLn = 0;
                                //Double mRestanteDonacion = RsVentas.Fields["n_MontoDonacion"].Value;

                                //SAPbobsCOM.Payments UniquePayment = null;
                                dynamic UniquePayment = null;

                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                {

                                    //UniquePayment = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);
                                    UniquePayment = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                    UniquePayment.DocType = BoRcptTypes.rCustomer;
                                    UniquePayment.CardCode = Trans.CardCode;
                                    UniquePayment.DocDate = Trans.DocDate;
                                    //UniquePayment.DueDate = Trans.DocDueDate; // Para Cheques?
                                    //UniquePayment.TaxDate = Trans.TaxDate; // Default System Date
                                    //UniquePayment.VatDate = Trans.VatDate; // ????
                                    //UniquePayment.Remarks = "Pago de Contado"; // No es necesario.
                                    //UniquePayment.JournalRemarks = String.Empty; // No es necesario.

                                    Int32 Serie = 0;

                                    if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                        if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                            UniquePayment.Series = Serie;

                                    UniquePayment.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                    UniquePayment.Invoices.DocEntry = DocEntryNum;

                                }

                                while (!RsDetPagos.EOF)
                                {

                                    CurrentDetPagRow++;

                                    Double mMontoLn = RsDetPagos.Fields["n_Monto"].Value;
                                    Double mFactor = RsDetPagos.Fields["n_Factor"].Value;
                                    Double mCantidad = Math.Round(mMontoLn / mFactor, 8);  //RsDetPagos.Fields["n_Cantidad"].Value;

                                    String mMoneda = RsDetPagos.Fields["c_CodMoneda"].Value;
                                    String mFormaPago = RsDetPagos.Fields["c_CodDenominacion"].Value;
                                    String LlaveFormaPago = mMoneda + ";" + mFormaPago;

                                    String MonedaPagoSAP = null;
                                    String TmpTenderCurrency = null;

                                    if (Program.ListaAsociacionMonedas != null)
                                        if (Program.ListaAsociacionMonedas.ContainsKey(mMoneda))
                                        {
                                            Program.ListaAsociacionMonedas.TryGetValue(mMoneda, out TmpTenderCurrency);
                                        }

                                    if (TmpTenderCurrency == null) TmpTenderCurrency = String.Empty;

                                    if (mVueltoRestante > 0)
                                    {

                                        if (TmpTenderCurrency.Equals(Properties.Settings.Default.SAP_CodigoMonedaPredeterminada,
                                        StringComparison.OrdinalIgnoreCase) && 
                                        mFormaPago.Equals("Efectivo", StringComparison.OrdinalIgnoreCase))
                                        {

                                            // Se determinó que el POS le resta el vuelto al Efectivo de la
                                            // moneda predeterminada. En estos casos entonces, no restamos.
                                            // Esto da pie a muchas inconsistencias pero bueno, asi esta hecho.
                                            // Hay que resolver de esta manera.

                                            mVueltoLn = 0;
                                            mPagoLn = Math.Round(mMontoLn, 8);

                                            // Es MAS!, en casos extraños pudiera quedar el Row con monto negativo...
                                            // Lo que significa que hay un vuelto que habría que restarle a otra línea.
                                            // entonces corregimos de la siguiente manera:

                                            if (mPagoLn < 0)
                                            {   // Modificamos el vuelto original por el vuelto Restante (el monto negativo)
                                                mVueltoRestante = Math.Abs(mPagoLn);
                                            }
                                            else
                                            {   // Si el monto quedo positivo entonces no quedo mas vuelto por restar a ninguna otra forma de pago.
                                                mVueltoRestante = 0;
                                            }

                                        }
                                        else
                                        {   // Si es cualquier otra cosa pero hay vuelto, restarsela
                                            // ya que el POS no se la resto. Caso Efectivo en Otras Monedas...
                                            mVueltoLn = (Math.Round(mMontoLn - mVueltoRestante, 8) >= 0 ?
                                            Math.Round(mVueltoRestante, 8) : Math.Round(mMontoLn, 8));
                                        }

                                        mPagoLn = Math.Round(mMontoLn - mVueltoLn, 8);
                                        mVueltoRestante = Math.Round(mVueltoRestante - mVueltoLn, 8);

                                    }
                                    else
                                    {
                                        mVueltoLn = 0;
                                        mPagoLn = Math.Round(mMontoLn, 8);
                                    }

                                    /*if (mRestanteDonacion > 0)
                                    {
                                        if (mRestanteDonacion > mPagoLn)
                                        {
                                            mRestanteDonacion = Math.Round(mRestanteDonacion - mPagoLn, 8);
                                            mPagoLn = 0;
                                        }
                                        else
                                        {
                                            mPagoLn = Math.Round(mPagoLn - mRestanteDonacion, 8);
                                            mRestanteDonacion = 0;
                                        }
                                    }*/

                                    //mPagoLn = Math.Round(mPagoLn, 2); // Mejor enviar el pago exactamente como es.

                                    if (mPagoLn > 0) // Monto final del pago - Vuelto - Donaciones.
                                    {

                                        SAPbobsCOM.Payments DetPag = null;

                                        if (!Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                        {

                                            DetPag = SAP.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                                            DetPag.DocType = BoRcptTypes.rCustomer;
                                            DetPag.CardCode = Trans.CardCode;
                                            DetPag.DocDate = Trans.DocDate;
                                            //DetPag.DueDate = Trans.DocDueDate; // Para Cheques?
                                            //DetPag.TaxDate = Trans.TaxDate; // Default System Date
                                            //DetPag.VatDate = Trans.VatDate; // ????
                                            //DetPag.Remarks = "Pago de Contado"; // No es necesario.
                                            //DetPag.JournalRemarks = String.Empty; // No es necesario.

                                            Int32 Serie = 0;

                                            if (!Properties.Settings.Default.SAP_SerieDocumentoPago.isUndefined())
                                                if (int.TryParse(Properties.Settings.Default.SAP_SerieDocumentoPago, out Serie))
                                                    DetPag.Series = Serie;

                                            DetPag.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                            DetPag.Invoices.DocEntry = DocEntryNum;
                                            //DetPag.Invoices.SumApplied = TotalDoc;

                                        }

                                        // Ahora Categorizar el Tipo de Pago.

                                        String mCodBanco = RsDetPagos.Fields["c_CodBanco"].Value;
                                        String mDesBanco = RsDetPagos.Fields["c_Banco"].Value;
                                        String mRef = RsDetPagos.Fields["c_Numero"].Value;

                                        MonedaPagoSAP = TmpTenderCurrency;

                                        String FormaPagoSAP = String.Empty;

                                        if (Program.ListaAsociacionFormaPago != null)
                                            if (Program.ListaAsociacionFormaPago.ContainsKey(LlaveFormaPago))
                                            {
                                                Program.ListaAsociacionFormaPago.TryGetValue(LlaveFormaPago, out FormaPagoSAP);
                                            }

                                        String BancoSAP = String.Empty;

                                        if (Program.ListaAsociacionBancos != null)
                                            if (Program.ListaAsociacionBancos.ContainsKey(mCodBanco))
                                            {
                                                Program.ListaAsociacionBancos.TryGetValue(mCodBanco, out BancoSAP);
                                            }

                                        if (BancoSAP == null) BancoSAP = String.Empty;

                                        Int64 BancoSAPNum = 1;

                                        if (!BancoSAP.isUndefined())
                                            if (Int64.TryParse(BancoSAP, out BancoSAPNum))

                                        if ((!Properties.Settings.Default.SAP_FormasPagoFallbackEfectivo &&
                                        FormaPagoSAP.isUndefined()) || 
                                        (!Properties.Settings.Default.SAP_FormasPagoFallbackMoneda &&
                                        MonedaPagoSAP.isUndefined()))
                                        {

                                            // Forma de pago no categorizada. Arreglar Setup

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + StellarDocID + "] en SAP. " +
                                            "Error de integridad. El documento posee una forma de pago que no esta " + 
                                            "categorizada para su envio a SAP => [" + mMoneda + "][" + mFormaPago + "]");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                                            "Error de integridad. El documento posee una forma de pago que no esta " +
                                            "categorizada para su envio a SAP => [" + mMoneda + "][" + mFormaPago + "]",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Invalid_DetPag", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                            RegistrosFallidos, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            DetPag = null;
                                            UniquePayment = null;
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        if ((!mFormaPago.Equals("Efectivo", StringComparison.OrdinalIgnoreCase)) &&
                                        (!Properties.Settings.Default.SAP_FormasPagoFallbackBanco) && BancoSAP.isUndefined())
                                        {

                                            // Forma de pago no categorizada. Arreglar Setup

                                            Program.Logger.EscribirLog("Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                                            "Error de integridad. El pago posee un codigo de banco que no esta " +
                                            "categorizado para su envio a SAP => [" + mCodBanco + "][" + mDesBanco + "]");

                                            InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                            "Error ingresando Transaccion POS [" + TransID + "] en SAP. " +
                                            "Error de integridad. El pago posee un codigo de banco que no esta " +
                                            "categorizado para su envio a SAP => [" + mCodBanco + "][" + mDesBanco + "]",
                                            "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                            TransID, "Invalid_Bank", "Validacion Stellar", Program.mCnLocal);

                                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                            RegistrosFallidos, Concepto, NumDoc);

                                            Trans = null; //Trans.Cancel();
                                            DetPag = null;
                                            UniquePayment = null;
                                            if (SAP.InTransaction)
                                                SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                            RsVentas.MoveNext();

                                            goto Continue;

                                        }

                                        switch (FormaPagoSAP.ToUpper())
                                        {

                                            /*case "CHECK":

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {

                                                    if (CurrentCheckPaymentRow > 1) UniquePayment.Checks.Add();

                                                    UniquePayment.CheckAccount = Properties.Settings.Default.SAP_ChecksAccount;
                                                    UniquePayment.Checks.BankCode = BancoSAP;

                                                    Int32 mCheckNum = 1;

                                                    if (Int32.TryParse(mRef, out mCheckNum))
                                                        UniquePayment.Checks.CheckNumber = mCheckNum;
                                                    else
                                                        UniquePayment.Checks.CheckNumber = 1;

                                                    UniquePayment.Checks.CheckSum = mPagoLn;

                                                }
                                                else
                                                {

                                                    if (CurrentCheckPaymentRow > 1) DetPag.Checks.Add();

                                                    DetPag.CheckAccount = Properties.Settings.Default.SAP_ChecksAccount;
                                                    DetPag.Checks.BankCode = BancoSAP;

                                                    Int32 mCheckNum = 1;

                                                    if (Int32.TryParse(mRef, out mCheckNum))
                                                        DetPag.Checks.CheckNumber = mCheckNum;
                                                    else
                                                        DetPag.Checks.CheckNumber = 1;

                                                    DetPag.Checks.CheckSum = mPagoLn;

                                                }

                                                break;

                                             * NO USAR: ESTO ES SOLO PARA OUTGOING PAYMENTS COMO INDICA EL SDK (PAGO A PROVEEDORES).
                                             */

                                            case "CREDIT":

                                                CurrentCardPaymentRow++;

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {
                                                    if (CurrentCardPaymentRow > 1) UniquePayment.CreditCards.Add();
                                                    UniquePayment.CreditCards.CreditCard = ((Int32)BancoSAPNum);
                                                    UniquePayment.CreditCards.CreditCardNumber = "12340000123400001234";
                                                    UniquePayment.CreditCards.CardValidUntil = Trans.DocDueDate;
                                                    UniquePayment.CreditCards.CreditSum = mPagoLn;
                                                    UniquePayment.CreditCards.VoucherNum = mRef;
                                                    UniquePayment.Invoices.SumApplied += mPagoLn;
                                                }
                                                else
                                                {
                                                    if (CurrentCardPaymentRow > 1) DetPag.CreditCards.Add();
                                                    DetPag.CreditCards.CreditCard = ((Int32)BancoSAPNum);
                                                    DetPag.CreditCards.CreditCardNumber = "12340000123400001234";
                                                    DetPag.CreditCards.CardValidUntil = Trans.DocDueDate;
                                                    DetPag.CreditCards.CreditSum = mPagoLn;
                                                    DetPag.CreditCards.VoucherNum = mRef;
                                                }

                                                break;

                                            case "TRANSFER":
                                                
                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {
                                                    UniquePayment.TransferAccount = Properties.Settings.Default.SAP_TransfersAccount;
                                                    UniquePayment.TransferDate = RsDetPagos.Fields["d_Fecha"].Value;
                                                    UniquePayment.TransferReference = mRef;
                                                    UniquePayment.TransferSum += mPagoLn;
                                                    UniquePayment.Invoices.SumApplied += mPagoLn;
                                                }
                                                else
                                                {
                                                    DetPag.TransferAccount = Properties.Settings.Default.SAP_TransfersAccount;
                                                    DetPag.TransferDate = RsDetPagos.Fields["d_Fecha"].Value;
                                                    DetPag.TransferReference = mRef;
                                                    DetPag.TransferSum = mPagoLn;
                                                }

                                                break;

                                            case "CASH":
                                            default:

                                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                                {
                                                    UniquePayment.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                                    UniquePayment.CashSum += mPagoLn;
                                                    UniquePayment.Invoices.SumApplied += mPagoLn;
                                                }
                                                else
                                                {
                                                    DetPag.CashAccount = Properties.Settings.Default.SAP_CashAccount;
                                                    DetPag.CashSum = mPagoLn;
                                                }

                                                break;

                                        }

                                        if (!Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                        {

                                            tmpresult = DetPag.Add();

                                            if (tmpresult != 0)
                                            {

                                                Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                                SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                                Program.Logger.EscribirLog("Error ingresando pago en Transaccion POS [" + TransID + "] en SAP. " +
                                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                                "Error ingresando pago en Transaccion POS [" + TransID + "] en SAP. " +
                                                "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                                TransID, mErrorNumber.ToString(), "Payments.Add()", Program.mCnLocal);

                                                ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                                RegistrosFallidos, Concepto, NumDoc);

                                                Trans = null; //Trans.Cancel();
                                                DetPag = null;
                                                if (SAP.InTransaction)
                                                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                                RsVentas.MoveNext();
                                                goto Continue;

                                            }

                                        }

                                    }

                                    RsDetPagos.MoveNext();

                                } // End While NOT RsDetPag.EOF

                                if (Properties.Settings.Default.SAP_UnSoloPaymentsPorFactura)
                                {

                                    tmpresult = UniquePayment.Add();

                                    if (tmpresult != 0)
                                    {

                                        Int32 mErrorNumber; String mErrorDesc = String.Empty;

                                        SAP.GetLastError(out mErrorNumber, out mErrorDesc);

                                        Program.Logger.EscribirLog("Error ingresando pagos de la factura en Transaccion POS [" + mUltDoc + "] en SAP. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]");

                                        InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                                        "Error ingresando pago único de factura en Transaccion POS [" + TransID + "] en SAP. " +
                                        "Datos del Error devuelto por SAP: [" + mErrorNumber.ToString() + "][" + mErrorDesc + "]",
                                        "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS",
                                        TransID, mErrorNumber.ToString(), "[Unique]Payments.Add()", Program.mCnLocal);

                                        ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                                        RegistrosFallidos, Concepto, NumDoc);

                                        Trans = null; //Trans.Cancel();
                                        UniquePayment = null;
                                        if (SAP.InTransaction)
                                            SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

                                        RsVentas.MoveNext();
                                        goto Continue;

                                    }

                                } // Fin Condicional UniquePayment

                            } // Si hay pagos

                        } // Si es venta y aplica pagos.

                        // Si todo salio bien, grabar.

                        if (SAP.InTransaction)
                        {

                            SAP.EndTransaction(BoWfTransOpt.wf_Commit);

                            Program.mCnLocal.Execute(
                            "UPDATE MA_PAGOS SET cs_Sync_SxS = '" + Program.gCorrelativo + "|" + LastDocEntry + "' " +
                            "WHERE (c_Concepto + c_Numero) = '" + TransID + "'", out RecordsAffected);

                            InsertarAuditoria(Program.LogIDAuditoria, Program.InfoLog, 
                            "Transaccion POS [" + StellarDocID + "] enviada con exito. " +
                            "SAP DocEntryNum [" + LastDocEntry + "]", "DBSync.ConstruirRegistrosDeVentas()", 
                            Concepto, NumDoc, LastDocEntry, "EnviarTransaccionPOS", Program.mCnLocal);

                        }
                        else
                        {

                            Program.Logger.EscribirLog("Error en SAP. Transaccion Inactiva / Rollback forzoso.");

                            InsertarAuditoria(Program.LogIDAuditoria, Program.WarningLog,
                            "Transaccion BD SAP Inactiva / Rollback forzoso.", "DBSync.ConstruirRegistrosDeVentas()",
                            "TransaccionPOS", TransID, String.Empty, "Validacion Stellar", Program.mCnLocal);

                            ControlarErrorRegistroVentas(ref RsVentas, ref RsItems, ref RsDetPagos,
                            RegistrosFallidos, Concepto, NumDoc);

                        }

                        RsVentas.MoveNext();

                    }

                }

                MarcarFallidos(ref RegistrosFallidos);

                // Proceso Finalizado.

                return;

	        }
	        catch (Exception Any)
	        {

                if (SAP.InTransaction)
                    SAP.EndTransaction(BoWfTransOpt.wf_RollBack);

		        Program.Logger.EscribirLog(Any, "ConstruirRegistrosDeVentas();" + "\n\n" +
                "mUltDoc=" + mUltDoc + "\n" +
                "mUltFec=" + mUltFec.ToString() + "\n" +
                "mUltTot=" + mUltTot.ToString() + "\n" + 
                "\n");

                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error aleatorio durante el procesamiento de transacciones de ventas y devoluciones." + "\t" + 
                "mUltDoc=" + mUltDoc + "\t" +
                "mUltFec=" + mUltFec.ToString() + "\t" +
                "mUltTot=" + mUltTot.ToString() + "\t",
                "DBSync.ConstruirRegistrosDeVentas()", "TransaccionPOS", mUltDoc, 
                (Any.HResult + "|" + Any.Message), "Procesando_Excepcion", Program.mCnLocal);

                if (Program.gDebugMode) {
                    MessageBox.Show(Any.Message + "\n" +
                    "mUltDoc=" + mUltDoc + "\n" +
                    "mUltFec=" + mUltFec.ToString() + "\n" +
                    "mUltTot=" + mUltTot.ToString() + "\n");
                }

	        }

        }

        private static Boolean ObtenerDatosVentas(out ADODB.Recordset pRsVentas, out ADODB.Recordset pRsItems,
        out ADODB.Recordset pRsDetPagos, out ADODB.Recordset pRsImpuestos, Boolean pReprocesarFallidos)
        {

            Boolean ObtenerDatosVentas = false;

            try 
	        {

                String mSQL;
                
                String ExcluirVNF = String.Empty;

                String mEstatus = String.Empty;

                if (Functions.ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", ref Program.mCnLocal))
                    ExcluirVNF = " AND bDocNoFiscal = 0";

                if (pReprocesarFallidos)
                    mEstatus = RegistrosPendientesFallidos;
                else
                    mEstatus = RegistrosPendientesPorLote;

                mSQL = "SELECT * , " + "\n" +
                "(DOC.c_Concepto + DOC.c_Numero) AS TransID " + "\n" + 
                "FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" +
                "WHERE (DOC.cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + " " +
                "ORDER BY DOC.ID";
    
                pRsVentas = new ADODB.Recordset();
    
                pRsVentas.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
    
                pRsVentas.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
    
                pRsVentas.ActiveConnection = null;
                
                if (Properties.Settings.Default.EnviarVentaAgrupada)
                {
                    mSQL = String.Empty +
                    "SELECT c_Concepto, c_Numero, MIN(n_Linea) AS n_Linea, Cod_Principal, Cod_Principal AS Codigo, " + "\n" +
                    "SUM(Cantidad) AS Cantidad, ABS(ROUND(Precio, 3, 0)) AS Precio, " + "\n" +
                    "ABS(ROUND(n_PrecioReal, 3, 0)) AS n_PrecioReal, " + "\n" +
                    "SUM(Subtotal) AS Subtotal, SUM(Impuesto) AS Impuesto, SUM(Total) AS Total, " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, SUM(Descuento) AS Descuento, n_TipoPrecio, " + "\n" +
                    "MIN(ID) AS ID, (c_Concepto + c_Numero) AS TransID " + "\n" +
                    "FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT (c_Concepto + c_Numero) FROM MA_PAGOS" + "\n" +
                    "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ") " + "\n" +
                    "GROUP BY Cod_Principal, ABS(ROUND(Precio, 3, 0)), ABS(ROUND(n_PrecioReal, 3, 0)), " + "\n" +
                    "Impuesto1, Impuesto2, Impuesto3, n_TipoPrecio, c_Concepto, c_Numero HAVING ABS(ROUND(SUM(Cantidad), 8, 0)) >= 0.0001 " + "\n" +
                    "ORDER BY MIN(ID) "; // NO ENVIAR PRODUCTOS REINTEGRADOS CUYA CANTIDAD QUEDO EN CERO.
                }
                else
                {
                    mSQL = "SELECT * FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Concepto + c_Numero) IN (" + "\n" +
                    "SELECT (c_Concepto + c_Numero) FROM MA_PAGOS" + "\n" +
                    "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ") " + "\n" +
                    "ORDER BY ID ";
                }
    
                pRsItems = new ADODB.Recordset();
    
                pRsItems.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
    
                pRsItems.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
    
                pRsItems.ActiveConnection = null;

                mSQL = "SELECT *, (c_Factura) AS TransID " + "\n" +
                "FROM [VAD20].[DBO].[MA_DETALLEPAGO] WHERE (c_Factura) IN (" + "\n" +
                "SELECT (c_Numero) FROM MA_PAGOS" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
                "ORDER BY c_Factura, " + "\n" +
                "(CASE WHEN c_CodDenominacion = 'Efectivo' THEN 1 ELSE 0 END) DESC, ID";
    
                pRsDetPagos = new ADODB.Recordset();
    
                pRsDetPagos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
    
                pRsDetPagos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
    
                pRsDetPagos.ActiveConnection = null;
    
                mSQL = "SELECT * FROM [VAD20].[DBO].[MA_PAGOS_IMPUESTOS] WHERE (c_Concepto + c_Numero) IN (" + "\n" +
                "SELECT (c_Concepto + c_Numero) FROM MA_PAGOS" + "\n" +
                "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";
    
                pRsImpuestos = new ADODB.Recordset();
    
                pRsImpuestos.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
    
                pRsImpuestos.Open(mSQL, Program.mCnLocal, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockReadOnly);
    
                pRsImpuestos.ActiveConnection = null;
    
                ObtenerDatosVentas = (pRsVentas.RecordCount > 0);

                return ObtenerDatosVentas;

	        }
	        catch (Exception Any)
	        {

                Program.Logger.EscribirLog(Any, "ObtenerDatosVentas();");

                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Buscando informacion de las Transacciones POS pendientes por procesar.", "DBSync.ObtenerDatosVentas()",
                "Ejecucion Agente", Program.gCorrelativo, (Any.HResult + "|" + Any.Message), "BuscarDatos", Program.mCnLocal);

                pRsVentas = null;
                pRsItems = null;
                pRsDetPagos = null;
                pRsImpuestos = null;

		        return ObtenerDatosVentas;
	        }

        }

        private static void ControlarErrorRegistroVentas(ref ADODB.Recordset pRsVentas, ref ADODB.Recordset pRsItems, 
        ref ADODB.Recordset pRsDetPagos, System.Collections.ArrayList pFallidos, String pConcepto, String pNumero)
        {

            pFallidos.Add( new String[] { pConcepto, pNumero } );
            
        }
        
        private static Boolean MarcarFallidos(ref System.Collections.ArrayList pListaFallidos)
        {

            try 
	        {

                Object Records;

                foreach (String[] Item in pListaFallidos)
                {
                    Program.mCnLocal.Execute("UPDATE [VAD20].[DBO].[MA_PAGOS] " + 
                    "SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + "\n" +
                    "WHERE c_Concepto = '" + Item[0] + "' " + 
                    "AND c_Numero = '" + Item[1] + "'", 
                    out Records);
                }

                return true;

	        }
	        catch (Exception Any)
	        {
                Program.Logger.EscribirLog (Any, "Error marcando registros de ventas fallidos.");
                InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog,
                "Error marcando registros de ventas o devoluciones fallidos.",
                "DBSync.MarcarFallidos()", "Ejecución Agente",
                Program.gCorrelativo, Program.mCnLocal);
                return false;
	        }
            
        }

        //   ACA SE AGREGA ESTA FUNCIÓN CON EL FIN DE GENERAR LAS AUDITORIAS QUE SE REQUIERAN PARA TODOS LOS CASOS.

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria,
        String DescripcionAuditoria,
        String DescripcionEvento,
        String Ventana,
        String TipoObjeto,
        String CodigoAfectado,
        String CodigoRetorno,
        String AccionRealizada,
        ADODB.Connection Conexion)
        {

            try
            {

                ADODB.Connection DB = null;
                //DB = new ADODB.Connection();
                DB = Conexion;

                ADODB.Recordset mRs = null; String SQL = String.Empty;

                mRs = new ADODB.Recordset();

                SQL = "SELECT * FROM [VAD10].[DBO].[MA_AUDITORIAS] WHERE 1 = 2";

                //DB.ConnectionString = Conexion.ConnectionString;
                //DB.Open();

                mRs.Open(SQL, DB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                mRs.AddNew();

                mRs.Fields["Cod_Prod"].Value = Program.gCodProducto;
                mRs.Fields["Nom_Prod"].Value = Program.ApplicationName;
                mRs.Fields["Tipo"].Value = TipoAuditoria;
                mRs.Fields["Descripcion"].Value = DescripcionAuditoria;
                mRs.Fields["Evento"].Value = DescripcionEvento;
                //mRs.Fields["Fecha"].Value = DateTime.Now; // Posee Valor Default GetDate()
                mRs.Fields["CodUsuario"].Value = "9999999999";
                mRs.Fields["Usuario"].Value = mRs.Fields["Nom_Prod"].Value;
                mRs.Fields["Ventana"].Value = Ventana;
                mRs.Fields["TipoObjAuditado"].Value = TipoObjeto;
                mRs.Fields["CodigoAuditado"].Value = CodigoAfectado;
                if (Functions.ExisteCampoRs(mRs, "CodigoRetorno"))
                    mRs.Fields["CodigoRetorno"].Value = CodigoRetorno;
                if (Functions.ExisteCampoRs(mRs, "AccionRealizada"))
                    mRs.Fields["AccionRealizada"].Value = AccionRealizada;

                mRs.Update();

                mRs.Close();

                // DB.Close();

                return true;

            }
            catch (Exception Any)
            {
                Program.Logger.EscribirLog(Any, "InsertarAuditoria()" + "\n" +
                "Cod_Prod=" + Program.gCodProducto + "\n" +
                "Nom_Prod=" + Program.ApplicationName + "\n" +
                "Tipo=" + TipoAuditoria + "\n" +
                "Descripcion=" + DescripcionAuditoria + "\n" +
                "Evento=" + DescripcionEvento + "\n" +
                "Ventana=" + Ventana + "\n" +
                "TipoObjAuditado=" + TipoObjeto + "\n" +
                "CodigoAuditado=" + CodigoAfectado + "\n" +
                "CodigoRetorno=" + CodigoRetorno + "\n" +
                "AccionRealizada=" + AccionRealizada + "\n");
                return false;
            }
            
        }

        public static Boolean InsertarAuditoria(Int64 TipoAuditoria, 
        String DescripcionAuditoria, 
        String DescripcionEvento, 
        String Ventana, 
        String TipoObjeto, 
        String CodigoAfectado, 
        ADODB.Connection Conexion)
        {
            return InsertarAuditoria(TipoAuditoria, DescripcionAuditoria, DescripcionEvento, 
            Ventana, TipoObjeto, CodigoAfectado, String.Empty, String.Empty, Conexion);
        }

    }

}
