﻿using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using isADS_SYNC.Clases;

namespace isADS_SYNC
{
    class FileSync
    {
        public void ExecuteSync(string sourceDir, string remoteDir)
        {
            FileSyncProvider origen = new FileSyncProvider(@sourceDir, null, FileSyncOptions.CompareFileStreams);
            FileSyncProvider destino = new FileSyncProvider(@remoteDir, null, FileSyncOptions.CompareFileStreams);

            try
            {
                origen.DetectChanges();
                //destino.DetectChanges();

                SyncOrchestrator so = new SyncOrchestrator();
                so.Direction = SyncDirectionOrder.Download;
                so.RemoteProvider = origen;
                so.LocalProvider = destino;

                SyncOperationStatistics syncStats = so.Synchronize();

                Console.WriteLine("Hora Inicio: " + syncStats.SyncStartTime);
                Console.WriteLine("Archivos Subidos: " + syncStats.UploadChangesTotal);
                Console.WriteLine("Archivos Descargados: " + syncStats.DownloadChangesTotal);
                Console.WriteLine("Hora Fin: " + syncStats.SyncEndTime);
            }
            catch (Exception e)
            {
                Console.WriteLine("Message:" + e.Message);
            }
            finally
            {
                origen.Dispose();
                destino.Dispose();
            }
        }

        private enum ContentType
        {
            File,
            Folder
        }


        private void Analyze(String Root, String Parent, String Content, ftp Ftp)
        {

            Root = Root.isUndefined() ? String.Empty : Root + "/";
            Parent = Parent.isUndefined() ? String.Empty : Parent + "\\"; 

            String LocalName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" + 
                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir 
                + Parent + Content;

            ContentType ContentType;
            
            // Doing this because we will control files and won't allow dots for folders because it is problematic...
            // so far we didn't find a way with the current ftp facilities to determine if an archive was either
            // file or folder in the hosting repository, that's why we enforce this kind of validation.
            if (Content.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;

            switch (ContentType)
            {
                case ContentType.File:
                {
                    if (!File.Exists(LocalName))
                    {
                        String FilePath = Root.isUndefined() ? Content : Root + Content;
                            
                        try
                        {
                            Ftp.download(FilePath, LocalName);
                                
                            // Byte[] file;
                            // file = File.ReadAllBytes(LocalName);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                    else
                    {
                        String FilePath = Root.isUndefined() ? Content : Root + Content;

                        try
                        {

                            long RemoteFileSize = Ftp.getFileSizeinBytes(FilePath);

                            long CurrentFileSize = new FileInfo(LocalName).Length;

                            if (RemoteFileSize != CurrentFileSize)
                            {
                                // Most likely the Previous File Download process was cut by any reason or the file got updates.
                                File.Delete(LocalName);
                                Ftp.download(FilePath, LocalName);
                            }

                        }
                        catch (Exception Any) { Console.WriteLine(Any.Message); }

                    }

                    try
                    {
                        if (File.Exists(LocalName))
                        {
                            String Ext = new FileInfo(LocalName).Extension;
                            if (Ext.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                            {                                                                       

                                //String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName); 
                                // Replace Filename by Filename without extension would work but
                                // would be trouble if by any means it would match more than one
                                // Better be sure and just take off the extension part.

                                String NewDir = LocalName.Substring(0, LocalName.Length - Ext.Length);

                                if (!System.IO.Directory.Exists(NewDir)){
                                    System.IO.Directory.CreateDirectory(NewDir);
                                    System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, NewDir);
                                }
                                else
                                {
                                    DirectoryInfo ExistingDirInfo = new DirectoryInfo(NewDir);
                                    if (ExistingDirInfo.GetDirectories().Length == 0 && ExistingDirInfo.GetFiles().Length == 0)
                                    {
                                        System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, NewDir);
                                    }
                                }                                    
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName);
                            String NewDir = LocalName.Replace(Content, ZipFileName);
                            System.IO.Directory.Delete(NewDir, true); 
                        }
                        catch (Exception) { }
                        Console.WriteLine(e.Message);
                    }

                    break;
                }

                case ContentType.Folder:
                {
                    if (!Content.StartsWith("Preview_", StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (!System.IO.Directory.Exists(LocalName))
                        {
                            System.IO.Directory.CreateDirectory(LocalName);
                        }

                        Root = Root + Content;
                        Parent = Parent + Content;                    

                        string[] DirectoryContents = Ftp.directoryListSimple(Root);
                        Array.Sort(DirectoryContents);

                        foreach (string SubContent in DirectoryContents){
                            if (SubContent.Equals("")) continue;

                            Analyze(Root, Parent, SubContent, Ftp);
                        }

                    }
                    break;
                }
            }
        }

        private void Analyze(String Root, String Parent, String Content, ftp Ftp, String[] OnlyNeededADS)
        {

            Root = Root.isUndefined() ? String.Empty : Root + "/";
            Parent = Parent.isUndefined() ? String.Empty : Parent + "\\";

            String LocalName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" +
                Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + @"\" + Properties.Settings.Default.CampaignFilesDir
                + Parent + Content;

            ContentType ContentType;

            // Doing this because we will control files and won't allow dots for folders because it is problematic...
            // so far we didn't find a way with the current ftp facilities to determine if an archive was either
            // file or folder in the hosting repository, that's why we enforce this kind of validation.
            if (Content.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;

            switch (ContentType)
            {
                case ContentType.File:
                    {
                        if (!File.Exists(LocalName))
                        {
                            String FilePath = Root.isUndefined() ? Content : Root + Content;

                            try
                            {
                                Ftp.download(FilePath, LocalName);

                                // Byte[] file;
                                // file = File.ReadAllBytes(LocalName);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            String FilePath = Root.isUndefined() ? Content : Root + Content;

                            try
                            {

                                long RemoteFileSize = Ftp.getFileSizeinBytes(FilePath);

                                long CurrentFileSize = new FileInfo(LocalName).Length;

                                if (RemoteFileSize != CurrentFileSize)
                                {
                                    // Most likely the Previous File Download process was cut by any reason or the file got updates.
                                    File.Delete(LocalName);
                                    Ftp.download(FilePath, LocalName);
                                }

                            } catch (Exception Any) { Console.WriteLine(Any.Message); }

                        }

                        try
                        {
                            if (File.Exists(LocalName))
                            {
                                String Ext = new FileInfo(LocalName).Extension;
                                if (Ext.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                                {

                                    //String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName); 
                                    // Replace Filename by Filename without extension would work but
                                    // would be trouble if by any means it would match more than one
                                    // Better be sure and just take off the extension part.

                                    String Dir = LocalName.Substring(0, LocalName.Length - Ext.Length);

                                    if (!System.IO.Directory.Exists(Dir))
                                    {
                                        System.IO.Directory.CreateDirectory(Dir);
                                        System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);                                        
                                    }
                                    else
                                    {

                                        /*
                                        DirectoryInfo ExistingDirInfo = new DirectoryInfo(Dir);
                                        if (ExistingDirInfo.GetDirectories().Length == 0 && ExistingDirInfo.GetFiles().Length == 0)
                                        {
                                            System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        }
                                        */

                                        long ExtractedBytes = Functions.GetZipExtractedSize(LocalName);
                                        long ExistingDirBytes = Functions.GetDirectorySize(Dir);

                                        if (ExistingDirBytes == 0) System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        else if (ExistingDirBytes != ExtractedBytes)
                                        {
                                            System.IO.Directory.Delete(Dir, true);
                                            System.IO.Compression.ZipFile.ExtractToDirectory(LocalName, Dir);
                                        }
                                        //else The Application is unmodified, fully extracted and should be displaying correctly.

                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            try
                            {
                                String ZipFileName = System.IO.Path.GetFileNameWithoutExtension(LocalName);
                                String Dir = LocalName.Replace(Content, ZipFileName);
                                System.IO.Directory.Delete(Dir, true);
                            }
                            catch (Exception) { }
                            Console.WriteLine(e.Message);
                        }

                        break;
                    }

                case ContentType.Folder:
                    {                       
                        if (!Content.StartsWith("Preview_", StringComparison.InvariantCultureIgnoreCase))
                        {

                            Root = Root + Content;
                            Parent = Parent + Content;

                            int ADSDirectoryLevel = 3;

                            Console.WriteLine(Root.Count(TmpChar => TmpChar.Equals('/')).ToString() + " - " + LocalName);

                            if (Root.Count(TmpChar => TmpChar.Equals('/')) == ADSDirectoryLevel)
                                if (!(OnlyNeededADS.Contains(Content, StringComparer.OrdinalIgnoreCase))) 
                                    break;

                            if (!System.IO.Directory.Exists(LocalName))
                            {
                                System.IO.Directory.CreateDirectory(LocalName);
                            }

                            string[] DirectoryContents = Ftp.directoryListSimple(Root);
                            Array.Sort(DirectoryContents);

                            foreach (string SubContent in DirectoryContents)
                            {
                                if (SubContent.Equals("")) continue;

                                Analyze(Root, Parent, SubContent, Ftp, OnlyNeededADS);
                            }

                        }
                        break;
                    }
            }
        }

        public void DownloadFromFTP(String OrganizationID)
        {
            // string downloadFile;
            string ContentName;
            // string localName;
            
            string[] registroFTP;

            ftp f = null;

            f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigwise.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigwise.com", "sgngemanager", "Bigwise123#")); // Current
            string folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);
            //f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigven.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigven.com", "sgngemanager", "Bigwise123#")); // Until Dec 2015
            //string folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);

            try
            {
                string[] ftpObject = f.directoryListSimple(folder);
                // string[] content;
                Array.Sort(ftpObject);
                // int index = ftpObject.Length - 2;

                foreach (string Object in ftpObject)
                {
                    if (Object.Equals("")) continue;

                    registroFTP = Object.Split('/');
                    ContentName = registroFTP[0];
                    // downloadFile = folder + "/" + ContentName;

                    Analyze(folder, String.Empty, ContentName, f);

                    ////localName = @"C:\ftptest\" + fileName;
                    //localName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" + Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + Properties.Settings.Default.CampaignFilesDir + fileName;

                    //ContentType ContentType;

                    //if (fileName.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;                    

                    ////content = f.directoryListDetailed(downloadFile);

                    ////foreach (string asdf in content)
                    ////{
                    ////    if (asdf.Contains("<DIR>"))
                    ////    {
                    ////        Console.WriteLine("Soy un DIR");
                    ////    }
                    ////}

                    //switch (ContentType)
                    //{
                    //    case ContentType.File:
                    //        {
                    //            if (!File.Exists(localName))
                    //            {
                    //                Byte[] file; 
                    //                try
                    //                {
                    //                    f.download(downloadFile, localName);
                    //                    file = File.ReadAllBytes(localName);
                    //                }
                    //                catch (Exception e)
                    //                {
                    //                    Console.WriteLine(e.Message);
                    //                }
                    //            }

                    //            break;
                    //        }
                    //    case ContentType.Folder:
                    //        {
                    //            if (!System.IO.Directory.Exists(localName))
                    //            {
                    //                System.IO.Directory.CreateDirectory(localName);
                    //            }

                    //            break;
                    //        }
                    //}


                    //if (!System.IO.Directory.Exists(localName))
                    //{
                    //    System.IO.Directory.CreateDirectory(localName);
                    //}

                    //if (!File.Exists(localName))
                    //{
                    //    try
                    //    {
                    //        f.download(downloadFile, localName);
                    //        file = File.ReadAllBytes(localName);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Console.WriteLine(e.Message);
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void DownloadFromFTP(String OrganizationID, String[] OnlyNeededADS)
        {
            // string downloadFile;
            string ContentName;
            // string localName;

            string[] registroFTP;

            ftp f = null;

            f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigwise.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigwise.com", "sgngemanager", "Bigwise123#")); // Current
            string folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);
            //f = (Properties.Settings.Default.RemoteDB_Beta ? new ftp("ftp://ftp.bigven.com", "sgngebeta", "Bigwise123#") : new ftp("ftp://ftp.bigven.com", "sgngemanager", "Bigwise123#")); // Until Dec 2015
            //string folder = (Properties.Settings.Default.RemoteDB_Beta ? "/" + OrganizationID : "/" + OrganizationID);

            try
            {
                string[] ftpObject = f.directoryListSimple(folder);
                // string[] content;
                Array.Sort(ftpObject);
                // int index = ftpObject.Length - 2;

                foreach (string Object in ftpObject)
                {
                    if (Object.Equals("")) continue;

                    registroFTP = Object.Split('/');
                    ContentName = registroFTP[0];
                    // downloadFile = folder + "/" + ContentName;

                    Analyze(folder, String.Empty, ContentName, f, OnlyNeededADS);

                    ////localName = @"C:\ftptest\" + fileName;
                    //localName = System.Environment.GetEnvironmentVariable("ProgramData") + @"\" + Properties.Settings.Default.ProgramDataDir + Properties.Settings.Default.ApplicationName + Properties.Settings.Default.CampaignFilesDir + fileName;

                    //ContentType ContentType;

                    //if (fileName.Contains(".")) ContentType = ContentType.File; else ContentType = ContentType.Folder;                    

                    ////content = f.directoryListDetailed(downloadFile);

                    ////foreach (string asdf in content)
                    ////{
                    ////    if (asdf.Contains("<DIR>"))
                    ////    {
                    ////        Console.WriteLine("Soy un DIR");
                    ////    }
                    ////}

                    //switch (ContentType)
                    //{
                    //    case ContentType.File:
                    //        {
                    //            if (!File.Exists(localName))
                    //            {
                    //                Byte[] file; 
                    //                try
                    //                {
                    //                    f.download(downloadFile, localName);
                    //                    file = File.ReadAllBytes(localName);
                    //                }
                    //                catch (Exception e)
                    //                {
                    //                    Console.WriteLine(e.Message);
                    //                }
                    //            }

                    //            break;
                    //        }
                    //    case ContentType.Folder:
                    //        {
                    //            if (!System.IO.Directory.Exists(localName))
                    //            {
                    //                System.IO.Directory.CreateDirectory(localName);
                    //            }

                    //            break;
                    //        }
                    //}


                    //if (!System.IO.Directory.Exists(localName))
                    //{
                    //    System.IO.Directory.CreateDirectory(localName);
                    //}

                    //if (!File.Exists(localName))
                    //{
                    //    try
                    //    {
                    //        f.download(downloadFile, localName);
                    //        file = File.ReadAllBytes(localName);
                    //    }
                    //    catch (Exception e)
                    //    {
                    //        Console.WriteLine(e.Message);
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
