﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace isAGENT_SxSBO.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    public sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Bigwise\\Stellar\\")]
        public string ProgramDataDir {
            get {
                return ((string)(this["ProgramDataDir"]));
            }
            set {
                this["ProgramDataDir"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("isAGENT_SxSBO")]
        public string ApplicationName {
            get {
                return ((string)(this["ApplicationName"]));
            }
            set {
                this["ApplicationName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("isAGENT_SxsBO-Credentials-MinimumSecurity. 0830342D83X0267121JJ24423EMD8353.4:354" +
            ";35")]
        public string EncryptionKey {
            get {
                return ((string)(this["EncryptionKey"]));
            }
            set {
                this["EncryptionKey"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Stellar_SQLServerName {
            get {
                return ((string)(this["Stellar_SQLServerName"]));
            }
            set {
                this["Stellar_SQLServerName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("VAD20")]
        public string Stellar_SQLDBName {
            get {
                return ((string)(this["Stellar_SQLDBName"]));
            }
            set {
                this["Stellar_SQLDBName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("SA")]
        public string Stellar_SQLUser {
            get {
                return ((string)(this["Stellar_SQLUser"]));
            }
            set {
                this["Stellar_SQLUser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Stellar_SQLPass {
            get {
                return ((string)(this["Stellar_SQLPass"]));
            }
            set {
                this["Stellar_SQLPass"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Stellar_TrustedConnection {
            get {
                return ((bool)(this["Stellar_TrustedConnection"]));
            }
            set {
                this["Stellar_TrustedConnection"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_Server {
            get {
                return ((string)(this["SAP_Server"]));
            }
            set {
                this["SAP_Server"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_LicenseServer {
            get {
                return ((string)(this["SAP_LicenseServer"]));
            }
            set {
                this["SAP_LicenseServer"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_DBName {
            get {
                return ((string)(this["SAP_DBName"]));
            }
            set {
                this["SAP_DBName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_DBUser {
            get {
                return ((string)(this["SAP_DBUser"]));
            }
            set {
                this["SAP_DBUser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_DBPass {
            get {
                return ((string)(this["SAP_DBPass"]));
            }
            set {
                this["SAP_DBPass"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_CompanyUser {
            get {
                return ((string)(this["SAP_CompanyUser"]));
            }
            set {
                this["SAP_CompanyUser"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_CompanyPass {
            get {
                return ((string)(this["SAP_CompanyPass"]));
            }
            set {
                this["SAP_CompanyPass"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("25")]
        public int SAP_LangID {
            get {
                return ((int)(this["SAP_LangID"]));
            }
            set {
                this["SAP_LangID"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int SAP_ServerTypeID {
            get {
                return ((int)(this["SAP_ServerTypeID"]));
            }
            set {
                this["SAP_ServerTypeID"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_TrustedConnection {
            get {
                return ((bool)(this["SAP_TrustedConnection"]));
            }
            set {
                this["SAP_TrustedConnection"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int ModalidadAgente {
            get {
                return ((int)(this["ModalidadAgente"]));
            }
            set {
                this["ModalidadAgente"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("*:*")]
        public string ListaAsociacionMonedas {
            get {
                return ((string)(this["ListaAsociacionMonedas"]));
            }
            set {
                this["ListaAsociacionMonedas"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string ListaAsociacionFormaPago {
            get {
                return ((string)(this["ListaAsociacionFormaPago"]));
            }
            set {
                this["ListaAsociacionFormaPago"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("*:*")]
        public string ListaAsociacionBancos {
            get {
                return ((string)(this["ListaAsociacionBancos"]));
            }
            set {
                this["ListaAsociacionBancos"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("30")]
        public int Stellar_ConnectionTimeout {
            get {
                return ((int)(this["Stellar_ConnectionTimeout"]));
            }
            set {
                this["Stellar_ConnectionTimeout"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("60")]
        public int Stellar_CommandTimeout {
            get {
                return ((int)(this["Stellar_CommandTimeout"]));
            }
            set {
                this["Stellar_CommandTimeout"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public long nRegistrosLote {
            get {
                return ((long)(this["nRegistrosLote"]));
            }
            set {
                this["nRegistrosLote"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("00.00:EXE|15.00:ISV15|18.00:ISV18")]
        public string TaxList1 {
            get {
                return ((string)(this["TaxList1"]));
            }
            set {
                this["TaxList1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("00.00:EXE|18.00:ISV18")]
        public string TaxList2 {
            get {
                return ((string)(this["TaxList2"]));
            }
            set {
                this["TaxList2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string TaxList3 {
            get {
                return ((string)(this["TaxList3"]));
            }
            set {
                this["TaxList3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9999999")]
        public long nLotesCorrida {
            get {
                return ((long)(this["nLotesCorrida"]));
            }
            set {
                this["nLotesCorrida"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(":N/A|0:N/A|1:Detalle|2:Mayoreo|3:Importador")]
        public string SAP_Linea_ListaTipoPrecio {
            get {
                return ((string)(this["SAP_Linea_ListaTipoPrecio"]));
            }
            set {
                this["SAP_Linea_ListaTipoPrecio"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_SerieDocumentoFactura {
            get {
                return ((string)(this["SAP_SerieDocumentoFactura"]));
            }
            set {
                this["SAP_SerieDocumentoFactura"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_SerieDocumentoPago {
            get {
                return ((string)(this["SAP_SerieDocumentoPago"]));
            }
            set {
                this["SAP_SerieDocumentoPago"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string SAP_SerieDocumentoNC {
            get {
                return ((string)(this["SAP_SerieDocumentoNC"]));
            }
            set {
                this["SAP_SerieDocumentoNC"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_SYS00000000001")]
        public string SAP_CashAccount {
            get {
                return ((string)(this["SAP_CashAccount"]));
            }
            set {
                this["SAP_CashAccount"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("9999999999")]
        public string Stellar_ClienteContado {
            get {
                return ((string)(this["Stellar_ClienteContado"]));
            }
            set {
                this["Stellar_ClienteContado"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C9999")]
        public string SAP_ClienteContado {
            get {
                return ((string)(this["SAP_ClienteContado"]));
            }
            set {
                this["SAP_ClienteContado"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_FormasPagoFallbackEfectivo {
            get {
                return ((bool)(this["SAP_FormasPagoFallbackEfectivo"]));
            }
            set {
                this["SAP_FormasPagoFallbackEfectivo"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_SYS00000000001")]
        public string SAP_ChecksAccount {
            get {
                return ((string)(this["SAP_ChecksAccount"]));
            }
            set {
                this["SAP_ChecksAccount"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("_SYS00000000001")]
        public string SAP_TransfersAccount {
            get {
                return ((string)(this["SAP_TransfersAccount"]));
            }
            set {
                this["SAP_TransfersAccount"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool EnviarVentaAgrupada {
            get {
                return ((bool)(this["EnviarVentaAgrupada"]));
            }
            set {
                this["EnviarVentaAgrupada"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_FormaPagoFactura_SinDetallePago {
            get {
                return ((bool)(this["SAP_FormaPagoFactura_SinDetallePago"]));
            }
            set {
                this["SAP_FormaPagoFactura_SinDetallePago"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("USD")]
        public string SAP_CodigoMonedaPredeterminada {
            get {
                return ((string)(this["SAP_CodigoMonedaPredeterminada"]));
            }
            set {
                this["SAP_CodigoMonedaPredeterminada"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_UnSoloPaymentsPorFactura {
            get {
                return ((bool)(this["SAP_UnSoloPaymentsPorFactura"]));
            }
            set {
                this["SAP_UnSoloPaymentsPorFactura"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool ObtenerListasAsociacionBD {
            get {
                return ((bool)(this["ObtenerListasAsociacionBD"]));
            }
            set {
                this["ObtenerListasAsociacionBD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_FormasPagoFallbackBanco {
            get {
                return ((bool)(this["SAP_FormasPagoFallbackBanco"]));
            }
            set {
                this["SAP_FormasPagoFallbackBanco"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_FormasPagoFallbackMoneda {
            get {
                return ((bool)(this["SAP_FormasPagoFallbackMoneda"]));
            }
            set {
                this["SAP_FormasPagoFallbackMoneda"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SAP_Linea_DeterminarPorcentajeDescuentoItem {
            get {
                return ((bool)(this["SAP_Linea_DeterminarPorcentajeDescuentoItem"]));
            }
            set {
                this["SAP_Linea_DeterminarPorcentajeDescuentoItem"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string APP_DEFINED_SAP_DI_API_VERSION {
            get {
                return ((string)(this["APP_DEFINED_SAP_DI_API_VERSION"]));
            }
            set {
                this["APP_DEFINED_SAP_DI_API_VERSION"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Stellar_CodigoMonedaPredeterminada {
            get {
                return ((string)(this["Stellar_CodigoMonedaPredeterminada"]));
            }
            set {
                this["Stellar_CodigoMonedaPredeterminada"] = value;
            }
        }
    }
}
