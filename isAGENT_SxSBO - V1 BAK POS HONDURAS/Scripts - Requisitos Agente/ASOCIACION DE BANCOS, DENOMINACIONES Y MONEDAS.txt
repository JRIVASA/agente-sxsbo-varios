Tras ejecutar el script de asociacion, se debe asignar dicho modulo a algun usuario.

El modulo esta ubicado dentro de Control de Puntos de Venta y se llama Asociacion de Datos SAP - Stellar.

Una vez asignado el m�dulo deber�n asignarse las asociaciones de c�digos de Monedas, Denominaciones y Bancos, es decir, unir Codigo de Stellar vs Codigo en SAP de cada uno de los elementos mencionados anteriormente.

A) En el caso de las monedas, no se esta utilizando dicha asociacion todav�a para el manejo multi moneda, dicho manejo estar�a por definir en caso de que fuera solicitado, pero de igual manera es obligatorio asignar el codigo de cada moneda correspondiente en SAP. En SAP los codigos de las monedas estan trabajados segun la nomenclatura ISO de 3 digitos.

Ejemplo:

Bolivar Soberano -> VES
Dolar Estadounidense -> USD
Euro -> EUR

Por lo tanto es necesario asignar el codigo correspondiente a cada moneda para habilitar la sincronizacion.

B) SAP no maneja formas de pago din�micas (Que se pueden crear tantas como sea necesario) sino que aparentemente solo maneja 3 posibilidades, Aqui mencionamos los c�digos que estamos utilizando para la integracion con SAP:

Tarjetas Credito / Debito -> CREDIT
Transferencias -> TRANSFER
Efectivo -> CASH

De manera que esas son los codigos que deben asociarse a las denominaciones creadas en Stellar. Ejemplo, si en Stellar POS se manejaran por separado tarjetas d�bito y cr�dito y sus codigos respectivos fueran "DBT" Y "TDC", y el de Transferencia "TRF", entonces se deber�a asociar del siguiente modo: 

Efectivo -> CASH
DBT -> CREDIT
TDC -> CREDIT
TRF -> TRANSFER
...

C) SAP tiene c�digos num�ricos para definidos para los bancos en el caso del procesamiento de tarjetas, por ello se necesita definir dichos codigos en los bancos tambien. Ejemplo: teniendo 3 bancos con codigos "ABC", "DEF" y "OTRO", la asociaci�n podr�a ser de esta forma dependiendo de que banco representen en SAP:

ABC -> 1
DEF -> 2
OTRO -> 10000
...